#!/bin/bash
if [ -z "$1" ]
then 
    echo "version argument missing"
    exit 1
fi

aws ecr get-login-password --region eu-west-1 --profile chaos | sudo docker login --username AWS --password-stdin 557778242555.dkr.ecr.eu-west-1.amazonaws.com

sudo docker rmi $(sudo docker images | grep 'chaos/eza/api' | tr -s ' ' | cut -d ' ' -f 3) --force

sudo docker build -t chaos/eza/api:$1 -f Dockerfile-Api .
sudo docker tag chaos/eza/api:$1 557778242555.dkr.ecr.eu-west-1.amazonaws.com/chaos/eza/api:$1
sudo docker push 557778242555.dkr.ecr.eu-west-1.amazonaws.com/chaos/eza/api:$1