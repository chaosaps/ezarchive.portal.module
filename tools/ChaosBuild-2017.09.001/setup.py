#!/usr/bin/env python

from distutils.core import setup

setup(name="ChaosBuild",
      version="2017.09.001",
      description="CHAOS Build Utilities",
      author="Jesper Fyhr Knudsen",
      author_email="jesper@fyhr.dk",
      packages=["chaosbuild"]
     )