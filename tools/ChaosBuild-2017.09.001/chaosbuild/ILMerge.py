from subprocess import Popen
import os


def merge(assemblies, output, ilmerge_dir=None, libraries=[]):
    if ilmerge_dir is not None:
        ilmerge_dir = os.path.join(ilmerge_dir, "ILMerge.exe")
    else:
        ilmerge_dir = "ILMerge.exe"

    libraries = ["/lib:" + lib for lib in libraries]
    process = Popen([ilmerge_dir,
                     "/out:" + output,
                     "/target:library",
                     ] + libraries + assemblies)

    while process.poll() is None:
        pass

    if process.returncode != 0:
        raise Exception("ILMerge step failed, check output")

    print("Merged", ", ".join(assemblies), "->", output)