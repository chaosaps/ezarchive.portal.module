from subprocess import Popen
import os


def test(assemblies, where=None, nunit_dir=None, no_result=True):
    if nunit_dir is not None:
        nunit_dir = os.path.join(nunit_dir, "nunit3-console")
    else:
        nunit_dir = "nunit3-console"

    if where is not None:
        where = ["--where", where]
    else:
        where = []

    if no_result:
        no_result = ["--noresult"]
    else:
        no_result = []

    process = Popen([nunit_dir, "--config=Release"] + no_result + assemblies + where)

    while process.poll() is None:
        pass

    if process.returncode != 0:
        raise Exception("NUnit step failed, check output")

