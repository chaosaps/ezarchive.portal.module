from subprocess import Popen
import os


def build(sln, msbuild_dir=None):
    if msbuild_dir is not None:
        msbuild_dir = os.path.join(msbuild_dir, "MSBUILD.exe")
    else:
        msbuild_dir = "MSBUILD.exe"

    print(msbuild_dir)
    process = Popen([msbuild_dir, "/verbosity:minimal", "/maxcpucount", "/p:Configuration=Release", sln])

    while process.poll() is None:
        pass

    print()
    print()

    if process.returncode != 0:
        raise Exception("MSBUILD step failed, check output")


