import os
from chaosbuild.MsBuild import build
from chaosbuild.ILMerge import merge
from chaosbuild.NUnit import test

build("EZArchive.Portal.Module.sln")

if not os.path.exists("build"):
    os.mkdir("build")

test(["EZArchive.Portal.Module.sln"], where="cat == unittest")

merge(["src/app/EZArchive.Portal.Module/bin/Release/EZArchive.Portal.Module.dll"],
      "build/EZArchive.Portal.Module.dll",
      "C:\\Program Files (x86)\\Reference Assemblies\\Microsoft\\Framework\\.NETFramework\\v4.6.1",
      libraries=["lib\\", "C:\\Program Files (x86)\\Reference Assemblies\\Microsoft\\Framework\\.NETFramework\\v4.6.1"],
      ilmerge_dir="tools/ILMerge/")