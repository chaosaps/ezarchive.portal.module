require 'bundler/setup'
require 'fileutils'

require 'albacore'

Configuration = ENV['CONFIGURATION'] || 'Release'

task :clean do
  FileUtils.rmtree 'tests'
  FileUtils.rmtree 'build'
end

task :prepare_compile => [:clean] do
  FileUtils.cp 'src/app/EZArchive.Portal.Module/App.config.sample', 'src/app/EZArchive.Portal.Module/App.config'
  FileUtils.cp 'src/test/EZArchive.Portal.Module.Test/App.config.sample', 'src/test/EZArchive.Portal.Module.Test/App.config'
end

desc 'Perform fast build (warn: doesn\'t d/l deps)'
build :quick_compile => [:prepare_compile] do |b|
  b.prop 'Configuration', Configuration
  b.logging = 'normal'
  b.sln     = 'EZArchive.Portal.Module.sln'
end

task :package_tests => [:quick_compile] do
  FileUtils.mkdir 'tests'

  FileUtils.cp 'src\app\EZArchive.Portal.Module\bin\Release\EZArchive.Portal.Module.dll', 'tests'
  FileUtils.cp 'lib\CHAOS.dll', 'tests'
  FileUtils.cp 'lib\Chaos.Portal.Mcm.dll', 'tests'
  FileUtils.cp 'lib\Chaos.Portal.dll', 'tests'
  FileUtils.cp 'lib\Chaos.Portal.Authentication.dll', 'tests'
  FileUtils.cp 'packages\Newtonsoft.Json.6.0.8\lib\net45\Newtonsoft.Json.dll', 'tests'
  FileUtils.cp 'packages\NUnit.2.6.3\lib\nunit.framework.dll', 'tests'
  FileUtils.cp 'packages\Moq.4.2.1409.1722\lib\net40\Moq.dll', 'tests'
  FileUtils.cp 'packages\AWSSDK.2.3.24.3\lib\net45\AWSSDK.dll', 'tests'

  system 'tools/ILMerge/ILMerge.exe',
    ['/out:tests\EZArchive.Portal.Module.Test.dll',
     '/target:library',
     '/ndebug',
     '/lib:lib',
     '/targetplatform:v4,c:\windows\Microsoft.Net\Framework64\v4.0.30319',
     '/lib:c:\windows\Microsoft.Net\Framework64\v4.0.30319',
     'src\test\EZArchive.Portal.Module.Test\bin\Release\EZArchive.Portal.Module.Test.dll'], clr_command: true
end

desc "Run all the tests"
test_runner :tests => [:package_tests] do |tests|
  tests.files = FileList['tests/EZArchive.Portal.Module.Test.dll']
  tests.add_parameter '/framework=4.0.30319'
  tests.exe = 'tools/NUnit-2.6.0.12051/bin/nunit-console.exe'
end

desc "Merges all production assemblies"
task :package => [:tests] do
  FileUtils.mkdir 'build'

  system 'tools/ILMerge/ILMerge.exe',
    ['/out:build\EZArchive.Portal.Module.dll',
      '/target:library',
      '/ndebug',
      '/lib:lib',
      '/targetplatform:v4,c:\windows\Microsoft.Net\Framework64\v4.0.30319',
      '/lib:c:\windows\Microsoft.Net\Framework64\v4.0.30319',
      'src\app\EZArchive.Portal.Module\bin\Release\EZArchive.Portal.Module.dll'], clr_command: true
end

task :default => :package
