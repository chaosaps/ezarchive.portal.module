if($args.Count -ne 1){
    exit 1
}

$version=$args[0]

docker images | ConvertFrom-String | where {$_.P1 -like "*chaos/eza/api*"} | % { docker rmi $_.P3 -f }

Invoke-Expression -Command (Get-ECRLoginCommand -ProfileName default).Command

docker build -t chaos/eza/api:$version -f Dockerfile-api .
docker tag chaos/eza/api:$version 557778242555.dkr.ecr.eu-west-1.amazonaws.com/chaos/eza/api:$version
docker push 557778242555.dkr.ecr.eu-west-1.amazonaws.com/chaos/eza/api:$version