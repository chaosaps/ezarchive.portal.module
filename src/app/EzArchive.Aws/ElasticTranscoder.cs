﻿using System;
using System.Threading.Tasks;
using Amazon.ElasticTranscoder;
using Amazon.ElasticTranscoder.Model;
using EzArchive.Application;

namespace EzArchive.Aws;

public class ElasticTranscoder : ITranscoder
{
	private readonly string pipelineId;

	public ElasticTranscoder(string pipelineId)
	{
		this.pipelineId = pipelineId;
	}

	public async Task TranscodeAsync(string inputKey, string outputKey, string presetId, bool createThumbnail)
	{
		using var et = new AmazonElasticTranscoderClient();

		var jobOutput = new CreateJobOutput
		{
			PresetId = presetId,
			Key = outputKey
		};

		if (createThumbnail)
			jobOutput.ThumbnailPattern =
				$"{outputKey.Substring(0, outputKey.LastIndexOf(".", StringComparison.Ordinal))}_{{count}}";

		var request = new CreateJobRequest
		{
			PipelineId = pipelineId,
			Input = new JobInput
			{
				Key = inputKey
			},
			Output = jobOutput
		};

		await et.CreateJobAsync(request);
	}

	public Task TranscodeAsync(string inputKey, string outputKey, string presetId) =>
		TranscodeAsync(inputKey, outputKey, presetId, false);
}