﻿using System;
using System.IO;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using EzArchive.Application;
using EzArchive.Application.Errors;
using File = EzArchive.Domain.File;

namespace EzArchive.Aws;

public class S3 : IStorage
{
	private readonly string bucket;

	public S3(string bucket)
	{
		this.bucket = bucket;
	}

	public async Task WriteAsync(string key, Stream stream, string contentType = "application/octet-stream")
	{
		try
		{
			using var s3 = new AmazonS3Client();
			stream.Position = 0;

			await s3.PutObjectAsync(new PutObjectRequest
			{
				BucketName = bucket,
				CannedACL = S3CannedACL.PublicRead,
				InputStream = stream,
				AutoCloseStream = true,
				ContentType = contentType,
				Key = key
			});
		}
		catch (AmazonS3Exception e)
		{
			throw new UnhandledException("Upload failed", e);
		}
	}

	public async Task DeleteAsync(string key)
	{
		try
		{
			using var s3 = new AmazonS3Client();
			var request = new DeleteObjectRequest
			{
				BucketName = bucket,
				Key = key
			};

			await s3.DeleteObjectAsync(request);
		}
		catch (AmazonS3Exception)
		{
			// throw new UnhandledException("Deletion failed on file: " + key, e);
		}
	}

	public async Task<Stream> ReadAsync(File file)
	{
		var bucketInfo = GetBucketInfo(file.Url);
		var s3 = new AmazonS3Client(new AmazonS3Config
		{
			ServiceURL = bucketInfo.url,
			RegionEndpoint = RegionEndpoint.EUWest1
		});
		var response = await s3.GetObjectAsync(bucketInfo.name, bucketInfo.key);


		return new S3DisposeWrappedStream(response.ResponseStream, response);
	}

	private (string name, string key, string url) GetBucketInfo(string url)
	{
		// expected format: bucketname={BASE_PATH};key={FOLDER_PATH}/{FILENAME};ServiceURL=http://region.amazonaws.com
		var args = url.Split(';');
		return (args[0].Substring("bucketname=".Length),
			args[1].Substring("key=".Length).TrimStart('/'),
			args[2].Substring("ServiceURL=".Length).Trim(' '));
	}

	private class S3DisposeWrappedStream : Stream
	{
		private readonly Stream stream;
		private readonly IDisposable disposable;

		public S3DisposeWrappedStream(Stream stream, IDisposable disposable)
		{
			this.stream = stream;
			this.disposable = disposable;
		}

		public override void Flush() =>
			stream.Flush();

		public override int Read(byte[] buffer, int offset, int count) =>
			stream.Read(buffer, offset, count);

		public override long Seek(long offset, SeekOrigin origin) =>
			stream.Seek(offset, origin);

		public override void SetLength(long value) =>
			stream.SetLength(value);

		public override void Write(byte[] buffer, int offset, int count)
		{
			stream.Write(buffer, offset, count);
		}

		protected override void Dispose(bool disposing)
		{
			stream.Dispose();
			base.Dispose(disposing);
			disposable.Dispose();
		}

		public override ValueTask DisposeAsync()
		{
			Dispose();

			return base.DisposeAsync();
		}

		public override bool CanRead => stream.CanRead;
		public override bool CanSeek => stream.CanSeek;
		public override bool CanWrite => stream.CanWrite;
		public override long Length => stream.Length;

		public override long Position
		{
			get => stream.Position;
			set => stream.Position = value;
		}
	}
}