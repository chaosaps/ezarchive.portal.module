using System;
using Microsoft.Extensions.DependencyInjection;

namespace EzArchive.WebApi;

public static class CorsPolicies
{
	public const string AnyOriginPolicy = "AnyOrigin";

	public static void AddCustomCorsPolicies(this IServiceCollection services)
	{
		services.AddCors(options =>
		{
			options.AddPolicy(AnyOriginPolicy, builder =>
				builder
					.AllowAnyOrigin()
					.AllowAnyHeader()
					.AllowAnyMethod()
					.SetPreflightMaxAge(new TimeSpan(24, 0, 0)));
		});
	}
}