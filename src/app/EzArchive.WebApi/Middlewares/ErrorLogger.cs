using System;
using System.Diagnostics;
using System.Threading.Tasks;
using EzArchive.Domain;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace EzArchive.WebApi.Middlewares;

public class ErrorLogger
{
	private readonly RequestDelegate next;

	public ErrorLogger(RequestDelegate next)
	{
		this.next = next;
	}

	public async Task InvokeAsync(HttpContext context, ILogger apiLogger)
	{
		var timer = new Stopwatch();
		timer.Start();

		try
		{
			await next(context);
		}
		catch (Exception e)
		{
			timer.Stop();
			apiLogger.LogFatal(context.Request.Path.Value, "", e, timer.Elapsed);
			throw;
		}
	}
}

public static class ErrorLoggerMiddlewareExtensions
{
	public static IApplicationBuilder UseErrorLogger(this IApplicationBuilder app)
	{
		if (app == null)
			throw new ArgumentNullException(nameof(app));

		return app.UseMiddleware<ErrorLogger>();
	}
}