using System;
using System.Net;
using System.Threading.Tasks;
using EzArchive.WebApi.Dtos.Results;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace EzArchive.WebApi.Middlewares;

public class CustomExceptionHandler
{
	private readonly RequestDelegate next;

	public CustomExceptionHandler(RequestDelegate next)
	{
		this.next = next;
	}

	public async Task InvokeAsync(HttpContext context)
	{
		try
		{
			await next(context);
		}
		catch (Exception e)
		{
			if (context.Response.HasStarted)
				return;

			context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

			var responseContent = JsonConvert.SerializeObject(new ResultWrapper(e));
			await SetResponseAsync(context, responseContent);
		}
	}

	private async Task SetResponseAsync(HttpContext context, string response)
	{
		context.Response.ContentType = "application/json";
		await context.Response.WriteAsync(response);
	}
}

public static class CustomExceptionHandlerMiddlewareExtensions
{
	public static IApplicationBuilder UseCustomExceptionHandler(this IApplicationBuilder app)
	{
		if (app == null)
			throw new ArgumentNullException(nameof(app));

		return app.UseMiddleware<CustomExceptionHandler>();
	}
}