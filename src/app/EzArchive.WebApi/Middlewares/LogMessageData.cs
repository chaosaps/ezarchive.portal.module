using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace EzArchive.WebApi.Middlewares;

public class LogMessageData
{
	public RequestData Request { get; }
	public ResponseData Response { get; }

	public LogMessageData(
		string requestPath,
		string requestHost,
		string requestMethod,
		string requestProtocol,
		IHeaderDictionary requestHeaders,
		IRequestCookieCollection requestCookies,
		IQueryCollection requestQuery,
		IFormCollection requestForm,
		IHeaderDictionary responseHeaders,
		IResponseCookies responseCookies,
		int responseStatusCode,
		string responseContentType,
		string responseContent)
	{
		Request = new RequestData(
			requestPath,
			requestHost,
			requestMethod,
			requestProtocol,
			requestHeaders,
			requestCookies,
			requestQuery,
			requestForm);
		Response = new ResponseData(
			responseHeaders,
			responseCookies,
			responseStatusCode,
			responseContentType,
			responseContent);
	}

	public override string ToString() =>
		JsonConvert.SerializeObject(this);

	private static bool ShouldBeMasked(string name)
	{
		return name.Contains("password", StringComparison.OrdinalIgnoreCase) ||
		       name.Contains("token", StringComparison.OrdinalIgnoreCase);
	}

	public class RequestData
	{
		public string Path { get; }
		public string Host { get; }
		public string Method { get; }
		public string Protocol { get; }
		public IHeaderDictionary Headers { get; }
		public IRequestCookieCollection Cookies { get; }
		public IEnumerable<KeyValuePair<string, StringValues>> Query { get; }
		public IEnumerable<KeyValuePair<string, StringValues>> Form { get; }

		public RequestData(
			string requestPath,
			string requestHost,
			string requestMethod,
			string requestProtocol,
			IHeaderDictionary requestHeaders,
			IRequestCookieCollection requestCookies,
			IQueryCollection requestQuery,
			IFormCollection requestForm)
		{
			Path = requestPath;
			Host = requestHost;
			Method = requestMethod;
			Protocol = requestProtocol;
			Headers = requestHeaders;
			Cookies = requestCookies;
			Query = requestQuery.SelectMany(Mask);
			Form = requestForm.SelectMany(Mask);
		}

		private IEnumerable<KeyValuePair<string, StringValues>> Mask(KeyValuePair<string, StringValues> pair)
		{
			if (ShouldBeMasked(pair.Key))
				yield return new KeyValuePair<string, StringValues>(pair.Key, new StringValues("**********"));
			else
				yield return pair;
		}
	}

	public class ResponseData
	{
		public IHeaderDictionary Headers { get; }
		public IResponseCookies Cookies { get; }
		public int StatusCode { get; }
		public string ContentType { get; }
		public JObject Content { get; }

		public ResponseData(
			IHeaderDictionary responseHeaders,
			IResponseCookies responseCookies,
			int responseStatusCode,
			string responseContentType,
			string responseContent)
		{
			Headers = responseHeaders;
			Cookies = responseCookies;
			StatusCode = responseStatusCode;
			ContentType = responseContentType;
			Content = MaskContent(responseContent);
		}

		private JObject MaskContent(string content)
		{
			try
			{
				var jObject = JObject.FromObject(JsonConvert.DeserializeObject(content));

				Mask(jObject);

				return jObject;
			}
			catch (JsonReaderException)
			{
				return null;
			}
		}

		private void Mask(JToken containerToken)
		{
			switch (containerToken.Type)
			{
				case JTokenType.Object:
				{
					foreach (JProperty child in containerToken.Children<JProperty>())
					{
						if (ShouldBeMasked(child.Name))
							child.Value = "**********";

						Mask(child.Value);
					}

					break;
				}
				case JTokenType.Array:
				{
					foreach (JToken child in containerToken.Children())
					{
						Mask(child);
					}

					break;
				}
			}
		}
	}
}