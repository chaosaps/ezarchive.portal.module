using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using EzArchive.Domain;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;

namespace EzArchive.WebApi.Middlewares;

public class RequestLogger
{
	private readonly RequestDelegate next;
	private readonly DateTime logRequestsUntil;

	public RequestLogger(RequestDelegate next, DateTime logRequestsUntil)
	{
		this.next = next;
		this.logRequestsUntil = logRequestsUntil;
	}

	public async Task InvokeAsync(HttpContext context, ILogger apiLogger)
	{
		if (ShouldNotLog())
		{
			await next(context);
		}
		else
		{
			var timer = new Stopwatch();
			timer.Start();

			var originalResponseBody = context.Response.Body;

			using (var memoryStream = new MemoryStream())
			{
				context.Response.Body = memoryStream;

				await next(context);

				var responseContent = await GetResponseContent();
				await ResetResponseAsync();

				apiLogger.LogInformation(
					"RequestLogger",
					new LogMessageData(
						context.Request.Path.Value,
						context.Request.Host.Value,
						context.Request.Method,
						context.Request.Protocol,
						context.Request.Headers,
						context.Request.Cookies,
						context.Request.Query,
						context.Request.HasFormContentType
							? context.Request.Form
							: new FormCollection(new Dictionary<string, StringValues>()),
						context.Response.Headers,
						context.Response.Cookies,
						context.Response.StatusCode,
						context.Response.ContentType,
						responseContent).ToString(),
					timer.Elapsed);

				Task<string> GetResponseContent()
				{
					memoryStream.Seek(0, SeekOrigin.Begin);
					return new StreamReader(memoryStream).ReadToEndAsync();
				}

				async Task ResetResponseAsync()
				{
					context.Response.Body = originalResponseBody;
					await context.Response.WriteAsync(responseContent);
				}
			}
		}

		bool ShouldNotLog() => DateTime.UtcNow > logRequestsUntil;
	}
}

public static class RequestLoggerMiddlewareExtensions
{
	public static IApplicationBuilder UseRequestLogger(this IApplicationBuilder app, DateTime logRequestsUntil)
	{
		if (app == null)
			throw new ArgumentNullException(nameof(app));

		return app.UseMiddleware<RequestLogger>(logRequestsUntil);
	}
}