using System.Linq;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace EzArchive.WebApi.SwashBuckle;

public class RemoveDefaultVersionRoute : IDocumentFilter
{
	public void Apply(OpenApiDocument swaggerDoc, DocumentFilterContext context)
	{
		var pathsWithoutVersion = swaggerDoc.Paths.Where(p => p.Key.Contains("version") == false).ToList();
		var pathsToDelete = pathsWithoutVersion.Where(p => swaggerDoc.Paths.ContainsKey("/v{version}" + p.Key))
			.ToList();

		foreach (var route in pathsToDelete)
			swaggerDoc.Paths.Remove(route.Key);
	}
}