using System;
using System.Threading.Tasks;
using EzArchive.Domain;
using Microsoft.AspNetCore.Mvc;

namespace EzArchive.WebApi.TaskExecutors;

public interface IAnonymousTaskExecutor
{
	IActionResult Execute<T>(
		Func<Result<T>> task,
		Func<T, IActionResult> onSuccess);

	IActionResult Execute<T>(
		SessionId sessionId,
		Func<Session, Result<T>> task,
		Func<T, IActionResult> onSuccess);

		Task<IActionResult> ExecuteAsync<T>(
			Func<Task<Result<T>>> task,
			Func<T, IActionResult> onSuccess);

		Task<IActionResult> ExecuteAsync(
			Func<Task<Result>> task,
			Func<IActionResult> onSuccess);
}
