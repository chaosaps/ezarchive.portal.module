using System;
using System.Threading.Tasks;
using EzArchive.Domain;
using Microsoft.AspNetCore.Mvc;

namespace EzArchive.WebApi.TaskExecutors;

public interface IAuthenticatedTaskExecutor
{
	IActionResult Execute(
		SessionId sessionId,
		Func<Session, Result> task,
		Func<IActionResult> onSuccess);
}

public interface IAuthenticatedResultTaskExecutor
{
	IActionResult Execute<T>(
		SessionId sessionId,
		Func<Session, Result<T>> task,
		Func<T, IActionResult> onSuccess);
}

public interface IAuthenticatedAsyncResultTaskExecutor
{
	Task<IActionResult> ExecuteAsync<T>(
		SessionId sessionId,
		Func<Session, Task<Result<T>>> task,
		Func<T, IActionResult> onSuccess);
}

public interface IAuthenticatedAsyncTaskExecutor
{
	Task<IActionResult> ExecuteAsync(
		SessionId sessionId,
		Func<Session, Task<Result>> task,
		Func<IActionResult> onSuccess);
}