using System;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Sessions;
using EzArchive.Domain;
using Microsoft.AspNetCore.Mvc;

namespace EzArchive.WebApi.TaskExecutors;

public class AuthenticatedTaskExecutor : AuthenticatedTaskExecutorBase, IAuthenticatedTaskExecutor,
	IAuthenticatedAsyncTaskExecutor
{
	public AuthenticatedTaskExecutor(
		ISessionGetter sessionGetter,
		ISessionUpdater sessionUpdater,
		UserId anonymousUserId)
		: base(sessionGetter, sessionUpdater, anonymousUserId)
	{
	}

	public IActionResult Execute(SessionId sessionId, Func<Session, Result> task, Func<IActionResult> onSuccess) =>
		Perform(() => task(GetSession(sessionId)), onSuccess);

	public Task<IActionResult> ExecuteAsync(SessionId sessionId, Func<Session, Task<Result>> task,
		Func<IActionResult> onSuccess) =>
		PerformAsync(() => task(GetSession(sessionId)), onSuccess);
}