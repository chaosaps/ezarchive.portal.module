using System;
using System.Threading.Tasks;
using EzArchive.Domain;
using Microsoft.AspNetCore.Mvc;

namespace EzArchive.WebApi.TaskExecutors;

public abstract class TaskExecutor
{
	protected IActionResult Perform<T>(
		Func<Result<T>> task,
		Func<T, IActionResult> onSuccess)
	{
		var result = task();

		return result.HasError
			? ErrorActionResultFactory.Create(result.ErrorCode)
			: onSuccess(result.Value);
	}

	protected async Task<IActionResult> PerformAsync<T>(
		Func<Task<Result<T>>> task,
		Func<T, IActionResult> onSuccess)
	{
		var result = await task();

		return result.HasError
			? ErrorActionResultFactory.Create(result.ErrorCode)
			: onSuccess(result.Value);
	}

	protected async Task<IActionResult> PerformAsync(
		Func<Task<Result>> task,
		Func<IActionResult> onSuccess)
	{
		var result = await task();

		return result.HasError
			? ErrorActionResultFactory.Create(result.ErrorCode)
			: onSuccess();
	}

	protected IActionResult Perform(
		Func<Result> task,
		Func<IActionResult> onSuccess)
	{
		var result = task();

		return result.HasError
			? ErrorActionResultFactory.Create(result.ErrorCode)
			: onSuccess();
	}
}