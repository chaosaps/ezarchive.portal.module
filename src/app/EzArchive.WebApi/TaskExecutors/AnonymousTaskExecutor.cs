using System;
using System.Threading.Tasks;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Sessions;
using EzArchive.Domain;
using Microsoft.AspNetCore.Mvc;

namespace EzArchive.WebApi.TaskExecutors;

public class AnonymousTaskExecutor : TaskExecutor, IAnonymousTaskExecutor
{
	private readonly ISessionGetter sessionGetter;
	private readonly ISessionUpdater sessionUpdater;

	public AnonymousTaskExecutor(ISessionGetter sessionGetter, ISessionUpdater sessionUpdater)
	{
		this.sessionGetter = sessionGetter;
		this.sessionUpdater = sessionUpdater;
	}

	public IActionResult Execute<T>(
		Func<Result<T>> task,
		Func<T, IActionResult> onSuccess) =>
		Perform(task, onSuccess);

	public IActionResult Execute<T>(
		SessionId sessionId,
		Func<Session, Result<T>> task,
		Func<T, IActionResult> onSuccess) =>
		Perform(() => task(GetSession(sessionId)), onSuccess);

		public Task<IActionResult> ExecuteAsync<T>(
			Func<Task<Result<T>>> task,
			Func<T, IActionResult> onSuccess)
		{
			return PerformAsync(task, onSuccess);
		}

		public Task<IActionResult> ExecuteAsync(
			Func<Task<Result>> task,
			Func<IActionResult> onSuccess)
		{
			return PerformAsync(task, onSuccess);
		}

		protected Session GetSession(SessionId sessionId)
		{
			var session = sessionGetter.Get(sessionId);

		if (session == null)
			throw new SessionDoesNotExistException("SessionGUID is invalid or has expired");

		sessionUpdater.Update(sessionId, session.UserId);

		return session;
	}
}
