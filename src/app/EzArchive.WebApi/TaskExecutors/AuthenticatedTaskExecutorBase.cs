using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Sessions;
using EzArchive.Domain;

namespace EzArchive.WebApi.TaskExecutors;

public abstract class AuthenticatedTaskExecutorBase : TaskExecutor
{
	private readonly ISessionGetter sessionGetter;
	private readonly ISessionUpdater sessionUpdater;
	private readonly UserId anonymousUserId;

	protected AuthenticatedTaskExecutorBase(ISessionGetter sessionGetter, ISessionUpdater sessionUpdater,
		UserId anonymousUserId)
	{
		this.sessionGetter = sessionGetter;
		this.sessionUpdater = sessionUpdater;
		this.anonymousUserId = anonymousUserId;
	}

	protected Session GetSession(SessionId sessionId)
	{
		var session = sessionGetter.Get(sessionId);

		if (session == null)
			throw new SessionDoesNotExistException("SessionGUID is invalid or has expired");

		sessionUpdater.Update(sessionId, session.UserId);

		return session;
	}
}