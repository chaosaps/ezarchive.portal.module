using System;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Sessions;
using EzArchive.Domain;
using Microsoft.AspNetCore.Mvc;

namespace EzArchive.WebApi.TaskExecutors;

public class AuthenticatedResultTaskExecutor : AuthenticatedTaskExecutorBase, IAuthenticatedResultTaskExecutor,
	IAuthenticatedAsyncResultTaskExecutor
{
	public AuthenticatedResultTaskExecutor(
		ISessionGetter sessionGetter,
		ISessionUpdater sessionUpdater,
		UserId anonymousUserId)
		: base(sessionGetter, sessionUpdater, anonymousUserId)
	{
	}

	public IActionResult Execute<T>(
		SessionId sessionId,
		Func<Session, Result<T>> task,
		Func<T, IActionResult> onSuccess)
	{
		return Perform(() => task(GetSession(sessionId)), onSuccess);
	}

	public Task<IActionResult> ExecuteAsync<T>(
		SessionId sessionId,
		Func<Session, Task<Result<T>>> task,
		Func<T, IActionResult> onSuccess)
	{
		return PerformAsync(() => task(GetSession(sessionId)), onSuccess);
	}
}