using System;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using Microsoft.AspNetCore.Mvc;

namespace EzArchive.WebApi;

public static class ErrorActionResultFactory
{
	public static IActionResult Create(ErrorCode errorCode)
	{
		return errorCode switch
		{
			ErrorCode.TestError => new ObjectResult(new ResultWrapper(errorCode, "Testing error handling")),
			ErrorCode.InvalidInput => new BadRequestObjectResult(new ResultWrapper(errorCode, "Input is invalid")),
			_ => throw new Exception()
		};
	}
}