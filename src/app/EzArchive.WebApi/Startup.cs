using System;
using System.Linq;
using System.Reflection;
using EzArchive.WebApi.Middlewares;
using EzArchive.WebApi.SetUp;
using EzArchive.WebApi.SwashBuckle;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace EzArchive.WebApi;

public class Startup
{
	private IConfiguration Configuration { get; }

	public Startup(IConfiguration configuration)
	{
		Configuration = configuration;
	}

	// This method gets called by the runtime. Use this method to add services to the container.
	// For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
	public void ConfigureServices(IServiceCollection services)
	{
		services
			.AddControllers()
			.AddNewtonsoftJson(opt => { opt.UseMemberCasing(); });

		services.AddCustomCorsPolicies();

		services.AddApiVersioning(
			options =>
			{
				options.ReportApiVersions = true;
				options.ApiVersionReader = new UrlSegmentApiVersionReader();
				options.AssumeDefaultVersionWhenUnspecified = true;
				options.ApiVersionSelector = new CurrentImplementationApiVersionSelector(options);
			});

		services.AddEzArchive(Configuration);

		services.AddSwaggerGen(c =>
		{
			c.SwaggerDoc("v6", new OpenApiInfo { Title = "EzArchive API v6", Version = "v6" });

			c.DocInclusionPredicate((docName, apiDesc) =>
			{
				if (!apiDesc.TryGetMethodInfo(out MethodInfo methodInfo))
					return false;

				var versions = methodInfo
					.GetCustomAttributes(true)
					.OfType<ApiVersionAttribute>()
					.SelectMany(attr => attr.Versions);

				return versions.Any(v => $"v{v.ToString()}" == docName);
			});

			c.DocumentFilter<RemoveDefaultVersionRoute>();
			c.OperationFilter<RemoveVersionFromParameter>();
			c.DocumentFilter<ReplaceVersionWithExactValueInPath>();

			c.EnableAnnotations();
		});
	}

	// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
	public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
	{
		app.UseSwagger();
		app.UseSwaggerUI(c => { c.SwaggerEndpoint($"v6/swagger.json", "EzArchive API v6"); });

		if (DateTimeOffset.TryParse(Configuration["LogRequestsUntil"], out var logRequestsUntil))
			app.UseRequestLogger(logRequestsUntil.UtcDateTime);

		app.UseCustomExceptionHandler();
		app.UseErrorLogger();
		app.UseCors(CorsPolicies.AnyOriginPolicy);
		app.UseRouting();
		app.UseAuthorization();
		app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
	}
}