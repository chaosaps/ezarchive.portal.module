using Microsoft.AspNetCore.Mvc;

namespace EzArchive.WebApi
{
	public static class ActionResultExtensionMethods
	{
		public static T ValueAs<T>(this IActionResult actionResult)
		{
			var objResult = (ObjectResult) actionResult;
			return (T) objResult.Value;
		}
	}
}