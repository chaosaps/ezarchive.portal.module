using System;
using System.Linq;
using Chaos.Authentication.Core.Model;
using Chaos.Authentication.Data.MySql;
using Chaos.Mcm.Data.MySql;
using Chaos.Mcm.Domain;
using Chaos.Net;
using Chaos.Portal.Core.Data;
using Chaos.Portal.Core.Data.MySql;
using Chaos.Portal.Core.Indexing.Solr;
using EzArchive.Application;
using EzArchive.Application.UseCases.Annotations;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Application.UseCases.AuthKeys.Create;
using EzArchive.Application.UseCases.AuthKeys.Login;
using EzArchive.Application.UseCases.DataDefinitions;
using EzArchive.Application.UseCases.Facets;
using EzArchive.Application.UseCases.Files;
using EzArchive.Application.UseCases.Formats;
using EzArchive.Application.UseCases.Labels;
using EzArchive.Application.UseCases.Passwords;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Application.UseCases.Search;
using EzArchive.Application.UseCases.SearchDefinitions;
using EzArchive.Application.UseCases.SecureCookies.Create;
using EzArchive.Application.UseCases.SecureCookies.Login;
using EzArchive.Application.UseCases.Sessions.Authenticate;
using EzArchive.Application.UseCases.Sessions.Create;
using EzArchive.Application.UseCases.Sessions.Get;
using EzArchive.Application.UseCases.Tags;
using EzArchive.Application.UseCases.Users;
using EzArchive.Application.UseCases.Users.GetAll;
using EzArchive.Application.UseCases.Users.GetCurrent;
using EzArchive.Aws;
using EzArchive.Data.InMemory;
using EzArchive.Data.Portal;
using EzArchive.Domain;
using EzArchive.Logging;
using EzArchive.Mandrill;
using EzArchive.WebApi.TaskExecutors;
using Mandrill;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using AuthKeyRepository = EzArchive.Data.Portal.AuthKeyRepository;
using PasswordAuthenticationRepository = EzArchive.Data.Portal.PasswordAuthenticationRepository;
using ProjectRepository = EzArchive.Data.Portal.ProjectRepository;
using SecureCookieRepository = EzArchive.Data.Portal.SecureCookieRepository;
using SessionRepository = EzArchive.Data.Portal.SessionRepository;
using TicketRepository = EzArchive.Data.Portal.TicketRepository;
using UserRepository = EzArchive.Data.Portal.UserRepository;

namespace EzArchive.WebApi.SetUp;

public static class EzArchiveSetUp
{
	private static readonly UserId AnonymousUserId = new UserId("3F3F3F3F-527D-3F4F-5E3F-48373F3F5200");

	public static void AddEzArchive(this IServiceCollection services, IConfiguration configuration)
	{
		var portalRepository = CreatePortalRepository(configuration);
		var mcmSettings = portalRepository.Module.GetValidSettings<McmSettings>("MCM");
		var ezaSettings = portalRepository.Module.GetValidSettings<EzaConfig>("EZ-Archive");
		var authenticationSettings = portalRepository.Module.GetValidSettings<AuthenticationSettings>("Authentication");
		var portalAuthenticationRepository = new AuthenticationRepository(authenticationSettings.ConnectionString);
		var logger = new PortalLogger(portalRepository.Log);
		var mcmRepository = new McmRepository().WithConfiguration(mcmSettings.ConnectionString);

		var ezUserSearchCore = new SolrCore(new HttpConnection(ezaSettings.SolrUrl), ezaSettings.UserView);
		var ezSearchCore = new SolrCore(new HttpConnection(ezaSettings.SolrUrl), ezaSettings.SearchView);

			var sessionRepository = new SessionRepository(portalRepository.Session, AnonymousUserId);
			var secureCookieRepository = new SecureCookieRepository(portalAuthenticationRepository);
			var credentialsValidator = new CredentialsValidator(portalAuthenticationRepository.Password);
			var sessionUserGetter = new SessionUserGetter(portalRepository.User);
			var authKeyRepository = new AuthKeyRepository(portalAuthenticationRepository);
			var userRepository = new UserRepository(
				portalRepository.User,
				mcmRepository.Object,
				mcmRepository.Metadata,
				portalRepository.Group,
				portalAuthenticationRepository.Password,
				ezUserSearchCore,
				new MetadataSchemaId(ezaSettings.Mcm.ProfileMetadataSchemaId),
				ezaSettings.AdminGroupId,
				ezaSettings.ContributorGroupId,
				ezaSettings.UserGroupId,
				ezaSettings.Mcm.ProfileObjectTypeId,
				ezaSettings.Mcm.UserFolderId);
			var dataDefinitionRepository = new DataDefinitionRepository(mcmRepository.MetadataSchema);
			var searchDefinitionRepository = SetUpSearchDefinitionRepository(ezaSettings, dataDefinitionRepository);
			var projectRepository = new CachedProjectRepository(
				new ProjectRepository(mcmRepository.Project, userRepository));
			var labelRepository = new CachedLabelRepository(
				projectRepository,
				new LabelRepository(mcmRepository.Label));
			var fileRepository =
				new FileRepository(mcmRepository.File, mcmRepository.Object, ezaSettings.Mcm.UploadDestinationId);
			var ticketRepository = new TicketRepository(portalRepository.Ticket);
			var emailSender = new MandrillEmailSender(new MandrillApi(ezaSettings.MandrillApiKey));
			var frontendUrl = new Uri(ezaSettings.FrontendUrl);

		var formatRepository = new FormatRepository(mcmRepository.Format);
			var searchRepository = new SearchRepository(
				ezSearchCore,
				searchDefinitionRepository,
				dataDefinitionRepository,
				labelRepository);
			var assetRepository = new AssetRepository(
				mcmRepository.Object,
				mcmRepository.Metadata,
				dataDefinitionRepository,
				userRepository,
				new Folder._Id(ezaSettings.Mcm.AssetFolderId),
				new MetadataSchemaId(ezaSettings.Mcm.AssetMetadataSchemaId));
			var passwordRepository = new PasswordAuthenticationRepository(
				portalRepository.User,
				portalAuthenticationRepository.Password);

		new IndexHelper(searchDefinitionRepository,
			dataDefinitionRepository); // must create to instantiate api correctly
		var s3 = new S3(ezaSettings.Aws.UploadBucket);

		services.AddSingleton<ILogger>(_ => logger);

		services.AddSingleton<IAnonymousTaskExecutor>(_ =>
			new AnonymousTaskExecutor(sessionRepository, sessionRepository));

		services.AddSingleton<IAuthenticatedResultTaskExecutor>(_ =>
			new AuthenticatedResultTaskExecutor(sessionRepository, sessionRepository, AnonymousUserId));

		services.AddSingleton<IAuthenticatedAsyncResultTaskExecutor>(_ =>
			new AuthenticatedResultTaskExecutor(sessionRepository, sessionRepository, AnonymousUserId));

		services.AddSingleton<IAuthenticatedAsyncTaskExecutor>(_ =>
			new AuthenticatedTaskExecutor(sessionRepository, sessionRepository, AnonymousUserId));

		services.AddSingleton<IAuthenticatedTaskExecutor>(_ =>
			new AuthenticatedTaskExecutor(sessionRepository, sessionRepository, AnonymousUserId));

		services.AddTransient<ICreateSessionUseCase>(_ =>
			new CreateSessionUseCase(sessionRepository, AnonymousUserId));

		services.AddTransient<IGetSessionUseCase>(_ =>
			new GetSessionUseCase(sessionRepository));

		services.AddTransient<ICreateSecureCookieUseCase>(_ =>
			new CreateSecureCookieUseCase(secureCookieRepository, secureCookieRepository));

		services.AddTransient<ILoginWithSecureCookieUseCase>(_ =>
			new LoginWithSecureCookieUseCase(secureCookieRepository, sessionRepository));

		services.AddTransient<IAuthenticateSessionUseCase>(_ =>
			new AuthenticateSessionUseCase(sessionUserGetter, sessionRepository, credentialsValidator));

		services.AddTransient<ILoginWithAuthKeyUseCase>(_ =>
			new LoginWithAuthKeyUseCase(authKeyRepository, sessionRepository));

		services.AddTransient<ICreateAuthKeyUseCase>(_ =>
			new CreateAuthKeyUseCase(authKeyRepository));

		services.AddTransient<IGetCurrentUserUseCase>(_ =>
			new GetCurrentUserUseCase(userRepository));

		services.AddTransient<Chaos.Portal.Domain.IGetCurrentUserUseCase>(_ =>
			new Chaos.Portal.Domain.GetCurrentUserUseCase(portalRepository.User));

		services.AddTransient(_ =>
			ezaSettings);

		services.AddTransient<IGetSearchDefinitionsUseCase>(_ =>
			new GetSearchDefinitionsUseCase(searchDefinitionRepository));

		services.AddTransient<IGetDataDefinitionUseCase>(_ =>
			new GetDataDefinitionUseCase(dataDefinitionRepository, userRepository));

		services.AddTransient<ISearchUseCase>(_ =>
			new SearchUseCase(
				searchRepository,
				searchDefinitionRepository,
				projectRepository,
				ezaSettings.DefaultSortField,
				ezaSettings.ArePermissionsHandledThroughProjects));

		services.AddTransient<IGetFacetsForQueryUseCase>(_ =>
			new GetFacetsForQueryUseCase(
				searchRepository,
				projectRepository,
				ezaSettings.ArePermissionsHandledThroughProjects,
				ezaSettings.FacetSettings.Select(fs =>
					new FacetSetting(
						fs.Header,
						fs.Position,
						fs.Fields.Select(f =>
							new FacetSetting.FacetSpecification(
								f.Key,
								(FacetSetting.FacetType)f.Type))))));

		services.AddTransient<IGetTagsUseCase>(_ =>
			new GetTagsUseCase(searchRepository, projectRepository, ezaSettings.ArePermissionsHandledThroughProjects));

		services.AddTransient<IGetAssetByIdUseCase>(_ =>
			new GetAssetByIdUseCase(assetRepository, userRepository));

		services.AddTransient<ISetAssetUseCase>(_ =>
			new SetAssetUseCase(userRepository, assetRepository, dataDefinitionRepository, searchRepository));

		services.AddTransient<IGetUsersUseCase>(_ =>
			new GetUsersUseCase(userRepository, AnonymousUserId));

		services.AddTransient<ISetTagAssetUseCase>(_ =>
			new SetTagAssetUseCase(assetRepository, userRepository, searchRepository));

		services.AddTransient<IGetProjectsUseCase>(_ =>
			new GetProjectsUseCase(projectRepository));

		services.AddTransient<ISetProjectsUseCase>(_ =>
			new SetProjectsUseCase(projectRepository));

		services.AddTransient<IDeleteProjectUseCase>(_ =>
			new DeleteProjectUseCase(projectRepository));

		services.AddTransient<IAssociateProjectWithUserUseCase>(_ =>
			new AssociateProjectWithUserUseCase(projectRepository));

		services.AddTransient<IDisassociateProjectWithUserUseCase>(_ =>
			new DisassociateProjectWithUserUseCase(projectRepository));

		services.AddTransient<ISetLabelUseCase>(_ =>
			new SetLabelUseCase(projectRepository, labelRepository));

		services.AddTransient<IDeleteLabelUseCase>(_ =>
			new DeleteLabelUseCase(projectRepository, labelRepository));

		services.AddTransient<ISetUserUseCase>(_ =>
			new SetUserUseCase(userRepository, AnonymousUserId));

		services.AddTransient<IDeleteUserUseCase>(_ =>
			new DeleteUserUseCase(userRepository, projectRepository));

		services.AddTransient<IReIndexAssetsUseCase>(_ =>
			new ReIndexAssetsUseCase(assetRepository, userRepository, searchRepository));

		services.AddTransient<ISetFileAccessForAssetUseCase>(_ =>
			new SetFileAccessForAssetUseCase(assetRepository, userRepository));

		services.AddTransient<ICreateFileReferenceUseCase>(_ =>
			new CreateFileReferenceUseCase(fileRepository, userRepository));

		services.AddTransient<IAssociateLabelWithAssetUseCase>(_ =>
			new AssociateLabelWithAssetUseCase(projectRepository, labelRepository, assetRepository, searchRepository));

		services.AddTransient<IDeleteAssetUseCase>(_ =>
			new DeleteAssetUseCase(assetRepository, userRepository, searchRepository, s3));

		services.AddTransient<IDisassociateLabelWithAssetUseCase>(_ =>
			new DisassociateLabelWithAssetUseCase(projectRepository, labelRepository, assetRepository,
				searchRepository));

		services.AddTransient<ISetAnnotationUseCase>(_ =>
			new SetAnnotationUseCase(userRepository, assetRepository, dataDefinitionRepository));

		services.AddTransient<IDeleteAnnotationUseCase>(_ =>
			new DeleteAnnotationUseCase(assetRepository, userRepository, searchRepository));

		services.AddTransient<ISetPermissionForAnnotationUseCase>(_ =>
			new SetPermissionForAnnotationUseCase(userRepository, assetRepository));

		services.AddTransient<IGetAnnotationDefinitionsUseCase>(_ =>
			new GetAnnotationDefinitionsUseCase(dataDefinitionRepository));

		services.AddTransient<ISearchUsersUseCase>(_ =>
			new SearchUsersUseCase(userRepository));

		services.AddTransient<IUpdateFileReferenceUseCase>(_ =>
			new UpdateFileReferenceUseCase(fileRepository, userRepository));

		services.AddTransient<IDeleteFileUseCase>(_ =>
			new DeleteFileUseCase(fileRepository, userRepository, s3));

		services.AddTransient<IGetFileStreamUseCase>(_ =>
			new GetFileStreamUseCase(fileRepository, s3));

		var otherFormatType = 4u;
		services.AddTransient<IUploadFileToAssetUseCase>(_ =>
				new UploadFileToAssetUseCase(fileRepository, userRepository, s3, otherFormatType));

			services.AddTransient<IGetFormatByIdUseCase>(_ =>
				new GetFormatByIdUseCase(formatRepository));

			services.AddTransient<IRequestResetPasswordUseCase>(_ =>
				new RequestResetPasswordUseCase(userRepository, ticketRepository, emailSender, frontendUrl));

			services.AddTransient<IUpdatePasswordUseCase>(
				_ => new UpdatePasswordUseCase(ticketRepository, passwordRepository));
		}

	private static ISearchDefinitionRepository SetUpSearchDefinitionRepository(
		EzaConfig config, IDataDefinitionRepository dataDefinitionRepository)
	{
		return new SearchDefinitionRepository(
			config.SearchDefinition.Select(d =>
				new SearchFieldDefinition(d.DisplayName, d.Ids, Type(d.Ids), d.IsSortable, d.IsVisible)));

			string Type(string[] ids)
			{
				if (ids.Any() is false)
					return "String";

			var firstId = ids.First();

			return dataDefinitionRepository
				.GetDataDefinition(firstId)
				.GetFieldDefinition(firstId).Type;
		}
	}

	private static IPortalRepository CreatePortalRepository(IConfiguration configuration) =>
		new PortalRepository().WithConfiguration(configuration["PortalConnectionString"]);
}
