using System;
using Chaos.Portal.Core.Data;
using Chaos.Portal.Core.Module;

namespace EzArchive.WebApi.SetUp;

public static class ModuleRepositoryExtensions
{
	public static T GetValidSettings<T>(this IModuleRepository moduleRepository, string settingsName)
		where T : IModuleSettings, new()
	{
		var settings = moduleRepository.Get(settingsName).ParseSettings<T>();

		if (settings.IsValid() == false)
			throw new Exception($"App can not start because the {settingsName} settings is not valid!");

		return settings;
	}
}