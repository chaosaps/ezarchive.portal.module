using Chaos.Portal.Core.Module;

namespace EzArchive.WebApi.SetUp;

public class McmSettings : IModuleSettings
{
	public string ConnectionString { get; set; }

	public bool IsValid()
	{
		return !string.IsNullOrEmpty(ConnectionString);
	}
}