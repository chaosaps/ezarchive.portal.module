using System;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzSystem;

[ApiController]
public class FullReIndexEndpoint : ControllerBase
{
	private readonly IAuthenticatedTaskExecutor taskExecutor;
	private readonly IReIndexAssetsUseCase useCase;

	public FullReIndexEndpoint(IAuthenticatedTaskExecutor taskExecutor, IReIndexAssetsUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzSystem" },
		Description = "Reindex assets")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<EndpointResult>))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EzSystem/FullReIndex"),
	 Route("EzSystem/FullReIndex")]
	public IActionResult Invoke(
		[FromQuery(Name = "sessionGUID")] Guid sessionId,
		[FromQuery] bool clean,
		[FromQuery] uint? objectTypeId = null)
	{
		return taskExecutor.Execute(new SessionId(sessionId),
			session => useCase.Invoke(session, clean, objectTypeId),
			() => Ok(new ResultWrapper<EndpointResult>(EndpointResult.Success())));
	}
}