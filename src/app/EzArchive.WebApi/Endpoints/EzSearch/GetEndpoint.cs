using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EzArchive.Application;
using EzArchive.Application.UseCases.Search;
using EzArchive.Domain;
using EZArchive.Domain;
using EzArchive.WebApi.Dtos.Inputs;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NodaTime.Text;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzSearch;

[ApiController]
public class GetEndpoint : ControllerBase
{
	private readonly IAuthenticatedAsyncResultTaskExecutor taskExecutor;
	private readonly ISearchUseCase useCase;

	public GetEndpoint(IAuthenticatedAsyncResultTaskExecutor taskExecutor, ISearchUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzSearch" },
		Description = "")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<SearchResult[]>))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EzSearch/Get"),
	 Route("EzSearch/Get")]
	public Task<IActionResult> Invoke(
		[FromQuery(Name = "sessionGUID")] Guid sessionId,
		[FromQuery] string q = null,
		[FromQuery] string filter = null,
		[FromQuery] string sort = null,
		[FromQuery] string tag = null,
		[FromQuery] string facets = null,
		[FromQuery(Name = "between")] string betweenJson = null,
		[FromQuery] int pageIndex = 0,
		[FromQuery] int pageSize = 10)
	{
		var searchQuery = string.IsNullOrWhiteSpace(q)
			? SearchQuery.AllQuery()
			: SearchQuery.Parse(q);
		var searchQueryFilter = string.IsNullOrWhiteSpace(filter)
			? new SearchQuery()
			: SearchQuery.Parse(filter);

		var facetsList = string.IsNullOrWhiteSpace(facets)
			? new List<string>()
			: facets.Split(",").Select(s => s.Trim()).ToList();

		return taskExecutor.ExecuteAsync(new SessionId(sessionId),
			session => useCase.InvokeAsync(
				session,
				searchQuery,
				searchQueryFilter,
				facetsList,
				sort,
				tag,
				Between(),
				new PageRequest(pageIndex, pageSize)),
			page => Ok(Map(page)));

		Between Between()
		{
			if (string.IsNullOrWhiteSpace(betweenJson))
				return new Between();

			var between = JsonConvert.DeserializeObject<BetweenDates>(betweenJson);

			if (string.IsNullOrWhiteSpace(between.From) || string.IsNullOrWhiteSpace(between.To))
				throw new ArgumentException("both From and To needs to have a value", nameof(betweenJson));

			return new Between(
				InstantPattern.General.Parse(between.From).Value,
				InstantPattern.General.Parse(between.To).Value);
		}
	}

	private ResultWrapper<SearchResult> Map(Page<Search> page) =>
		new ResultWrapper<SearchResult>(
			new PagedResult<SearchResult>(
				page.FoundCount,
				page.StartIndex,
				page.Results.Select(r =>
					new SearchResult(
						r.Id.ToString(),
						r.TypeId,
						r.Fields.Select(f => new SearchResult.KeyValueResult(f.Key, f.Value))))));
}