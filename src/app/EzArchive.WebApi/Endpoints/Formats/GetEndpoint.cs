using System.Threading.Tasks;
using EzArchive.Application.UseCases.Formats;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Format = EzArchive.WebApi.Dtos.Results.Format;

namespace EzArchive.WebApi.Endpoints.Formats
{
	[ApiController]
	public class GetEndpoint : ControllerBase
	{
		private readonly IAnonymousTaskExecutor taskExecutor;
		private readonly IGetFormatByIdUseCase useCase;

		public GetEndpoint(IAnonymousTaskExecutor taskExecutor, IGetFormatByIdUseCase useCase)
		{
			this.taskExecutor = taskExecutor;
			this.useCase = useCase;
		}
		
		[SwaggerOperation(
			Tags = new[] {"Formats"},
			Description = "Get format by Id")]
		[SwaggerResponse(200, Type = typeof(ResultWrapper<Format>))]
		[ApiVersion("6")]
		[HttpGet,
		 Route("v{version:apiVersion}/Formats/{id}"),
		 Route("Formats/{id}")]
		public Task<IActionResult> Invoke([FromRoute] uint id)
		{
			return taskExecutor.ExecuteAsync(
				() => useCase.InvokeAsync(new FormatId(id)),
				format => Ok(new ResultWrapper<Format>(new Format(format.Id.Value, format.Extension))));
		}
	}
}