using EzArchive.Application.UseCases.Sessions.Create;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.Sessions;

[ApiController]
public class CreateSessionEndpoint : ControllerBase
{
	private readonly IAnonymousTaskExecutor taskExecutor;
	private readonly ICreateSessionUseCase useCase;

	public CreateSessionEndpoint(IAnonymousTaskExecutor taskExecutor, ICreateSessionUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Description = "Create a session which needs to be authenticated before it can be used for calling the APi",
		Tags = new[] { "Authentication" })]
	[SwaggerResponse(200, Description = "The unauthenticated session",
		Type = typeof(ResultWrapper<Session>))]
	[SwaggerResponse(500, "Internal server error", typeof(ResultWrapper))]
	[HttpGet, Route("Session/Create"), Route("v{version:apiVersion}/Session/Create")]
	[ApiVersion("6")]
	public IActionResult Create()
	{
		return taskExecutor.Execute(
			() => useCase.Invoke(),
			result => Ok(new ResultWrapper<Session>(Session.FromDomain(result))));
	}
}