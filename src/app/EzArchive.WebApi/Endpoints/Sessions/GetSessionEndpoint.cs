using System;
using EzArchive.Application.UseCases.Sessions.Get;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Session = EzArchive.WebApi.Dtos.Results.Session;

namespace EzArchive.WebApi.Endpoints.Sessions;

[ApiController]
public class GetSessionEndpoint : ControllerBase
{
	private readonly IAuthenticatedResultTaskExecutor taskExecutor;
	private readonly IGetSessionUseCase useCase;

	public GetSessionEndpoint(IAuthenticatedResultTaskExecutor taskExecutor, IGetSessionUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(Tags = new[] { "Authentication" })]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<Session>))]
	[SwaggerResponse(500, "Internal server error", typeof(ResultWrapper))]
	[HttpGet, Route("Session/Get"), Route("v{version:apiVersion}/Session/Get")]
	[ApiVersion("6")]
	public IActionResult Get([FromQuery(Name = "sessionGUID")] Guid sessionId)
	{
		return taskExecutor.Execute(
			new SessionId(sessionId),
			session => useCase.Invoke(new SessionId(sessionId)),
			session => Ok(new ResultWrapper<Session>(Session.FromDomain(session)))
		);
	}
}