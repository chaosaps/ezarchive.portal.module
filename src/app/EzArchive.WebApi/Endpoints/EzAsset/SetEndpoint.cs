using System;
using System.Collections.Generic;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzAsset;

[ApiController]
public class SetEndpoint : ControllerBase
{
	private readonly IAuthenticatedResultTaskExecutor taskExecutor;
	private readonly ISetAssetUseCase useCase;

	public SetEndpoint(IAuthenticatedResultTaskExecutor taskExecutor, ISetAssetUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzAsset" },
		Description = "Create or update an Asset")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<Dtos.Results.EzAsset>))]
	[ApiVersion("6")]
	[HttpPost,
	 Route("v{version:apiVersion}/EzAsset/Set"),
	 Route("EzAsset/Set")]
	public IActionResult Invoke(
		[FromForm(Name = "sessionGUID")] Guid sessionGuid,
		[FromForm] Dictionary<string, string> data)
	{
		var sessionId = new SessionId(sessionGuid);

		data = Request != null &&
		       Request.Form.ContainsKey(nameof(data))
			? JsonConvert.DeserializeObject<Dictionary<string, string>>(Request.Form[nameof(data)])
			: data;

		return taskExecutor.Execute(
			sessionId,
			session => useCase.Invoke(session, data),
			asset => Ok(new ResultWrapper<Dtos.Results.EzAsset>(new Dtos.Results.EzAsset(asset, sessionId))));
	}
}