using System;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzAsset;

[ApiController]
public class SetTagEndpoint : ControllerBase
{
	private readonly IAuthenticatedTaskExecutor taskExecutor;
	private readonly ISetTagAssetUseCase useCase;

	public SetTagEndpoint(IAuthenticatedTaskExecutor taskExecutor, ISetTagAssetUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzAsset" },
		Description = "Set tags for asset")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<Dtos.Results.EzAsset>))]
	[ApiVersion("6")]
	[HttpPost,
	 Route("v{version:apiVersion}/EzAsset/SetTag"),
	 Route("EzAsset/SetTag")]
	public IActionResult Invoke(
		[FromForm(Name = "sessionGUID")] Guid sessionGuid,
		[FromForm] Guid id,
		[FromForm] string tags)
	{
		return taskExecutor.Execute(
			new SessionId(sessionGuid),
			session => useCase.Invoke(
				session,
				new AssetId(id),
				string.IsNullOrWhiteSpace(tags) ? new string[0] : tags.Split(",")),
			() => Ok(new ResultWrapper<EndpointResult>(EndpointResult.Success())));
	}
}