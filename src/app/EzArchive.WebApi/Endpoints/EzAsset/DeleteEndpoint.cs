using System;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzAsset;

[ApiController]
public class DeleteEndpoint : ControllerBase
{
	private readonly IAuthenticatedAsyncTaskExecutor taskExecutor;
	private readonly IDeleteAssetUseCase useCase;

	public DeleteEndpoint(IAuthenticatedAsyncTaskExecutor taskExecutor, IDeleteAssetUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzAsset" },
		Description = "Delete an asset")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<Dtos.Results.EzAsset>))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EzAsset/Delete"),
	 Route("EzAsset/Delete")]
	public Task<IActionResult> InvokeAsync(
		[FromQuery(Name = "sessionGUID")] Guid sessionGuid,
		[FromQuery] Guid id)
	{
		return taskExecutor.ExecuteAsync(
			new SessionId(sessionGuid),
			session => useCase.InvokeAsync(
				session,
				new AssetId(id)),
			() => Ok(new ResultWrapper<EndpointResult>(EndpointResult.Success())));
	}
}