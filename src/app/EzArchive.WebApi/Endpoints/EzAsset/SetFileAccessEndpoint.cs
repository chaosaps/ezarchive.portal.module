using System;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzAsset;

[ApiController]
public class SetFileAccessEndpoint : ControllerBase
{
	private readonly IAuthenticatedTaskExecutor taskExecutor;
	private readonly ISetFileAccessForAssetUseCase useCase;

	public SetFileAccessEndpoint(IAuthenticatedTaskExecutor taskExecutor, ISetFileAccessForAssetUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzAsset" },
		Description = "Set FileAccess for asset")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<EndpointResult>))]
	[ApiVersion("6")]
	[HttpPost,
	 Route("v{version:apiVersion}/EzAsset/SetFileAccess"),
	 Route("EzAsset/SetFileAccess")]
	public IActionResult Invoke(
		[FromForm(Name = "sessionGUID")] Guid sessionGuid,
		[FromForm] Guid id,
		[FromForm] bool requireLogin = false)
	{
		return taskExecutor.Execute(
			new SessionId(sessionGuid),
			session => useCase.Invoke(
				session,
				new AssetId(id),
				requireLogin),
			() => Ok(new ResultWrapper<EndpointResult>(EndpointResult.Success())));
	}

	[SwaggerOperation(
		Tags = new[] { "EzAsset" },
		Description = "Set FileAccess for asset")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<EndpointResult>))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EzAsset/SetFileAccess"),
	 Route("EzAsset/SetFileAccess")]
	public IActionResult InvokeGet(
		[FromQuery(Name = "sessionGUID")] Guid sessionGuid,
		[FromQuery] Guid id,
		[FromQuery] bool requireLogin = false)
	{
		return Invoke(sessionGuid, id, requireLogin);
	}
}