using System;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzAsset;

[ApiController]
public class GetEndpoint : ControllerBase
{
	private readonly IAnonymousTaskExecutor taskExecutor;
	private readonly IGetAssetByIdUseCase useCase;

	public GetEndpoint(IAnonymousTaskExecutor taskExecutor, IGetAssetByIdUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzAsset" },
		Description = "Get an Asset by it's Id")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<Dtos.Results.EzAsset>))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EzAsset/Get"),
	 Route("EzAsset/Get")]
	public IActionResult Invoke(
		[FromQuery(Name = "sessionGUID")] Guid sessionId,
		[FromQuery] Guid id)
	{
		return taskExecutor.Execute(new SessionId(sessionId),
			session => useCase.Invoke(session, new AssetId(id)),
			result => Ok(new ResultWrapper<Dtos.Results.EzAsset>(
				new Dtos.Results.EzAsset(
					result,
					new SessionId(sessionId)))));
	}
}