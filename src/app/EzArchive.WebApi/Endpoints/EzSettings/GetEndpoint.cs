using EzArchive.Application;
using EzArchive.WebApi.Dtos.Results;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzSettings;

[ApiController]
public class GetEndpoint : ControllerBase
{
	private readonly EzaConfig config;

	public GetEndpoint(EzaConfig config)
	{
		this.config = config;
	}

	[SwaggerOperation(
		Tags = new[] { "EZSettings" },
		Description = "Get the EZA Settings")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<EZSettings>))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EZSettings/Get"),
	 Route("EZSettings/Get")]
	public IActionResult Invoke()
	{
		return Ok(new ResultWrapper<EZSettings>(EZSettings.From(config)));
	}
}