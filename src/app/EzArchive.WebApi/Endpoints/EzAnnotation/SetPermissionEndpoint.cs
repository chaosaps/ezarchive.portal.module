using System;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Annotations;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzAnnotation;

[ApiController]
public class SetPermissionEndpoint : ControllerBase
{
	private readonly IAuthenticatedAsyncTaskExecutor taskExecutor;
	private readonly ISetPermissionForAnnotationUseCase useCase;

	public SetPermissionEndpoint(
		IAuthenticatedAsyncTaskExecutor taskExecutor,
		ISetPermissionForAnnotationUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzAnnotation" },
		Description = "Set permission for an Annotation")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<EndpointResult>))]
	[ApiVersion("6")]
	[HttpPost,
	 Route("v{version:apiVersion}/EzAnnotation/SetPermission"),
	 Route("EzAnnotation/SetPermission")]
	public Task<IActionResult> InvokeAsync(
		[FromForm(Name = "sessionGUID")] Guid sessionGuid,
		[FromForm] Guid assetId,
		[FromForm] Guid id,
		[FromForm] uint permission)
	{
		return taskExecutor.ExecuteAsync(
			new SessionId(sessionGuid),
			session => useCase.InvokeAsync(
				session,
				new AssetId(assetId),
				new AnnotationId(id),
				(AnnotationPermission)permission),
			() => Ok(new ResultWrapper<EndpointResult>(EndpointResult.Success())));
	}
}