using System;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Annotations;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzAnnotation;

[ApiController]
public class DeleteEndpoint : ControllerBase
{
	private readonly IAuthenticatedAsyncTaskExecutor taskExecutor;
	private readonly IDeleteAnnotationUseCase useCase;

	public DeleteEndpoint(IAuthenticatedAsyncTaskExecutor taskExecutor, IDeleteAnnotationUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzAnnotation" },
		Description = "Delete an annotations")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<EndpointResult>))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EzAnnotation/Delete"),
	 Route("EzAnnotation/Delete")]
	public Task<IActionResult> InvokeAsync(
		[FromQuery(Name = "sessionGUID")] Guid sessionGuid,
		[FromQuery] Guid assetId,
		[FromQuery] Guid id)
	{
		return taskExecutor.ExecuteAsync(
			new SessionId(sessionGuid),
			session => useCase.InvokeAsync(
				session,
				new AssetId(assetId),
				new AnnotationId(id)),
			() => Ok(new ResultWrapper<EndpointResult>(EndpointResult.Success())));
	}
}