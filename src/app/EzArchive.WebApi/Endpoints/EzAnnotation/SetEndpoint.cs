using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Annotations;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;
using AnnotationVisibility = EzArchive.WebApi.Dtos.Inputs.AnnotationVisibility;
using DataDefinition = EzArchive.Domain.DataDefinition;

namespace EzArchive.WebApi.Endpoints.EzAnnotation;

[ApiController]
public class SetEndpoint : ControllerBase
{
	private readonly IAuthenticatedAsyncResultTaskExecutor taskExecutor;
	private readonly ISetAnnotationUseCase useCase;

	public SetEndpoint(
		IAuthenticatedAsyncResultTaskExecutor taskExecutor,
		ISetAnnotationUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzAnnotation" },
		Description = "Create or update an Annotation")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<IdentifierResult>))]
	[ApiVersion("6")]
	[HttpPost,
	 Route("v{version:apiVersion}/EzAnnotation/Set"),
	 Route("EzAnnotation/Set")]
	public Task<IActionResult> InvokeAsync(
		[FromForm(Name = "sessionGUID")] Guid sessionGuid,
		[FromForm] Guid assetId,
		[FromForm] Guid definitionId,
		[FromForm] Dictionary<string, string> data,
		[FromForm] AnnotationVisibility visibility = null)
	{
		data = Request != null &&
		       Request.Form.ContainsKey(nameof(data))
			? JsonConvert.DeserializeObject<Dictionary<string, string>>(Request.Form[nameof(data)])
			: data;

		visibility = Request != null &&
		             Request.Form.ContainsKey(nameof(visibility))
			? JsonConvert.DeserializeObject<AnnotationVisibility>(Request.Form[nameof(visibility)])
			: visibility;

		return taskExecutor.ExecuteAsync(
			new SessionId(sessionGuid),
			session => useCase.InvokeAsync(
				session,
				new AssetId(assetId),
				new DataDefinition.__Id(definitionId),
				data,
				AnnotationVisibility.ToDomain(visibility)),
			annotationId =>
				Ok(new ResultWrapper<IdentifierResult>(new IdentifierResult(annotationId.Value.ToString()))));
	}
}