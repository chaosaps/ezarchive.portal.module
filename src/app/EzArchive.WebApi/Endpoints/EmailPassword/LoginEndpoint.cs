using System;
using EzArchive.Application.UseCases.Sessions.Authenticate;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EmailPassword;

[ApiController]
public class LoginEndpoint : ControllerBase
{
	private readonly IAnonymousTaskExecutor taskExecutor;
	private readonly IAuthenticateSessionUseCase useCase;

	public LoginEndpoint(IAnonymousTaskExecutor taskExecutor, IAuthenticateSessionUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(Description = "Authenticates session using user credentials",
		Tags = new[] { "Authentication" })]
	[SwaggerResponse(200, "Information about the authenticating user", Type = typeof(ResultWrapper<UserInfo>))]
	[SwaggerResponse(500, "Internal server error", typeof(ResultWrapper))]
	[HttpPost, Route("EmailPassword/Login"), Route("v{version:apiVersion}/EmailPassword/Login")]
	[ApiVersion("6")]
	public IActionResult Login(
		[FromForm(Name = "sessionGUID")] Guid sessionId,
		[FromForm] string email, [FromForm] string password)
	{
		return taskExecutor.Execute(
			() => useCase.Invoke(
				new SessionId(sessionId),
				new UserName(email),
				new Password(password)),
			result => Ok(new ResultWrapper<UserInfo>(UserInfo.FromDomain(result.Session, result.User))));
	}
}