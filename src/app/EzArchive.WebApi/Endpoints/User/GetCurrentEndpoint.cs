using System;
using Chaos.Portal.Domain;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Session = EzArchive.Domain.Session;
using SessionId = EzArchive.Domain.SessionId;
using UserInfo = Chaos.Portal.Core.Data.Model.UserInfo;

namespace EzArchive.WebApi.Endpoints.User;

[ApiController]
public class GetCurrentEndpoint : ControllerBase
{
	private readonly IAuthenticatedResultTaskExecutor taskExecutor;
	private readonly IGetCurrentUserUseCase useCase;

	public GetCurrentEndpoint(IAuthenticatedResultTaskExecutor taskExecutor, IGetCurrentUserUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "User" },
		Description = "Get the current user")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<Dtos.Results.UserInfo>[]))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/User/GetCurrent"),
	 Route("v{version:apiVersion}/User/Get"),
	 Route("User/Get"),
	 Route("User/GetCurrent")]
	public IActionResult Invoke([FromQuery(Name = "sessionGUID")] Guid sessionId)
	{
		return taskExecutor.Execute(
			new SessionId(sessionId),
			session => WrapUseCaseInvocation(session),
			r => Ok(new ResultWrapper<Dtos.Results.UserInfo>(Dtos.Results.UserInfo.FromDomain(r)))
		);
	}

	private Domain.Result<UserInfo> WrapUseCaseInvocation(Session session)
	{
		var result = useCase.Invoke(new Chaos.Portal.Domain.SessionId(session.Id.Value));

		return result.HasError
			? new Domain.Result<UserInfo>(ErrorCode.InvalidInput)
			: new Domain.Result<UserInfo>(result.Value);
	}
}