using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EzArchive.Application;
using EzArchive.Application.UseCases.Facets;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Inputs;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NodaTime.Text;
using Swashbuckle.AspNetCore.Annotations;
using FacetResponse = EzArchive.WebApi.Dtos.Results.FacetResponse;

namespace EzArchive.WebApi.Endpoints.EzFacet;

[ApiController]
public class GetEndpoint : ControllerBase
{
	private readonly IGetFacetsForQueryUseCase useCase;
	private readonly IAuthenticatedAsyncResultTaskExecutor taskExecutor;

	public GetEndpoint(IAuthenticatedAsyncResultTaskExecutor taskExecutor, IGetFacetsForQueryUseCase useCase)
	{
		this.useCase = useCase;
		this.taskExecutor = taskExecutor;
	}

	[SwaggerOperation(
		Tags = new[] { "EzFacet" },
		Description = "Get facets for a query")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<FacetResponse[]>))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EzFacet/Get"),
	 Route("EzFacet/Get")]
	public Task<IActionResult> Invoke(
		[FromQuery(Name = "sessionGUID")] Guid sessionId,
		[FromQuery] string q = null,
		[FromQuery] string filter = null,
		[FromQuery] string tag = null,
		[FromQuery] string facets = null,
		[FromQuery(Name = "between")] string betweenJson = null,
		[FromQuery] int pageIndex = 0,
		[FromQuery] int pageSize = 10,
		[FromQuery] string rangeStart = null,
		[FromQuery] string rangeEnd = null,
		[FromQuery] string rangeGap = null)
	{
		var searchQuery = string.IsNullOrWhiteSpace(q)
			? SearchQuery.AllQuery()
			: SearchQuery.Parse(q);
		var searchQueryFilter = string.IsNullOrWhiteSpace(filter)
			? new SearchQuery()
			: SearchQuery.Parse(filter);

		var facetsList = string.IsNullOrWhiteSpace(facets)
			? new List<string>()
			: facets.Split(",").Select(s => s.Trim()).ToList();

		var facetRange = string.IsNullOrWhiteSpace(rangeStart) ||
		                 string.IsNullOrWhiteSpace(rangeEnd) ||
		                 string.IsNullOrWhiteSpace(rangeGap)
			? null
			: new FacetRange(rangeStart, rangeEnd, rangeGap);

		return taskExecutor.ExecuteAsync(
			new SessionId(sessionId),
			session => useCase.InvokeAsync(
				session,
				searchQuery,
				searchQueryFilter,
				facetsList,
				tag,
				Between(),
				facetRange),
			facets => Ok(Map(facets)));

		Between Between()
		{
			if (string.IsNullOrWhiteSpace(betweenJson))
				return new Between();

			var between = JsonConvert.DeserializeObject<BetweenDates>(betweenJson);

			if (string.IsNullOrWhiteSpace(between.From) || string.IsNullOrWhiteSpace(between.To))
				throw new ArgumentException("both From and To needs to have a value", nameof(betweenJson));

			return new Between(
				InstantPattern.General.Parse(between.From).Value,
				InstantPattern.General.Parse(between.To).Value);
		}
	}

	private static ResultWrapper<FacetResponse> Map(
		IReadOnlyList<Application.UseCases.Facets.FacetResponse> facets) =>
		new ResultWrapper<FacetResponse>(
			facets.Select(f =>
					new FacetResponse(
						f.Header,
						f.Position,
						f.Fields.Select(field =>
							new FacetResponse.FieldFacet(
								field.Value,
								field.Facets.Select(facet =>
									new FacetResponse.Facet(
										facet.Key,
										facet.Count))))))
				.ToList());
}