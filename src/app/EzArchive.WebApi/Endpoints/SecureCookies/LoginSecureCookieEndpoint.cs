using System;
using EzArchive.Application.UseCases.SecureCookies.Login;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.SecureCookies;

[ApiController]
public class LoginSecureCookieEndpoint : ControllerBase
{
	private readonly IAnonymousTaskExecutor taskExecutor;
	private readonly ILoginWithSecureCookieUseCase useCase;

	public LoginSecureCookieEndpoint(IAnonymousTaskExecutor taskExecutor, ILoginWithSecureCookieUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Description = "Authenticates session using secure cookie",
		Tags = new[] { "Authentication" })]
	[SwaggerResponse(200, Description = "Refreshed secure cookie",
		Type = typeof(ResultWrapper<Dtos.Results.SecureCookie>))]
	[SwaggerResponse(500, "Internal server error", typeof(ResultWrapper))]
	[HttpPost, Route("SecureCookie/Login"), Route("v{version:apiVersion}/SecureCookie/Login")]
	[ApiVersion("6")]
	public IActionResult Login(
		[FromForm(Name = "sessionGUID")] Guid sessionId,
		[FromForm(Name = "guid")] Guid secureCookieGuid,
		[FromForm] Guid passwordGuid)
	{
		return taskExecutor.Execute(
			() => useCase.Login(
				new SessionId(sessionId), new SecureCookieId(secureCookieGuid), new PasswordGuid(passwordGuid)),
			secureCookie => Ok(new ResultWrapper<Dtos.Results.SecureCookie>(
				Dtos.Results.SecureCookie.FromDomain(secureCookie))));
	}
}