using System;
using EzArchive.Application.UseCases.SecureCookies.Create;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.SecureCookies;

[ApiController]
public class CreateSecureCookieEndpoint : ControllerBase
{
	private readonly IAuthenticatedResultTaskExecutor taskExecutor;
	private readonly ICreateSecureCookieUseCase useCase;

	public CreateSecureCookieEndpoint(IAuthenticatedResultTaskExecutor taskExecutor, ICreateSecureCookieUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Description = "Creates a secure cookie that can be used for authenticating sessions.",
		Tags = new[] { "Authentication" })]
	[SwaggerResponse(200, "Secure cookie with it's password", Type = typeof(ResultWrapper<Dtos.Results.SecureCookie>))]
	[SwaggerResponse(500, "Internal server error", typeof(ResultWrapper))]
	[HttpGet, Route("SecureCookie/Create"), Route("v{version:apiVersion}/SecureCookie/Create")]
	[ApiVersion("6")]
	public IActionResult Create([FromQuery(Name = "sessionGUID")] Guid sessionId)
	{
		return taskExecutor.Execute(
			new SessionId(sessionId),
			(user) => useCase.Invoke(user.UserId, new SessionId(sessionId)),
			result => Ok(new ResultWrapper<Dtos.Results.SecureCookie>(
				Dtos.Results.SecureCookie.FromDomain(result))));
	}
}