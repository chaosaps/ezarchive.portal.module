using System;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzProject;

[ApiController]
public class DeleteEndpoint : ControllerBase
{
	private readonly IAuthenticatedTaskExecutor taskExecutor;
	private readonly IDeleteProjectUseCase useCase;

	public DeleteEndpoint(IAuthenticatedTaskExecutor taskExecutor, IDeleteProjectUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzProject" },
		Description = "Delete the project")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<EndpointResult>))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EzProject/Delete"),
	 Route("EzProject/Delete")]
	public IActionResult Invoke(
		[FromQuery(Name = "sessionGUID")] Guid sessionGuid,
		[FromQuery] string id)
	{
		return taskExecutor.Execute(
			new SessionId(sessionGuid),
			session => useCase.Invoke(
				session,
				new ProjectId(id)),
			() => Ok(new ResultWrapper<EndpointResult>(EndpointResult.Success())));
	}
}