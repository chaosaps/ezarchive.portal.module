using System;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzProject;

[ApiController]
public class DisassociateWithEndpoint : ControllerBase
{
	private readonly IAuthenticatedTaskExecutor taskExecutor;
	private readonly IDisassociateProjectWithUserUseCase useCase;

	public DisassociateWithEndpoint(IAuthenticatedTaskExecutor taskExecutor,
		IDisassociateProjectWithUserUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzProject" },
		Description = "Disassociate user with project")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<EndpointResult>))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EzProject/DisassociateWith"),
	 Route("EzProject/DisassociateWith")]
	public IActionResult Invoke(
		[FromQuery(Name = "sessionGUID")] Guid sessionGuid,
		[FromQuery] string id,
		[FromQuery] Guid userId)
	{
		return taskExecutor.Execute(
			new SessionId(sessionGuid),
			session => useCase.Invoke(
				session,
				new ProjectId(id),
				new UserId(userId)),
			() => Ok(new ResultWrapper<EndpointResult>(EndpointResult.Success())));
	}
}