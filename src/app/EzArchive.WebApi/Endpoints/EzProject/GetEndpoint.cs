using System;
using System.Collections.Generic;
using System.Linq;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Project = EzArchive.WebApi.Dtos.Results.Project;

namespace EzArchive.WebApi.Endpoints.EzProject;

[ApiController]
public class GetEndpoint : ControllerBase
{
	private readonly IGetProjectsUseCase useCase;
	private readonly IAuthenticatedResultTaskExecutor taskExecutor;

	public GetEndpoint(IAuthenticatedResultTaskExecutor taskExecutor, IGetProjectsUseCase useCase)
	{
		this.useCase = useCase;
		this.taskExecutor = taskExecutor;
	}

	[SwaggerOperation(
		Tags = new[] { "EzProject" },
		Description = "Get Projects")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<Project[]>))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EzProject/Get"),
	 Route("EzProject/Get")]
	public IActionResult Invoke([FromQuery(Name = "sessionGUID")] Guid sessionId) =>
		taskExecutor.Execute(
			new SessionId(sessionId),
			session => useCase.Invoke(session),
			projects => Ok(Map(projects)));

	[SwaggerOperation(
		Tags = new[] { "EzProject" },
		Description = "Get Projects")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<Project[]>))]
	[ApiVersion("6")]
	[HttpPost,
	 Route("v{version:apiVersion}/EzProject/Get"),
	 Route("EzProject/Get")]
	public IActionResult InvokePost([FromForm(Name = "sessionGUID")] Guid sessionId) =>
		Invoke(sessionId);

	private static ResultWrapper<Project> Map(IEnumerable<Domain.Project> projects) =>
		new ResultWrapper<Project>(
			projects.Select(p => new Project(p)).ToList());
}