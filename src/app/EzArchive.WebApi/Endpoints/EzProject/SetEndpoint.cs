using System;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Inputs;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;
using Project = EzArchive.WebApi.Dtos.Results.Project;

namespace EzArchive.WebApi.Endpoints.EzProject;

[ApiController]
public class SetEndpoint : ControllerBase
{
	private readonly ISetProjectsUseCase useCase;
	private readonly IAuthenticatedResultTaskExecutor taskExecutor;

	public SetEndpoint(IAuthenticatedResultTaskExecutor taskExecutor, ISetProjectsUseCase useCase)
	{
		this.useCase = useCase;
		this.taskExecutor = taskExecutor;
	}

	[SwaggerOperation(
		Tags = new[] { "EzProject" },
		Description = "Create or update a Project")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<Project>))]
	[ApiVersion("6")]
	[HttpPost,
	 Route("v{version:apiVersion}/EzProject/Set"),
	 Route("EzProject/Set")]
	public IActionResult Invoke(
		[FromForm(Name = "sessionGUID")] Guid sessionId,
		[FromForm] SetProjectRequest project)
	{
		project = Request != null && Request.Form.ContainsKey(nameof(project))
			? JsonConvert.DeserializeObject<SetProjectRequest>(Request.Form[nameof(project)])
			: project;

		var projectId = string.IsNullOrWhiteSpace(project.Identifier)
			? null
			: new ProjectId(uint.Parse(project.Identifier));

		if (string.IsNullOrWhiteSpace(project.Name))
			return BadRequest(new ResultWrapper(ErrorCode.InvalidInput, $"Name is null"));

		return taskExecutor.Execute(
			new SessionId(sessionId),
			session => useCase.Invoke(session, projectId, project.Name),
			p => Ok(new ResultWrapper<Project>(new Project(p))));
	}
}