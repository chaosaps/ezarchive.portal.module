using System;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzProject;

[ApiController]
public class AssociateWithEndpoint : ControllerBase
{
	private readonly IAuthenticatedTaskExecutor taskExecutor;
	private readonly IAssociateProjectWithUserUseCase useCase;

	public AssociateWithEndpoint(IAuthenticatedTaskExecutor taskExecutor, IAssociateProjectWithUserUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzProject" },
		Description = "Associate user with project")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<EndpointResult>))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EzProject/AssociateWith"),
	 Route("EzProject/AssociateWith")]
	public IActionResult Invoke(
		[FromQuery(Name = "sessionGUID")] Guid sessionGuid,
		[FromQuery] string id,
		[FromQuery] Guid userId)
	{
		return taskExecutor.Execute(
			new SessionId(sessionGuid),
			session => useCase.Invoke(
				session,
				new ProjectId(id),
				new UserId(userId)),
			() => Ok(new ResultWrapper<EndpointResult>(EndpointResult.Success())));
	}
}