using EzArchive.Application.UseCases.SearchDefinitions;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzSearchDefinition;

[ApiController]
public class GetEndpoint : ControllerBase
{
	private readonly IAnonymousTaskExecutor taskExecutor;
	private readonly IGetSearchDefinitionsUseCase useCase;

	public GetEndpoint(IAnonymousTaskExecutor taskExecutor, IGetSearchDefinitionsUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EZSearchDefinition" },
		Description = "Get all search definitions")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<SearchDefinitionResult>))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EzSearchDefinition/Get"),
	 Route("EzSearchDefinition/Get")]
	public IActionResult Invoke()
	{
		return taskExecutor.Execute(
			() => useCase.Invoke(),
			definitions => Ok(new ResultWrapper<SearchDefinitionResult>(new SearchDefinitionResult(definitions))));
	}
}