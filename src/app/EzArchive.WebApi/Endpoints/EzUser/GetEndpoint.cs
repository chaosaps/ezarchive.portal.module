using System;
using System.Linq;
using EzArchive.Application.UseCases.Users.GetAll;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzUser;

[ApiController]
public class GetEndpoint : ControllerBase
{
	private readonly IAuthenticatedResultTaskExecutor taskExecutor;
	private readonly IGetUsersUseCase useCase;

	public GetEndpoint(IAuthenticatedResultTaskExecutor taskExecutor, IGetUsersUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Description = "Get the current User",
		Tags = new[] { "EzUser" })]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<Dtos.Results.EzUser[]>))]
	[SwaggerResponse(500, "Internal server error", typeof(ResultWrapper))]
	[HttpGet, Route("EzUser/Get"), Route("v{version:apiVersion}/EzUser/Get")]
	[ApiVersion("6")]
	public IActionResult Invoke([FromQuery(Name = "sessionGUID")] Guid sessionId)
	{
		return taskExecutor.Execute(new SessionId(sessionId),
			session => useCase.Invoke(session.UserId),
			users => Ok(new ResultWrapper<Dtos.Results.EzUser>(
				users.Select(Dtos.Results.EzUser.FromDomain))));
	}

	[SwaggerOperation(
		Description = "Get the current User",
		Tags = new[] { "EzUser" })]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<Dtos.Results.EzUser[]>))]
	[SwaggerResponse(500, "Internal server error", typeof(ResultWrapper))]
	[HttpPost, Route("EzUser/Get"), Route("v{version:apiVersion}/EzUser/Get")]
	[ApiVersion("6")]
	public IActionResult InvokePost([FromForm(Name = "sessionGUID")] Guid sessionId) =>
		Invoke(sessionId);
}