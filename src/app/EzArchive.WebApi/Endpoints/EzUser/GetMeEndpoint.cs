using System;
using EzArchive.Application.UseCases.Users.GetCurrent;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzUser;

[ApiController]
public class GetMeEndpoint : ControllerBase
{
	private readonly IAuthenticatedResultTaskExecutor taskExecutor;
	private readonly IGetCurrentUserUseCase useCase;

	public GetMeEndpoint(IAuthenticatedResultTaskExecutor taskExecutor, IGetCurrentUserUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Description = "Get the current User",
		Tags = new[] { "EzUser" })]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<Dtos.Results.EzUser>))]
	[SwaggerResponse(500, "Internal server error", typeof(ResultWrapper))]
	[HttpGet, Route("EzUser/Me"), Route("v{version:apiVersion}/EzUser/Me")]
	[ApiVersion("6")]
	public IActionResult GetSessionUserAsync([FromQuery(Name = "sessionGUID")] Guid sessionId)
	{
		return taskExecutor.Execute(new SessionId(sessionId),
			session => useCase.Invoke(session.UserId),
			ezUser => Ok(new ResultWrapper<Dtos.Results.EzUser>(Dtos.Results.EzUser.FromDomain(ezUser))));
	}
}