using System;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzUser;

[ApiController]
public class DeleteEndpoint : ControllerBase
{
	private readonly IAuthenticatedTaskExecutor taskExecutor;
	private readonly IDeleteUserUseCase useCase;

	public DeleteEndpoint(IAuthenticatedTaskExecutor taskExecutor, IDeleteUserUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzUser" },
		Description = "Delete the user")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<EndpointResult>))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EzUser/Delete"),
	 Route("EzUser/Delete")]
	public IActionResult Invoke(
		[FromQuery(Name = "sessionGUID")] Guid sessionGuid,
		[FromQuery] Guid id)
	{
		return taskExecutor.Execute(
			new SessionId(sessionGuid),
			session => useCase.Invoke(
				session,
				new UserId(id)),
			() => Ok(new ResultWrapper<EndpointResult>(EndpointResult.Success())));
	}
}