using System;
using System.Linq;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzUser;

[ApiController]
public class SearchEndpoint : ControllerBase
{
	private readonly IAuthenticatedAsyncResultTaskExecutor taskExecutor;
	private readonly ISearchUsersUseCase useCase;

	public SearchEndpoint(IAuthenticatedAsyncResultTaskExecutor taskExecutor, ISearchUsersUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Description = "Search among the Users",
		Tags = new[] { "EzUser" })]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<SearchUser>))]
	[SwaggerResponse(500, "Internal server error", typeof(ResultWrapper))]
	[HttpGet,
	 Route("EzUser/Search"),
	 Route("v{version:apiVersion}/EzUser/Search")]
	[ApiVersion("6")]
	public Task<IActionResult> InvokeAsync(
		[FromQuery(Name = "sessionGUID")] Guid sessionId,
		[FromQuery] string q = "",
		[FromQuery] uint pageSize = 10)
	{
		return taskExecutor.ExecuteAsync(new SessionId(sessionId),
			session => useCase.InvokeAsync(session, q, pageSize),
			users => Ok(
				new ResultWrapper<SearchUser>(
					users.Select(u => new SearchUser(u.id?.ToString(), u.name)))));
	}
}