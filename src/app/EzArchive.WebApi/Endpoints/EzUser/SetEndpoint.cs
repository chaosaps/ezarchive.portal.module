using System;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;
using SetUserRequest = EzArchive.WebApi.Dtos.Inputs.SetUserRequest;

namespace EzArchive.WebApi.Endpoints.EzUser;

[ApiController]
public class SetEndpoint : ControllerBase
{
	private readonly IAuthenticatedResultTaskExecutor taskExecutor;
	private readonly ISetUserUseCase useCase;

	public SetEndpoint(IAuthenticatedResultTaskExecutor taskExecutor, ISetUserUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzUser" },
		Description = "Create or update a user")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<Dtos.Results.EzUser>))]
	[ApiVersion("6")]
	[HttpPost,
	 Route("v{version:apiVersion}/EzUser/Set"),
	 Route("EzUser/Set")]
	public IActionResult Invoke(
		[FromForm(Name = "sessionGUID")] Guid sessionId,
		[FromForm] SetUserRequest data,
		[FromForm] string password)
	{
		data = Request != null && Request.Form.ContainsKey(nameof(data))
			? JsonConvert.DeserializeObject<SetUserRequest>(Request.Form[nameof(data)])
			: data;

		return taskExecutor.Execute(
			new SessionId(sessionId),
			session => useCase.Invoke(
				session,
				new Application.UseCases.Users.SetUserRequest(
					string.IsNullOrWhiteSpace(data.Identifier) ? null : new UserId(data.Identifier),
					data.Email,
					data.Name,
					data.Permission,
					data.Preferences,
					string.IsNullOrWhiteSpace(password) ? null : new Password(password))),
			r => Ok(new ResultWrapper<Dtos.Results.EzUser>(Dtos.Results.EzUser.FromDomain(r)))
		);
	}
}