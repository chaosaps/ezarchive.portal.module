using System;
using EzArchive.Application.UseCases.Labels;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzLabel;

[ApiController]
public class AssociateWithEndpoint : ControllerBase
{
	private readonly IAuthenticatedTaskExecutor taskExecutor;
	private readonly IAssociateLabelWithAssetUseCase useCase;

	public AssociateWithEndpoint(IAuthenticatedTaskExecutor taskExecutor, IAssociateLabelWithAssetUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzLabel" },
		Description = "Associate an asset with a label")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<Dtos.Results.EzAsset>))]
	[ApiVersion("6")]
	[HttpPost,
	 Route("v{version:apiVersion}/EzLabel/AssociateWith"),
	 Route("EzLabel/AssociateWith")]
	public IActionResult Invoke(
		[FromForm(Name = "sessionGUID")] Guid sessionGuid,
		[FromForm] uint id,
		[FromForm] Guid assetId)
	{
		return taskExecutor.Execute(
			new SessionId(sessionGuid),
			session => useCase.Invoke(
				session,
				new LabelId(id),
				new AssetId(assetId)),
			() => Ok(new ResultWrapper<EndpointResult>(EndpointResult.Success())));
	}

	[SwaggerOperation(
		Tags = new[] { "EzLabel" },
		Description = "Associate an asset with a label")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<Dtos.Results.EzAsset>))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EzLabel/AssociateWith"),
	 Route("EzLabel/AssociateWith")]
	public IActionResult InvokeGet(
		[FromQuery(Name = "sessionGUID")] Guid sessionGuid,
		[FromQuery] uint id,
		[FromQuery] Guid assetId)
	{
		return Invoke(sessionGuid, id, assetId);
	}
}