using System;
using EzArchive.Application.UseCases.Labels;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Inputs;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzLabel;

[ApiController]
public class SetEndpoint : ControllerBase
{
	private readonly ISetLabelUseCase useCase;
	private readonly IAuthenticatedResultTaskExecutor taskExecutor;

	public SetEndpoint(IAuthenticatedResultTaskExecutor taskExecutor, ISetLabelUseCase useCase)
	{
		this.useCase = useCase;
		this.taskExecutor = taskExecutor;
	}

	[SwaggerOperation(
		Tags = new[] { "EzLabel" },
		Description = "Create or update a Label")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<Dtos.Results.EzLabel>))]
	[ApiVersion("6")]
	[HttpPost,
	 Route("v{version:apiVersion}/EzLabel/Set"),
	 Route("EzLabel/Set")]
	public IActionResult Invoke(
		[FromForm(Name = "sessionGUID")] Guid sessionId,
		[FromForm] string projectId,
		[FromForm] SetLabelRequest label)
	{
		label = Request != null && Request.Form.ContainsKey(nameof(label))
			? JsonConvert.DeserializeObject<SetLabelRequest>(Request.Form[nameof(label)])
			: label;

		return taskExecutor.Execute(
			new SessionId(sessionId),
			session => useCase.Invoke(session, ProjectId(), LabelId(), label.Name),
			l => Ok(new ResultWrapper<Dtos.Results.EzLabel>(new Dtos.Results.EzLabel(l))));

		ProjectId ProjectId() =>
			string.IsNullOrWhiteSpace(projectId)
				? null
				: new ProjectId(uint.Parse(projectId));

		LabelId LabelId() =>
			string.IsNullOrWhiteSpace(label.Identifier)
				? null
				: new LabelId(uint.Parse(label.Identifier));
	}
}