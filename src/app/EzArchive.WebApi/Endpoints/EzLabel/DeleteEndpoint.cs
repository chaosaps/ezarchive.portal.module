using System;
using EzArchive.Application.UseCases.Labels;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzLabel;

[ApiController]
public class DeleteEndpoint : ControllerBase
{
	private readonly IAuthenticatedTaskExecutor taskExecutor;
	private readonly IDeleteLabelUseCase useCase;

	public DeleteEndpoint(IAuthenticatedTaskExecutor taskExecutor, IDeleteLabelUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzLabel" },
		Description = "Delete the label")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<EndpointResult>))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EzLabel/Delete"),
	 Route("EzLabel/Delete")]
	public IActionResult Invoke(
		[FromQuery(Name = "sessionGUID")] Guid sessionGuid,
		[FromQuery] string id)
	{
		return taskExecutor.Execute(
			new SessionId(sessionGuid),
			session => useCase.Invoke(
				session,
				new LabelId(id)),
			() => Ok(EndpointResult.Success()));
	}
}