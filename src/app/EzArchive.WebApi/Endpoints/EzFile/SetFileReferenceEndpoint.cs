using System;
using EzArchive.Application.UseCases.Files;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzFile;

[ApiController]
public class SetFileReferenceEndpoint : ControllerBase
{
	private readonly IAuthenticatedTaskExecutor taskExecutor;
	private readonly IUpdateFileReferenceUseCase useCase;

	public SetFileReferenceEndpoint(IAuthenticatedTaskExecutor taskExecutor, IUpdateFileReferenceUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzFile" },
		Description = "Update filename of a file reference")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<EndpointResult>))]
	[ApiVersion("6")]
	[HttpPost,
	 Route("v{version:apiVersion}/EzFile/Set"),
	 Route("EzFile/Set")]
	public IActionResult Invoke(
		[FromForm(Name = "sessionGUID")] Guid sessionId,
		[FromForm] uint id,
		[FromForm] string name)
	{
		return taskExecutor.Execute(
			new SessionId(sessionId),
			session => useCase.Invoke(session, new FileId(id), name),
			() => Ok(new ResultWrapper<EndpointResult>(EndpointResult.Success())));
	}
}