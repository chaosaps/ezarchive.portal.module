using System;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Files;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzFile;

[ApiController]
public class DeleteFileEndpoint : ControllerBase
{
	private readonly IAuthenticatedAsyncTaskExecutor taskExecutor;
	private readonly IDeleteFileUseCase useCase;

	public DeleteFileEndpoint(
		IAuthenticatedAsyncTaskExecutor taskExecutor,
		IDeleteFileUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzFile" },
		Description = "Delete a file")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<EndpointResult>))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EzFile/Delete"),
	 Route("EzFile/Delete")]
	public Task<IActionResult> InvokeAsync(
		[FromQuery(Name = "sessionGUID")] Guid sessionId,
		[FromQuery] uint id)
	{
		return taskExecutor.ExecuteAsync(
			new SessionId(sessionId),
			session => useCase.InvokeAsync(session, new FileId(id)),
			() => Ok(new ResultWrapper<EndpointResult>(EndpointResult.Success())));
	}

	[SwaggerOperation(
		Tags = new[] { "EzFile" },
		Description = "Delete a file")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<EndpointResult>))]
	[ApiVersion("6")]
	[HttpPost,
	 Route("v{version:apiVersion}/EzFile/Delete"),
	 Route("EzFile/Delete")]
	public Task<IActionResult> InvokePostAsync(
		[FromForm(Name = "sessionGUID")] Guid sessionId,
		[FromForm] uint id)
	{
		return taskExecutor.ExecuteAsync(
			new SessionId(sessionId),
			session => useCase.InvokeAsync(session, new FileId(id)),
			() => Ok(new ResultWrapper<EndpointResult>(EndpointResult.Success())));
	}
}