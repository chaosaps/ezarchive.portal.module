using System;
using System.IO;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Files;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzFile;

[ApiController]
public class UploadEndpoint : ControllerBase
{
	private readonly IAuthenticatedAsyncTaskExecutor taskExecutor;
	private readonly IUploadFileToAssetUseCase useCase;

	public UploadEndpoint(IAuthenticatedAsyncTaskExecutor taskExecutor, IUploadFileToAssetUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

		[SwaggerOperation(
			Tags = new[] {"EzFile"},
			Description = "Upload a file to an Asset")]
		[SwaggerResponse(200, Type = typeof(ResultWrapper<EndpointResult>))]
		[ApiVersion("6")]
		[HttpPost,
		 Route("v{version:apiVersion}/EzFile/Upload"),
		 Route("EzFile/Upload")]
		[RequestSizeLimit(2_000_000_000)]
		public Task<IActionResult> InvokeAsync(
			[FromForm(Name = "sessionGUID")] Guid sessionId,
			[FromForm] Guid assetId)
		{
			if (Request?.Form?.Files?.Count != 1)
				throw new IOException("No data found");

		return taskExecutor.ExecuteAsync(
			new SessionId(sessionId),
			session => useCase.InvokeAsync(
				session,
				new AssetId(assetId),
				Request.Form.Files[0].FileName,
				Request.Form.Files[0].OpenReadStream()),
			() => Ok(new ResultWrapper<EndpointResult>(EndpointResult.Success())));
	}
}
