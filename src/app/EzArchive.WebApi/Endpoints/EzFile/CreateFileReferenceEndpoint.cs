using System;
using EzArchive.Application.UseCases.Files;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzFile;

[ApiController]
public class CreateFileReferenceEndpoint : ControllerBase
{
	private readonly IAuthenticatedResultTaskExecutor taskExecutor;
	private readonly ICreateFileReferenceUseCase useCase;

	public CreateFileReferenceEndpoint(IAuthenticatedResultTaskExecutor taskExecutor,
		ICreateFileReferenceUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzFile" },
		Description = "Create a file reference")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<IdentifierResult>))]
	[ApiVersion("6")]
	[HttpPost,
	 Route("v{version:apiVersion}/EzFile/Create"),
	 Route("EzFile/Create")]
	public IActionResult Invoke(
		[FromForm(Name = "sessionGUID")] Guid sessionGuid,
		[FromForm] Guid assetId,
		[FromForm] string fileName,
		[FromForm] string originalFileName,
		[FromForm] string folderPath,
		[FromForm] uint formatId,
		[FromForm] string parentId = null)
	{
		return taskExecutor.Execute(
			new SessionId(sessionGuid),
			session => useCase.Invoke(
				session,
				new AssetId(assetId),
				fileName,
				originalFileName,
				folderPath,
				formatId,
				string.IsNullOrWhiteSpace(parentId) || "null".Equals(parentId, StringComparison.OrdinalIgnoreCase)
					? (uint?)null
					: uint.Parse(parentId)),
			id => Ok(new ResultWrapper<IdentifierResult>(new IdentifierResult(id.Value.ToString()))));
	}
}