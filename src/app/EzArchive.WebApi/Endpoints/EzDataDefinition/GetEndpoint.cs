using System;
using System.Linq;
using EzArchive.Application.UseCases.DataDefinitions;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using DataDefinition = EzArchive.WebApi.Dtos.Results.DataDefinition;

namespace EzArchive.WebApi.Endpoints.EzDataDefinition;

[ApiController]
public class GetEndpoint : ControllerBase
{
	private readonly IAuthenticatedResultTaskExecutor taskExecutor;
	private readonly IGetDataDefinitionUseCase useCase;

	public GetEndpoint(IAuthenticatedResultTaskExecutor taskExecutor, IGetDataDefinitionUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EZDataDefinition" },
		Description = "Get all data definitions")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<DataDefinition[]>))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EzDataDefinition/Get"),
	 Route("EzDataDefinition/Get")]
	public IActionResult Invoke(
		[FromQuery(Name = "sessionGUID")] Guid sessionId)
	{
		return taskExecutor.Execute(
			new SessionId(sessionId),
			session => useCase.Invoke(session),
			defs => Ok(new ResultWrapper<DataDefinition>(defs.Select(d => new DataDefinition(d)))));
	}
}