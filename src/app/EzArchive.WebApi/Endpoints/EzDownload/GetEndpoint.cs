using System;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Files;
using EzArchive.Domain;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzDownload;

[ApiController]
public class GetEndpoint : ControllerBase
{
	private readonly IAuthenticatedAsyncResultTaskExecutor taskExecutor;
	private readonly IAnonymousTaskExecutor anonTaskExecutor;
	private readonly IGetFileStreamUseCase useCase;

	public GetEndpoint(
		IAuthenticatedAsyncResultTaskExecutor taskExecutor,
		IAnonymousTaskExecutor anonTaskExecutor,
		IGetFileStreamUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.anonTaskExecutor = anonTaskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzDownload" },
		Description = "Download a file by it's ID")]
	[SwaggerResponse(200, Description = "File stream")]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EzDownload/Get"),
	 Route("EzDownload/Get")]
	public Task<IActionResult> Invoke(
		[FromQuery(Name = "sessionGUID")] Guid? sessionId,
		[FromQuery] uint fileId)
	{
		if (sessionId.HasValue)
			return taskExecutor.ExecuteAsync(
				new SessionId(sessionId.Value),
				session => useCase.InvokeAsync(session, new FileId(fileId)),
				r => new FileStreamResult(r.stream, r.contentType));
		else
			return anonTaskExecutor.ExecuteAsync(
				() => useCase.InvokeAsync(new FileId(fileId)),
				r => new FileStreamResult(r.stream, r.contentType));
	}
}