using System.Linq;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Annotations;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzAnnotationDefinition;

[ApiController]
public class GetEndpoint : ControllerBase
{
	private readonly IAnonymousTaskExecutor taskExecutor;
	private readonly IGetAnnotationDefinitionsUseCase useCase;

	public GetEndpoint(IAnonymousTaskExecutor taskExecutor, IGetAnnotationDefinitionsUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzAnnotationDefinition" },
		Description = "Get a list of all annotation definitions")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<DataDefinition[]>))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EzAnnotationDefinition/Get"),
	 Route("EzAnnotationDefinition/Get")]
	public Task<IActionResult> InvokeAsync()
	{
		return taskExecutor.ExecuteAsync(
			() => useCase.InvokeAsync(),
			definitions => Ok(new ResultWrapper<DataDefinition>(definitions.Select(d => new DataDefinition(d)))));
	}
}