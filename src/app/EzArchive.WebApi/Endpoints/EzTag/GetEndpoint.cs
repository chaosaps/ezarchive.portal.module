using System;
using System.Linq;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Tags;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.EzTag;

[ApiController]
public class GetEndpoint : ControllerBase
{
	private readonly IAuthenticatedAsyncResultTaskExecutor taskExecutor;
	private readonly IGetTagsUseCase useCase;

	public GetEndpoint(IAuthenticatedAsyncResultTaskExecutor taskExecutor, IGetTagsUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Tags = new[] { "EzTag" },
		Description = "Query the tags in use")]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<FacetResponse.Facet[]>))]
	[ApiVersion("6")]
	[HttpGet,
	 Route("v{version:apiVersion}/EzTag/Get"),
	 Route("EzTag/Get")]
	public Task<IActionResult> Invoke(
		[FromQuery(Name = "sessionGUID")] Guid sessionId, [FromQuery] string q)
	{
		return taskExecutor.ExecuteAsync(new SessionId(sessionId),
			session => useCase.InvokeAsync(session, q),
			tags => Ok(
				new ResultWrapper<FacetResponse.Facet>(tags.Select(t => new FacetResponse.Facet(t.Key, t.Count)))));
	}
}