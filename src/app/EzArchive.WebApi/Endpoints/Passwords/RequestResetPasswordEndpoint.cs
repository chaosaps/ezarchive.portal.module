using System.Threading.Tasks;
using EzArchive.Application.UseCases.Passwords;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Inputs;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.Passwords
{
	[ApiController]
	public class RequestResetPasswordEndpoint : ControllerBase
	{
		private readonly IAnonymousTaskExecutor taskExecutor;
		private readonly IRequestResetPasswordUseCase useCase;

		public RequestResetPasswordEndpoint(IAnonymousTaskExecutor taskExecutor, IRequestResetPasswordUseCase useCase)
		{
			this.taskExecutor = taskExecutor;
			this.useCase = useCase;
		}

		[SwaggerOperation(Tags = new[] {"Passwords"})]
		[SwaggerResponse(200,
			Description = "Reset password email was sent to the user", Type = typeof(ResultWrapper<EndpointResult>))]
		[SwaggerResponse(400, "Invalid input", typeof(ResultWrapper<EndpointResult>))]
		[SwaggerResponse(401, "Unauthorized", typeof(ResultWrapper<EndpointResult>))]
		[SwaggerResponse(500, "Internal server error", typeof(ResultWrapper<EndpointResult>))]
		[ApiVersion("6")]
		[HttpPost,
		 Route("v{version:apiVersion}/Passwords/RequestResetPassword"),
		 Route("Passwords/RequestResetPassword")]
		public Task<IActionResult> Invoke([FromBody] RequestResetPassword requestResetPassword)
		{
			if (requestResetPassword.IsValid() == false)
				return Task.FromResult<IActionResult>(
					BadRequest(new ResultWrapper(ErrorCode.InvalidInput, "Email is missing")));

			return taskExecutor.ExecuteAsync(
				() => useCase.InvokeAsync(new EmailAddress(requestResetPassword.EmailAddress)),
				() => Ok(new ResultWrapper<EndpointResult>(EndpointResult.Success())));
		}
	}
}