using System.Threading.Tasks;
using EzArchive.Application.UseCases.Passwords;
using EzArchive.Domain;
using EzArchive.Domain.Tickets;
using EzArchive.WebApi.Dtos.Inputs;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.Passwords
{
	[ApiController]
	public class UpdateEndpoint : ControllerBase
	{
		private readonly IAnonymousTaskExecutor taskExecutor;
		private readonly IUpdatePasswordUseCase useCase;

		public UpdateEndpoint(IAnonymousTaskExecutor taskExecutor, IUpdatePasswordUseCase useCase)
		{
			this.taskExecutor = taskExecutor;
			this.useCase = useCase;
		}

		[SwaggerOperation(Tags = new[] {"Passwords"})]
		[SwaggerResponse(200, Description = "Reset password authorized by ticket",
			Type = typeof(ResultWrapper<EndpointResult>))]
		[SwaggerResponse(400, "Invalid input", typeof(ResultWrapper<EndpointResult>))]
		[SwaggerResponse(401, "Unauthorized", typeof(ResultWrapper<EndpointResult>))]
		[SwaggerResponse(500, "Internal server error", typeof(ResultWrapper<EndpointResult>))]
		[ApiVersion("6")]
		[HttpPut,
		 Route("v{version:apiVersion}/Passwords"),
		 Route("Passwords")]
		public Task<IActionResult> InvokeAsync([FromBody] ResetPassword resetPassword)
		{
			var password = new Password(resetPassword.Password);

			if (password.ConformsToPasswordPolicy() == false)
				return Task.FromResult<IActionResult>(
					BadRequest(new ResultWrapper(ErrorCode.InvalidInput, "Password does not conform to policy")));

			return taskExecutor.ExecuteAsync(
				() => useCase.InvokeAsync(new TicketId(resetPassword.TicketId), password),
				() => Ok(new ResultWrapper<EndpointResult>(EndpointResult.Success())));
		}
	}
}