using System;
using EzArchive.Application.UseCases.AuthKeys.Create;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace EzArchive.WebApi.Endpoints.AuthKeys;

[ApiController]
public class CreateAuthKeyEndpoint : ControllerBase
{
	private readonly IAuthenticatedResultTaskExecutor taskExecutor;
	private readonly ICreateAuthKeyUseCase useCase;

	public CreateAuthKeyEndpoint(IAuthenticatedResultTaskExecutor taskExecutor, ICreateAuthKeyUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Description = "Creates a token that can for authenticating sessions",
		Tags = new[] { "Authentication" })]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<Dtos.Results.AuthKey>))]
	[SwaggerResponse(500, "Internal server error", typeof(ResultWrapper))]
	[HttpPost, Route("AuthKey/Create"), Route("v{version:apiVersion}/AuthKey/Create")]
	[ApiVersion("6")]
	public IActionResult Create([FromForm(Name = "sessionGUID")] Guid sessionId, [FromForm] string name) =>
		taskExecutor.Execute(
			new SessionId(sessionId),
			session => useCase.Invoke(session.UserId, name),
			authKey => Ok(new ResultWrapper<Dtos.Results.AuthKey>(new Dtos.Results.AuthKey(authKey))));
}