using EzArchive.Application.UseCases.AuthKeys.Login;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using Session = EzArchive.WebApi.Dtos.Results.Session;

namespace EzArchive.WebApi.Endpoints.AuthKeys;

[ApiController]
public class LoginAuthKeyEndpoint : ControllerBase
{
	private readonly IAnonymousTaskExecutor taskExecutor;
	private readonly ILoginWithAuthKeyUseCase useCase;

	public LoginAuthKeyEndpoint(IAnonymousTaskExecutor taskExecutor, ILoginWithAuthKeyUseCase useCase)
	{
		this.taskExecutor = taskExecutor;
		this.useCase = useCase;
	}

	[SwaggerOperation(
		Description = "Authenticates session using token",
		Tags = new[] { "Authentication" })]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<Session>))]
	[SwaggerResponse(500, "Internal server error", typeof(ResultWrapper))]
	[HttpGet, Route("AuthKey/Login"), Route("v{version:apiVersion}/AuthKey/Login")]
	[ApiVersion("6")]
	public IActionResult Login([FromQuery] string token) =>
		taskExecutor.Execute(
			() => useCase.Invoke(AuthToken.LevelOne.CreateWithoutHashing(token)),
			session => Ok(new ResultWrapper<Session>(Session.FromDomain(session))));

	[SwaggerOperation(
		Description = "Authenticates session using token",
		Tags = new[] { "Authentication" })]
	[SwaggerResponse(200, Type = typeof(ResultWrapper<Session>))]
	[SwaggerResponse(500, "Internal server error", typeof(ResultWrapper))]
	[HttpPost, Route("AuthKey/Login"), Route("v{version:apiVersion}/AuthKey/Login")]
	[ApiVersion("6")]
	public IActionResult LoginPost([FromForm] string token) =>
		Login(token);
}