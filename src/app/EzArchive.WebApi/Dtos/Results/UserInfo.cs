using System;
using EzArchive.Domain;

namespace EzArchive.WebApi.Dtos.Results;

public class UserInfo
{
	public Guid Guid { get; }
	public uint SystemPermissions { get; }
	public string Email { get; }
	public long? SessionDateCreated { get; }
	public long? SessionDateModified { get; }
	public string FullName => "Chaos.Portal.Core.Data.Model.UserInfo";

	private UserInfo(
		Guid guid,
		uint systemPermissions,
		string email,
		long? sessionDateCreated,
		long? sessionDateModified)
	{
		Guid = guid;
		SystemPermissions = systemPermissions;
		Email = email;
		SessionDateCreated = sessionDateCreated;
		SessionDateModified = sessionDateModified;
	}

	public static UserInfo FromDomain(Domain.Session session, SessionUser sessionUser)
	{
		return new UserInfo(
			session.Id.Value,
			sessionUser.SystemPermissions,
			sessionUser.UserName.Value,
			session.CreatedOn.ToUnixTimeMilliSeconds(),
			session.ModifiedOn.ToUnixTimeMilliSeconds()
		);
	}

	public static UserInfo FromDomain(Chaos.Portal.Core.Data.Model.UserInfo user)
	{
		return new UserInfo(
			user.SessionGuid.Value,
			user.SystemPermissions ?? 0,
			user.Email,
			user.SessionDateCreated.Value.ToUnixTimeMilliSeconds(),
			user.SessionDateModified.Value.ToUnixTimeMilliSeconds());
	}
}