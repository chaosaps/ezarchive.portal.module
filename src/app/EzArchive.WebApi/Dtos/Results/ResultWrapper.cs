using System;
using System.Collections.Generic;
using System.Linq;
using EzArchive.Domain;

namespace EzArchive.WebApi.Dtos.Results;

public class ResultWrapper
{
	public Header Header { get; } = new Header();
	public Body Body { get; }
	public Error Error { get; }

	public ResultWrapper(Exception e) =>
		Error = new Error(e);

	public ResultWrapper(ErrorCode errorCode, string message) =>
		Error = new Error(errorCode, message);
}

public class Body
{
}

public class ResultWrapper<T>
{
	public Header Header { get; } = new Header();
	public Body<T> Body { get; }
	public Error Error { get; } = new Error();

	public ResultWrapper(IEnumerable<T> results) =>
		Body = new Body<T>(results);

	public ResultWrapper(T result) =>
		Body = new Body<T>(result);

	public ResultWrapper(PagedResult<T> results) =>
		Body = new Body<T>(results);
}

public class Body<T>
{
	public uint Count { get; }
	public uint TotalCount { get; }
	public IList<T> Results { get; }

	public Body(IEnumerable<T> results)
	{
		Results = results.ToList();
		Count = (uint)Results.Count;
		TotalCount = (uint)Results.Count;
	}

	public Body(T result)
	{
		Results = new List<T> { result };
		Count = (uint)Results.Count;
		TotalCount = (uint)Results.Count;
	}

	public Body(PagedResult<T> paged)
	{
		Results = paged.Results.ToList();
		Count = paged.Count;
		TotalCount = paged.FoundCount;
	}
}

public class PagedResult<T>
{
	public uint Count => (uint)Results.Count();
	public uint FoundCount { get; }
	public uint StartIndex { get; }
	public IEnumerable<T> Results { get; }

	public PagedResult(uint foundCount, uint startIndex, IEnumerable<T> results)
	{
		FoundCount = foundCount;
		StartIndex = startIndex;
		Results = results;
	}
}

public class Error
{
	public string Fullname { get; }
	public uint Code { get; }
	public string Message { get; }
	public Error InnerException { get; }

	public Error()
	{
		Fullname = null;
		Message = null;
	}

	public Error(Exception e)
	{
		Fullname = e.GetType().FullName;
		Message = e.Message;
		InnerException = e.InnerException != null ? new Error(e.InnerException) : null;
	}

	public Error(ErrorCode errorCode, string message)
	{
		Fullname = null;
		Code = (uint)errorCode;
		Message = message;
	}
}

public class Header
{
	public double Duration => 0.0;
}