namespace EzArchive.WebApi.Dtos.Results;

public class SearchUser
{
	public SearchUser(string identifier, string name)
	{
		Identifier = identifier;
		Name = name;
	}

	public string Identifier { get; }
	public string Name { get; }
}