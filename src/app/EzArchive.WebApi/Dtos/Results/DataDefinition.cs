using System.Collections.Generic;
using System.Linq;
using EzArchive.Application.UseCases.DataDefinitions;

namespace EzArchive.WebApi.Dtos.Results;

public class DataDefinition
{
	public DataDefinition(
		string identifier,
		string name,
		string type,
		string typeId,
		bool isEditable,
		string description,
		IDictionary<string, FieldDefinition> fields)
	{
		Identifier = identifier;
		Name = name;
		Type = type;
		TypeId = typeId;
		IsEditable = isEditable;
		Description = description;
		Fields = fields;
	}

	public DataDefinition(DataDefinitionUserResponse definition)
	{
		Identifier = definition.Identifier;
		Name = definition.Name;
		Type = definition.Type.ToString();
		TypeId = definition.TypeId.ToString();
		Description = definition.Description;
		Fields = definition.Fields.ToDictionary(field => field.Id, fd => new FieldDefinition(fd));
		IsEditable = definition.IsEditable;
	}

	public string Identifier { get; }
	public string Name { get; }
	public string Type { get; }
	public string TypeId { get; }
	public bool IsEditable { get; }
	public string Description { get; }
	public IDictionary<string, FieldDefinition> Fields { get; }

	public class FieldDefinition
	{
		protected FieldDefinition(string displayName, string type, string[] options, bool isRequired,
			string placeholder)
		{
			DisplayName = displayName;
			Type = type;
			Options = options;
			IsRequired = isRequired;
			Placeholder = placeholder;
		}

		public FieldDefinition(Domain.FieldDefinition fieldDefinition) : this(fieldDefinition.DisplayName,
			fieldDefinition.Type,
			fieldDefinition.Options,
			fieldDefinition.IsRequired,
			fieldDefinition.Placeholder)
		{
		}

		public string DisplayName { get; }
		public string Type { get; }
		public string[] Options { get; }
		public bool IsRequired { get; }
		public string Placeholder { get; }
	}
}