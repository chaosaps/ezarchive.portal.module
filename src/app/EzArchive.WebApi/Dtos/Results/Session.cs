using System;

namespace EzArchive.WebApi.Dtos.Results;

public class Session
{
	public Guid Guid { get; }
	public Guid UserGuid { get; }
	public long DateCreated { get; }
	public long DateModified { get; }
	public string FullName => "Chaos.Portal.Core.Data.Model.Session";

	private Session(Guid guid, Guid userGuid, long dateCreated, long dateModified)
	{
		Guid = guid;
		UserGuid = userGuid;
		DateCreated = dateCreated;
		DateModified = dateModified;
	}

	public static Session FromDomain(Domain.Session result) =>
		new Session(
			result.Id.Value,
			result.UserId.Value,
			result.CreatedOn.ToUnixTimeMilliSeconds(),
			result.ModifiedOn.ToUnixTimeMilliSeconds());
}