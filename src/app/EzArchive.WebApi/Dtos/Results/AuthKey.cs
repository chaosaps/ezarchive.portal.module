using System;
using EzArchive.Application.UseCases.AuthKeys.Create;

namespace EzArchive.WebApi.Dtos.Results;

public class AuthKey
{
	public string Token { get; }
	public string Name { get; }
	public Guid UserGuid { get; }

	public AuthKey(string token, string name, Guid userGuid)
	{
		Token = token;
		Name = name;
		UserGuid = userGuid;
	}

	public AuthKey(CreateAuthKeyRequest authKey)
	{
		Token = authKey.Token.Value;
		Name = authKey.Name;
		UserGuid = authKey.UserId.Value;
	}
}