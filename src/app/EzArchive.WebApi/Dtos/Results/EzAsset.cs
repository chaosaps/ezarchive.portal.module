using System;
using System.Collections.Generic;
using System.Linq;
using EzArchive.Domain;

namespace EzArchive.WebApi.Dtos.Results;

public class EzAsset
{
	public EzAsset(Asset asset, SessionId sessionId)
	{
		Identifier = asset.Id.Value.ToString();
		TypeId = asset.Type.Value.ToString();
		DoFilesRequireLogin = asset.DoFilesRequireLogin;
		Files = asset.Files.Select(f => new _FileReference(f)).ToList();
		Annotations = asset.AnnotationsGroups.Select(ag => new _AnnotationGroup(ag)).ToList();
		Tags = asset.Tags;
		Data = asset.Data.Select(d => new _Data(d)).ToList();
	}

	public string Identifier { get; }
	public string TypeId { get; }
	public bool DoFilesRequireLogin { get; }
	public IReadOnlyList<_FileReference> Files { get; }
	public IReadOnlyList<_AnnotationGroup> Annotations { get; }
	public IReadOnlyList<string> Tags { get; }
	public IReadOnlyList<_Data> Data { get; }

	public string FullName { get; } = "EZArchive.Portal.Module.Api.Results.AssetResult";

	public class _Data
	{
		public _Data(Asset._Data data)
		{
			Name = data.Name;
			Fields = data.Fields.ToDictionary(kv => kv.Key, kv => kv.Value.ToString());
		}

		public string Name { get; }
		public IDictionary<string, string> Fields { get; }
	}

	public class _AnnotationGroup
	{
		public _AnnotationGroup(Asset._AnnotationGroup annotationGroup)
		{
			DefinitionId = annotationGroup.DefinitionId;
			Name = annotationGroup.Name;
			Data = annotationGroup.Annotations
				.Select(ag => ag.ToDictionary(
					a => a.Key,
					a => a.Value.ToString()))
				.ToList();
		}

		public Guid DefinitionId { get; }
		public string Name { get; }
		public IList<Dictionary<string, string>> Data { get; }
	}

	public class _FileReference
	{
		public _FileReference(Asset._FileReference fileReference)
		{
			Identifier = fileReference.Id;
			Name = fileReference.Name;
			Type = fileReference.Type;
			Destinations = fileReference.Destinations.Select(d => new _Destination(d)).ToList();
		}

		public string Identifier { get; }
		public string Name { get; }
		public string Type { get; }
		public IList<_Destination> Destinations { get; }

		public class _Destination
		{
			public _Destination(Asset._FileReference._Destination destination)
			{
				Type = destination.Type;
				Url = destination.Url;
			}

			public string Type { get; }
			public string Url { get; }
		}
	}
}