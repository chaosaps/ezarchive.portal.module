namespace EzArchive.WebApi.Dtos.Results
{
	public record Format(uint Id, string Extension);
}