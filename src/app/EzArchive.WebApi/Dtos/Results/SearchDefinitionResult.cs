using System.Collections.Generic;
using System.Linq;
using EzArchive.Domain;

namespace EzArchive.WebApi.Dtos.Results;

public class SearchDefinitionResult
{
	public IEnumerable<SearchFieldDefinitionResult> Fields { get; }
	public string FullName = "EZArchive.Portal.Module.Api.Results.SearchDefinitionResult";

	public SearchDefinitionResult(IEnumerable<SearchFieldDefinitionResult> fields)
	{
		Fields = fields;
	}

	public SearchDefinitionResult(IReadOnlyList<SearchFieldDefinition> definitions)
		: this(definitions.Select(d => new SearchFieldDefinitionResult(d)))
	{
	}

	public class SearchFieldDefinitionResult
	{
		public SearchFieldDefinitionResult(string displayName, bool isSortable, string type)
		{
			DisplayName = displayName;
			IsSortable = isSortable;
			Type = type;
		}

		public SearchFieldDefinitionResult(SearchFieldDefinition definition)
			: this(definition.DisplayName, definition.IsSortable, definition.Type)
		{
		}

		public string DisplayName { get; }
		public bool IsSortable { get; }
		public string Type { get; }
	}
}