using EzArchive.Application;

namespace EzArchive.WebApi.Dtos.Results;

public class EZSettings
{
	public string DisplayNotificationsLevel { get; }
	public string LoginLevel { get; }
	public string ArchiveName { get; }
	public uint[] PageSizes { get; }
	public uint MaxNumberOfPagesShown { get; }
	public string[] AssetTitleKeyWords { get; }
	public string EmbedMode { get; }
	public uint MinimumFacetHitsForEmptySearch { get; }
	public string FullName { get; } = "EZArchive.Portal.Module.Api.Endpoints.SettingsResult";

	public EZSettings(
		string displayNotificationsLevel,
		string loginLevel,
		string archiveName,
		uint[] pageSizes,
		uint maxNumberOfPagesShown,
		string[] assetTitleKeyWords,
		string embedMode,
		uint minimumFacetHitsForEmptySearch)
	{
		DisplayNotificationsLevel = displayNotificationsLevel;
		LoginLevel = loginLevel;
		ArchiveName = archiveName;
		PageSizes = pageSizes;
		MaxNumberOfPagesShown = maxNumberOfPagesShown;
		AssetTitleKeyWords = assetTitleKeyWords;
		EmbedMode = embedMode;
		MinimumFacetHitsForEmptySearch = minimumFacetHitsForEmptySearch;
	}


	public static EZSettings From(EzaConfig config)
	{
		return new EZSettings(
			config.DisplayNotificationsLevel,
			config.LoginLevel,
			config.ArchiveName,
			config.PageSizes,
			config.MaxNumberOfPagesShown,
			config.AssetTitleKeyWords,
			config.EmbedMode,
			config.MinimumFacetHitsForEmptySearch);
	}
}