using System;

namespace EzArchive.WebApi.Dtos.Results;

public class SecureCookie
{
	public Guid Guid { get; }
	public Guid PasswordGuid { get; }
	public string FullName => "Chaos.Portal.Authentication.Data.Dto.v6.SecureCookie";

	public SecureCookie(Guid guid, Guid passwordGuid)
	{
		Guid = guid;
		PasswordGuid = passwordGuid;
	}

	public static SecureCookie FromDomain(Domain.SecureCookie result) =>
		new SecureCookie(result.Id.Value, result.PasswordGuid.Value);
}