namespace EzArchive.WebApi.Dtos.Results;

public class EndpointResult
{
	public bool WasSuccess { get; private set; }

	public string FullName => "Chaos.Portal.v5.Extension.Result.EndpointResult";

	public static EndpointResult Success()
	{
		return new EndpointResult { WasSuccess = true };
	}

	public static EndpointResult Failed()
	{
		return new EndpointResult { WasSuccess = false };
	}
}