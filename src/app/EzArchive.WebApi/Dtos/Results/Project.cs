using System.Collections.Generic;
using System.Linq;

namespace EzArchive.WebApi.Dtos.Results;

public class Project
{
	public Project(Domain.Project project)
	{
		Identifier = project.Id.Value.ToString();
		Name = project.Name;
		Labels = project.Labels.Select(l => new EzLabel(l)).ToList();
		Users = project.Users.Select(u => EzUser.FromDomain(u)).ToList();
	}

	public string Identifier { get; }
	public string Name { get; }
	public IReadOnlyList<EzUser> Users { get; }
	public IReadOnlyList<EzLabel> Labels { get; }
	public string FullName => "EZArchive.Portal.Module.Api.Results.ProjectResult";
}