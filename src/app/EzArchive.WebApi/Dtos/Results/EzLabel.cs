namespace EzArchive.WebApi.Dtos.Results;

public class EzLabel
{
	public EzLabel(string identifier, string name)
	{
		Identifier = identifier;
		Name = name;
	}

	public EzLabel(Domain.Label label) : this(label.Id.Value.ToString(), label.Name)
	{
	}

	public string Identifier { get; }
	public string Name { get; }

	public string FullName => "EZArchive.Portal.Module.Api.Results.LabelResult";
}