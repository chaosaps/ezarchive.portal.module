namespace EzArchive.WebApi.Dtos.Results;

public class IdentifierResult
{
	public IdentifierResult(string id)
	{
		Identifier = id;
	}

	public string Identifier { get; }
}