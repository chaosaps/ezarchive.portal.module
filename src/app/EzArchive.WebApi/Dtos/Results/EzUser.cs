using EzArchive.Domain;

namespace EzArchive.WebApi.Dtos.Results;

public class EzUser
{
	public string Identifier { get; }
	public string Email { get; }
	public string Name { get; }
	public string Permission { get; }
	public bool IsAdministrator { get; }
	public string Preferences { get; }

	public string FullName => "EZArchive.Portal.Module.Api.Results.EzUserResult";

	private EzUser(
		string identifier,
		string email,
		string name,
		string permission,
		bool isAdministrator,
		string preferences)
	{
		Identifier = identifier;
		Email = email;
		Name = name;
		Permission = permission;
		IsAdministrator = isAdministrator;
		Preferences = preferences;
	}

	public static EzUser FromDomain(User user) =>
		new EzUser(
			user.Id.Value.ToString(),
			user.Email,
			user.Name,
			user.Permission,
			user.IsAdministrator,
			user.Preferences);
}