using System.Collections.Generic;

namespace EzArchive.WebApi.Dtos.Results;

public class SearchResult
{
	public string Identifier { get; }
	public string TypeId { get; }
	public IEnumerable<KeyValueResult> Fields { get; }

	public SearchResult(string identifier, string typeId, IEnumerable<KeyValueResult> fields)
	{
		Identifier = identifier;
		TypeId = typeId;
		Fields = fields;
	}

	public class KeyValueResult
	{
		public string Key { get; }
		public string Value { get; }

		public KeyValueResult(string key, string value)
		{
			Key = key;
			Value = value;
		}
	}
}