﻿namespace EzArchive.WebApi.Dtos.Inputs;

public class BetweenDates
{
	public BetweenDates(string from, string to)
	{
		From = from;
		To = to;
	}

	public string From { get; }
	public string To { get; }
}