namespace EzArchive.WebApi.Dtos.Inputs;

public class AnnotationVisibility
{
	public AnnotationVisibility()
	{
	}

	public AnnotationVisibility(string scope)
	{
		Scope = scope;
	}

	public string Scope { get; }

	public Domain.AnnotationVisibility ToDomain()
	{
		return Scope == Domain.AnnotationVisibility.Private.Scope
			? Domain.AnnotationVisibility.Private
			: Domain.AnnotationVisibility.Public;
	}

	public static Domain.AnnotationVisibility ToDomain(AnnotationVisibility visibility)
	{
		return visibility != null && visibility.Scope == Domain.AnnotationVisibility.Private.Scope
			? Domain.AnnotationVisibility.Private
			: Domain.AnnotationVisibility.Public;
	}
}