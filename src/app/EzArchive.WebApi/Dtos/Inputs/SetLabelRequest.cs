using Newtonsoft.Json;

namespace EzArchive.WebApi.Dtos.Inputs;

public class SetLabelRequest
{
	public SetLabelRequest()
	{
	}

	[JsonConstructor]
	public SetLabelRequest(string identifier, string name)
	{
		Identifier = identifier;
		Name = name;
	}

	public string Identifier { get; }
	public string Name { get; }
}