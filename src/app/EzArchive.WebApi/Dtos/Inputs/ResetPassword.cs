using System;

namespace EzArchive.WebApi.Dtos.Inputs
{
	public class ResetPassword
	{
		public Guid TicketId { get; }
		public string Password { get; }

		public ResetPassword(Guid ticketId, string password)
		{
			TicketId = ticketId;
			Password = password;
		}
	}
}