using Newtonsoft.Json;

namespace EzArchive.WebApi.Dtos.Inputs;

public class SetUserRequest
{
	public SetUserRequest()
	{
	}

	[JsonConstructor]
	public SetUserRequest(
		string identifier,
		string email,
		string name,
		string permission,
		string preferences)
	{
		Identifier = identifier;
		Email = email;
		Name = name;
		Permission = permission;
		Preferences = preferences;
	}

	public string Identifier { get; }
	public string Email { get; }
	public string Name { get; }
	public string Permission { get; }
	public string Preferences { get; }
}