using Newtonsoft.Json;

namespace EzArchive.WebApi.Dtos.Inputs;

public class SetProjectRequest
{
	public SetProjectRequest()
	{
	}

	[JsonConstructor]
	public SetProjectRequest(string identifier, string name)
	{
		Identifier = identifier;
		Name = name;
	}

	public string Identifier { get; }
	public string Name { get; }
}