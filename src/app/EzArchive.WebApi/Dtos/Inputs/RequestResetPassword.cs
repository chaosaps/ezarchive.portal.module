namespace EzArchive.WebApi.Dtos.Inputs
{
	public class RequestResetPassword
	{
		public string EmailAddress { get; }
		
		public RequestResetPassword(string emailAddress)
		{
			EmailAddress = emailAddress;
		}

		public bool IsValid()
		{
			return !string.IsNullOrWhiteSpace(EmailAddress);
		}
	}
}