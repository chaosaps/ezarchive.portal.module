using System;

namespace EzArchive.WebApi.Dtos;

public static class DateTimeExtensions
{
	private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

	public static long ToUnixTimeMilliSeconds(this DateTime dt) =>
		(long)(dt - Epoch).TotalMilliseconds;

	public static DateTime FromUnixTimeMillisecondsToDateTime(this long l) =>
		new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddMilliseconds(l);
}