﻿using System.Collections.Generic;
using EzArchive.Synchronizer.Core.Model;

namespace EzArchive.Synchronizer.Data
{
  public interface ISearchRepository
  {
    Page<Search> All();
    Page<Search> Query(SearchQuery q, SearchQuery filter, string tag, IEnumerable<string> facets, uint pageIndex, uint pageSize, string sort = "");
  }
}