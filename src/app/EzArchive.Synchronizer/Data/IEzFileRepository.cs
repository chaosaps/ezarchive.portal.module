using System;
using System.Collections.Generic;
using EzArchive.Synchronizer.Core.Model;

namespace EzArchive.Synchronizer.Data
{
  public interface IEzFileRepository
  {
    uint Set(Guid assetId, uint? parentId, string fileName, string originalFilename, string folderpath, uint formatId, uint id = 0);
    bool Delete(uint id);
    IEnumerable<Asset.FileReference.Destination> GetIncludingChildren(uint id);
	  uint Set(uint id, string originalFilename);
  }
}