using System.Collections.Generic;

namespace EzArchive.Synchronizer.Data
{
  public interface IDefinitionRepository
  {
    IEnumerable<Core.Model.DataDefinition> GetAnnotations();
    IEnumerable<Core.Model.DataDefinition> GetDataDefinitions();

    bool IsTypeValid(uint typeId);
    Core.Model.DataDefinition GetDataDefinition(string id);
    void SetDataDefinition(Core.Model.DataDefinition definition);
  }
}