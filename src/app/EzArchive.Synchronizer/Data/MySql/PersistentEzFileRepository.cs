﻿using System;
using System.Collections.Generic;
using Chaos.Mcm.Core.Model;
using Chaos.Mcm.Data;
using EzArchive.Synchronizer.Core;
using EzArchive.Synchronizer.Core.Model;

namespace EzArchive.Synchronizer.Data.MySql
{
  public class PersistentEzFileRepository : IEzFileRepository
  {
	  private readonly ContextNonStatic _context;
	  private IMcmRepository Mcm { get; set; }

    public PersistentEzFileRepository(IMcmRepository mcm, ContextNonStatic context)
    {
	    _context = context;
	    Mcm = mcm;
    }

    public uint Set(Guid assetId, uint? parentId, string filename, string originalFilename, string folderpath, uint formatId, uint id = 0)
    {
	    var file = new File
	    {
		    ObjectGuid = assetId,
		    ParentID = parentId,
		    Filename = filename,
		    OriginalFilename = originalFilename,
		    FolderPath = folderpath,
		    FormatID = formatId,
		    Id = id,
				DestinationID = _context.Config.Mcm.UploadDestinationId
	    };

	    return Mcm.File.Set(file);
    }

		public uint Set(uint id, string originalFilename)
		{
			var file = Mcm.File.Get(id);
			file.OriginalFilename = originalFilename;

			return Mcm.File.Set(file);
		}

		public bool Delete(uint id)
    {
      return 0 < Mcm.FileDelete(id);
    }

    public IEnumerable<Asset.FileReference.Destination> GetIncludingChildren(uint id)
    {
      var parent = Mcm.File.Get(id);
      yield return new Asset.FileReference.Destination()
        {
          Url = parent.FolderPath + "/" + parent.Filename
        };

      foreach (var file in Mcm.File.Get(parentId: id))
      {
        yield return new Asset.FileReference.Destination()
        {
          Url = file.FolderPath + "/" + file.Filename
        };
      }
      
    }
  }
}