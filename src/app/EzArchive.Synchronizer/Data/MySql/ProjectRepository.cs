﻿using System;
using System.Collections.Generic;
using System.Linq;
using Chaos.Mcm.Data;
using EzUser = EzArchive.Synchronizer.Core.Model.EzUser;
using Label = Chaos.Mcm.Core.Model.Label;
using Project = Chaos.Mcm.Core.Model.Project;

namespace EzArchive.Synchronizer.Data.MySql
{
	class ProjectRepository : IProjectRepository
	{
		public IMcmRepository Mcm { get; set; }
		public IEzUserRepository UserRepository { get; set; }

		public ProjectRepository(IMcmRepository mcm, IEzUserRepository userRepository)
		{
			Mcm = mcm;
			UserRepository = userRepository;
		}

		public Core.Model.Project Set(Core.Model.Project project)
		{
			var result = Mcm.Project.Set(new Project
				{
					Identifier = project.Identifier == null ? (uint?) null : uint.Parse(project.Identifier),
					Name = project.Name
				});

			return new Core.Model.Project
				{
					Identifier = result.Identifier.ToString(),
					Name = result.Name
				};
		}

		public IEnumerable<Core.Model.Project> Get(string id = null, string userId = null, string labelId = null)
		{
			var results = Mcm.Project.Get(id == null ? (uint?) null : uint.Parse(id),
			                              userId == null ? (Guid?) null : Guid.Parse(userId),
			                              labelId == null ? (uint?) null : uint.Parse(labelId));

			foreach (var project in results)
			{
				var labels = Mcm.Label.Get(project.Identifier.Value);
				var users = GetUsers(project);

				yield return new Core.Model.Project
				{
					Identifier = project.Identifier.ToString(),
					Name = project.Name,
					Users = users.ToList(),
					Labels = labels.Select(l => new Core.Model.Label
						{
							Identifier = l.Identifier.ToString(),
							Name = l.Name
						}).ToList()
				};
			}
		}

		private IEnumerable<EzUser> GetUsers(Project project)
		{
			var lst = new List<EzUser>();

			foreach (var id in project.UserIds)
			{
				try
				{
					lst.Add(UserRepository.Get(id));
				}
				catch (System.Exception)
				{
					// ignored
				}
			}

			return lst;
		}

		public bool AddUser(string id, string userId)
		{
			return Mcm.Project.AddUser(uint.Parse(id), Guid.Parse(userId));
		}

		public bool RemoveUser(string id, string userId)
		{
			return Mcm.Project.RemoveUser(uint.Parse(id), Guid.Parse(userId));
		}

		public Core.Model.Label AssociateLabel(string id, Core.Model.Label label)
		{
			var lbl = new Label
			{
				Identifier = label.Identifier == null ? (uint?) null : uint.Parse(label.Identifier), 
				Name = label.Name
			};

			var associateLabel = Mcm.Label.Set(uint.Parse(id), lbl);
			label.Identifier = associateLabel.Identifier.ToString();

			return label;
		}

		public bool Delete(string id)
		{
			return Mcm.Project.Delete(uint.Parse(id));
		}
	}
}
