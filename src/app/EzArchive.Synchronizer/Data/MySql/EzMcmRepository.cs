﻿using Chaos.Authentication.Data;
using Chaos.Mcm.Data;
using Chaos.Portal.Core.Data;
using Chaos.Portal.Core.Indexing.View;
using EzArchive.Synchronizer.Core;

namespace EzArchive.Synchronizer.Data.MySql
{
  public class EzMcmRepository : IEzArchiveRepository
  {
    public IAssetRepository Asset { get; set; }
    public ISearchRepository Search { get; set; }
    public IEzUserRepository EzUser { get; set; }
    public IEzFileRepository EzFile { get; set; }
    public IDefinitionRepository Definition { get; set; }
	  public IProjectRepository Project { get; set; }
	  public ILabelRepository Label { get; set; }

	  public EzMcmRepository(
		  ContextNonStatic context, 
		  IPortalRepository portal, 
		  IMcmRepository mcm, 
		  IAuthenticationRepository authentication,
		  IViewManager viewManager)
    {
			EzUser = new PersistentUserRepository(context, portal, mcm, authentication, viewManager);
      Search = new PersistentSearchRepository(context, viewManager);
      Asset = new PersistentAssetRepository(context, mcm, viewManager, new Core.CachedEzUserGetter(EzUser));
      EzFile = new PersistentEzFileRepository(mcm, context);
      Definition = new DefinitionRepository(mcm);
			Project = new ProjectRepository(mcm, EzUser);
			Label = new PersistentLabelRepository(mcm);
    }
  }

}