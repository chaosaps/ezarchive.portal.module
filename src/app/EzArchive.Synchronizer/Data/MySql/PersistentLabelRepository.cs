﻿using System;
using System.Collections.Generic;
using System.Linq;
using Chaos.Mcm.Data;

namespace EzArchive.Synchronizer.Data.MySql
{
	public class PersistentLabelRepository : ILabelRepository
	{
		public IMcmRepository Mcm { get; set; }

		public PersistentLabelRepository(IMcmRepository mcm)
		{
			Mcm = mcm;
		}

		public void SetAssociation(string id, string assetId)
		{
			Mcm.Label.AssociationWithObject(uint.Parse(id), Guid.Parse(assetId));
		}

		public void DeleteAssociation(string id, string assetId)
		{
			Mcm.Label.DisassociationWithObject(uint.Parse(id), Guid.Parse(assetId));
		}

		public IEnumerable<Core.Model.Label> Get(string assetId)
		{
			return Mcm.Label.Get(objectId: Guid.Parse(assetId)).Select(l => new Core.Model.Label
				{
					Identifier = l.Identifier.ToString(),
					Name = l.Name,
					ProjectId = l.ProjectId
				});
		}

		public bool Delete(string id)
		{
			return Mcm.Label.Delete(uint.Parse(id));
		}
	}
}
