﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Chaos.Mcm.Core.Model;
using Chaos.Mcm.Data;
using Chaos.Portal.Core.Exceptions;
using Chaos.Portal.Core.Indexing.View;
using EzArchive.Synchronizer.Core;
using EzArchive.Synchronizer.Core.Model;
using Asset = EzArchive.Synchronizer.Core.Model.Asset;
using DateTimeValue = EzArchive.Synchronizer.Core.Model.DateTimeValue;
using Object = Chaos.Mcm.Core.Model.Object;
using StringValue = EzArchive.Synchronizer.Core.Model.StringValue;
using TableValue = EzArchive.Synchronizer.Core.Model.TableValue;
using TextValue = EzArchive.Synchronizer.Core.Model.TextValue;
using Value = EzArchive.Synchronizer.Core.Model.Value;

namespace EzArchive.Synchronizer.Data.MySql
{
    public class PersistentAssetRepository : IAssetRepository
    {
        private readonly ContextNonStatic _context;
        private IMcmRepository Mcm { get; }
        private IViewManager ViewManager { get; }
        private IEzUserGetter EzUserGetter { get; }

        public PersistentAssetRepository(ContextNonStatic context, IMcmRepository mcm, IViewManager viewManager, IEzUserGetter ezUserGetter)
        {
            _context = context;
            Mcm = mcm;
            ViewManager = viewManager;
            EzUserGetter = ezUserGetter;
        }

        public Asset Set(Asset asset)
        {
            return RetrySet(asset, 10);
        }

        private Asset RetrySet(Asset asset, uint retriess = 10)
        {
            try
            {
                if (asset.Identifier == null)
                {
                    var id = Guid.NewGuid();
                    asset.Identifier = id.ToString();
                }

                var objectGuid = Guid.Parse(asset.Identifier);
                var obj = Mcm.ObjectGet(objectGuid,
                    includeMetadata: true,
                    includeFiles: true);

                if (obj == null)
                {
                    Mcm.ObjectCreate(objectGuid, asset.TypeId, _context.Config.Mcm.AssetFolderId);
                    obj = Mcm.ObjectGet(objectGuid,
                        includeMetadata: true,
                        includeFiles: true);
                }

                SaveAssetXml(asset, obj, objectGuid);
                SaveFieldXml(asset, obj, objectGuid);
                SaveAnnotationXml(asset, obj, objectGuid);

                var updated = Get(objectGuid);
                ViewManager.Index(updated);

                return updated;
            }
            catch (System.Exception e)
            {
                if (retriess > 0)
                    RetrySet(asset, --retriess);

                throw;
            }
        }

        private void SaveAssetXml(Asset asset, Object obj, Guid objectGuid)
        {
            var assetMetadata =
                obj.Metadatas.FirstOrDefault(
                    meta => meta.MetadataSchemaGuid == _context.Config.Mcm.AssetMetadataSchemaId);
            var assetMetadataId = assetMetadata == null ? Guid.NewGuid() : assetMetadata.Guid;
            var assetXml = CreateAssetXml(asset);
            var revisionId = assetMetadata == null ? 0 : assetMetadata.RevisionID;
            Mcm.MetadataSet(objectGuid, assetMetadataId, _context.Config.Mcm.AssetMetadataSchemaId, null, revisionId,
                assetXml,
                asset.LastChangedByUserId);
        }

        private void SaveFieldXml(Asset asset, Object obj, Guid objectGuid)
        {
            foreach (var assetData in asset.Data)
            {
                var fieldMetadata =
                    obj.Metadatas.FirstOrDefault(meta => meta.MetadataSchemaGuid ==
                                                         Guid.Parse(assetData.DataDefinition.Id));
                var fieldMetadataId = fieldMetadata == null ? Guid.NewGuid() : fieldMetadata.Guid;
                var fieldXml = CreateFieldXml(assetData);
                var revisionId = fieldMetadata == null ? 0 : fieldMetadata.RevisionID;
                Mcm.MetadataSet(objectGuid, fieldMetadataId, Guid.Parse(assetData.DataDefinition.Id), "da", revisionId,
                    fieldXml,
                    asset.LastChangedByUserId);
            }
        }

        private void SaveAnnotationXml(Asset asset, Object obj, Guid objectGuid)
        {
            foreach (var annotationGroup in asset.Annotations)
            {
                var annotationMetadata =
                    obj.Metadatas.FirstOrDefault(meta => meta.MetadataSchemaGuid == annotationGroup.DefinitionId);
                var annotationMetadataId = annotationMetadata == null ? Guid.NewGuid() : annotationMetadata.Guid;
                var annotationXml = CreateAnnotationXml(annotationGroup);
                var revisionId = annotationMetadata == null ? 0 : annotationMetadata.RevisionID;
                Mcm.MetadataSet(objectGuid, annotationMetadataId, annotationGroup.DefinitionId, null, revisionId,
                    annotationXml,
                    asset.LastChangedByUserId);
            }
        }

        public static XDocument CreateAssetXml(Asset asset)
        {
            var xml = new XDocument();
            var root = new XElement("Chaos.Asset");
            var tags = new XElement("Tags");

            foreach (var tag in asset.Tags)
                tags.Add(new XElement("Tag", tag));

            root.Add(new XElement("DoFilesRequireLogin", asset.DoFilesRequireLogin));
            root.Add(tags);
            xml.Add(root);

            return xml;
        }

        public static XDocument CreateFieldXml(AssetData data)
        {
            var xml = new XDocument();
            var root = new XElement("Chaos.Data");
            var fieldsElement = new XElement("Fields");
            xml.Add(root);
            root.Add(fieldsElement);

            foreach (var field in data.Fields)
            {
                var f = data.DataDefinition.Fields.FirstOrDefault(item => item.Id == field.Key);

                if (f == null) continue;

                var fieldElement = CreateFieldXml(field);
                var name = new XAttribute("Name", field.Key);
                var type = new XAttribute("Type", f.Type);

                fieldElement.Add(name);
                fieldElement.Add(type);
                fieldsElement.Add(fieldElement);
            }

            return xml;
        }

        private static XElement CreateFieldXml(KeyValuePair<string, Value> field)
        {
            var tableValue = field.Value as TableValue;
            var stringValue = field.Value as StringValue;
            var dateTimeValue = field.Value as DateTimeValue;
            var textValue = field.Value as TextValue;
            var boolValue = field.Value as BooleanValue;

            if (stringValue != null)
                return new XElement("Field", field.Value.ToString());

            if (textValue != null)
                return new XElement("Field", field.Value.ToString());

            if (dateTimeValue != null)
                return new XElement("Field", field.Value.ToString());
            
            if (boolValue != null)
                return new XElement("Field", field.Value.ToString());

            if (tableValue != null)
            {
                var fieldElement = new XElement("Field");

                foreach (var row in tableValue.Value)
                {
                    var rowElement = new XElement("Row");

                    foreach (var cell in row)
                        rowElement.Add(new XElement("Cell", cell));

                    fieldElement.Add(rowElement);
                }

                return fieldElement;
            }

            throw new NotImplementedException("Value type not implemented in PersistentAssetRepository");
        }

        public static XDocument CreateAnnotationXml(Asset.AnnotationGroup annotationGroup)
        {
            var xml = new XDocument();
            var root = new XElement("Annotations");

            foreach (var annotation in annotationGroup.Annotations)
            {
                var element = new XElement("Annotation");

                foreach (var pair in annotation)
                {
                    var fieldXml = CreateFieldXml(pair);
                    fieldXml.Name = pair.Key;

                    element.Add(fieldXml);
                }

                root.Add(element);
            }

            xml.Add(root);
            return xml;
        }

        public IEnumerable<Asset> Get(uint? objectTypeId = null, uint index = 0u)
        {
            while (true)
            {
                var pageSize = 1000u;
                var objs = ObjectGet(objectTypeId, index, pageSize).ToList();

                foreach (var obj in objs)
                    yield return Map(obj);

                if (objs.Count < pageSize)
                    break;

                index++;
            }
        }

        public IEnumerable<Asset> Get(uint? objectTypeId = null)
        {
            var index = 0u;

            while (true)
            {
                var pageSize = 1000u;
                var objs = ObjectGet(objectTypeId, index, pageSize);

                foreach (var obj in objs)
                    yield return Map(obj);

                if (objs.Count < pageSize)
                    break;

                index++;
            }
        }

        private IList<Object> ObjectGet(uint? objectTypeId, uint index, uint pageSize, uint retries = 10)
        {
            try
            {
                return Mcm.ObjectGet(folderID: _context.Config.Mcm.AssetFolderId,
                    includeMetadata: true,
                    includeFiles: true,
                    pageIndex: index,
                    pageSize: pageSize,
                    objectTypeId: objectTypeId);
            }
            catch (System.Exception e)
            {
                if(retries > 0)
                    return ObjectGet(objectTypeId, index, pageSize, --retries);

                throw;
            }
        }

        public Asset Get(Guid id)
        {
            var obj = Mcm.ObjectGet(id,
                includeMetadata: true,
                includeFiles: true);

            return Map(obj);
        }

        private Asset Map(Object obj)
        {
            var asset = new Asset(_context)
            {
                Identifier = obj.Guid.ToString(),
                TypeId = obj.ObjectTypeID,
                Data = ExtractData(obj).ToList(),
                Files = ExtractFiles(obj),
                Tags = ExtractTags(obj).ToArray(),
                DoFilesRequireLogin = ExtractAccessSettings(obj)
            };

            foreach (var annotationGroup in ExtractAnnotations(obj))
                asset.Annotations.Add(annotationGroup);

            return asset;
        }

        private bool ExtractAccessSettings(Object obj)
        {
            var fieldMetadata =
                obj.Metadatas.FirstOrDefault(
                    item => item.MetadataSchemaGuid == _context.Config.Mcm.AssetMetadataSchemaId);

            if (fieldMetadata == null)
                return false;

            var access = fieldMetadata.MetadataXml.Descendants("DoFilesRequireLogin").SingleOrDefault();

            if (access == null)
                return false;

            return access.Value.ToLower() == "true";
        }

        private IEnumerable<AssetData> ExtractData(Object obj)
        {
            foreach (var fieldMetadata in obj.Metadatas)
            {
                var definition =
                    _context.Repository.Definition.GetDataDefinitions()
                        .SingleOrDefault(d => d.Id == fieldMetadata.MetadataSchemaGuid.ToString());

                if (definition == null)
                    continue;

                var fields = new Dictionary<string, Value>();

                foreach (var fieldDefinition in definition.Fields.Where(f => f.Type == "Boolean"))
                {
                    if (!fields.ContainsKey(fieldDefinition.Id) && fieldDefinition.Options != null && fieldDefinition.Options.Any())
                        fields.Add(fieldDefinition.Id, new BooleanValue(fieldDefinition.Options.First() == "true"));
                }
                
                foreach (var field in fieldMetadata.MetadataXml.Descendants("Field"))
                {
                    var fieldName = field.Attribute("Name").Value;
                    var fieldType = field.Attribute("Type").Value;
                    var fieldValue = ExtractField(fieldType, field);

                    if (fields.ContainsKey(fieldName))
                        fields[fieldName] = fieldValue;
                    else
                        fields.Add(fieldName, fieldValue);
                }

                yield return new AssetData(_context, definition.Name)
                {
                    Fields = fields
                };
            }
        }

        private static Value ExtractField(string fieldType, XElement field)
        {
            switch (fieldType)
            {
                case "Text":
                    return new StringValue(field.Value);
                case "String":
                    return new StringValue(field.Value);
                case "MultipleChoice":
                    return new StringValue(field.Value);
                case "Boolean":
                    return new BooleanValue(field.Value.ToLower() == "true");
                case "Datetime":
                case "DateTime":
                    return new DateTimeValue(field.Value);
                case "Table":
                {
                    var table = new TableValue();

                    foreach (var rowElement in field.Elements("Row"))
                    {
                        var row = new List<string>();

                        foreach (var cellElement in rowElement.Elements("Cell"))
                            row.Add(cellElement.Value);

                        table.Value.Add(row);
                    }

                    return table;
                }
            }

            throw new NotImplementedException("Unsupported Field type: " + fieldType);
        }

        private IEnumerable<string> ExtractTags(Object obj)
        {
            var fieldMetadata =
                obj.Metadatas.FirstOrDefault(
                    item => item.MetadataSchemaGuid == _context.Config.Mcm.AssetMetadataSchemaId);

            if (fieldMetadata == null)
                return new string[0];

            return fieldMetadata.MetadataXml.Descendants("Tag").Select(field => field.Value);
        }

        private IEnumerable<Asset.AnnotationGroup> ExtractAnnotations(Object obj)
        {
            foreach (var metadata in obj.Metadatas)
            {
                if (metadata.MetadataXml.Root.Name != "Annotations") continue;

                var dataDefinition =
                    _context.Repository.Definition.GetAnnotations()
                        .SingleOrDefault(ad => ad.Identifier == metadata.MetadataSchemaGuid.ToString());

                if (dataDefinition == null)
                    throw new UnhandledException(
                        "Annotation Definition [" + metadata.MetadataSchemaGuid + "] not found");

                var annotations = ExtractAnnotations(metadata).ToList();

                if (!annotations.Any()) continue;

                yield return new Asset.AnnotationGroup
                {
                    Name = dataDefinition.Name,
                    DefinitionId = Guid.Parse(dataDefinition.Identifier),
                    Annotations = annotations
                };
            }
        }

        public IEnumerable<IDictionary<string, Value>> ExtractAnnotations(Metadata metadata)
        {
            var definition =
                _context.Repository.Definition.GetAnnotations()
                    .Single(ad => ad.Identifier == metadata.MetadataSchemaGuid.ToString());

            foreach (var aElement in metadata.MetadataXml.Descendants("Annotation"))
            {
                var dict = new Dictionary<string, Value>();
                dict.Add("__OwnerId", new StringValue(""));
                dict.Add("__Owner", new StringValue("System"));
                dict.Add("__Permission", new StringValue("0"));
                
                foreach (var field in aElement.Elements())
                {
                    var name = field.Name.ToString();

                    try
                    {
                        if (field.Name.ToString() == "Identifier")
                            dict.Add(name, new StringValue(field.Value));
                        else if (field.Name.ToString() == "__OwnerId")
                        {
                            if (Guid.TryParse(field.Value, out var id))
                            {
                                var ezUser = EzUserGetter.Get(id);
                                dict["__OwnerId"] = new StringValue(ezUser.Identifier);
                                dict["__Owner"] = new StringValue(ezUser.Name);
                            }
                        }
                        else if (field.Name.ToString() == "__Permission")
                            dict["__Permission"] = new StringValue(field.Value);
                        else
                        {
                            var type = definition.GetFieldDefinition(name).Type;

                            if(dict.ContainsKey(name))
                                Console.WriteLine(name);
                            
                            dict.Add(name, ExtractField(type, field));
                        }
                    }
                    catch (InvalidOperationException)
                    {
                    }
                }

                yield return dict;
            }
        }

        private static IList<Asset.FileReference> ExtractFiles(Object obj)
        {
            var files = new Dictionary<uint, Asset.FileReference>();

            foreach (var fileInfo in obj.Files)
            {
                if (fileInfo.ParentID.HasValue)
                {
                    if (!files.ContainsKey(fileInfo.ParentID.Value))
                    {
                        var f = new Asset.FileReference();
                        f.Identifier = fileInfo.ParentID.Value.ToString();
                        f.Type = fileInfo.FormatType;
                        f.Name = fileInfo.OriginalFilename;

                        files.Add(fileInfo.Identifier, f);
                    }

                    var file = files[fileInfo.ParentID.Value];
                    file.Destinations.Add(new Asset.FileReference.Destination
                    {
                        Type = fileInfo.MimeType,
                        Url = fileInfo.URL
                    });
                }
                else
                {
                    if (!files.ContainsKey(fileInfo.Identifier))
                    {
                        var f = new Asset.FileReference();
                        f.Identifier = fileInfo.Identifier.ToString();
                        f.Type = fileInfo.FormatType;
                        f.Name = fileInfo.OriginalFilename;

                        files.Add(fileInfo.Identifier, f);
                    }

                    var file = files[fileInfo.Identifier];
                    file.Destinations.Add(new Asset.FileReference.Destination
                    {
                        Type = fileInfo.MimeType,
                        Url = fileInfo.URL
                    });
                }
            }

            return files.Values.ToList();
        }

        public bool Delete(Guid id)
        {
            var wasDeleted = Mcm.ObjectDelete(id) == 1;

            if (wasDeleted)
                ViewManager.GetView("EzSearch").Delete(id.ToString());

            return wasDeleted;
        }
    }
}