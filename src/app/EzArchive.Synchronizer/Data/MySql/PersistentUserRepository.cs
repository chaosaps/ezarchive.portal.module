﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using Chaos.Authentication.Core.Domain;
using Chaos.Authentication.Core.Model;
using Chaos.Authentication.Data;
using Chaos.Mcm.Data;
using Chaos.Portal.Core.Data;
using Chaos.Portal.Core.Data.Model;
using Chaos.Portal.Core.Indexing.Solr.Request;
using Chaos.Portal.Core.Indexing.View;
using EzArchive.Synchronizer.Core;
using EzArchive.Synchronizer.Data.View.User;
using EzUser = EzArchive.Synchronizer.Core.Model.EzUser;

namespace EzArchive.Synchronizer.Data.MySql
{
  public class PersistentUserRepository : IEzUserRepository
  {
    private readonly ContextNonStatic _context;
    private IPortalRepository Portal { get; set; }
    private IMcmRepository Mcm { get; set; }
    private IAuthenticationRepository Authentication { get; set; }
	  private IViewManager ViewManager { get; set; }
		public UserView UserView { get { return (UserView) ViewManager.GetView("EzUser"); } }

	  public PersistentUserRepository(ContextNonStatic context, IPortalRepository portal, IMcmRepository mcm, IAuthenticationRepository authentication, IViewManager viewManager)
    {
      _context = context;
      Portal = portal;
      Mcm = mcm;
      Authentication = authentication;
		  ViewManager = viewManager;
    }

    public EzUser Set(EzUser user)
    {
      CreateIfNew(user);
      UpdateUserProfile(user);

      return user;
    }

    private void CreateIfNew(EzUser user)
    {
      try
      {
        var usr = Portal.UserInfoGet(user.Email);

        if (usr.Guid.ToString() != user.Identifier)
          throw new System.Exception("Email already in use");
      }
      catch (ArgumentException)
      {
      }

      if (user.Identifier == null)
      {
        var userId = Guid.NewGuid();

        user.Identifier = userId.ToString();

        Portal.UserCreate(userId, user.Email);
      }

      if (Mcm.ObjectGet(Guid.Parse(user.Identifier)) == null)
        Mcm.ObjectCreate(Guid.Parse(user.Identifier), _context.Config.Mcm.ProfileObjectTypeId,
          _context.Config.Mcm.UserFolderId);
    }

	  private void UpdateUserProfile(EzUser user)
    {
      var userId = Guid.Parse(user.Identifier);
      var xml = SerializeXml(user);
	    var profileObject = Mcm.ObjectGet(userId, includeMetadata: true);
			var profileMetadata = profileObject.Metadatas.SingleOrDefault(m => m.MetadataSchemaGuid == _context.Config.Mcm.ProfileMetadataSchemaId);

			Mcm.MetadataSet(userId, userId, _context.Config.Mcm.ProfileMetadataSchemaId, null, profileMetadata == null ? 0 : profileMetadata.RevisionID, xml, userId);
      Portal.UserSet(userId, user.Email);

      var groups = Portal.GroupGet(null, null, null, userId);

      if (!WasPermissionChanged(userId, user.Permission))
        return;

      foreach (var group in groups)
        Portal.GroupRemoveUser(group.Guid, userId, null);

      if (user.Permission == "Administrator")
        Portal.GroupAddUser(_context.Config.AdminGroupId, userId, 0, null);

      if (user.Permission == "Contributor")
        Portal.GroupAddUser(_context.Config.ContributorGroupId, userId, 0, null);

      if (user.Permission == "User")
        Portal.GroupAddUser(_context.Config.UserGroupId, userId, 0, null);
    }

    private bool WasPermissionChanged(Guid userId, string newPermission)
    {
      var groups = Portal.GroupGet(null, null, null, userId);

      if (newPermission == "Administrator" && groups.Any(group => @group.Guid == _context.Config.AdminGroupId))
        return false;
      if (newPermission == "Contributor" && groups.Any(group => @group.Guid == _context.Config.ContributorGroupId))
        return false;
      if (newPermission == "User" && groups.Any(group => @group.Guid == _context.Config.UserGroupId))
        return false;

      return true;
    }

    private EzUser CreateEzUser(UserInfo user, Chaos.Mcm.Core.Model.Object profile)
    {
      var ezUser = new EzUser
        {
          Identifier = user.Guid.ToString(),
          Email = user.Email,
          Permission = DeterminePermission(user)
        };

      if (profile != null)
      {
        var metadata = profile.Metadatas.FirstOrDefault(item => item.MetadataSchemaGuid == _context.Config.Mcm.ProfileMetadataSchemaId);
        
        if (metadata != null)
        {
          var p = DeserializeXml<EzUser>(metadata.MetadataXml);
          ezUser.Name = p.Name;
          ezUser.Preferences = p.Preferences;
	        ezUser.WayfAttributes = p.WayfAttributes;
        }
      }

      return ezUser;
    }

    private T DeserializeXml<T>(XDocument metadataMetadataXml)
    {
      var ser = new XmlSerializer(typeof(T));
      
      using (var reader = metadataMetadataXml.CreateReader())
      {
        return (T) ser.Deserialize(reader);
      }
    }

    private XDocument SerializeXml<T>(T obj)
    {
      var ser = new XmlSerializer(typeof(T));

      var stream = new MemoryStream();
      ser.Serialize(stream, obj);
      stream.Position = 0;
      
      return XDocument.Load(stream);
    }

    private string DeterminePermission(UserInfo user)
    {
      var groups = Portal.GroupGet(null, null, null, user.Guid);

      if (groups.Any(group => @group.Guid == _context.Config.AdminGroupId))
        return "Administrator";
      if (groups.Any(group => @group.Guid == _context.Config.ContributorGroupId))
        return "Contributor";
      if (groups.Any(group => @group.Guid == _context.Config.UserGroupId))
        return "User";

      return null;
    }

    public EzUser Get(Guid id)
    {
      var user = Portal.UserInfoGet(id, null, null, null).FirstOrDefault();
      if (user == null)
        throw new System.Exception("User not found Id: " + id);

      var profile = Mcm.ObjectGet(id, true);

      return CreateEzUser(user, profile);
    }

    public IEnumerable<EzUser> Get()
    {
      var users = GetAllUsers();
      var profiles = Mcm.ObjectGet(null, 0, (uint) users.Count(), objectTypeId: _context.Config.Mcm.ProfileObjectTypeId, includeMetadata: true);

      foreach (var userInfo in users)
      {
        var profile = profiles.FirstOrDefault(item => item.Guid == userInfo.Guid);

        yield return CreateEzUser(userInfo, profile);
      }
    }

    private IEnumerable<UserInfo> GetAllUsers()
    {
      var users = new Dictionary<Guid, UserInfo>();
      var userInfoGet = Portal.UserInfoGet(null, null, null, null);

      // Ensure users are only added once (we dont care about sessions)
      foreach (var userInfo in userInfoGet.Where(userInfo => !users.ContainsKey(userInfo.Guid)))
        users.Add(userInfo.Guid, userInfo);

      return users.Values;
    }

    public void SetPassword(Guid id, string newPassword)
    {
      var passwordSettings = new PasswordSettings {Iterations = 1000, UseSalt = true};
      var passwordHelper = new PasswordHelper(passwordSettings);
      var hash = passwordHelper.GenerateHash(newPassword, id.ToString());

      Authentication.EmailPasswordUpdate(id, hash);
    }

    public bool Delete(Guid id)
    {
      Mcm.ObjectDelete(id);
      Authentication.EmailPasswordDelete(id);
      Portal.UserDelete(id);

      return true;
    }

	  public IEnumerable<EzUser> Search(string q, uint pageSize)
	  {
		  var query = new SolrQuery
			  {
					Query = string.Format("t_name:{0}*",q),
					PageSize = pageSize
			  };

			return UserView.Query(query).Results.Select(vd => ((UserViewData)vd).Dto);
	  }
  }
}