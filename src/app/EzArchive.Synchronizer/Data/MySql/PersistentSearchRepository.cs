﻿using System.Collections.Generic;
using System.Linq;
using Chaos.Portal.Core.Indexing.Solr.Request;
using Chaos.Portal.Core.Indexing.View;
using EzArchive.Synchronizer.Core;
using EzArchive.Synchronizer.Core.Exception;
using EzArchive.Synchronizer.Core.Model;
using EzArchive.Synchronizer.Data.View.Search;

namespace EzArchive.Synchronizer.Data.MySql
{
  public class PersistentSearchRepository : ISearchRepository
  {
	  private readonly ContextNonStatic _context;
	  private IViewManager ViewManager { get; set; }

    public PersistentSearchRepository(ContextNonStatic context, IViewManager viewManager)
    {
	    _context = context;
	    ViewManager = viewManager;
    }

    public Page<Search> All()
    {
      return Query(SearchQuery.AllQuery(_context), SearchQuery.AllQuery(_context), "*", new string[0], 0, 1000);
    }

    public Page<Search> Query(SearchQuery q, SearchQuery filter, string tag, IEnumerable<string> facets, uint pageIndex, uint pageSize, string sort = "")
    {
			if(filter == null)
				filter = SearchQuery.AllQuery(_context);

      if (tag != null)
				filter.AndQuery("{Tags}:EXACT "+ tag);

			filter.AndQuery(CreateFacets(facets.ToList()), false);

			var queryString = q.ToString();

			if (!string.IsNullOrEmpty(sort))
      {
        var posOfSeparator = sort.LastIndexOf(" ");
        var displayName = sort.Substring(0, posOfSeparator);
        var sortOrder = sort.Substring(posOfSeparator);
        sort = GenerateSortValue(displayName, sortOrder);
      }
			else if (queryString == "*:*")
				sort = _context.Config.DefaultSortField;

			var eDismaxQuery = new EDismaxQuery
	    {
		    Query = queryString,
		    QueryFields = "Fulltext",
		    PageIndex = pageIndex,
		    PageSize = pageSize,
		    Filter = filter.ToString(),
				BoostQuery = new List<string> { "{!edismax qf=s_search_titel^5 v=$q bq=}" }, // TODO handle the search field better
		    Sort = sort
	    };

			var result = ViewManager.GetView("EzSearch").Query(eDismaxQuery);

      var dtos = result.Results.Cast<SearchViewData>().Select(res => res.Dto);

      return new Page<Search>{FoundCount = result.FoundCount, StartIndex = result.StartIndex, Results = dtos};
    }

	  public string CreateFacets(IList<string> facets)
	  {
		  if (!facets.Any()) return "";

			var sq = new SearchQuery(_context);

		  foreach (var facet in facets)
			  sq.AndQuery(facet.Replace(":", ":EXACT "));

		  return sq.ToString();
	  }

	  private string GenerateSortValue(string displayName, string sortOrder)
    {
      var field = _context.Config.GetSearchFieldDefinition(displayName);

	    if (field.IsSortable)
		    return IndexHelper.GetFormattedSortKey(field.Type(_context), field.DisplayName.Replace(" ", "_").ToLower()) + " " + sortOrder;

      throw new SearchQueryParserException($"Field: {displayName} is not sortable");
    }

    private static string Sanitize(string input)
    {
      var sanitized = input.Replace(",", "");
      sanitized = sanitized.Replace("&", "%26");

      return sanitized;
    }
  }
}