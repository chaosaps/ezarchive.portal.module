using System.Collections.Generic;

namespace EzArchive.Synchronizer.Data
{
	public interface IProjectRepository
	{
		EzArchive.Synchronizer.Core.Model.Project Set(EzArchive.Synchronizer.Core.Model.Project project);
		IEnumerable<EzArchive.Synchronizer.Core.Model.Project> Get(string id = null, string userId = null, string labelId = null);
		bool AddUser(string id, string userId);
		bool RemoveUser(string id, string userId);
		EzArchive.Synchronizer.Core.Model.Label AssociateLabel(string id, EzArchive.Synchronizer.Core.Model.Label label);
		bool Delete(string id);
	}
}