﻿using System.Collections.Generic;
using Chaos.Portal.Core.Data.Model;
using Chaos.Portal.Core.Indexing.View;
using EzArchive.Synchronizer.Core;
using EzArchive.Synchronizer.Core.Model;

namespace EzArchive.Synchronizer.Data.View.Search
{
  public class SearchView : AView
  {
    private readonly ContextNonStatic _context;

    public SearchView(ContextNonStatic context) : base("EzSearch")
    {
      _context = context;
    }

    public override IPagedResult<IResult> Query(Chaos.Portal.Core.Indexing.IQuery query)
    {
      return Query<SearchViewData>(query);
    }

    public override IList<IViewData> Index(object objectsToIndex)
    {
      var asset = objectsToIndex as Asset;

      if (asset == null)
        return new List<IViewData>();

      var labels = _context.Repository.Label.Get(asset.Identifier);
      var search = new SearchBuilder(_context).Build(asset);

      return new[] {new SearchViewData(_context) {Dto = search, Src = asset, Labels = labels}};
    }
  }
}