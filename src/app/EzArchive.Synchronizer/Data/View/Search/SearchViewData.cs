﻿using System;
using System.Collections.Generic;
using System.Linq;
using Chaos.Portal.Core.Indexing.View;
using EzArchive.Synchronizer.Core;
using EzArchive.Synchronizer.Core.Model;
using Newtonsoft.Json;

namespace EzArchive.Synchronizer.Data.View.Search
{
  public class SearchViewData : IViewData
  {
    private readonly ContextNonStatic _context;

    public SearchViewData(ContextNonStatic context)
    {
      _context = context;
      Src = new Asset(_context);
      Labels = new Label[0];
    }

    public IEnumerable<KeyValuePair<string, string>> GetIndexableFields()
    {
      yield return UniqueIdentifier;

      foreach (var assetData in Src.Data)
      {
        foreach (var field in assetData.Fields)
        {
          var id = assetData.DataDefinition.Name + "." + field.Key;
          IEnumerable<string> values = new List<string>();

          try
          {
            values = IndexHelper.GetIndexValue(id, field.Value, _context);
          }
          catch (InvalidOperationException e)
          {
          }

          foreach (var value in values)
          {
            if (!string.IsNullOrEmpty(value))
              yield return new KeyValuePair<string, string>(IndexHelper.GetIndexKey(id, _context), value);
          }
        }
      }

      foreach (var searchField in Dto.Fields)
      {
        var sfd = _context.Config.GetSearchFieldDefinition(searchField.Key);

        var formattedValue = IndexHelper.GetFormattedValue(searchField.Value, sfd.Type(_context));

        yield return
          new KeyValuePair<string, string>(IndexHelper.GetFormattedSearchKey(sfd.Type(_context), searchField.Key),
            formattedValue);

        if(sfd.IsSortable)
          yield return new KeyValuePair<string, string>(IndexHelper.GetFormattedSortKey(sfd.Type(_context), searchField.Key),
            formattedValue);
      }

      foreach (var annotationGroup in Src.Annotations)
      foreach (var annotation in annotationGroup.Annotations)
      foreach (var pair in annotation)
      {
        var key = annotationGroup.Name.Replace(" ", "_").Replace(".", "_");
        key += "_" + pair.Key.Replace(" ", "_").Replace(".", "_");
        yield return new KeyValuePair<string, string>("sm_an_" + key, pair.Value.ToString());
      }

      yield return new KeyValuePair<string, string>("s_document_typeid", Dto.TypeId);

      foreach (var tag in Src.Tags)
        yield return new KeyValuePair<string, string>("sm_tags", tag.ToLower());

      foreach (var label in Labels)
        yield return new KeyValuePair<string, string>("sm_labels", label.Identifier);

      foreach (var projectId in Labels.Select(l => l.ProjectId).Distinct())
        yield return new KeyValuePair<string, string>("sm_projectids", projectId.ToString());
    }

    [JsonIgnore]
    public KeyValuePair<string, string> UniqueIdentifier 
      => new KeyValuePair<string, string>("Id", Dto.Identifier);

    public Core.Model.Search Dto { get; set; }

    [JsonIgnore]
    public string FullName 
      => "SearchViewData";

    [JsonIgnore]
    public Asset Src { get; set; }

    [JsonIgnore]
    public IEnumerable<Label> Labels { get; set; }
  }
}