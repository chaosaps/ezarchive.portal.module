﻿using System.Collections.Generic;
using Chaos.Portal.Core.Indexing.View;
using EzArchive.Synchronizer.Core.Model;

namespace EzArchive.Synchronizer.Data.View.User
{
  public class UserViewData : IViewData
  {
    public EzUser Dto { get; set; }

    public UserViewData(EzUser user)
    {
      Dto = user;
    }

    public IEnumerable<KeyValuePair<string, string>> GetIndexableFields()
    {
      yield return UniqueIdentifier;
      yield return new KeyValuePair<string, string>("t_name", Dto.Name);
    }

    public KeyValuePair<string, string> UniqueIdentifier { get{return new KeyValuePair<string, string>("Id", Dto.Identifier);} }
    public string FullName { get; private set; }
  }
}