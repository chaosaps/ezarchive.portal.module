﻿using System.Collections.Generic;
using Chaos.Portal.Core.Data.Model;
using Chaos.Portal.Core.Indexing;
using Chaos.Portal.Core.Indexing.View;
using EzArchive.Synchronizer.Core.Model;

namespace EzArchive.Synchronizer.Data.View.User
{
  public class UserView : AView
  {
    public UserView() : base("EzUser")
    {
    }

    public override IList<IViewData> Index(object objectsToIndex)
    {
      var user = objectsToIndex as EzUser;

      if(user == null) return new List<IViewData>();
      if(user.Name == null) return new List<IViewData>();

      return new IViewData[] { new UserViewData(user) };
    }

    public override IPagedResult<IResult> Query(IQuery query)
    {
      return Query<UserViewData>(query);
    }
  }
}