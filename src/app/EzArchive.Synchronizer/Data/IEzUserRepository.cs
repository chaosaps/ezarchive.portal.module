using System;
using System.Collections.Generic;

namespace EzArchive.Synchronizer.Data
{
  public interface IEzUserRepository : IEzUserGetter
  {
    Core.Model.EzUser Set(Core.Model.EzUser user);
    IEnumerable<Core.Model.EzUser> Get();
    void SetPassword(Guid id, string newPassword);
    bool Delete(Guid id);
	  IEnumerable<Core.Model.EzUser> Search(string s, uint pageSize);
  }

  public interface IEzUserGetter
  {
    Core.Model.EzUser Get(Guid id);
  }
}