namespace EzArchive.Synchronizer.Data
{
  public interface IEzArchiveRepository
  {
    IAssetRepository Asset { get; set; }
    ISearchRepository Search { get; set; }
    IEzUserRepository EzUser { get; set; }
    IEzFileRepository EzFile { get; set; }
    IDefinitionRepository Definition { get; set; }
	  IProjectRepository Project { get; set; }
	  ILabelRepository Label { get; set; }
  }
}