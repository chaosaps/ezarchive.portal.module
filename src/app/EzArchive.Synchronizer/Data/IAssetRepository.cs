using System;
using System.Collections.Generic;
using EzArchive.Synchronizer.Core.Model;

namespace EzArchive.Synchronizer.Data
{
  public interface IAssetRepository
  {
    Asset Set(Asset asset);
    IEnumerable<Asset> Get(uint? objectTypeId = null);
    Asset Get(Guid id);
    bool Delete(Guid id);
    IEnumerable<Asset> Get(uint? objectTypeId = null, uint index = 0u);
  }
}