using System.Collections.Generic;

namespace EzArchive.Synchronizer.Data
{
	public interface ILabelRepository
	{
		void SetAssociation(string id, string assetId);
		void DeleteAssociation(string id, string assetId);
		IEnumerable<Core.Model.Label> Get(string assetId);
		bool Delete(string id);
	}
}