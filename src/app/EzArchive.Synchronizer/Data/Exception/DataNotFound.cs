﻿using System.Runtime.Serialization;

namespace EzArchive.Synchronizer.Data.Exception
{
  public class DataNotFound : System.Exception
  {
    public DataNotFound()
    {
    }

    public DataNotFound(string message) : base(message)
    {
    }

    public DataNotFound(string message, System.Exception innerException) : base(message, innerException)
    {
    }

    protected DataNotFound(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }
  }
}