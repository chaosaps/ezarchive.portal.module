﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using Chaos.Authentication.Data;
using Chaos.Authentication.Data.MySql;
using Chaos.Mcm.Core.Model;
using Chaos.Mcm.Data;
using Chaos.Mcm.Data.MySql;
using Chaos.Portal.Core.Cache;
using Chaos.Portal.Core.Cache.InMemory;
using Chaos.Portal.Core.Data;
using Chaos.Portal.Core.Data.Model;
using Chaos.Portal.Core.Indexing.View;
using EzArchive.Synchronizer.Core;
using EzArchive.Synchronizer.Core.Configuration;
using EzArchive.Synchronizer.Core.Model;
using EzArchive.Synchronizer.Data;
using EzArchive.Synchronizer.Data.MySql;
using Newtonsoft.Json;
using Label = Chaos.Mcm.Core.Model.Label;
using MetadataSchema = Chaos.Mcm.Core.Model.MetadataSchema;
using Object = Chaos.Mcm.Core.Model.Object;
using Project = Chaos.Mcm.Core.Model.Project;

namespace EzArchive.Synchronizer
{
  class Program
  {
    private static EzArchiveContext _destinationArchive;
    private static MD5 _md5;
    private static readonly string PublishKey = "IsPublished";
    private static Stopwatch _stopwatch;
    private static readonly Dictionary<Guid, Object> AllSourceObjects = new Dictionary<Guid, Object>();

    static void Main(string[] args)
    {
      if (args.Length != 1)
      {
        Console.WriteLine("Missing path to Configuration.json");
        return;
      }

      _stopwatch = new Stopwatch();
      _stopwatch.Start();
      
      var arguments = Arguments.Load(args[0]);
      var SourceArchives = new List<SourceEzArchiveContext>();

      foreach (var portalConnectionString in arguments.Src)
        SourceArchives.Add(CreateSourceArchive(portalConnectionString));

      _destinationArchive = CreateEzArchiveContext(arguments.Dst.ConnectionString);
      _md5 = System.Security.Cryptography.MD5.Create();

      var dstDestinations = _destinationArchive.Mcm.Destination.Get(null, null);
      var dstAccessProviders = _destinationArchive.Mcm.AccessProvider.Get();
      var dstProjects = _destinationArchive.Mcm.Project.Get();
      var dstLabels = _destinationArchive.Mcm.Label.Get();
      var dstFolders = _destinationArchive.Mcm.Folder.Get();
      var dstAdminUser = _destinationArchive.Portal.UserInfoGet("admin");
      var dstObjects = GetObjects(_destinationArchive.Mcm).ToDictionary(o => o.Guid);

      foreach (var sourceArchive in SourceArchives)
      {
        Print($"Synchronize source archive: {sourceArchive.Id}");
        Print("Create Project and Label");
        var label = CreateProjectAndLabel(sourceArchive, dstProjects, dstLabels, dstAdminUser);
        
        Print("Create Folders");
        var rootFolderId = CreateFolders(dstFolders, dstAdminUser);

        Print("Sync Destinations");
        SyncDestinations(sourceArchive, dstDestinations);

        Print("Sync AccessProviders");
        SyncAccessProviders(sourceArchive, dstDestinations, dstAccessProviders);

        Print("Sync MetadataSchema");
        SyncMetadataSchemas(sourceArchive);
        
        Print("Sync Assets");
        SyncAssets(sourceArchive, dstObjects, label, rootFolderId, dstAdminUser);
      }
      
      Print("Remove unpublished Assets");
      var fullReIndexRequired = RemoveUnpublishedAssets(dstObjects);
        
      Print("Indexing destination");
      IndexDestination(arguments.Dst.Endpoint, fullReIndexRequired);
        
      Print($"Synchronization complete @ {DateTime.Now:u}");
    }

    private static void IndexDestination(string url, bool performClean = true)
    {
      var clean = performClean ? "true" : "false";
      new Chaos.Net.HttpConnection(url)
        .Get("v6/EZSystem/FullReIndex", $"password=1414280e-a36d-4675-a598-c2931eb3d8f0&format=json3&clean={clean}", timeout: 60*60*1000);
      
      Console.WriteLine($"Perform Full ReIndex: {clean}");
    }

    private static bool RemoveUnpublishedAssets(Dictionary<Guid, Object> dstObjects)
    {
      var noAssetsDeleted = false;
      
      foreach (var dstObj in dstObjects)
      {
        if (AllSourceObjects.ContainsKey(dstObj.Key)) continue;
        
        Print($"\tDelete Asset: {dstObj.Key}");

        _destinationArchive.Mcm.Object.Delete(dstObj.Key);

        noAssetsDeleted = true;
      }

      return noAssetsDeleted;
    }

    private static void SyncAssets(SourceEzArchiveContext sourceArchive, IDictionary<Guid, Object> dstObjects, Label label, uint rootFolderId, UserInfo dstAdminUser)
    {
      var dstMetadatas = dstObjects.SelectMany(o => o.Value.Metadatas).ToDictionary(m => m.Guid);
      var objects = GetObjects(sourceArchive.Mcm).Where(IsPublished).ToList();
        var name = $"1:{sourceArchive.Id}";
        var dstDestination = _destinationArchive.Mcm.Destination.Get(null, null).SingleOrDefault(d => d.Name == name);

        if (dstDestination == null)
        {
          Print($"ERROR:: Destination {name} not found");
          return;
        }
        
        foreach (var obj in objects)
        {
          AllSourceObjects.Add(obj.Guid, obj);
          
          var objectGuid = obj.Guid;
          if (!dstObjects.ContainsKey(objectGuid))
          {
            Print($"\tCreating: {objectGuid}");
            _destinationArchive.Mcm.Object.Create(objectGuid, obj.ObjectTypeID, rootFolderId);
            _destinationArchive.Mcm.Label.AssociationWithObject(label.Identifier.Value, objectGuid);

            var fileMap = new Dictionary<uint, uint>();

            foreach (var file in obj.Files.OrderBy(f => f.ParentID).ToList())
            {
              if(fileMap.ContainsKey(file.Identifier))
                continue;
              
              uint? parentId = null;
              if (file.ParentID != null)
                parentId = fileMap[file.ParentID.Value];
              
              var fileid = _destinationArchive.Mcm.File.Create(
                objectGuid, 
                parentId, 
                dstDestination.Id.Value, 
                file.Filename,
                file.OriginalFilename, 
                file.FolderPath, 
                file.FormatID);
              
              fileMap.Add(file.Identifier, fileid);
              
              Print($"\t\tSetting file: {fileid} - {file.FolderPath}/{file.Filename}");
            }
          }

          foreach (var metadata in obj.Metadatas)
          {
            var metadataSchemaGuid = CreateMetadaSchemaGuid(metadata.MetadataSchemaGuid);

            if (dstMetadatas.ContainsKey(metadata.Guid) &&
                dstMetadatas[metadata.Guid].MetadataXml.ToString() == metadata.MetadataXml.ToString())
              continue;
            
            try
            {
              _destinationArchive.Mcm.Metadata.Set(
                objectGuid,
                metadata.Guid,
                metadataSchemaGuid,
                metadata.LanguageCode,
                dstMetadatas.ContainsKey(metadata.Guid) ? dstMetadatas[metadata.Guid].RevisionID : 0,
                metadata.MetadataXml,
                dstAdminUser.Guid);
              
              Print($"\t\tSetting metadata: {metadata.Guid}" +
                    $" - rev: {metadata.RevisionID}" +
                    $" - schema: {metadata.MetadataSchemaGuid} => {metadataSchemaGuid}");
            }
            catch (InvalidRevision)
            {
              // no changes
            }
          }          
        }
    }

    private static bool IsPublished(Object obj)
    {
      foreach (var fieldMetadata in obj.Metadatas)
      {
        foreach (var field in fieldMetadata.MetadataXml.Descendants("Field"))
        {
          if(field.Attribute("Name").Value != PublishKey)
            continue;

          return field.Value.ToLower() == "true";
        }
      }

      return true;
    }

    private static object printLock = new Object();
      
    private static void Print(string message)
    {
      lock (printLock)
      {
        Console.WriteLine($"{_stopwatch.Elapsed}: {message}");
      }
    }

    private static IEnumerable<Chaos.Mcm.Core.Model.Object> GetObjects(IMcmRepository mcm)
    {
      var index = 0u;

      while (true)
      {
        var pageSize = 1000u;
        var objs = mcm.Object.Get(objectTypeId: 1, includeFiles:true, includeMetadata:true, pageIndex: index, pageSize: pageSize);

        foreach (var obj in objs)
          yield return obj;

        if (objs.Count < pageSize)
          break;

        index++;
      }
    }

    private static uint CreateFolders(IList<Folder> dstFolders, UserInfo dstAdminUser)
    {
      var usersFolder = dstFolders.SingleOrDefault(f => f.Name == "Users");
      var rootFolder = dstFolders.SingleOrDefault(f => f.Name == "Root");

      if (usersFolder == null)
        _destinationArchive.Mcm.Folder.Create(dstAdminUser.Guid, null, "Users", null, 1u);

      if (rootFolder == null)
        return _destinationArchive.Mcm.Folder.Create(dstAdminUser.Guid, null, "Root", null, 1u);

      return rootFolder.ID;
    }

    private static Label CreateProjectAndLabel(SourceEzArchiveContext sourceArchive, IEnumerable<Project> dstProjects,
      IEnumerable<Label> dstLabels, UserInfo dstAdminUser)
    {
      var projectName = sourceArchive.Id;
      var project = dstProjects.SingleOrDefault(p => p.Name == projectName);

      if (project == null)
      {
        project = _destinationArchive.Mcm.Project.Set(new Chaos.Mcm.Core.Model.Project
        {
          Name = projectName
        });

        _destinationArchive.Mcm.Project.AddUser(project.Identifier.Value, dstAdminUser.Guid);
      }

      var labelName = projectName;
      var label = dstLabels.SingleOrDefault(l => l.ProjectId == project.Identifier && l.Name == labelName);
      if (label == null)
        label = _destinationArchive.Mcm.Label.Set(project.Identifier.Value,
          new Chaos.Mcm.Core.Model.Label
          {
            Name = labelName,
            ProjectId = project.Identifier.Value
          });
      
      return label;
    }

    private static void SyncMetadataSchemas(SourceEzArchiveContext sourceArchive)
    {
      foreach (var metadataSchema in sourceArchive.Mcm.MetadataSchema.Get(null))
      {
        var newId = CreateMetadaSchemaGuid(metadataSchema.Guid);

        Print($"\t Setting: {metadataSchema.Guid} => {newId}");

        _destinationArchive.Mcm.MetadataSchema.Set(new Chaos.Mcm.Core.Model.MetadataSchema
        {
          Guid = newId,
          Name = metadataSchema.Name,
          Schema = metadataSchema.Schema
        });
      }
    }

    private static Guid CreateMetadaSchemaGuid(Guid guid)
    {
      return ConvertToGuid(guid.ToString());
    }

    private static Guid ConvertToGuid(string toHash)
    {
      var data = _md5.ComputeHash(Encoding.UTF8.GetBytes(toHash));
      var newId = new Guid(data);
      return newId;
    }

    private static void SyncAccessProviders(SourceEzArchiveContext sourceArchive, IList<Destination> dstDestinations,
      IList<AccessProvider> dstAccessProviders)
    {
      var destinations = sourceArchive.Mcm.Destination.Get(null, null);
      var accessProviders = sourceArchive.Mcm.AccessProvider.Get();
      dstDestinations = _destinationArchive.Mcm.Destination.Get(null, null);

      foreach (var destination in destinations)
      {
        var name = $"{destination.Id}:{sourceArchive.Id}";
        var dstDestination = dstDestinations.SingleOrDefault(d => d.Name == name);

        var dstAPsOnDestination = dstAccessProviders.Where(ap => ap.DestinationId == dstDestination.Id).ToList();
        var srcAPsOnDestination = accessProviders.Where(ap => ap.DestinationId == destination.Id).ToList();

        // if same number then safe to update
        if (dstAPsOnDestination.Count == srcAPsOnDestination.Count)
          for (var i = 0; i < srcAPsOnDestination.Count; i++)
          {
            dstAPsOnDestination[i].BasePath = srcAPsOnDestination[i].BasePath;
            dstAPsOnDestination[i].StringFormat = srcAPsOnDestination[i].StringFormat;
            dstAPsOnDestination[i].Token = srcAPsOnDestination[i].Token;

            _destinationArchive.Mcm.AccessProvider.Set(dstAPsOnDestination[i]);
          }
        else
        {
          // else delete and insert
          foreach (var accessProvider in dstAPsOnDestination)
          {
            _destinationArchive.Mcm.AccessProvider.Delete(accessProvider.Id.Value);
          }

          foreach (var accessProvider in srcAPsOnDestination)
          {
            _destinationArchive.Mcm.AccessProvider.Set(new AccessProvider
            {
              BasePath = accessProvider.BasePath,
              StringFormat = accessProvider.StringFormat,
              DestinationId = dstDestination.Id.Value,
              Token = accessProvider.Token
            });
          }
        }
      }
    }

    private static void SyncDestinations(SourceEzArchiveContext sourceArchive, IList<Destination> dstDestinations)
    {
      var destinations = sourceArchive.Mcm.Destination.Get(null, null);

      foreach (var destination in destinations)
      {
        var name = $"{destination.Id}:{sourceArchive.Id}";
        var dstDestination = dstDestinations.SingleOrDefault(d => d.Name == name);

        if (dstDestination == null)
        {
          dstDestination = new Destination(null, destination.SubscriptionGuid, name, DateTime.Now);
          _destinationArchive.Mcm.Destination.Set(dstDestination);
        }
      }
    }

    private static EzArchiveContext CreateEzArchiveContext(string portalConntectionString)
    {
      var context = new ContextNonStatic();
      var portal = new PortalRepository().WithConfiguration(portalConntectionString);
      var mcmConfig = portal.Module.Get("MCM").ParseSettings<Chaos.Mcm.Core.Model.McmModuleConfiguration>();
      var mcm = new McmRepository().WithConfiguration(mcmConfig.ConnectionString);
      var authenticationConfig = portal.Module.Get("Authentication")
        .ParseSettings<Chaos.Authentication.Core.Model.AuthenticationSettings>();
      var authentication = new AuthenticationRepository(authenticationConfig.ConnectionString);
      var cache = new Cache();
      var vm = new ViewManager(new ConcurrentDictionary<string, IView>(), cache);
      var ezArchive = new EzMcmRepository(context, portal, mcm, authentication, vm);

      context.Repository = ezArchive;
      context.Config = portal.Module.Get("EZ-Archive").ParseSettings<Config>();

      var ezArchiveContext = new EzArchiveContext()
      {
        Portal = portal,
        Mcm = mcm,
        Authentication = authentication,
        ViewManager = vm,
        Cache = cache,
        EzArchive = ezArchive
      };

      return ezArchiveContext;
    }

    private static SourceEzArchiveContext CreateSourceArchive(NamedConnectionString portalConnectionString)
    {
      var context = new ContextNonStatic();
      var portal = new PortalRepository().WithConfiguration(portalConnectionString.ConnectionString);
      var mcmConfig = portal.Module.Get("MCM").ParseSettings<Chaos.Mcm.Core.Model.McmModuleConfiguration>();
      var mcm = new McmRepository().WithConfiguration(mcmConfig.ConnectionString);
      var authenticationConfig = portal.Module.Get("Authentication")
        .ParseSettings<Chaos.Authentication.Core.Model.AuthenticationSettings>();
      var authentication = new AuthenticationRepository(authenticationConfig.ConnectionString);
      var cache = new Cache();
      var vm = new ViewManager(new ConcurrentDictionary<string, IView>(), cache);
      var ezArchive = new EzMcmRepository(context, portal, mcm, authentication, vm);

      context.Repository = ezArchive;
      context.Config = portal.Module.Get("EZ-Archive").ParseSettings<Config>();

      var ezArchiveContext = new SourceEzArchiveContext
      {
        Portal = portal,
        Mcm = mcm,
        Authentication = authentication,
        ViewManager = vm,
        Cache = cache,
        EzArchive = ezArchive,
        Id = portalConnectionString.Name
      };

      return ezArchiveContext;
    }

    private static void PrintAsset(Asset asset)
    {
      Console.WriteLine(asset.Identifier);

      foreach (var pair in asset.Data.SelectMany(m => m.Fields))
        Console.WriteLine($"\t{pair.Key}: {pair.Value}");
    }
  }

  class SourceEzArchiveContext : EzArchiveContext
  {
    public string Id { get; set; }
  }

  class EzArchiveContext
  {
    public IPortalRepository Portal { get; set; }
    public IAuthenticationRepository Authentication { get; set; }
    public IMcmRepository Mcm { get; set; }
    public IViewManager ViewManager { get; set; }
    public ICache Cache { get; set; }
    public IEzArchiveRepository EzArchive { get; set; }
  }

  class Arguments
  {
    public IList<NamedConnectionString> Src { get; set; }
    public DestinationSettings Dst { get; set; }

    public Arguments()
    {
      Src = new List<NamedConnectionString>();
      Dst = new DestinationSettings();
    }

    public static Arguments Load(string path)
    {
      var content = System.IO.File.ReadAllText(path);
      return JsonConvert.DeserializeObject<Arguments>(content);
    }
  }

  internal class DestinationSettings
  {
    public string Endpoint { get; set; }
    public string ConnectionString { get; set; }
  }

  internal class NamedConnectionString
  {
    public string Name { get; set; }
    public string ConnectionString { get; set; }
  }
}