﻿using System.Collections.Generic;
using System.Linq;
using EzArchive.Synchronizer.Core.Exception;

namespace EzArchive.Synchronizer.Core.Model
{
  public class SearchQuery
  {
    private readonly ContextNonStatic _context;
    private const string DEFAULT_QUERY = "*:*";
    private List<string> Queries = new List<string>();

    public SearchQuery(ContextNonStatic context)
    {
      _context = context;
    }

    public static SearchQuery AllQuery(ContextNonStatic context)
    {
      return Parse(context, DEFAULT_QUERY);
    }

    public static SearchQuery Parse(ContextNonStatic context, string query)
    {
      if (string.IsNullOrEmpty(query)) query = DEFAULT_QUERY;

      var sq = new SearchQuery(context);
      sq.AndQuery(query);

      return sq;
    }

    public override string ToString()
    {
      if(Queries.Count == 1)
        return Queries.Single();

      return "(" + string.Join(")AND(", Queries) + ")";
    }

    public void AndQuery(string query, bool parseQuery = true)
    {
      if (parseQuery) Queries.Add(ParseSingleQuery(query.Trim()));
      else
      {
        if (string.IsNullOrEmpty(query)) return;

        Queries.Add(query.Trim());
      };
    }
	  
    private string ParseSingleQuery(string query)
    {
      if (query == "*:*") return query;

      var colonIndex = query.IndexOf(':');

      if (colonIndex == -1) return query.Replace("+", "%2B");
      if (colonIndex == query.Length - 1) throw new SearchQueryParserException("Query must be in the format");

      var field = query.Substring(0, colonIndex);
      var value = query.Substring(colonIndex + 1);
      var formattedValue = GetFormattedValue(value);

      var key = IndexHelper.GetIndexKey(field, _context);

      return $"{key}:{formattedValue}";
    }

    private static string GetFormattedValue(string value)
    {
      if (value.ToLower().StartsWith("between"))
      {
        var from = value.Split(' ')[1];
        var to = value.Split(' ')[2];

        return $"[{from} TO {to}]";
      }
      
      if (value.ToLower().StartsWith("exact"))
      {
        var val = value.Substring(value.IndexOf(' ')+1);

        return $"\"{val}\"";
      }

      if (value.ToLower().StartsWith("or"))
      {
        var val = value.Split(' ').Skip(1);

        return $"({string.Join(" OR ", val)})";
      }

      return value;
    }
  }
}