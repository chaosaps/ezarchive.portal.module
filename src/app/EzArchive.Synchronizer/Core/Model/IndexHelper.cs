﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace EzArchive.Synchronizer.Core.Model
{
  public static class IndexHelper
  {
	  public static string GetIndexKey(string id, ContextNonStatic context)
		{
			if (id.ToLower().StartsWith("{search}"))
			{
				var key = id.Split('.')[1];
				var sfd = context.Config.GetSearchFieldDefinition(key);
				
				return GetFormattedSearchKey(sfd.Type(context), key);
			}
			if (id.ToLower().StartsWith("{document}"))
			{
				var key = id.Split('.')[1];
				
				return "s_document_" + key.ToLower();
			}
			if (id.ToLower().StartsWith("{label}"))
				return "sm_labels";
		  if (id.ToLower().StartsWith("{projects}"))
			  return "sm_projectids";
			if (id.ToLower().StartsWith("{tags}"))
				return "sm_tags";

			var split = id.Split('.');
			var schema = split[0];
			var field = split[1];

			var type = context.Repository.Definition.GetDataDefinition(id).GetFieldDefinition(id).Type;

			return GetFormattedKey(type, schema, field);
		}

    public static string GetFormattedKey(string type, string schema, string field)
    {
      if (type == "Datetime")
        return ("d_" + schema + "_" + field).Replace(" ", "_").ToLower();
			if (type == "Text")
        return ("t_" + schema + "_" + field).Replace(" ", "_").ToLower();
	    if(type == "Table")
		    return ("sm_" + schema + "_" + field).Replace(" ", "_").ToLower();
		    
      return ("s_" + schema + "_" + field).Replace(" ", "_").ToLower();
    }

			public static IEnumerable<string> GetIndexValue(string id, Value value, ContextNonStatic context)
    {
			if (id.ToLower().StartsWith("{document}"))
				return GetFormattedValue(value, "String");
	    if (id.ToLower().StartsWith("{search}"))
	    {
		    var key = id.Split('.')[1];
		    var sfd = context.Config.GetSearchFieldDefinition(key);

		    return GetFormattedValue(value, sfd.Type(context));
	    }
	    
	    var type = context.Repository.Definition.GetDataDefinition(id).GetFieldDefinition(id).Type;

	    return GetFormattedValue(value, type);
    }
	  
	  public static IEnumerable<string> GetFormattedValue(Value value, string type)
	  {
		  if (type == "Datetime")
		  {
			  var datetime = DateTime.Parse(value.ToString(), null, DateTimeStyles.AdjustToUniversal);
			  yield return datetime.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'");
		  }
			else if (type == "Table")
		  {
			  var table = value as TableValue;

			  foreach (var row in table.Value)
				  yield return string.Join(" ", row.Select(Sanitize));
		  }
			else
		  	yield return Sanitize(value.ToString());
	  }

    public static string GetFormattedValue(string value, string type)
    {
      if (type == "Datetime")
      {
        var datetime = DateTime.Parse(value, null, DateTimeStyles.AdjustToUniversal);
        return datetime.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'");
      }

      return Sanitize(value);
    }

    private static string Sanitize(string input)
    {
      var sanitized = input
	      .Replace(",", "")
	      .Replace("\r", " ")
	      .Replace("\n", " ");

      return sanitized;
    }

	  public static string GetFormattedSearchKey(string type, string key)
	  {
		  return GetFormattedKey(type, "search", key);
	  }

	  public static string GetFormattedSortKey(string type, string key)
	  {
			return GetFormattedKey(type == "Text" ? "String" : type, "sort", key);
		}
  }
}