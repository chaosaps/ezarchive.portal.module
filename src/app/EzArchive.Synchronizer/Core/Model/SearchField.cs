﻿namespace EzArchive.Synchronizer.Core.Model
{
  public class SearchField
  {
    public string Key { get; set; }
    public string Value { get; set; }
  }
}