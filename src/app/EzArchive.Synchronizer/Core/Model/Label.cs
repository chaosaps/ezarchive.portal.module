﻿namespace EzArchive.Synchronizer.Core.Model
{
	public class Label
	{
		public string Identifier { get; set; }
		public string Name { get; set; }
		public uint ProjectId { get; set; }
	}
}
