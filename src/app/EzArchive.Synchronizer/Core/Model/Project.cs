﻿using System.Collections.Generic;

namespace EzArchive.Synchronizer.Core.Model
{
	public class Project : IIdentifiable
	{
		public string Identifier { get; set; }
		public string Name { get; set; }
		public IList<EzUser> Users { get; set; }
		public IList<Label> Labels { get; set; }

		public Project()
		{
			Users = new List<EzUser>();
			Labels = new List<Label>();
		}
	}
}