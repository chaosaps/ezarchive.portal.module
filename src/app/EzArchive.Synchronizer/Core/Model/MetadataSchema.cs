﻿using System;
using System.Xml.Linq;
using Chaos.Portal.Core.Data.Model;
using Newtonsoft.Json;

namespace EzArchive.Synchronizer.Core.Model
{
  public class MetadataSchema : AResult
  {
    public Guid Guid { get; set; }

    public string Name { get; set; }

    public XDocument SchemaXmls
    {
      get
      {
        return XDocument.Parse(Schema);
      }
      set
      {
        Schema = value.ToString(SaveOptions.DisableFormatting);
      }
    }

    public string Schema { get; set; }

    public DateTime DateCreated { get; set; }

    public MetadataSchema(Guid guid, string name, string schema, DateTime dateCreated)
    {
      Guid = guid;
      Name = name;
      Schema = schema;
      DateCreated = dateCreated;
    }

    public MetadataSchema()
    {
    }

    public T GetSchema<T>()
    {
      return JsonConvert.DeserializeObject<T>(Schema);
    }
  }
}