﻿namespace EzArchive.Synchronizer.Core.Model
{
  public class EzUser : IIdentifiable
  {
    public string Identifier { get; set; }

    public string Email { get; set; }

    public string Name { get; set; }

    public bool IsAdministrator => Permission == "Administrator";

    public string Permission { get; set; }

    public string WayfAttributes { get; set; }
    
		public string Preferences { get; set; }
  }
}