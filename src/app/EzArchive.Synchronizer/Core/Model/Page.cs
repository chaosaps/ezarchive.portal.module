﻿using System.Collections.Generic;

namespace EzArchive.Synchronizer.Core.Model
{
  public class Page<T>
  {
    public uint FoundCount { get; set; }
    public uint StartIndex { get; set; }
    public IEnumerable<T> Results { get; set; }

    public Page()
    {
      Results = new T[0];
    }
  }
}