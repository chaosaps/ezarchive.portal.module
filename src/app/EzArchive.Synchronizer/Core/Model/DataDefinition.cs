using System.Collections.Generic;
using System.Linq;
using EzArchive.Synchronizer.Core.Configuration;

namespace EzArchive.Synchronizer.Core.Model
{
	public class DataDefinition : IIdentifiable
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string[] CanWrite { get; set; }
		public uint TypeId { get; set; }
		public DefinitionType Type { get; set; }

		public IList<FieldDefinition> Fields { get; set; }

		public bool IsValid()
		{
			return !string.IsNullOrEmpty(Name) || TypeId != 0;
		}

		public DataDefinition()
		{
			Fields = new List<FieldDefinition>();
			CanWrite = new string[0];
		}

		public string Identifier
		{
			get { return Id; }
			set { Id = value; }
		}

		public FieldDefinition GetFieldDefinition(string id)
		{
			if (id.Contains('.'))
				id = id.Split('.')[1];

			var firstOrDefault = Fields.FirstOrDefault(i => i.Id == id);
			if (firstOrDefault == null)
				throw new System.InvalidOperationException("The field (" + id + ") is not located on definition (" + Name + ")");

			return firstOrDefault;
		}

		public bool CanEdit(string role)
		{
			return role != null && (!CanWrite.Any() || CanWrite.Any(r => r == role));
		}
	}

	public enum DefinitionType
	{
		Metadata = 0,
		Annotation = 1
	}
}