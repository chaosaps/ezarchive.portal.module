﻿using System.Collections.Generic;

namespace EzArchive.Synchronizer.Core.Model
{
  public class Search : IIdentifiable
  {
    public Search()
    {
      Fields = new List<SearchField>();
      Tags = new string[0];
    }

    public string Identifier { get; set; }

    public string TypeId { get; set; }

    public IList<SearchField> Fields { get; set; }

    public IEnumerable<string> Tags { get; set; }
  }
}