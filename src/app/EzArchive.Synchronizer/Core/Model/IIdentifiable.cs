namespace EzArchive.Synchronizer.Core.Model
{
  public interface IIdentifiable
  {
    string Identifier { get; set; }
  }
}