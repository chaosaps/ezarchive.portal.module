namespace EzArchive.Synchronizer.Core.Configuration
{
	public class FacetSetting
	{
		public FacetSetting()
		{
			Fields = new FacetSpecification[0];
		}

		public string Header { get; set; }
		public string Position { get; set; }
		public FacetSpecification[] Fields { get; set; }
	}

	public class FacetSpecification
	{
		public string Key { get; set; }
		public FacetType Type { get; set; }
	}

	public enum FacetType
	{
		Field = 0,
		Range = 1
	}
}