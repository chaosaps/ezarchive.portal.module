namespace EzArchive.Synchronizer.Core.Configuration
{
  public class AwsConfig
  {
    public AwsConfig()
    {
      AudioPresets = new Preset[0];
      VideoPresets = new Preset[0];
    }

    public string AccessKey { get; set; }
    public string SecretAccessKey { get; set; }
    public string UploadBucket { get; set; }
    public string PipelineId { get; set; }
    public Preset[] AudioPresets { get; set; }
    public Preset[] VideoPresets { get; set; }
  }
}