namespace EzArchive.Synchronizer.Core.Configuration
{
  public class FieldDefinition
  {
    private string _DisplayName;

    public string Id { get; set; }
    public string Type { get; set; }
    public string[] Options { get; set; }
    public string Placeholder { get; set; }
    public string Note { get; set; }
    public bool IsRequired { get; set; }

    public string DisplayName
    {
      get { return string.IsNullOrEmpty(_DisplayName) ? Id : _DisplayName; }
      set { _DisplayName = value; }
    }
  }
}
