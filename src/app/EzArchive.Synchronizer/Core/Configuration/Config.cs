using System;
using System.Collections.Generic;
using System.Linq;

namespace EzArchive.Synchronizer.Core.Configuration
{
  public class Config : Chaos.Portal.Core.Module.IModuleSettings
  {
    public Config()
    {
      SearchDefinition = new List<SearchFieldDefinition>();
      FacetSettings = new FacetSetting[0];
      Mcm = new McmConfig();
      Aws = new AwsConfig();
    }

    public bool IsValid()
    {
			return !string.IsNullOrEmpty(DisplayNotificationsLevel) && !string.IsNullOrEmpty(SearchView) && !string.IsNullOrEmpty(UserView) &&
             AdminGroupId != Guid.Empty && ContributorGroupId != Guid.Empty && UserGroupId != Guid.Empty && Mcm.UploadDestinationId != 0;
    }

    public IEnumerable<SearchFieldDefinition> SearchDefinition { get; set; }

    public SearchFieldDefinition GetSearchFieldDefinition(string displayName)
    {
      var sfd = SearchDefinition.FirstOrDefault(item => item.DisplayName == displayName);
      
      if(sfd == null) throw new System.Exception("Invalid Field Name: " + displayName);

      return sfd;
    }

		public bool ArePermissionsHandledThroughProjects { get; set; }
    public string DisplayNotificationsLevel { get; set; }
    public string LoginLevel { get; set; }
    public string ArchiveName { get; set; }
    public uint MaxNumberOfPagesShown { get; set; }
    public uint[] PageSizes { get; set; }
    public string[] AssetTitleKeyWords { get; set; }
    public FacetSetting[] FacetSettings { get; set; }

    public string SearchView { get; set; }
    public string UserView { get; set; }

    public bool IsSandbox { get; set; }

    public McmConfig Mcm { get; set; }

    public AwsConfig Aws { get; set; }

    public Guid AdminGroupId { get; set; }
    public Guid ContributorGroupId { get; set; }
    public Guid UserGroupId { get; set; }

    public string DefaultSortField { get; set; }

    public uint MinimumFacetHitsForEmptySearch { get; set; }
    public string EmbedMode { get; set; }
  }

  public class SearchFieldDefinition
  {
    public string DisplayName { get; set; }
    public string[] Ids { get; set; }
    public bool IsSortable { get; set; }
    public bool IsVisible { get; set; }

    public SearchFieldDefinition(ContextNonStatic context)
    {
      IsVisible = true;
    }

    public string Type(ContextNonStatic context)
    {
        var firstId = Ids.First();
        return context.Repository.Definition.GetDataDefinition(firstId).GetFieldDefinition(firstId).Type;
    }
  }
}