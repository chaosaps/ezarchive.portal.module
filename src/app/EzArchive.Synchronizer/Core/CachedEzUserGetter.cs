﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace EzArchive.Synchronizer.Core
{
  public class CachedEzUserGetter : Data.IEzUserGetter
  {
    private readonly Data.IEzUserGetter _userGetter;
    private IDictionary<Guid, Model.EzUser> _cache;
    
    public CachedEzUserGetter(Data.IEzUserGetter userGetter)
    {
      _cache = new ConcurrentDictionary<Guid, Model.EzUser>();
      _userGetter = userGetter;
    }

    public Model.EzUser Get(Guid id)
    {
      if (_cache.ContainsKey(id))
        return _cache[id];

      var user = _userGetter.Get(id);
      
      _cache.Add(id, user);

      return user;
    }
  }
}