﻿using EzArchive.Synchronizer.Core.Configuration;
using EzArchive.Synchronizer.Data;

namespace EzArchive.Synchronizer.Core
{
  public static class Context1
  {
    static Context1()
    {
      Config = new Config();
    }

    public static IEzArchiveRepository Repository { get; set; }
    public static Config Config { get; set; }
  }
  
  public class ContextNonStatic
  {
    public ContextNonStatic()
    {
      Config = new Config();
    }

    public IEzArchiveRepository Repository { get; set; }
    public Config Config { get; set; }
  }
}