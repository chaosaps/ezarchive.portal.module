﻿using System.Runtime.Serialization;

namespace EzArchive.Synchronizer.Core.Exception
{
  public class SearchQueryParserException : System.Exception
  {
    public SearchQueryParserException()
    {
    }

    public SearchQueryParserException(string message) : base(message)
    {
    }

    public SearchQueryParserException(string message, System.Exception innerException) : base(message, innerException)
    {
    }

    protected SearchQueryParserException(SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }
  }
}