using System;
using System.Collections.Generic;
using Chaos.Portal.Core.Module;

namespace EzArchive.Application;

public class EzaConfig : IModuleSettings
{
	public Guid AdminGroupId { get; set; }
	public Guid ContributorGroupId { get; set; }
	public Guid UserGroupId { get; set; }
	public McmConfig Mcm { get; set; }
	public FacetSetting[] FacetSettings { get; set; }
	public string DisplayNotificationsLevel { get; set; }
	public string LoginLevel { get; set; }
	public string ArchiveName { get; set; }
	public uint MaxNumberOfPagesShown { get; set; }
	public uint[] PageSizes { get; set; }
	public string[] AssetTitleKeyWords { get; set; }
	public uint MinimumFacetHitsForEmptySearch { get; set; }
	public string EmbedMode { get; set; }
	public string SolrUrl { get; set; }
	public string SearchView { get; set; }
	public string UserView { get; set; }
	public string DefaultSortField { get; set; }
	public bool ArePermissionsHandledThroughProjects { get; set; }
	public IEnumerable<SearchFieldDefinition> SearchDefinition { get; set; }
	public AwsConfig Aws { get; set; }
		public string MandrillApiKey { get; set; }
		public string FrontendUrl { get; set; }

	public bool IsValid() =>
		AdminGroupId != Guid.Empty &&
		ContributorGroupId != Guid.Empty &&
		UserGroupId != Guid.Empty &&
		Mcm.UploadDestinationId != 0 &&
			string.IsNullOrWhiteSpace(MandrillApiKey) == false &&
			string.IsNullOrWhiteSpace(FrontendUrl) == false;

	public class FacetSetting
	{
		public FacetSetting()
		{
			Fields = new FacetSpecification[0];
		}

		public string Header { get; set; }
		public string Position { get; set; }
		public FacetSpecification[] Fields { get; set; }

		public class FacetSpecification
		{
			public string Key { get; set; }
			public FacetType Type { get; set; }
		}

		public enum FacetType
		{
			Field = 0,
			Range = 1
		}
	}

	public class McmConfig
	{
		public McmConfig(
			uint profileObjectTypeId,
			uint assetFolderId,
			uint userFolderId,
			uint uploadDestinationId,
			Guid profileMetadataSchemaId,
			Guid assetMetadataSchemaId)
		{
			ProfileObjectTypeId = profileObjectTypeId;
			AssetFolderId = assetFolderId;
			UserFolderId = userFolderId;
			UploadDestinationId = uploadDestinationId;
			ProfileMetadataSchemaId = profileMetadataSchemaId;
			AssetMetadataSchemaId = assetMetadataSchemaId;
		}

		public uint ProfileObjectTypeId { get; }
		public uint AssetFolderId { get; }
		public uint UserFolderId { get; }
		public uint UploadDestinationId { get; }
		public Guid ProfileMetadataSchemaId { get; }
		public Guid AssetMetadataSchemaId { get; }
	}

	public class SearchFieldDefinition
	{
		public string DisplayName { get; }
		public string[] Ids { get; }
		public bool IsSortable { get; }
		public bool IsVisible { get; }

		public SearchFieldDefinition(string displayName, string[] ids, bool isSortable, bool isVisible = true)
		{
			DisplayName = displayName;
			Ids = ids;
			IsSortable = isSortable;
			IsVisible = isVisible;
		}
	}

	public class AwsConfig
	{
		public AwsConfig(string uploadBucket, string pipelineId,
			Preset[] audioPresets, Preset[] videoPresets)
		{
			UploadBucket = uploadBucket;
			PipelineId = pipelineId;
			AudioPresets = audioPresets;
			VideoPresets = videoPresets;
		}

		public string UploadBucket { get; }
		public string PipelineId { get; }
		public Preset[] AudioPresets { get; }
		public Preset[] VideoPresets { get; }

		public class Preset
		{
			public Preset(string id, string extension, uint formatId)
			{
				Id = id;
				Extension = extension;
				FormatId = formatId;
			}

			public string Id { get; }
			public string Extension { get; }
			public uint FormatId { get; }
		}
	}
}
