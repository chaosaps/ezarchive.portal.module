using System.IO;
using System.Threading.Tasks;
using File = EzArchive.Domain.File;

namespace EzArchive.Application;

public interface IStorage
{
	Task WriteAsync(string key, Stream stream, string contentType = "application/octet-stream");
	Task DeleteAsync(string key);
	Task<Stream> ReadAsync(File file);
}