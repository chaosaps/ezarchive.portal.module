using System.Collections.Generic;
using System.Linq;

namespace EzArchive.Application;

public class FacetSetting
{
	public FacetSetting(string header, string position, IEnumerable<FacetSpecification> fields)
	{
		Header = header;
		Position = position;
		Fields = fields.ToList();
	}

	public string Header { get; }
	public string Position { get; }
	public IReadOnlyList<FacetSpecification> Fields { get; }

	public class FacetSpecification
	{
		public FacetSpecification(string key, FacetType type)
		{
			Key = key;
			Type = type;
		}

		public string Key { get; }
		public FacetType Type { get; }
	}

	public enum FacetType
	{
		Field = 0,
		Range = 1
	}
}