using EzArchive.Domain;

namespace EzArchive.Application;

public interface IReplaceUrlPlaceholders
{
	Asset._FileReference Invoke(Asset._FileReference fileReference);
}