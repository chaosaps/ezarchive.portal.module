using System.Linq;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Labels;

public class SetLabelUseCase : ISetLabelUseCase
{
	private readonly IProjectRepository projectRepository;
	private readonly ILabelRepository labelRepository;

	public SetLabelUseCase(IProjectRepository projectRepository, ILabelRepository labelRepository)
	{
		this.projectRepository = projectRepository;
		this.labelRepository = labelRepository;
	}

	public Result<Label> Invoke(Session session, ProjectId projectId, LabelId id, string name)
	{
		if (NotMemberOfProject())
			throw new InsufficientPermissionsException("The requested project is not accessible by the User");

		var label = labelRepository.Set(projectId, id, name);

		return new Result<Label>(label);

		bool NotMemberOfProject()
		{
			var project = projectRepository.Get(projectId);
			return project == null || project.Users.Any(u => u.Id == session.UserId) == false;
		}
	}
}