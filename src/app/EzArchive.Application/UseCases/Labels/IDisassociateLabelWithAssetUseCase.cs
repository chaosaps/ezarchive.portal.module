using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Labels;

public interface IDisassociateLabelWithAssetUseCase
{
	Result Invoke(Session session, LabelId labelId, AssetId assetId);
}