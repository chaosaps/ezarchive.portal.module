using System.Collections.Generic;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Labels;

public interface ILabelRepository
{
	IEnumerable<Label> Get(AssetId id);
	void SetAssociation(LabelId id, AssetId assetId);
	void DeleteAssociation(LabelId id, AssetId assetId);
	bool Delete(LabelId id);
	Label Set(ProjectId projectId, LabelId id, string name);
}