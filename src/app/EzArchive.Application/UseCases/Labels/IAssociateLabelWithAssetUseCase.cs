using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Labels;

public interface IAssociateLabelWithAssetUseCase
{
	Result Invoke(Session session, LabelId labelId, AssetId assetId);
}