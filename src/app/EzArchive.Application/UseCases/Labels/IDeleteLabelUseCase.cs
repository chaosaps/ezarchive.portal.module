using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Labels;

public interface IDeleteLabelUseCase
{
	Result Invoke(Session session, LabelId labelId);
}