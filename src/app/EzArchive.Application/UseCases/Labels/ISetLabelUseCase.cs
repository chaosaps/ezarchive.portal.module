using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Labels;

public interface ISetLabelUseCase
{
	Result<Label> Invoke(Session session, ProjectId projectId, LabelId id, string name);
}