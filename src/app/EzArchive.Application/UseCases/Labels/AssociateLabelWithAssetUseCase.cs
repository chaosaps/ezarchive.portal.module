using System.Linq;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Application.UseCases.Search;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Labels;

public class AssociateLabelWithAssetUseCase : IAssociateLabelWithAssetUseCase
{
	private readonly IProjectRepository projectRepository;
	private readonly ILabelRepository labelRepository;
	private readonly IAssetRepository assetRepository;
	private readonly ISearchRepository searchRepository;

	public AssociateLabelWithAssetUseCase(
		IProjectRepository projectRepository,
		ILabelRepository labelRepository,
		IAssetRepository assetRepository,
		ISearchRepository searchRepository)
	{
		this.projectRepository = projectRepository;
		this.labelRepository = labelRepository;
		this.assetRepository = assetRepository;
		this.searchRepository = searchRepository;
	}

	public Result Invoke(Session session, LabelId labelId, AssetId assetId)
	{
		if (NotMemberOfProject())
			throw new InsufficientPermissionsException("User is not a member of the Project");

		labelRepository.SetAssociation(labelId, assetId);
		searchRepository.Set(assetRepository.Get(assetId));

		return Result.Ok();

		bool NotMemberOfProject()
		{
			var project = projectRepository.Get(labelId);
			return project == null || project.Users.Any(u => u.Id == session.UserId) == false;
		}
	}
}