using System.Linq;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Labels;

public class DeleteLabelUseCase : IDeleteLabelUseCase
{
	private readonly IProjectRepository projectRepository;
	private readonly ILabelRepository labelRepository;

	public DeleteLabelUseCase(IProjectRepository projectRepository, ILabelRepository labelRepository)
	{
		this.projectRepository = projectRepository;
		this.labelRepository = labelRepository;
	}

	public Result Invoke(Session session, LabelId labelId)
	{
		if (NotMemberOfProject())
			throw new InsufficientPermissionsException("User is not a member of the Project");

		labelRepository.Delete(labelId);

		return Result.Ok();

		bool NotMemberOfProject()
		{
			var project = projectRepository.Get(labelId);
			return project == null || project.Users.Any(u => u.Id == session.UserId) == false;
		}
	}
}