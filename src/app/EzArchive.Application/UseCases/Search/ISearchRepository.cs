using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Chaos.Portal.Core.Indexing;
using Chaos.Portal.Core.Indexing.Solr.Response;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Search;

public interface ISearchRepository
{
	void Set(Asset asset);
	void Set(IEnumerable<Asset> asset, Action<int> progress = null);
	void Delete();
	void Delete(AssetId assetId);
	Task<Page<EZArchive.Domain.Search>> SearchAsync(IQuery query);
	Task<FacetResponse> FacetAsync(IQuery query);
}