using System.Collections.Generic;
using System.Threading.Tasks;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Search;

public interface ISearchUseCase
{
	Task<Result<Page<EZArchive.Domain.Search>>> InvokeAsync(Session session,
		SearchQuery searchQuery,
		SearchQuery searchQueryFilter,
		IList<string> facetsList,
		string sort,
		string tag,
		Between between,
		PageRequest pageRequest);
}