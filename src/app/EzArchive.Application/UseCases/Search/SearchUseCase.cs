using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Chaos.Portal.Core.Indexing.Solr.Request;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Application.UseCases.SearchDefinitions;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Search;

public class SearchUseCase : ISearchUseCase
{
	private readonly ISearchRepository searchRepository;
	private readonly ISearchDefinitionRepository searchDefinitionRepository;
	private readonly IProjectRepository projectRepository;
	private readonly string defaultSortField;
	private readonly bool permissionsAreHandledThroughProjects;

	public SearchUseCase(
		ISearchRepository searchRepository,
		ISearchDefinitionRepository searchDefinitionRepository,
		IProjectRepository projectRepository,
		string defaultSortField,
		bool permissionsAreHandledThroughProjects)
	{
		this.searchRepository = searchRepository;
		this.searchDefinitionRepository = searchDefinitionRepository;
		this.projectRepository = projectRepository;
		this.defaultSortField = defaultSortField;
		this.permissionsAreHandledThroughProjects = permissionsAreHandledThroughProjects;
	}

	public async Task<Result<Page<EZArchive.Domain.Search>>> InvokeAsync(
		Session session,
		SearchQuery searchQuery,
		SearchQuery searchQueryFilter,
		IList<string> facetsList,
		string sort,
		string tag,
		Between between,
		PageRequest pageRequest)
	{
		ApplyTag();
		ApplyProjects();
		ApplyFacets();
		ApplyDateRestrictions();

		var eDismaxQuery = new EDismaxQuery
		{
			Query = searchQuery.ToString(),
			QueryFields = "Fulltext",
			Filter = searchQueryFilter.ToString(),
			Sort = Sort(),
			PageIndex = pageRequest.PageIndex,
			PageSize = pageRequest.PageSize
		};

		var page = await searchRepository.SearchAsync(eDismaxQuery);

		return new Result<Page<EZArchive.Domain.Search>>(
			new Page<EZArchive.Domain.Search>(
				page.FoundCount,
				page.StartIndex,
				page.Results));

		void ApplyTag()
		{
			if (tag != null)
				searchQueryFilter.AndQuery("{Tags}:EXACT " + tag);
		}

		void ApplyProjects()
		{
			if (permissionsAreHandledThroughProjects)
			{
				var projects = projectRepository.Get(session.UserId);
				searchQueryFilter.AndQuery($"{{Projects}}:OR {string.Join(" ", projects.Select(p => p.Id.Value))}");
			}
		}

		void ApplyFacets()
		{
			searchQueryFilter.AndQuery(CreateFacets(facetsList), false);
		}

		void ApplyDateRestrictions()
		{
			if (between != null)
			{
				if (between?.HasValue == false)
					return;

				var singleDate = $"(d_search_udsendelsesdato:[{between.From} TO {between.To}])";
				var programmes = BetweenDateFields("d_programoversigt_larm_metadata_startdate",
					"d_programoversigt_larm_metadata_enddate");
				searchQueryFilter.AndQuery(
					$"({singleDate}OR{programmes})",
					false);
			}

			string BetweenDateFields(string startDateField, string endDateField) =>
				$"(" +
				$"{startDateField}:[{between.From} TO {between.To}]" +
				$")" +
				$"OR" +
				$"(" +
				$"{endDateField}:[{between.From} TO {between.To}]" +
				$")" +
				$")" +
				$"OR" +
				$"(" +
				$"(" +
				$"{startDateField}:[* TO {between.From}]" +
				$")" +
				$"AND" +
				$"(" +
				$"{endDateField}:[{between.To} TO *]" +
				$")";
		}

		// When specified Sort is always used.
		// When not specified the default sort is used if no search criteria are specified,
		// 		otherwise no sorting is set, and the result will be sorted by relevance
		string Sort()
		{
			// When sort is set, use it to define the sorting
			if (string.IsNullOrEmpty(sort) == false)
			{
				var posOfSeparator = sort.LastIndexOf(" ");
				var displayName = sort.Substring(0, posOfSeparator).Trim();
				var sortOrder = sort.Substring(posOfSeparator).Trim();
				return GenerateSortValue(displayName, sortOrder);
			}

			var noSearchCriteria = searchQuery.ToString() == "*:*";
			return noSearchCriteria
				? defaultSortField
				: ""; // sort by relevance
		}

		string GenerateSortValue(string displayName, string sortOrder)
		{
			var field = searchDefinitionRepository.Get(displayName);

			if (field.IsSortable)
				return IndexHelper.GetFormattedSortKey(field.Type, field.DisplayName.Replace(" ", "_").ToLower()) +
				       " " +
				       sortOrder;

			throw new SearchQuery.ParsingException(displayName);
		}

		string CreateFacets(IList<string> facets)
		{
			if (!facets.Any()) return "";

			var sq = new SearchQuery();

			foreach (var facet in facets)
				sq.AndQuery(facet.Replace(":", ":EXACT "));

			return sq.ToString();
		}
	}
}