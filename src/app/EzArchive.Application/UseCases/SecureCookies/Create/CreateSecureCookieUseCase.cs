using System;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.SecureCookies.Create;

public class CreateSecureCookieUseCase : ICreateSecureCookieUseCase
{
	private readonly ISecureCookieCreator secureCookieCreator;
	private readonly ISecureCookieGetter secureCookieGetter;

	public CreateSecureCookieUseCase(ISecureCookieCreator secureCookieCreator, ISecureCookieGetter secureCookieGetter)
	{
		this.secureCookieCreator = secureCookieCreator;
		this.secureCookieGetter = secureCookieGetter;
	}

	public Result<SecureCookie> Invoke(UserId userId, SessionId sessionId)
	{
		var id = new SecureCookieId(Guid.NewGuid());
		var passwordGuid = new PasswordGuid(Guid.NewGuid());

		secureCookieCreator.Create(new CreateSecureCookieRequest(
			id, userId, sessionId, passwordGuid));

		var secureCookie = secureCookieGetter.Get(id, userId, passwordGuid);

		return new Result<SecureCookie>(secureCookie);
	}
}