using EzArchive.Domain;

namespace EzArchive.Application.UseCases.SecureCookies.Create;

public class CreateSecureCookieRequest
{
	public SecureCookieId Id { get; }
	public UserId UserId { get; }
	public SessionId SessionId { get; }
	public PasswordGuid PasswordGuid { get; }

	public CreateSecureCookieRequest(
		SecureCookieId id,
		UserId userId,
		SessionId sessionId,
		PasswordGuid passwordGuid)
	{
		Id = id;
		UserId = userId;
		PasswordGuid = passwordGuid;
		SessionId = sessionId;
	}
}