using EzArchive.Domain;

namespace EzArchive.Application.UseCases.SecureCookies.Create;

public interface ICreateSecureCookieUseCase
{
	Result<SecureCookie> Invoke(UserId userId, SessionId sessionId);
}