using EzArchive.Application.UseCases.SecureCookies.Create;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.SecureCookies;

public interface ISecureCookieRepository : ISecureCookieGetter, ISecureCookieCreator, ISecureCookieConsumer
{
}

public interface ISecureCookieGetter
{
	SecureCookie Get(SecureCookieId id, UserId userId, PasswordGuid passwordGuid);
	SecureCookie Get(SecureCookieId id, PasswordGuid passwordGuid);
}

public interface ISecureCookieCreator
{
	void Create(CreateSecureCookieRequest createSecureCookieRequest);
}

public interface ISecureCookieConsumer
{
	void Use(SecureCookieId id, UserId userId);
}