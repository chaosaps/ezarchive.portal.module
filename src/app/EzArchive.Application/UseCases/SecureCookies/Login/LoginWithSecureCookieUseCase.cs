using System;
using Chaos.Authentication.Exception;
using EzArchive.Application.UseCases.SecureCookies.Create;
using EzArchive.Application.UseCases.Sessions;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.SecureCookies.Login;

public class LoginWithSecureCookieUseCase : ILoginWithSecureCookieUseCase
{
	private readonly ISecureCookieRepository secureCookieRepository;
	private readonly ISessionUpdater sessionUpdater;

	public LoginWithSecureCookieUseCase(
		ISecureCookieRepository secureCookieRepository,
		ISessionUpdater sessionUpdater)
	{
		this.secureCookieRepository = secureCookieRepository;
		this.sessionUpdater = sessionUpdater;
	}

	public Result<SecureCookie> Login(SessionId sessionId, SecureCookieId id, PasswordGuid passwordGuid)
	{
		var secureCookie = secureCookieRepository.Get(id, passwordGuid);

		if (secureCookie == null)
			throw new LoginException("Cookie not found");

		secureCookieRepository.Use(id, secureCookie.UserId);

		if (secureCookie.UsedOn.HasValue)
			throw new SecureCookieAlreadyConsumedException("All the users cookies has been deleted");

		var newPasswordGuid = new PasswordGuid(Guid.NewGuid());
		secureCookieRepository.Create(
			new CreateSecureCookieRequest(id, secureCookie.UserId, sessionId, newPasswordGuid));

		sessionUpdater.Update(sessionId, secureCookie.UserId);

		var newSecureCookie = secureCookieRepository.Get(id, newPasswordGuid);

		return new Result<SecureCookie>(newSecureCookie);
	}
}