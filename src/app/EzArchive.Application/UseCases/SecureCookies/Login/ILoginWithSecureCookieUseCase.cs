using EzArchive.Domain;

namespace EzArchive.Application.UseCases.SecureCookies.Login;

public interface ILoginWithSecureCookieUseCase
{
	Result<SecureCookie> Login(SessionId sessionId, SecureCookieId id, PasswordGuid passwordGuid);
}