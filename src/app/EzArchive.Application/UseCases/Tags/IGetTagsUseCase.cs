using System.Collections.Generic;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Facets;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Tags;

public interface IGetTagsUseCase
{
	Task<Result<IReadOnlyList<FacetResponse.Facet>>> InvokeAsync(Session session, string tagPrefix);
}