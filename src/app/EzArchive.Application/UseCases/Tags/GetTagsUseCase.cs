using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Chaos.Portal.Core.Indexing.Solr.Request;
using EzArchive.Application.UseCases.Facets;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Application.UseCases.Search;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Tags;

public class GetTagsUseCase : IGetTagsUseCase
{
	private readonly ISearchRepository searchRepository;
	private readonly IProjectRepository projectRepository;
	private readonly bool permissionsAreHandledThroughProjects;
	private string tagsKey = "sm_tags";

	public GetTagsUseCase(
		ISearchRepository searchRepository,
		IProjectRepository projectRepository,
		bool permissionsAreHandledThroughProjects)
	{
		this.searchRepository = searchRepository;
		this.projectRepository = projectRepository;
		this.permissionsAreHandledThroughProjects = permissionsAreHandledThroughProjects;
	}

	public async Task<Result<IReadOnlyList<FacetResponse.Facet>>> InvokeAsync(Session session, string tagPrefix)
	{
		var query = new SolrQuery
		{
			Query = "*:*",
			Filter = Filter().ToString(),
			Facet = $"field:{tagsKey}&facet.mincount=1&facet.prefix={tagPrefix?.ToLower()}",
			PageSize = 0
		};

		SearchQuery Filter() =>
			permissionsAreHandledThroughProjects
				? new SearchQuery().ApplyFilterByProjects(projectRepository.Get(session.UserId))
				: new SearchQuery();

		var result = (await searchRepository.FacetAsync(query))
			.FacetFieldsResult
			.SelectMany(field => field.Facets)
			.Select(facet =>
				new FacetResponse.Facet(facet.Value, facet.Count))
			.ToList();
		return new Result<IReadOnlyList<FacetResponse.Facet>>(result);
	}
}