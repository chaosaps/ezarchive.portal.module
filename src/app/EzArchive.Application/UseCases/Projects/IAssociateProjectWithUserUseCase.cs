using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Projects;

public interface IAssociateProjectWithUserUseCase
{
	Result Invoke(Session session, ProjectId id, UserId userId);
}