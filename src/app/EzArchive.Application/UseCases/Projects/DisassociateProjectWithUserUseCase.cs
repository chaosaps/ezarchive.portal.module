using System.Linq;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Projects;

public class DisassociateProjectWithUserUseCase : IDisassociateProjectWithUserUseCase
{
	private readonly IProjectRepository projectRepository;

	public DisassociateProjectWithUserUseCase(IProjectRepository projectRepository)
	{
		this.projectRepository = projectRepository;
	}

	public Result Invoke(Session session, ProjectId id, UserId userId)
	{
		if (CurrentUserIsNotMemberOfProject())
			throw new InsufficientPermissionsException("User is not a member of the Project");

		projectRepository.RemoveUser(id, userId);

		return Result.Ok();

		bool CurrentUserIsNotMemberOfProject()
		{
			return projectRepository.Get(id).Users.Any(u => u.Id == session.UserId) == false;
		}
	}
}