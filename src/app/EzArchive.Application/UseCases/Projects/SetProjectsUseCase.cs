using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Projects;

public class SetProjectsUseCase : ISetProjectsUseCase
{
	private readonly IProjectRepository projectRepository;

	public SetProjectsUseCase(IProjectRepository projectRepository)
	{
		this.projectRepository = projectRepository;
	}

	public Result<Project> Invoke(Session session, ProjectId id, string name)
	{
		var project = projectRepository.Set(id, name);

		if (id == null)
			projectRepository.AddUser(project.Id, session.UserId);

		var result = projectRepository.Get(project.Id);

		return new Result<Project>(result);
	}
}