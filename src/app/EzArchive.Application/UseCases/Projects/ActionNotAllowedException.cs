using System;

namespace EzArchive.Application.UseCases.Projects;

public class ActionNotAllowedException : Exception
{
	public ActionNotAllowedException(string message) : base(message)
	{
	}
}