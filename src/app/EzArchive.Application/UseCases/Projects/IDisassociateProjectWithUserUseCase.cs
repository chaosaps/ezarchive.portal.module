using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Projects;

public interface IDisassociateProjectWithUserUseCase
{
	Result Invoke(Session session, ProjectId id, UserId userId);
}