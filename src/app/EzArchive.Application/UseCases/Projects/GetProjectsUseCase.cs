using System.Collections.Generic;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Projects;

public class GetProjectsUseCase : IGetProjectsUseCase
{
	private readonly IProjectRepository projectRepository;

	public GetProjectsUseCase(IProjectRepository projectRepository)
	{
		this.projectRepository = projectRepository;
	}

	public Result<IEnumerable<Project>> Invoke(Session session)
	{
		var results = projectRepository.Get(session.UserId);
		return new Result<IEnumerable<Project>>(results);
	}
}