using System.Collections.Generic;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Projects;

public interface IGetProjectsUseCase
{
	Result<IEnumerable<Project>> Invoke(Session session);
}