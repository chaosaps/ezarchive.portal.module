using System.Collections.Generic;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Projects;

public interface IProjectRepository
{
	IReadOnlyList<Project> Get(UserId userId);
	Project Get(ProjectId id);
	Project Get(LabelId labelId);
	Project Set(ProjectId id, string name);
	void AddUser(ProjectId id, UserId userId);
	void Delete(ProjectId id);
	void RemoveUser(ProjectId id, UserId userId);
}