using System.Linq;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Projects;

public class DeleteProjectUseCase : IDeleteProjectUseCase
{
	private readonly IProjectRepository projectRepository;

	public DeleteProjectUseCase(IProjectRepository projectRepository)
	{
		this.projectRepository = projectRepository;
	}

	public Result Invoke(Session session, ProjectId id)
	{
		var project = projectRepository.Get(id);

		if (NoAccessToProject())
			throw new InsufficientPermissionsException("User is not a member of the Project");

		if (HasLabels())
			throw new ActionNotAllowedException("A Project that is associated to Labels cannot be deleted");

		projectRepository.Delete(id);

		return Result.Ok();

		bool NoAccessToProject() =>
			project.Users
				.Any(u => u.Id == session.UserId) == false;

		bool HasLabels() =>
			project.Labels.Any();
	}
}