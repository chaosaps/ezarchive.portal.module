using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Projects;

public interface ISetProjectsUseCase
{
	Result<Project> Invoke(Session session, ProjectId id, string name);
}