using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Projects;

public interface IDeleteProjectUseCase
{
	Result Invoke(Session session, ProjectId id);
}