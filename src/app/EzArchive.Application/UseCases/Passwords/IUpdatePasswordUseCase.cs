using System.Threading.Tasks;
using EzArchive.Domain;
using EzArchive.Domain.Tickets;

namespace EzArchive.Application.UseCases.Passwords
{
	public interface IUpdatePasswordUseCase
	{
		Task<Result> InvokeAsync(TicketId ticketId, Password password);
	}
}