﻿using System;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;
using EzArchive.Domain.Emails;
using EzArchive.Domain.Tickets;

namespace EzArchive.Application.UseCases.Passwords
{
	public class RequestResetPasswordUseCase : IRequestResetPasswordUseCase
	{
		private readonly IUserRepository userGetter;
		private readonly ITicketCreator ticketCreator;
		private readonly IEmailSender emailSender;
		private readonly Uri frontendUrl;

		public RequestResetPasswordUseCase(
			IUserRepository userGetter,
			ITicketCreator ticketCreator,
			IEmailSender emailSender,
			Uri frontendUrl)
		{
			this.userGetter = userGetter;
			this.ticketCreator = ticketCreator;
			this.emailSender = emailSender;
			this.frontendUrl = frontendUrl;
		}

		public async Task<Result> InvokeAsync(EmailAddress emailAddress)
		{
			var user = userGetter.Get(emailAddress.Value);
			if (user == null)
				return Result.Ok();

			var resetPasswordTicket = new ResetPasswordTicket(new TicketId(), user.Id);
			ticketCreator.Create(resetPasswordTicket);

			await emailSender.SendAsync(new ResetPasswordEmail(user, resetPasswordTicket, frontendUrl));

			return Result.Ok();
		}
	}
}