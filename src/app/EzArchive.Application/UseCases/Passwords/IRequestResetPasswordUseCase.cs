using System.Threading.Tasks;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Passwords
{
	public interface IRequestResetPasswordUseCase
	{
		Task<Result> InvokeAsync(EmailAddress emailAddress);
	}
}