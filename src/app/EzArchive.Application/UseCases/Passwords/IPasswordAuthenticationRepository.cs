using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Passwords
{
	public interface IPasswordAuthenticationRepository
	{
		bool UserHasPasswordHash(UserId userId, HashedPassword hashedPassword);
		void Create(UserId userId, HashedPassword hashedPassword);
	}
}