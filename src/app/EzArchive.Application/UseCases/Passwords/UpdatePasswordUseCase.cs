using System.Threading.Tasks;
using EzArchive.Domain;
using EzArchive.Domain.Tickets;

namespace EzArchive.Application.UseCases.Passwords
{
	public class UpdatePasswordUseCase : IUpdatePasswordUseCase
	{
		private readonly ITicketGetter ticketGetter;
		private readonly IPasswordAuthenticationRepository passwordAuthenticationRepository;

		public UpdatePasswordUseCase(
			ITicketGetter ticketGetter,
			IPasswordAuthenticationRepository passwordAuthenticationRepository)
		{
			this.ticketGetter = ticketGetter;
			this.passwordAuthenticationRepository = passwordAuthenticationRepository;
		}

		public Task<Result> InvokeAsync(TicketId ticketId, Password password)
		{
			var ticket = ticketGetter.UseResetPasswordTicket(ticketId);
			if (ticket == null)
				return Task.FromResult(new Result(ErrorCode.TicketNotFound));

			var userId = ticket.UserId;
			passwordAuthenticationRepository.Create(userId, password.GenerateHash(userId.ToString()));

			return Task.FromResult(Result.Ok());
		}
	}
}