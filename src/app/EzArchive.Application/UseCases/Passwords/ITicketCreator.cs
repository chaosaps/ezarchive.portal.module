using EzArchive.Domain.Tickets;

namespace EzArchive.Application.UseCases.Passwords
{
	public interface ITicketCreator
	{
		void Create(Ticket ticket);
	}
	
	public interface ITicketGetter
	{
		ResetPasswordTicket UseResetPasswordTicket(TicketId ticketId);
	}

	public interface ITicketRepository : ITicketCreator, ITicketGetter
	{
	}
}