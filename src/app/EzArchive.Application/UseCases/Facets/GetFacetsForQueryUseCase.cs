using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Chaos.Portal.Core.Indexing.Responses;
using Chaos.Portal.Core.Indexing.Solr.Request;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Application.UseCases.Search;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Facets;

public class GetFacetsForQueryUseCase : IGetFacetsForQueryUseCase
{
	private readonly ISearchRepository searchRepository;
	private readonly IProjectRepository projectRepository;
	private readonly bool permissionsAreHandledThroughProjects;
	private readonly IEnumerable<FacetSetting> facetSettings;

	public GetFacetsForQueryUseCase(
		ISearchRepository searchRepository,
		IProjectRepository projectRepository,
		bool permissionsAreHandledThroughProjects,
		IEnumerable<FacetSetting> facetSettings)
	{
		this.searchRepository = searchRepository;
		this.projectRepository = projectRepository;
		this.permissionsAreHandledThroughProjects = permissionsAreHandledThroughProjects;
		this.facetSettings = facetSettings;
	}

	public async Task<Result<IReadOnlyList<FacetResponse>>> InvokeAsync(
		Session session,
		SearchQuery searchQuery,
		SearchQuery searchQueryFilter,
		IList<string> facetsList,
		string tag,
		Between between,
		FacetRange facetRange)
	{
		ApplyTag();
		ApplyProjects();
		ApplyFacets();
		ApplyDateRestrictions();

		var facetResponses = await QueryForFacets();

		return new Result<IReadOnlyList<FacetResponse>>(facetResponses);

		void ApplyTag()
		{
			if (tag != null)
				searchQueryFilter.AndQuery("{Tags}:EXACT " + tag);
		}

		void ApplyProjects()
		{
			if (permissionsAreHandledThroughProjects)
				searchQueryFilter.ApplyFilterByProjects(projectRepository.Get(session.UserId));
		}

		void ApplyFacets()
		{
			searchQueryFilter.AndQuery(CreateFacets(facetsList), false);
		}

		void ApplyDateRestrictions()
		{
			if (between != null)
			{
				if (between?.HasValue == false)
					return;

				var singleDate = $"(d_search_udsendelsesdato:[{between.From} TO {between.To}])";
				var programmes = BetweenDateFields("d_programoversigt_larm_metadata_startdate",
					"d_programoversigt_larm_metadata_enddate");
				searchQueryFilter.AndQuery(
					$"({singleDate}OR{programmes})",
					false);
			}

			string BetweenDateFields(string startDateField, string endDateField) =>
				$"(" +
				$"{startDateField}:[{between.From} TO {between.To}]" +
				$")" +
				$"OR" +
				$"(" +
				$"{endDateField}:[{between.From} TO {between.To}]" +
				$")" +
				$")" +
				$"OR" +
				$"(" +
				$"(" +
				$"{startDateField}:[* TO {between.From}]" +
				$")" +
				$"AND" +
				$"(" +
				$"{endDateField}:[{between.To} TO *]" +
				$")";
		}

		string CreateFacets(IList<string> facets)
		{
			if (!facets.Any()) return "";

			var sq = new SearchQuery();

			foreach (var facet in facets)
				sq.AndQuery(facet.Replace(":", ":EXACT "));

			return sq.ToString();
		}

		async Task<IReadOnlyList<FacetResponse>> QueryForFacets()
		{
			var result = new List<FacetResponse>();

			foreach (var facetSetting in facetSettings)
			{
				var eDismaxQuery = new EDismaxQuery
				{
					Query = searchQuery.ToString(),
					QueryFields = "Fulltext",
					Filter = searchQueryFilter.ToString(),
					PageIndex = 0,
					PageSize = 0,
					Facet = "&" + FacetQueryString(facetSetting, facetRange)
				};

				var facets = await searchRepository.FacetAsync(eDismaxQuery);
				var fields = GetFacetResults(facets)
					.Select(facetResult =>
						new FacetResponse.FieldFacet(
							facetSetting.Fields.Single(f => IndexHelper.GetIndexKey(f.Key) == facetResult.Value).Key,
							facetResult.Facets
								.Select(f => new FacetResponse.Facet(f.Value, f.Count))))
					.ToList();

				result.Add(new FacetResponse(
					facetSetting.Header,
					facetSetting.Position,
					fields));
			}

			return result.AsReadOnly();

			IEnumerable<IFacetsResult> GetFacetResults(Chaos.Portal.Core.Indexing.Solr.Response.FacetResponse res)
			{
				if (res == null)
					yield break;

				foreach (var result in res.FacetFieldsResult)
					yield return result;

				foreach (var result in res.FacetQueriesResult)
					yield return result;

				foreach (var result in res.Ranges)
					yield return result;
			}

			string FacetQueryString(FacetSetting facetSetting, FacetRange range)
			{
				var facetQuery = "facet.mincount=0";

				foreach (var field in facetSetting.Fields)
				{
					var indexKey = IndexHelper.GetIndexKey(field.Key);

					switch (field.Type)
					{
						case FacetSetting.FacetType.Field:
							facetQuery += "field=" + indexKey;
							break;
						case FacetSetting.FacetType.Range:
							if (range == null ||
							    string.IsNullOrEmpty(range.Start) ||
							    string.IsNullOrEmpty(range.End) ||
							    string.IsNullOrEmpty(range.Gap))
								continue;

							facetQuery += $"(range {indexKey} {range.Start} {range.End} {range.Gap})";
							break;
					}
				}

				return facetQuery;
			}
		}
	}
}