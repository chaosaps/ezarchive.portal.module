namespace EzArchive.Application.UseCases.Facets;

public class FacetRange
{
	public FacetRange(string start, string end, string gap)
	{
		Start = start;
		End = end;
		Gap = gap;
	}

	public string Start { get; }
	public string End { get; }
	public string Gap { get; }
}