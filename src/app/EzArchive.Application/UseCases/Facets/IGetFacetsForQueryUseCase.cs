using System.Collections.Generic;
using System.Threading.Tasks;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Facets;

public interface IGetFacetsForQueryUseCase
{
	Task<Result<IReadOnlyList<FacetResponse>>> InvokeAsync(
		Session session,
		SearchQuery searchQuery,
		SearchQuery searchQueryFilter,
		IList<string> facetsList,
		string tag,
		Between between,
		FacetRange facetRange);
}