using System.Collections.Generic;
using System.Linq;

namespace EzArchive.Application.UseCases.Facets;

public class FacetResponse
{
	public FacetResponse(string header, string position, IEnumerable<FieldFacet> fields)
	{
		Header = header;
		Position = position;
		Fields = fields.ToList();
	}

	public string Header { get; }
	public string Position { get; }
	public IList<FieldFacet> Fields { get; }

	public class FieldFacet
	{
		public FieldFacet(string value, IEnumerable<Facet> facets)
		{
			Value = value;
			Facets = facets.ToList();
		}

		public string Value { get; }

		public IList<Facet> Facets { get; }
	}

	public class Facet
	{
		public Facet(string key, uint count)
		{
			Key = key;
			Count = count;
		}

		public string Key { get; }

		public uint Count { get; }
	}
}