using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Assets;

public interface IReIndexAssetsUseCase
{
	Result Invoke(Session session, bool clean, uint? objectTypeId = null);
}