using System.Collections.Generic;
using System.Threading.Tasks;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Assets;

public interface IAssetRepository
{
	Asset Set(Asset asset);
	Asset Get(AssetId id);
	IEnumerable<Asset> Get(uint? objectTypeId, uint pageIndex = 0);
	Task DeleteAsync(AssetId assetId);
}