using System.Linq;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Assets;

public class SetFileAccessForAssetUseCase : ISetFileAccessForAssetUseCase
{
	private readonly IAssetRepository assetRepository;
	private readonly IUserRepository userRepository;

	public SetFileAccessForAssetUseCase(IAssetRepository assetRepository, IUserRepository userRepository)
	{
		this.assetRepository = assetRepository;
		this.userRepository = userRepository;
	}

	public Result Invoke(Session session, AssetId assetId, bool requireLogin)
	{
		if (UserDoesNotHavePermissionToEdit())
			throw new InsufficientPermissionsException("The current user has insufficient permissions");

		var asset = assetRepository.Get(assetId);
		assetRepository.Set(
			new Asset(
				asset.Id,
				asset.Type,
				session.UserId,
				requireLogin,
				asset.Tags,
				asset.Data,
				asset.AnnotationsGroups,
				asset.Files));

		return Result.Ok();

		bool UserDoesNotHavePermissionToEdit() =>
			new[] { "Contributor", "Administrator" }
				.Any(perm => perm == userRepository.Get(session.UserId).Permission) == false;
	}
}