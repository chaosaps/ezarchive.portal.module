using System.Linq;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Search;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Assets;

public class SetTagAssetUseCase : ISetTagAssetUseCase
{
	private readonly IAssetRepository assetRepository;
	private readonly IUserRepository userRepository;
	private readonly ISearchRepository searchRepository;

	public SetTagAssetUseCase(
		IAssetRepository assetRepository,
		IUserRepository userRepository,
		ISearchRepository searchRepository)
	{
		this.assetRepository = assetRepository;
		this.userRepository = userRepository;
		this.searchRepository = searchRepository;
	}

	public Result Invoke(Session session, AssetId assetId, params string[] tags)
	{
		if (UserDoesNotHavePermissionToEdit())
			throw new InsufficientPermissionsException("The current user has insufficient permissions");

		var asset = assetRepository.Get(assetId);
		var updatedAsset = new Asset(
			asset.Id,
			asset.Type,
			session.UserId,
			asset.DoFilesRequireLogin,
			tags.Distinct(),
			asset.Data,
			asset.AnnotationsGroups,
			asset.Files);

		assetRepository.Set(updatedAsset);
		searchRepository.Set(updatedAsset);

		return Result.Ok();

		bool UserDoesNotHavePermissionToEdit() =>
			new[] { "Contributor", "Administrator" }
				.Any(perm => perm == userRepository.Get(session.UserId).Permission) == false;
	}
}