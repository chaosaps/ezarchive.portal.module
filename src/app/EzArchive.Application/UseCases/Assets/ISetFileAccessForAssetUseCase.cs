using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Assets;

public interface ISetFileAccessForAssetUseCase
{
	Result Invoke(Session session, AssetId assetId, bool requireLogin);
}