using System;
using System.Collections.Generic;
using System.Linq;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.DataDefinitions;
using EzArchive.Application.UseCases.Search;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;
using EzArchive.Domain.Values;

namespace EzArchive.Application.UseCases.Assets;

public class SetAssetUseCase : ISetAssetUseCase
{
	private readonly IUserRepository userRepository;
	private readonly IAssetRepository assetRepository;
	private readonly IDataDefinitionRepository dataDefinitionRepository;
	private readonly ISearchRepository searchRepository;

	public SetAssetUseCase(
		IUserRepository userRepository,
		IAssetRepository assetRepository,
		IDataDefinitionRepository dataDefinitionRepository,
		ISearchRepository searchRepository)
	{
		this.userRepository = userRepository;
		this.assetRepository = assetRepository;
		this.dataDefinitionRepository = dataDefinitionRepository;
		this.searchRepository = searchRepository;
	}

	public Result<Asset> Invoke(Session session, IDictionary<string, string> data)
	{
		if (UserDoesNotHavePermissionToEdit())
			throw new InsufficientPermissionsException("The current user has insufficient permissions");

		var asset = IsNewAsset()
			? Create(session, data)
			: Update(session, data);

		searchRepository.Set(asset);

		return new Result<Asset>(asset);

		bool UserDoesNotHavePermissionToEdit() =>
			new[] { "Contributor", "Administrator" }
				.Any(perm => perm == userRepository.Get(session.UserId).Permission) == false;

		bool IsNewAsset() =>
			data.ContainsKey("Identifier") == false || assetRepository.Get(new AssetId(data["Identifier"])) == null;
	}

	private Asset Update(Session session, IDictionary<string, string> data)
	{
		var asset = assetRepository.Get(new AssetId(data["Identifier"]));


		var updatedAsset = new Asset(
			asset.Id,
			asset.Type,
			session.UserId,
			asset.DoFilesRequireLogin,
			asset.Tags,
			Data(),
			asset.AnnotationsGroups,
			asset.Files);

		assetRepository.Set(updatedAsset);

		return updatedAsset;

		IEnumerable<Asset._Data> Data()
		{
			var merged = new Dictionary<DataDefinition, IDictionary<string, Value>>();

			foreach (var field in ExistingFields())
			{
				if (merged.ContainsKey(field.Item1) == false)
					merged.Add(field.Item1, new Dictionary<string, Value>());

				if (merged[field.Item1].ContainsKey(field.Item2.Key) == false)
					merged[field.Item1].Add(field.Item2.Key, field.Item2.Value);
				else
					merged[field.Item1][field.Item2.Key] = field.Item2.Value;
			}

			var updatedFields = UpdatedFields().ToList();
			foreach (var field in updatedFields)
			{
				if (merged.ContainsKey(field.Item1) == false)
					merged.Add(field.Item1, new Dictionary<string, Value>());

				if (merged[field.Item1].ContainsKey(field.Item2.Key) == false)
					merged[field.Item1].Add(field.Item2.Key, field.Item2.Value);
				else
					merged[field.Item1][field.Item2.Key] = field.Item2.Value;
			}

			return merged.Select(r => new Asset._Data(r.Value, r.Key)).ToList();

			IEnumerable<(DataDefinition, KeyValuePair<string, Value>)> UpdatedFields()
			{
				var updatedFields = data
					.Where(kv => kv.Key != "Identifier" && kv.Key != "TypeId")
					.GroupBy(kv => dataDefinitionRepository.GetDataDefinition(kv.Key), kv => kv)
					.Select(kv =>
						new Asset._Data(
							kv.ToDictionary(pair => GetKey(pair), f => ToValue(kv.Key.GetFieldDefinition(f.Key), f)),
							kv.Key))
					.ToList();

				foreach (var d in updatedFields)
				foreach (var field in d.Fields)
					yield return (d.DataDefinition, field);
			}

			IEnumerable<(DataDefinition, KeyValuePair<string, Value>)> ExistingFields()
			{
				foreach (var d in asset.Data)
				foreach (var field in d.Fields)
					yield return (d.DataDefinition, field);
			}
		}
	}

	private Asset Create(Session session, IDictionary<string, string> data)
	{
		var type = new Asset._Type(data["TypeId"]);

		EnsureTypeIsValid();

		var asset = new Asset(
			data.ContainsKey("Identifier") ? new AssetId(data["Identifier"]) : AssetId.New(),
			type,
			session.UserId,
			true,
			new List<string>(),
			Data(),
			new List<Asset._AnnotationGroup>(),
			new List<Asset._FileReference>());

		assetRepository.Set(asset);

		return asset;

		void EnsureTypeIsValid()
		{
			if (dataDefinitionRepository.GetDataDefinitions().All(d => d.TypeId != type.Value))
				throw new ArgumentException($"{type.Value} does not match an existing DataDefinition", "TypeId");
		}

		IEnumerable<Asset._Data> Data() =>
			data
				.Where(kv => kv.Key != "Identifier" && kv.Key != "TypeId")
				.GroupBy(kv => dataDefinitionRepository.GetDataDefinition(kv.Key), kv => kv)
				.Select(kv =>
					new Asset._Data(kv.ToDictionary(GetKey, f => ToValue(kv.Key.GetFieldDefinition(f.Key), f)),
						kv.Key));
	}

	private static string GetKey(KeyValuePair<string, string> f) =>
		f.Key.Split(".")[1];

	private Value ToValue(FieldDefinition fieldDefinition, KeyValuePair<string, string> kv) =>
		Value.Create(fieldDefinition, kv.Value);
}