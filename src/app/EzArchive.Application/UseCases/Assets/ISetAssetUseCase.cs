using System.Collections.Generic;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Assets;

public interface ISetAssetUseCase
{
	Result<Asset> Invoke(Session session, IDictionary<string, string> data);
}