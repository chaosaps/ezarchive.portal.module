using System.Linq;
using System.Threading.Tasks;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Search;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Assets;

public class DeleteAssetUseCase : IDeleteAssetUseCase
{
	private readonly IAssetRepository assetRepository;
	private readonly IUserRepository userRepository;
	private readonly ISearchRepository searchRepository;
	private readonly IStorage storage;

	public DeleteAssetUseCase(
		IAssetRepository assetRepository,
		IUserRepository userRepository,
		ISearchRepository searchRepository,
		IStorage storage)
	{
		this.assetRepository = assetRepository;
		this.userRepository = userRepository;
		this.searchRepository = searchRepository;
		this.storage = storage;
	}

	public async Task<Result> InvokeAsync(Session session, AssetId assetId)
	{
		if (UserDoesNotHavePermissionToEdit())
			throw new InsufficientPermissionsException("The current user has insufficient permissions");

		var asset = assetRepository.Get(assetId);

		await Task.WhenAll(asset.Files
			.SelectMany(fr => fr.Destinations)
			.Select(destination => storage.DeleteAsync(destination.Url)));

		await assetRepository.DeleteAsync(assetId);
		searchRepository.Delete(assetId);

		return Result.Ok();

		bool UserDoesNotHavePermissionToEdit() =>
			new[] { "Contributor", "Administrator" }
				.Any(perm => perm == userRepository.Get(session.UserId).Permission) == false;
	}
}