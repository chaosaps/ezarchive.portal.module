using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Assets;

public interface ISetTagAssetUseCase
{
	Result Invoke(Session session, AssetId assetId, params string[] tags);
}