using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EzArchive.Application.Errors;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;
using EzArchive.Domain.Values;
using Newtonsoft.Json;

namespace EzArchive.Application.UseCases.Assets;

public class GetAssetByIdUseCase : IGetAssetByIdUseCase
{
	private const string SessionGuidMissing = "{SESSION_GUID_MISSING}";
	private const string WayfAttributesPlaceholder = "{WAYF_ATTRIBUTES}";
	private const string VisibilityKey = "__Visibility";
	private const string OwnerIdKey = "__OwnerId";

	private readonly IAssetRepository assetRepository;
	private readonly IUserRepository userRepository;

	public GetAssetByIdUseCase(IAssetRepository assetRepository, IUserRepository userRepository)
	{
		this.assetRepository = assetRepository;
		this.userRepository = userRepository;
	}

	public Result<Asset> Invoke(Session session, AssetId id)
	{
		var asset = assetRepository.Get(id);

		if (asset == null)
			throw new DataNotFoundException();

		var user = userRepository.Get(session.UserId);

		if (session.IsAnonymous && asset.DoFilesRequireLogin)
		{
			return new Result<Asset>(
				new Asset(
					asset.Id,
					asset.Type,
					asset.LastChangedByUserId,
					asset.DoFilesRequireLogin,
					asset.Tags,
					asset.Data,
					FilterAnnotations(),
					new List<Asset._FileReference>()));
		}

		return new Result<Asset>(
			new Asset(
				asset.Id,
				asset.Type,
				asset.LastChangedByUserId,
				asset.DoFilesRequireLogin,
				asset.Tags,
				asset.Data,
				FilterAnnotations(),
				asset.Files.Select(f => Replace(f, session.Id, user.WayfAttributes))));

		IList<Asset._AnnotationGroup> FilterAnnotations()
		{
			return asset.AnnotationsGroups.Select(ag =>
					new Asset._AnnotationGroup(
						ag.Name,
						ag.DefinitionId,
						ag.Annotations.Where(a => IsPubliclyVisible(a) || IsPrivateAndOwnedByCurrentUser(a))
							.ToList()))
				.ToList();
		}

		bool IsPubliclyVisible(IDictionary<string, Value> a) =>
			a.ContainsKey(VisibilityKey) == false ||
			a[VisibilityKey].ToString() == AnnotationVisibility.Public.ToString();

		bool IsPrivateAndOwnedByCurrentUser(IDictionary<string, Value> a) =>
			a.ContainsKey(OwnerIdKey) == false ||
			a.ContainsKey(VisibilityKey) == false ||
			a[VisibilityKey].ToString() == AnnotationVisibility.Private.ToString() &&
			a[OwnerIdKey].ToString() == session.UserId.Value.ToString();
	}

	public Asset._FileReference Replace(Asset._FileReference fileReference, SessionId sessionId, string wayfAttributes)
	{
		return new Asset._FileReference(
			fileReference.Id,
			fileReference.Name,
			fileReference.Type,
			fileReference.Destinations
				.Select(d =>
					new Asset._FileReference._Destination(
						d.Type,
						d.Url
							.Replace(SessionGuidMissing, sessionId.Value.ToString())
							.Replace(WayfAttributesPlaceholder, ToBase64(WayfAttributes())))).ToList());

		string ToBase64(string wayfAttributes)
		{
			var attributesAsBytes = Encoding.UTF8.GetBytes(wayfAttributes);

			return Convert.ToBase64String(attributesAsBytes);
		}

		string WayfAttributes()
		{
			if (string.IsNullOrEmpty(wayfAttributes) || wayfAttributes == "{WAYF_ATTRIBUTES_MISSING}")
				return "{WAYF_ATTRIBUTES_MISSING}";

			var attributesJson = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(wayfAttributes);

			return
				"{" +
				$"  \"eduPersonTargetedID\":[\"{WayfAttributeOrDefault(attributesJson, "eduPersonTargetedID")}\"]," +
				$"  \"eduPersonPrincipleName\":[\"{WayfAttributeOrDefault(attributesJson, "eduPersonPrincipleName")}\"]," +
				$"  \"eduPersonPrimaryAffiliation\":[\"{WayfAttributeOrDefault(attributesJson, "eduPersonPrimaryAffiliation")}\"]," +
				$"  \"schacHomeOrganization\":[\"{WayfAttributeOrDefault(attributesJson, "schacHomeOrganization")}\"]," +
				$"  \"eduPersonScopedAffiliation\":[\"{WayfAttributeOrDefault(attributesJson, "eduPersonScopedAffiliation")}\"]" +
				"}";
		}

		string WayfAttributeOrDefault(Dictionary<string, List<string>> attributesJson, string key)
		{
			if (!attributesJson.ContainsKey(key))
				return "";

			var attribute = attributesJson[key].FirstOrDefault();

			if (attribute == null) return "";

			return attribute;
		}
	}
}