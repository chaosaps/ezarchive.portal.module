using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Assets;

public interface IGetAssetByIdUseCase
{
	Result<Asset> Invoke(Session session, AssetId id);
}