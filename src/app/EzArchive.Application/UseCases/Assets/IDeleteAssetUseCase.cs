using System.Threading.Tasks;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Assets;

public interface IDeleteAssetUseCase
{
	Task<Result> InvokeAsync(Session session, AssetId assetId);
}