using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Search;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Assets;

public class ReIndexAssetsUseCase : IReIndexAssetsUseCase
{
	private readonly IAssetRepository assetRepository;
	private readonly IUserRepository userRepository;
	private readonly ISearchRepository searchRepository;

	public ReIndexAssetsUseCase(
		IAssetRepository assetRepository,
		IUserRepository userRepository,
		ISearchRepository searchRepository)
	{
		this.assetRepository = assetRepository;
		this.userRepository = userRepository;
		this.searchRepository = searchRepository;
	}

	public Result Invoke(Session session, bool clean, uint? objectTypeId = null)
	{
		if (userRepository.Get(session.UserId).IsAdministrator == false)
			throw new InsufficientPermissionsException("This action requires Administrator permission");

		if (clean)
		{
			searchRepository.Delete();
			userRepository.IndexDelete();
		}

		searchRepository.Set(assetRepository.Get(objectTypeId));
		userRepository.Index(userRepository.Get());

		return Result.Ok();
	}
}