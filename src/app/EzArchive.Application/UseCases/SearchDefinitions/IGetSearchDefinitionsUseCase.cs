using System.Collections.Generic;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.SearchDefinitions;

public interface IGetSearchDefinitionsUseCase
{
	public Result<IReadOnlyList<SearchFieldDefinition>> Invoke();
}