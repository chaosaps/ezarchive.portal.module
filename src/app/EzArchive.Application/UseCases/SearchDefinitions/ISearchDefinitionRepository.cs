using System.Collections.Generic;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.SearchDefinitions;

public interface ISearchDefinitionRepository
{
	IReadOnlyList<SearchFieldDefinition> Get();
	SearchFieldDefinition Get(string displayName);
}