using System.Collections.Generic;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.SearchDefinitions;

public class GetSearchDefinitionsUseCase : IGetSearchDefinitionsUseCase
{
	private readonly ISearchDefinitionRepository searchDefinitionRepository;

	public GetSearchDefinitionsUseCase(ISearchDefinitionRepository searchDefinitionRepository)
	{
		this.searchDefinitionRepository = searchDefinitionRepository;
	}

	public Result<IReadOnlyList<SearchFieldDefinition>> Invoke()
	{
		return new Result<IReadOnlyList<SearchFieldDefinition>>(searchDefinitionRepository.Get());
	}
}