using System.Threading.Tasks;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Formats
{
	public class GetFormatByIdUseCase : IGetFormatByIdUseCase
	{
		private readonly IFormatRepository formatRepository;

		public GetFormatByIdUseCase(IFormatRepository formatRepository)
		{
			this.formatRepository = formatRepository;
		}
		
		public Task<Result<Format>> InvokeAsync(FormatId id)
		{
			var format = formatRepository.Get(id);
			return Task.FromResult(
				new Result<Format>(
					format));
		}
	}
}