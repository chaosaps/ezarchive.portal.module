using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Formats
{
	public interface IFormatRepository
	{
		Format Get(FormatId id);
	}
}