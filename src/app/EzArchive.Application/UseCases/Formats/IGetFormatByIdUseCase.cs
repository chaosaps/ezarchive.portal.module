using System.Threading.Tasks;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Formats
{
	public interface IGetFormatByIdUseCase
	{
		Task<Result<Format>> InvokeAsync(FormatId id);
	}
}