using System.Collections.Generic;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.DataDefinitions;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Annotations;

public interface IGetAnnotationDefinitionsUseCase
{
	Task<Result<IEnumerable<DataDefinitionUserResponse>>> InvokeAsync();
}