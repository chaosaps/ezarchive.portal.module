using System.Threading.Tasks;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Annotations;

public interface IDeleteAnnotationUseCase
{
	Task<Result> InvokeAsync(Session session, AssetId assetId, AnnotationId annotationId);
}