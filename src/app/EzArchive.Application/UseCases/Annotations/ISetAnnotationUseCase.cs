using System.Collections.Generic;
using System.Threading.Tasks;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Annotations;

public interface ISetAnnotationUseCase
{
	Task<Result<AnnotationId>> InvokeAsync(
		Domain.Session session,
		AssetId assetId,
		DataDefinition.__Id definitionId,
		Dictionary<string, string> data,
		AnnotationVisibility visibility);
}