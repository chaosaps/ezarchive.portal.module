using System.Linq;
using System.Threading.Tasks;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.Errors;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;
using EzArchive.Domain.Values;

namespace EzArchive.Application.UseCases.Annotations;

public class SetPermissionForAnnotationUseCase : ISetPermissionForAnnotationUseCase
{
	private const string OwnerIdKey = "__OwnerId";
	private const string OwnerKey = "__Owner";
	private const string PermissionKey = "__Permission";
	private const string VisibilityKey = "__Visibility";

	private readonly IUserRepository userRepository;
	private readonly IAssetRepository assetRepository;

	public SetPermissionForAnnotationUseCase(IUserRepository userRepository, IAssetRepository assetRepository)
	{
		this.userRepository = userRepository;
		this.assetRepository = assetRepository;
	}

	public Task<Result> InvokeAsync(Session session, AssetId assetId, AnnotationId annotationId,
		AnnotationPermission permission)
	{
		if (UserDoesNotHavePermissionToEdit())
			throw new InsufficientPermissionsException("The current user has insufficient permissions");

		var asset = assetRepository.Get(assetId);

		if (asset?.GetAnnotation(annotationId) == null)
			throw new DataNotFoundException();

		var annotation = asset.GetAnnotation(annotationId);


		var hasNoOwner = annotation.ContainsKey(OwnerIdKey) == false;
		if (hasNoOwner)
		{
			annotation.Add(OwnerIdKey, new StringValue(session.UserId.ToString()));
			annotation.Add(PermissionKey, new StringValue(((uint)permission).ToString()));
		}

		var currentUserIsOwner = annotation.ContainsKey(OwnerIdKey) &&
		                         string.IsNullOrWhiteSpace(annotation[OwnerIdKey].ToString()) ||
		                         annotation[OwnerIdKey].ToString() == session.UserId.ToString();

		if (currentUserIsOwner == false)
			throw new InsufficientPermissionsException("Only the Owner can change the scope of an Annotation");

		if (annotation.ContainsKey(PermissionKey))
			annotation[PermissionKey] = new StringValue(((uint)permission).ToString());
		else
			annotation.Add(PermissionKey, new StringValue(((uint)permission).ToString()));

		assetRepository.Set(asset);

		return Task.FromResult(Result.Ok());

		bool UserDoesNotHavePermissionToEdit() =>
			new[] { "Contributor", "Administrator" }
				.Any(perm => perm == userRepository.Get(session.UserId).Permission) == false;
	}
}