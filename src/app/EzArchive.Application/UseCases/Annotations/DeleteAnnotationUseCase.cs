using System.Linq;
using System.Threading.Tasks;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Application.UseCases.Search;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Annotations;

public class DeleteAnnotationUseCase : IDeleteAnnotationUseCase
{
	private readonly IAssetRepository assetRepository;
	private readonly IUserRepository userRepository;
	private readonly ISearchRepository searchRepository;

	public DeleteAnnotationUseCase(
		IAssetRepository assetRepository,
		IUserRepository userRepository,
		ISearchRepository searchRepository)
	{
		this.assetRepository = assetRepository;
		this.userRepository = userRepository;
		this.searchRepository = searchRepository;
	}

	public Task<Result> InvokeAsync(Session session, AssetId assetId, AnnotationId annotationId)
	{
		if (UserDoesNotHavePermissionToEdit())
			throw new InsufficientPermissionsException("The current user has insufficient permissions");

		var asset = assetRepository.Get(assetId);

		if (asset == null)
			return Task.FromResult(new Result(ErrorCode.AssetNotFound));

		var annotation = asset.GetAnnotation(annotationId);

		if (annotation == null)
			return Task.FromResult(new Result(ErrorCode.AnnotationNotFound));

		var updatedAsset = asset.RemoveAnnotation(annotationId);

		assetRepository.Set(updatedAsset);
		searchRepository.Set(updatedAsset);

		return Task.FromResult(Result.Ok());

		bool UserDoesNotHavePermissionToEdit() =>
			new[] { "Contributor", "Administrator" }
				.Any(perm => perm == userRepository.Get(session.UserId).Permission) == false;
	}
}