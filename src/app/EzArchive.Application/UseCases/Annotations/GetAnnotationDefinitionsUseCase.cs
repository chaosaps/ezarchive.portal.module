using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.DataDefinitions;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Annotations;

public class GetAnnotationDefinitionsUseCase : IGetAnnotationDefinitionsUseCase
{
	private readonly IDataDefinitionRepository dataDefinitionRepository;

	public GetAnnotationDefinitionsUseCase(IDataDefinitionRepository dataDefinitionRepository)
	{
		this.dataDefinitionRepository = dataDefinitionRepository;
	}

	public Task<Result<IEnumerable<DataDefinitionUserResponse>>> InvokeAsync() =>
		Task.FromResult(
			new Result<IEnumerable<DataDefinitionUserResponse>>(
				dataDefinitionRepository
					.GetAnnotations()
					.Select(d => new DataDefinitionUserResponse(
						d.Id,
						d.Name,
						d.Type,
						d.TypeId,
						false,
						d.Description,
						d.Fields))));
}