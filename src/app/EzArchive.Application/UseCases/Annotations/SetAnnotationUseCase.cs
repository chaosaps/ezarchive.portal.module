using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Application.UseCases.DataDefinitions;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;
using EzArchive.Domain.Values;

namespace EzArchive.Application.UseCases.Annotations;

public class SetAnnotationUseCase : ISetAnnotationUseCase
{
	private const string OwnerIdKey = "__OwnerId";
	private const string OwnerKey = "__Owner";
	private const string PermissionKey = "__Permission";
	private const string VisibilityKey = "__Visibility";

	private readonly IUserRepository userRepository;
	private readonly IAssetRepository assetRepository;
	private readonly IDataDefinitionRepository dataDefinitionRepository;

	public SetAnnotationUseCase(
		IUserRepository userRepository,
		IAssetRepository assetRepository,
		IDataDefinitionRepository dataDefinitionRepository)
	{
		this.userRepository = userRepository;
		this.assetRepository = assetRepository;
		this.dataDefinitionRepository = dataDefinitionRepository;
	}

	public Task<Result<AnnotationId>> InvokeAsync(
		Session session,
		AssetId assetId,
		DataDefinition.__Id definitionId,
		Dictionary<string, string> data,
		AnnotationVisibility visibility)
	{
		if (UserDoesNotHavePermissionToEdit())
			throw new InsufficientPermissionsException("The current user has insufficient permissions");

		var asset = assetRepository.Get(assetId);
		var definiton = dataDefinitionRepository.GetAnnotations(definitionId);
		AnnotationId annotationId;

		if (NewAnnotation())
		{
			annotationId = AnnotationId.New();
			var annotation = new Dictionary<string, Value>
			{
				{ "Identifier", new StringValue(annotationId.Value.ToString()) },
				{ OwnerIdKey, new StringValue(session.UserId.Value.ToString()) },
				{ VisibilityKey, new StringValue(visibility.ToString()) },
				{ PermissionKey, new StringValue(((uint)AnnotationPermission.User).ToString()) }
			};

			foreach (var field in data.Where(pair =>
				         new[] { "Identifier", OwnerIdKey, OwnerKey, PermissionKey }.Any(k => k != pair.Key)))

			{
				var fieldDefinition = definiton.Fields.FirstOrDefault(f => f.Id == field.Key);
				if (fieldDefinition == null)
					continue;

				if (annotation.ContainsKey(field.Key))
					annotation[field.Key] = ToValue(fieldDefinition, field.Value);
				else
					annotation.Add(field.Key, ToValue(fieldDefinition, field.Value));
			}

			asset.SetAnnotation(definiton, annotation);
		}
		else
		{
			annotationId = new AnnotationId(data["Identifier"]);
			var annotation = asset.AnnotationsGroups
				.Where(ag => ag.DefinitionId == definitionId.Value)
				.SelectMany(ag => ag.Annotations)
				.FirstOrDefault(a => a["Identifier"].ToString() == annotationId.ToString());

			if (annotation == null)
				throw new Exception($"Annotation with id '{annotationId}' not found");

			foreach (var field in data.Where(pair =>
				         new[] { "Identifier", OwnerIdKey, OwnerKey, PermissionKey }.Any(k => k != pair.Key)))

			{
				var fieldDefinition = definiton.Fields.FirstOrDefault(f => f.Id == field.Key);
				if (fieldDefinition == null)
					continue;

				if (annotation.ContainsKey(field.Key))
					annotation[field.Key] = ToValue(fieldDefinition, field.Value);
				else
					annotation.Add(field.Key, ToValue(fieldDefinition, field.Value));
			}

			asset.SetAnnotation(definiton, annotation);
		}

		assetRepository.Set(asset);
		return Task.FromResult(new Result<AnnotationId>(annotationId));

		bool UserDoesNotHavePermissionToEdit() =>
			new[] { "Contributor", "Administrator" }
				.Any(perm => perm == userRepository.Get(session.UserId).Permission) == false;

		bool NewAnnotation() =>
			data.ContainsKey("Identifier") == false;
	}

	private Value ToValue(FieldDefinition fieldDefinition, string value) =>
		fieldDefinition.Type.ToLower() switch
		{
			"string" => new StringValue(value),
			"datetime" => new DateTimeValue(value),
			"table" => new TableValue(value),
			"boolean" => new BooleanValue(value.ToLower() == "true"),
			_ => throw new NotImplementedException($"FieldDefinition Type of '{fieldDefinition.Type}' not implemented")
		};
}