using System.Collections.Generic;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.DataDefinitions;

public interface IGetDataDefinitionUseCase
{
	Result<IReadOnlyList<DataDefinitionUserResponse>> Invoke(Session session);
}