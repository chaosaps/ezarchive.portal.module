using System.Collections.Generic;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.DataDefinitions;

public interface IDataDefinitionRepository
{
	public IReadOnlyList<DataDefinition> GetDataDefinitions();
	public DataDefinition GetDataDefinition(string id);
	IEnumerable<DataDefinition> GetAnnotations();
	DataDefinition GetAnnotations(DataDefinition.__Id definitionId);
}