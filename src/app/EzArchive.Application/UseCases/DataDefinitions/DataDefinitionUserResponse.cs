using System.Collections.Generic;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.DataDefinitions;

public class DataDefinitionUserResponse
{
	public DataDefinitionUserResponse(
		string identifier,
		string name,
		DefinitionType type,
		uint typeId,
		bool isEditable,
		string description,
		IList<FieldDefinition> fields)
	{
		Identifier = identifier;
		Name = name;
		Type = type;
		TypeId = typeId;
		IsEditable = isEditable;
		Description = description;
		Fields = fields;
	}

	public string Identifier { get; }
	public string Name { get; }
	public uint TypeId { get; }
	public DefinitionType Type { get; }
	public bool IsEditable { get; }
	public string Description { get; }
	public IList<FieldDefinition> Fields { get; }
}