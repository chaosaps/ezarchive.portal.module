using System.Collections.Generic;
using System.Linq;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.DataDefinitions;

public class GetDataDefinitionUseCase : IGetDataDefinitionUseCase
{
	private readonly IDataDefinitionRepository dataDefinitionRepository;
	private readonly IUserRepository userRepository;

	public GetDataDefinitionUseCase(IDataDefinitionRepository dataDefinitionRepository, IUserRepository userRepository)
	{
		this.dataDefinitionRepository = dataDefinitionRepository;
		this.userRepository = userRepository;
	}

	public Result<IReadOnlyList<DataDefinitionUserResponse>> Invoke(Session session)
	{
		var user = userRepository.Get(session.UserId);

		var responses = dataDefinitionRepository
			.GetDataDefinitions()
			.Select(d => new DataDefinitionUserResponse(
				d.Id,
				d.Name,
				d.Type,
				d.TypeId,
				d.CanEdit(user.Permission),
				d.Description,
				d.Fields))
			.ToList();

		return new Result<IReadOnlyList<DataDefinitionUserResponse>>(responses);
	}
}