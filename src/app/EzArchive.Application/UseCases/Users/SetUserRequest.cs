using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Users;

public class SetUserRequest
{
	public SetUserRequest(
		UserId userId, string email, string name, string permission, string preferences, Password password)
	{
		UserId = userId;
		Email = email;
		Name = name;
		Permission = permission;
		Preferences = preferences;
		Password = password;
	}

	public UserId UserId { get; }
	public string Email { get; }
	public string Name { get; }
	public string Permission { get; }
	public string Preferences { get; }
	public Password Password { get; }
}