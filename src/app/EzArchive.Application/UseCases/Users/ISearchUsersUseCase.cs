using System.Collections.Generic;
using System.Threading.Tasks;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Users;

public interface ISearchUsersUseCase
{
	Task<Domain.Result<IEnumerable<(UserId id, string name)>>>
		InvokeAsync(Session session, string query, uint pageSize);
}