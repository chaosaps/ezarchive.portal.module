using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Users;

public interface IDeleteUserUseCase
{
	Result Invoke(Session session, UserId userId);
}