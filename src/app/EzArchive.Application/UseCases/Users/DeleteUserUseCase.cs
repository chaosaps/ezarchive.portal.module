using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Users;

public class DeleteUserUseCase : IDeleteUserUseCase
{
	private readonly IUserRepository userRepository;
	private readonly IProjectRepository projectRepository;

	public DeleteUserUseCase(IUserRepository userRepository, IProjectRepository projectRepository)
	{
		this.userRepository = userRepository;
		this.projectRepository = projectRepository;
	}

	public Result Invoke(Session session, UserId userId)
	{
		if (userRepository.Get(session.UserId).IsAdministrator == false || session.UserId == userId)
			throw new InsufficientPermissionsException("This action requires Administrator permission");

		foreach (var project in projectRepository.Get(userId))
			projectRepository.RemoveUser(project.Id, userId);

		userRepository.Delete(userId);


		return Result.Ok();
	}
}