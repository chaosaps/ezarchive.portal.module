using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Users.GetCurrent;

public interface IGetCurrentUserUseCase
{
	Result<User> Invoke(UserId userId);
}