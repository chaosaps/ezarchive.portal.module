using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Users.GetCurrent;

public class GetCurrentUserUseCase : IGetCurrentUserUseCase
{
	private readonly IUserRepository userRepository;

	public GetCurrentUserUseCase(IUserRepository userRepository)
	{
		this.userRepository = userRepository;
	}

	public Result<User> Invoke(UserId userId)
	{
		var user = userRepository.Get(userId);

		return new Result<User>(user);
	}
}