using System.Collections.Generic;
using System.Threading.Tasks;
using Chaos.Portal.Core.Indexing.Solr.Request;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Users;

public interface IUserRepository
{
	User Get(UserId userId);
	User Set(User user);
	IEnumerable<User> Get();
	User Get(string email);
	void SetPassword(string email, HashedPassword password);
	void Delete(UserId userId);
	Task<IEnumerable<(UserId id, string name)>> SearchAsync(SolrQuery query);
	void Index(User user);
	void Index(IEnumerable<User> users);
	void IndexDelete();
}
