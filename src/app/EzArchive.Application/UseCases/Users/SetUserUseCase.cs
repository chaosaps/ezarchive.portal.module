using Chaos.Portal.Core.Exceptions;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Users;

public class SetUserUseCase : ISetUserUseCase
{
	private readonly IUserRepository userRepository;
	private readonly UserId anonymousUserId;

	public SetUserUseCase(IUserRepository userRepository, UserId anonymousUserId)
	{
		this.userRepository = userRepository;
		this.anonymousUserId = anonymousUserId;
	}

	public Result<User> Invoke(Session session, SetUserRequest request)
	{
		var user = userRepository.Get(session.UserId);

		if (CurrentUserIsNotAdministrator() && IsCreating() ||
		    CurrentUserIsNotAdministrator() && IsUpdating() && IsUpdatingSelf() == false)
			throw new InsufficientPermissionsException("This action requires Administrator permission");

		if (request.UserId == anonymousUserId)
			throw new InsufficientPermissionsException("Can't change anonymous user");

		return IsCreating()
			? new Result<User>(Create(request))
			: new Result<User>(Update(user, request));

		bool IsCreating() =>
			request.UserId == null;

		bool CurrentUserIsNotAdministrator() =>
			user.IsAdministrator == false;

		bool IsUpdating() =>
			request.UserId != null;

		bool IsUpdatingSelf() =>
			session.UserId == request.UserId;
	}

	private User Create(SetUserRequest request)
	{
		var user = userRepository.Set(new User(
			null,
			request.Email,
			request.Name,
			request.Permission,
			request.Preferences,
			""));

		if (request.Password != null)
			userRepository.SetPassword(
				user.Email,
				request.Password.GenerateHash(user.Id.Value.ToString()));

		return user;
	}

	private User Update(User requester, SetUserRequest request)
	{
		var userToUpdate = userRepository.Get(request.UserId);

		if (string.IsNullOrEmpty(request.Permission) == false &&
		    userToUpdate.Permission != request.Permission &&
		    (requester.IsAdministrator == false ||
		     requester.Id == request.UserId))
			throw new InsufficientPermissionsException("This action requires Administrator permission");

		if (request.Password != null)
			userRepository.SetPassword(
				userToUpdate.Email,
				request.Password.GenerateHash(userToUpdate.Id.Value.ToString()));

		return userRepository.Set(new User(
			userToUpdate.Id,
			request.Email ?? userToUpdate.Email,
			request.Name ?? userToUpdate.Name,
			request.Permission ?? userToUpdate.Permission,
			request.Preferences ?? userToUpdate.Preferences,
			userToUpdate.WayfAttributes));
	}
}