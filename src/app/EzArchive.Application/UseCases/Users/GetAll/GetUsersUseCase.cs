using System.Collections.Generic;
using System.Linq;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Users.GetAll;

public class GetUsersUseCase : IGetUsersUseCase
{
	private readonly IUserRepository userRepository;
	private readonly UserId anonymousUserId;

	public GetUsersUseCase(IUserRepository userRepository, UserId anonymousUserId)
	{
		this.userRepository = userRepository;
		this.anonymousUserId = anonymousUserId;
	}

	public Result<IEnumerable<User>> Invoke(UserId userId)
	{
		var user = userRepository.Get(userId);
		if (user.IsAdministrator == false)
			throw new InsufficientPermissionsException("This action requires Administrator permission");

		var results = userRepository
			.Get()
			.Where(u => u.Id != anonymousUserId);

		return new Result<IEnumerable<User>>(results);
	}
}