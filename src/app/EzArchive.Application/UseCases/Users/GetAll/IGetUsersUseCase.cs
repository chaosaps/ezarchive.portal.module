using System.Collections.Generic;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Users.GetAll;

public interface IGetUsersUseCase
{
	Result<IEnumerable<User>> Invoke(UserId userId);
}