using System.Collections.Generic;
using System.Threading.Tasks;
using Chaos.Portal.Core.Indexing.Solr.Request;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Users;

public class SearchUsersUseCase : ISearchUsersUseCase
{
	private readonly IUserRepository userRepository;

	public SearchUsersUseCase(IUserRepository userRepository)
	{
		this.userRepository = userRepository;
	}

	public async Task<Result<IEnumerable<(UserId id, string name)>>> InvokeAsync(
		Session session, string q, uint pageSize)
	{
		var query = new SolrQuery
		{
			Query = $"t_name:{q}*",
			PageSize = pageSize
		};

		var result = await userRepository.SearchAsync(query);

		return new Result<IEnumerable<(UserId id, string name)>>(result);
	}
}