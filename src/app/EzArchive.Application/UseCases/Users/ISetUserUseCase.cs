using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Users;

public interface ISetUserUseCase
{
	Result<User> Invoke(Session session, SetUserRequest request);
}