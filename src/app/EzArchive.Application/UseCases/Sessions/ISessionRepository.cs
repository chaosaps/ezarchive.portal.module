using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Sessions;

public interface ISessionRepository : ISessionCreator, ISessionGetter, ISessionUpdater
{
}

public interface ISessionCreator
{
	Session Create(UserId userId);
}

public interface ISessionGetter
{
	Session Get(SessionId sessionId);
}

public interface ISessionUpdater
{
	void Update(SessionId sessionId, UserId userId);
}