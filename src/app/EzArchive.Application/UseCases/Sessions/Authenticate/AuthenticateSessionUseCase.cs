using System;
using Chaos.Authentication.Exception;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Sessions.Authenticate;

public class AuthenticateSessionUseCase : IAuthenticateSessionUseCase
{
	private readonly ISessionUserGetter sessionUserGetter;
	private readonly ISessionRepository sessionRepository;
	private readonly ICredentialsValidator credentialsValidator;

	public AuthenticateSessionUseCase(
		ISessionUserGetter sessionUserGetter,
		ISessionRepository sessionRepository,
		ICredentialsValidator credentialsValidator)
	{
		this.sessionUserGetter = sessionUserGetter;
		this.sessionRepository = sessionRepository;
		this.credentialsValidator = credentialsValidator;
	}

	public Result<(Session Session, SessionUser User)> Invoke(SessionId sessionId, UserName userName, Password password)
	{
		var user = GetUser();

		if (PasswordIsInvalid())
			throw LoginException();

		AuthenticateSession();

		return new Result<(Session, SessionUser)>((sessionRepository.Get(sessionId), user));

		bool PasswordIsInvalid() =>
			credentialsValidator.AreCredentialsValid(user.UserId, password) == false;

		void AuthenticateSession()
		{
			try
			{
				sessionRepository.Update(sessionId, user.UserId);
			}
			catch (SessionDoesNotExistException)
			{
				throw new SessionDoesNotExistException("SessionGUID is invalid or has expired");
			}
		}

		SessionUser GetUser()
		{
			try
			{
				return sessionUserGetter.Get(userName);
			}
			catch (ArgumentException e) when (e.Message == "Email not found")
			{
				throw LoginException();
			}
		}
	}

	private static LoginException LoginException() =>
		new LoginException("Login failed, either email or password is incorrect");
}