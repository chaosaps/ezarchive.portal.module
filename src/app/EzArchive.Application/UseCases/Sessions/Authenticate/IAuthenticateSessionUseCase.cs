using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Sessions.Authenticate;

public interface IAuthenticateSessionUseCase
{
	Result<(Session Session, SessionUser User)> Invoke(SessionId sessionId, UserName userName, Password password);
}