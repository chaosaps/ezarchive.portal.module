using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Sessions.Authenticate;

public interface ISessionUserGetter
{
	SessionUser Get(UserName userName);
	SessionUser Get(UserId userId);
}