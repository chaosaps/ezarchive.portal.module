using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Sessions.Create;

public class CreateSessionUseCase : ICreateSessionUseCase
{
	private readonly ISessionRepository sessionCreator;
	private readonly UserId anonymousUserId;

	public CreateSessionUseCase(ISessionRepository sessionCreator, UserId anonymousUserId)
	{
		this.sessionCreator = sessionCreator;
		this.anonymousUserId = anonymousUserId;
	}

	public Result<Session> Invoke()
	{
		var session = sessionCreator.Create(anonymousUserId);
		return new Result<Session>(session);
	}
}