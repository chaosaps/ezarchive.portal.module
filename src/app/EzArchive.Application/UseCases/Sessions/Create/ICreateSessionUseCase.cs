using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Sessions.Create;

public interface ICreateSessionUseCase
{
	Result<Session> Invoke();
}