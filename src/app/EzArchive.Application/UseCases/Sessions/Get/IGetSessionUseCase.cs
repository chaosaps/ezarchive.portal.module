using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Sessions.Get;

public interface IGetSessionUseCase
{
	Result<Session> Invoke(SessionId sessionId);
}