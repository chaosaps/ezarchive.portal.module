using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Sessions.Get;

public class GetSessionUseCase : IGetSessionUseCase
{
	private readonly ISessionGetter sessionGetter;

	public GetSessionUseCase(ISessionGetter sessionGetter)
	{
		this.sessionGetter = sessionGetter;
	}

	public Result<Session> Invoke(SessionId sessionId)
	{
		var session = sessionGetter.Get(sessionId);
		return new Result<Session>(session);
	}
}