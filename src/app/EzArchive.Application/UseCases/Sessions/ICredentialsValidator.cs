using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Sessions;

public interface ICredentialsValidator
{
	bool AreCredentialsValid(UserId userId, Password password);
}