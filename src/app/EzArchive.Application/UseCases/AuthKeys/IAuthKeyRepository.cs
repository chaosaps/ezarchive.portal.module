using EzArchive.Domain;

namespace EzArchive.Application.UseCases.AuthKeys;

public interface IAuthKeyRepository : IAuthKeyCreator, IAuthKeyGetter
{
}

public interface IAuthKeyCreator
{
	void Create(AuthKey authKey);
}

public interface IAuthKeyGetter
{
	AuthKey Get(AuthToken.LevelTwo token);
}