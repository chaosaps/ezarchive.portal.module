using EzArchive.Domain;

namespace EzArchive.Application.UseCases.AuthKeys.Login;

public interface ILoginWithAuthKeyUseCase
{
	Result<Session> Invoke(AuthToken.LevelOne token);
}