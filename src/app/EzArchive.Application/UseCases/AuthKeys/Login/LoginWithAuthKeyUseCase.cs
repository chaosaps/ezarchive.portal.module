using System;
using EzArchive.Application.UseCases.Sessions;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.AuthKeys.Login;

public class LoginWithAuthKeyUseCase : ILoginWithAuthKeyUseCase
{
	private readonly IAuthKeyRepository authKeyRepository;
	private readonly ISessionRepository sessionRepository;

	public LoginWithAuthKeyUseCase(
		IAuthKeyRepository authKeyRepository,
		ISessionRepository sessionRepository)
	{
		this.authKeyRepository = authKeyRepository;
		this.sessionRepository = sessionRepository;
	}

	public Result<Session> Invoke(AuthToken.LevelOne token)
	{
		var authKey = authKeyRepository.Get(AuthToken.LevelTwo.Create(token));
		if (authKey == null)
			throw new ArgumentException($"Invalid token '{token.Value}'");

		var session = sessionRepository.Create(authKey.UserId);
		return new Result<Session>(session);
	}
}