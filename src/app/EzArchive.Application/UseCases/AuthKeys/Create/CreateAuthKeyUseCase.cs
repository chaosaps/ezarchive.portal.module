using EzArchive.Domain;

namespace EzArchive.Application.UseCases.AuthKeys.Create;

public class CreateAuthKeyUseCase : ICreateAuthKeyUseCase
{
	private readonly IAuthKeyCreator authKeyCreator;

	public CreateAuthKeyUseCase(IAuthKeyCreator authKeyCreator)
	{
		this.authKeyCreator = authKeyCreator;
	}

	public Result<CreateAuthKeyRequest> Invoke(UserId userId, string name)
	{
		var createAuthKey = new CreateAuthKeyRequest(
			userId,
			name);
		var authKey = new AuthKey(
			AuthToken.LevelTwo.Create(createAuthKey.Token),
			createAuthKey.UserId,
			createAuthKey.Name);
		authKeyCreator.Create(authKey);

		return new Result<CreateAuthKeyRequest>(createAuthKey);
	}
}