using EzArchive.Domain;

namespace EzArchive.Application.UseCases.AuthKeys.Create;

public interface ICreateAuthKeyUseCase
{
	Result<CreateAuthKeyRequest> Invoke(UserId userId, string name);
}