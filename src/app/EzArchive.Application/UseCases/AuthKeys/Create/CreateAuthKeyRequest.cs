using EzArchive.Domain;

namespace EzArchive.Application.UseCases.AuthKeys.Create;

public class CreateAuthKeyRequest
{
	public AuthToken.LevelOne Token { get; }
	public UserId UserId { get; }
	public string Name { get; }

	public CreateAuthKeyRequest(UserId userId, string name)
	{
		Token = AuthToken.LevelOne.Create(userId, name);
		Name = name;
		UserId = userId;
	}
}