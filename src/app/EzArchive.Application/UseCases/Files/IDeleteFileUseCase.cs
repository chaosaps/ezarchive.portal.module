using System.Threading.Tasks;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Files;

public interface IDeleteFileUseCase
{
	Task<Result> InvokeAsync(Session session, FileId id);
}