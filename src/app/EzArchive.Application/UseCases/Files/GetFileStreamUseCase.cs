using System;
using System.IO;
using System.Threading.Tasks;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Files;

public class GetFileStreamUseCase : IGetFileStreamUseCase
{
	private readonly IFileRepository fileRepository;
	private readonly IStorage storage;

	public GetFileStreamUseCase(
		IFileRepository fileRepository,
		IStorage storage)
	{
		this.fileRepository = fileRepository;
		this.storage = storage;
	}

	public async Task<Result<(Stream stream, string contentType, string fileName)>> InvokeAsync(
		Session session, FileId fileId)
	{
		var file = fileRepository.GetFile(fileId, "S3");

		var stream = await storage.ReadAsync(file);
		var result = (
			stream,
			file.MimeType,
			file.OriginalFilename);

		return new Result<(Stream stream, string contentType, string fileName)>(result);
	}

	public Task<Result<(Stream stream, string contentType, string fileName)>> InvokeAsync(FileId fileId)
	{
		throw new NotImplementedException();
	}
}