using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Files;

public class UploadFileToAssetUseCase : IUploadFileToAssetUseCase
{
	private readonly IFileRepository fileRepository;
	private readonly IUserRepository userRepository;
	private readonly IStorage storage;
	private readonly uint formatId;

	public UploadFileToAssetUseCase(
		IFileRepository fileRepository,
		IUserRepository userRepository,
		IStorage storage,
		uint formatId)
	{
		this.fileRepository = fileRepository;
		this.userRepository = userRepository;
		this.storage = storage;
		this.formatId = formatId;
	}

	public async Task<Result> InvokeAsync(Session session, AssetId assetId, string originalFilename, Stream stream)
	{
		if (UserDoesNotHavePermissionToEdit())
			throw new InsufficientPermissionsException("The current user has insufficient permissions");

		var folderpath = "Objects/" + assetId;
		var sourceFilename = $"{Guid.NewGuid().ToString()}{Path.GetExtension(originalFilename)}";

		fileRepository.Set(assetId, null, sourceFilename, originalFilename, folderpath, formatId);

		await storage.WriteAsync($"{folderpath}/{sourceFilename}", stream);

		return Result.Ok();

		bool UserDoesNotHavePermissionToEdit() =>
			new[] { "Contributor", "Administrator" }
				.Any(perm => perm == userRepository.Get(session.UserId).Permission) == false;
	}
}