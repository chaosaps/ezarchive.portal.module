using System.IO;
using System.Threading.Tasks;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Files;

public interface IUploadFileToAssetUseCase
{
	Task<Result> InvokeAsync(Session session, AssetId assetId, string originalFilename, Stream stream);
}