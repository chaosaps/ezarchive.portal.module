using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Files;

public interface ICreateFileReferenceUseCase
{
	Result<FileId> Invoke(
		Session session,
		AssetId assetId,
		string fileName,
		string originalFileName,
		string folderPath,
		uint formatId,
		uint? parentId);
}