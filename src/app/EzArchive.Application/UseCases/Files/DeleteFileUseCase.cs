using System.Linq;
using System.Threading.Tasks;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Files;

public class DeleteFileUseCase : IDeleteFileUseCase
{
	private readonly IFileRepository fileRepository;
	private readonly IUserRepository userRepository;
	private readonly IStorage storage;

	public DeleteFileUseCase(
		IFileRepository fileRepository,
		IUserRepository userRepository,
		IStorage storage)
	{
		this.fileRepository = fileRepository;
		this.userRepository = userRepository;
		this.storage = storage;
	}

	public Task<Result> InvokeAsync(Session session, FileId id)
	{
		if (UserDoesNotHavePermissionToEdit())
			throw new InsufficientPermissionsException("The current user has insufficient permissions");

		var files = fileRepository.Get(id).ToList();
		Task.WhenAll(files.Select(destination => storage.DeleteAsync(destination.Url)));

		fileRepository.Delete(id);

		return Task.FromResult(Result.Ok());

		bool UserDoesNotHavePermissionToEdit() =>
			new[] { "Contributor", "Administrator" }
				.Any(perm => perm == userRepository.Get(session.UserId).Permission) == false;
	}
}
