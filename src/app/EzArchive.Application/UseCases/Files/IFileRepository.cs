using System.Collections.Generic;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Files;

public interface IFileRepository
{
	FileId Set(
		AssetId assetId,
		uint? parentId,
		string filename,
		string originalFilename,
		string folderPath,
		uint formatId,
		FileId id = null);

	void Set(FileId id, string newFileName);
	IEnumerable<Asset._FileReference._Destination> Get(FileId id);
	void Delete(FileId id);
	File GetFile(FileId id, string token);
}