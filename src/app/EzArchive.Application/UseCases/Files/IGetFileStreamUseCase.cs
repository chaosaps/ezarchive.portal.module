using System.IO;
using System.Threading.Tasks;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Files;

public interface IGetFileStreamUseCase
{
	Task<Result<(Stream stream, string contentType, string fileName)>> InvokeAsync(Session session, FileId fileId);
	Task<Result<(Stream stream, string contentType, string fileName)>> InvokeAsync(FileId fileId);
}