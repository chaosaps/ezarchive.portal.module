using System.Linq;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Files;

public interface IUpdateFileReferenceUseCase
{
	Result Invoke(Session session, FileId id, string fileName);
}

public class UpdateFileReferenceUseCase : IUpdateFileReferenceUseCase
{
	private readonly IFileRepository fileRepository;
	private readonly IUserRepository userRepository;

	public UpdateFileReferenceUseCase(IFileRepository fileRepository, IUserRepository userRepository)
	{
		this.fileRepository = fileRepository;
		this.userRepository = userRepository;
	}

	public Result Invoke(Session session, FileId id, string fileName)
	{
		if (UserDoesNotHavePermissionToEdit())
			throw new InsufficientPermissionsException("The current user has insufficient permissions");

		fileRepository.Set(id, fileName);

		return Result.Ok();

		bool UserDoesNotHavePermissionToEdit() =>
			new[] { "Contributor", "Administrator" }
				.Any(perm => perm == userRepository.Get(session.UserId).Permission) == false;
	}
}