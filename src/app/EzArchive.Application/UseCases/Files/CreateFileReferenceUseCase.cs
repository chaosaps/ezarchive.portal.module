using System.Linq;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;

namespace EzArchive.Application.UseCases.Files;

public class CreateFileReferenceUseCase : ICreateFileReferenceUseCase
{
	private readonly IFileRepository fileRepository;
	private readonly IUserRepository userRepository;

	public CreateFileReferenceUseCase(IFileRepository fileRepository, IUserRepository userRepository)
	{
		this.fileRepository = fileRepository;
		this.userRepository = userRepository;
	}

	public Result<FileId> Invoke(
		Session session,
		AssetId assetId,
		string fileName,
		string originalFileName,
		string folderPath,
		uint formatId,
		uint? parentId)
	{
		if (UserDoesNotHavePermissionToEdit())
			throw new InsufficientPermissionsException("The current user has insufficient permissions");

		var fileId = fileRepository.Set(assetId, parentId, fileName, originalFileName, folderPath, formatId);

		return new Result<FileId>(fileId);

		bool UserDoesNotHavePermissionToEdit() =>
			new[] { "Contributor", "Administrator" }
				.Any(perm => perm == userRepository.Get(session.UserId).Permission) == false;
	}
}