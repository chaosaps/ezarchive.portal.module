using System.Collections.Generic;

namespace EzArchive.Application;

public class Page<T>
{
	public Page(uint foundCount, uint startIndex, IEnumerable<T> results)
	{
		FoundCount = foundCount;
		StartIndex = startIndex;
		Results = results;
	}

	public uint FoundCount { get; }
	public uint StartIndex { get; }
	public IEnumerable<T> Results { get; }
}