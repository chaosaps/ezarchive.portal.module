namespace EzArchive.Application;

public class PageRequest
{
	public PageRequest(int pageIndex, int pageSize)
	{
		PageIndex = (uint)pageIndex;
		PageSize = (uint)pageSize;
	}

	public uint PageIndex { get; }
	public uint PageSize { get; }
	public uint PageOffset => PageIndex * PageSize;
}