using System.Threading.Tasks;

namespace EzArchive.Application;

public interface ITranscoder
{
	Task TranscodeAsync(string inputKey, string outputKey, string presetId, bool createThumbnail);
	Task TranscodeAsync(string inputKey, string outputKey, string presetId);
}