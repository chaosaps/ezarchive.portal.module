using System.Threading.Tasks;
using EzArchive.Domain.Emails;

namespace EzArchive.Application
{
	public interface IEmailSender
	{
		Task SendAsync(ResetPasswordEmail email);
	}
}