using System;

namespace EzArchive.Application.Errors;

public class DataNotFoundException : Exception
{
}