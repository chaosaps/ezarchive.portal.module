using System;

namespace EzArchive.Application.Errors;

public class UnhandledException : Exception
{
	public UnhandledException(string message) : base(message)
	{
	}

	public UnhandledException(string message, Exception innerException) : base(message, innerException)
	{
	}
}