using System;

namespace EzArchive.Application.Errors;

public class SearchQueryParserException : Exception
{
	public SearchQueryParserException() : base("Query must be in the format")
	{
	}
}