﻿using System;
using System.Collections.Generic;
using System.Linq;
using EzArchive.Application.Errors;
using EzArchive.Domain;

namespace EzArchive.Application;

public class SearchQuery
{
	private const string DefaultQuery = "*:*";
	private readonly List<string> queries = new List<string>();

	public static SearchQuery AllQuery() =>
		Parse(DefaultQuery);

	public static SearchQuery Parse(string query)
	{
		if (string.IsNullOrEmpty(query)) query = DefaultQuery;
		if (query.StartsWith("AND", StringComparison.InvariantCultureIgnoreCase))
		{
			var sq = new SearchQuery();
			foreach (var subQuery in UnWrap(query.Substring(3)))
			{
				sq.AndQuery(subQuery);
			}

			return sq;
		}
		else
		{
			var sq = new SearchQuery();
			sq.AndQuery(query);

			return sq;
		}
	}

	private static IEnumerable<string> UnWrap(string query)
	{
		query = query.Trim();
		var split = query.Split(' ');
		var currentQuery = "";

		foreach (var s in split)
		{
			if (s.Contains(":") && currentQuery != "" && !IsDateTime(s))
			{
				yield return currentQuery.TrimEnd();

				currentQuery = "";
			}

			currentQuery += s + " ";
		}

		yield return currentQuery;
	}

	private static bool IsDateTime(string candidate) =>
		DateTime.TryParse(candidate, out _);

	public override string ToString() =>
		queries.Count switch
		{
			0 => string.Empty,
			1 => queries.Single(),
			_ => $"({string.Join(")AND(", queries)})"
		};

	public void AndQuery(string query, bool parseQuery = true)
	{
		if (parseQuery) queries.Add(ParseSingleQuery(query.Trim()));
		else
		{
			if (string.IsNullOrEmpty(query)) return;

			queries.Add(query.Trim());
		}
	}

	private static string ParseSingleQuery(string query)
	{
		if (query == "*:*") return query;

		var colonIndex = query.IndexOf(':');

		if (colonIndex == -1) return query.Replace("+", "%2B");
		if (colonIndex == query.Length - 1) throw new SearchQueryParserException();

		var field = query.Substring(0, colonIndex);
		var value = query.Substring(colonIndex + 1);
		var formattedValue = GetFormattedValue(value);

		var key = IndexHelper.GetIndexKey(field);

		return string.IsNullOrEmpty(key)
			? formattedValue
			: $"{key}:{formattedValue}";
	}

	private static string GetFormattedValue(string value)
	{
		if (value.ToLower().StartsWith("between_fields"))
		{
			var from = value.Split(' ')[1];
			var to = value.Split(' ')[2];

			return $"[{from} TO {to}]";
		}

		if (value.ToLower().StartsWith("between"))
		{
			var from = value.Split(' ')[1];
			var to = value.Split(' ')[2];

			return $"[{from} TO {to}]";
		}

		if (value.ToLower().StartsWith("exact"))
		{
			var val = value.Substring(value.IndexOf(' ') + 1);

			return $"\"{val}\"";
		}

		if (value.ToLower().StartsWith("or"))
		{
			var val = value.Split(' ').Skip(1);

			return $"({string.Join(" OR ", val)})";
		}

		return value;
	}

	public SearchQuery ApplyFilterByProjects(IEnumerable<Project> projects)
	{
		AndQuery($"{{Projects}}:OR {string.Join(" ", projects.Select(p => p.Id.Value))}");
		return this;
	}

	public class ParsingException : Exception
	{
		public ParsingException(string displayName) : base($"Field: {displayName} is not sortable")
		{
		}
	}
}