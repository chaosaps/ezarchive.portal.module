namespace EzArchive.Domain;

public enum AnnotationPermission : uint
{
	User = 0u,
	Contributor = 1u,
	Owner = 2147483648u
}