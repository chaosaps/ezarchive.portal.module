using System;
using System.Security.Cryptography;
using System.Text;

namespace EzArchive.Domain;

public abstract class AuthToken : ValueObject<string>
{
	private AuthToken(string value) : base(value)
	{
	}

	private static string GetHashed(string secret)
	{
		var byteHash = SHA256.Create().ComputeHash(Encoding.Unicode.GetBytes(secret));

		return BitConverter.ToString(byteHash).Replace("-", "").ToLower();
	}

	public class LevelOne : AuthToken
	{
		private LevelOne(string value) : base(value)
		{
		}

		public static LevelOne Create(UserId userId, string name) =>
			CreateWithoutHashing(GetHashed($"{name}{userId.Value}{DateTime.UtcNow}"));

		public static LevelOne CreateWithoutHashing(string token) =>
			new LevelOne(token);
	}

	public class LevelTwo : AuthToken
	{
		private LevelTwo(string value) : base(value)
		{
		}

		public static LevelTwo Create(LevelOne token) =>
			CreateWithoutHashing(GetHashed(token.Value));

		public static LevelTwo CreateWithoutHashing(string token) =>
			new LevelTwo(token);
	}
}