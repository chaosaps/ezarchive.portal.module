namespace EzArchive.Domain;

public enum DefinitionType
{
	Metadata = 0,
	Annotation = 1
}