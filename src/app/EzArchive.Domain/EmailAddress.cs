using System.Text.RegularExpressions;

namespace EzArchive.Domain
{
	public class EmailAddress : ValueObject<string>
	{
		public EmailAddress(string value) : base(value)
		{
		}


		public bool IsValid() => 
			Regex.IsMatch(Value, @".+\@.+\..+");
	}
}