using System;
using System.Collections.Generic;
using System.Linq;
using EzArchive.Domain.Values;

namespace EzArchive.Domain;

public class Asset
{
	public Asset(
		AssetId id,
		_Type type,
		UserId lastChangedByUserId,
		bool doFilesRequireLogin,
		IEnumerable<string> tags, IEnumerable<_Data> data,
		IEnumerable<_AnnotationGroup> annotations,
		IEnumerable<_FileReference> files)
	{
		Id = id;
		Type = type;
		Data = data.ToList().AsReadOnly();
		LastChangedByUserId = lastChangedByUserId;
		DoFilesRequireLogin = doFilesRequireLogin;
		Tags = tags.ToList().AsReadOnly();
		AnnotationsGroups = annotations.ToList();
		Files = files.ToList();
	}

	public AssetId Id { get; }
	public _Type Type { get; }
	public IList<_Data> Data { get; }
	public UserId LastChangedByUserId { get; }
	public bool DoFilesRequireLogin { get; }
	public IReadOnlyList<string> Tags { get; }
	public IList<_AnnotationGroup> AnnotationsGroups { get; }
	public IList<_FileReference> Files { get; }

	public Value FindField(string name, string id) =>
		Data
			.Single(item => item.Name == name)
			.Fields
			.Single(item => item.Key == id)
			.Value;

	public class _Type : ValueObject<uint>
	{
		public _Type(uint value) : base(value)
		{
		}

		public _Type(string value) : base(uint.Parse(value))
		{
		}
	}

	public class _Data
	{
		public _Data(IDictionary<string, Value> fields, DataDefinition dataDefinition)
		{
			Name = dataDefinition.Name;
			Fields = fields;
			DataDefinition = dataDefinition;
		}

		public string Name { get; }
		public IDictionary<string, Value> Fields { get; }
		public DataDefinition DataDefinition { get; }

		public void SetField(string key, Value value)
		{
			if (Fields.ContainsKey(key) == false)
				Fields.Add(key, value);
			else
				Fields[key] = value;
		}
	}

	public IDictionary<string, Value> GetAnnotation(AnnotationId annotationId) =>
		AnnotationsGroups
			.SelectMany(ag => ag.Annotations)
			.SingleOrDefault(a => a["Identifier"].ToString() == annotationId.Value.ToString());

	public Asset RemoveAnnotation(AnnotationId annotationId)
	{
		var annotations = AnnotationsGroups
			.Select(ag => new _AnnotationGroup(
				ag.Name,
				ag.DefinitionId,
				ag.Annotations.Where(a => a["Identifier"].ToString() != annotationId.ToString())));

		return new Asset(
			Id,
			Type,
			LastChangedByUserId,
			DoFilesRequireLogin,
			Tags,
			Data,
			annotations,
			Files);
	}

	public class _AnnotationGroup
	{
		public _AnnotationGroup(string name, Guid definitionId, IEnumerable<IDictionary<string, Value>> annotations)
		{
			Name = name;
			DefinitionId = definitionId;
			Annotations = annotations.ToList();
		}

		public string Name { get; }
		public Guid DefinitionId { get; }
		public IList<IDictionary<string, Value>> Annotations { get; }
	}

	public class _FileReference
	{
		public _FileReference(string id, string name, string type, IList<_Destination> destinations)
		{
			Id = id;
			Name = name;
			Type = type;
			Destinations = destinations;
		}

		public string Id { get; }
		public string Name { get; }
		public string Type { get; }
		public IList<_Destination> Destinations { get; }

		public class _Destination
		{
			public _Destination(string type, string url)
			{
				Type = type;
				Url = url;
			}

			public string Type { get; }
			public string Url { get; }
		}
	}

	public void SetAnnotation(DataDefinition definition, IDictionary<string, Value> annotation)
	{
		if (AnnotationsGroups.Any(ag => ag.DefinitionId.ToString() == definition.Id) == false)
		{
			AnnotationsGroups.Add(
				new _AnnotationGroup(
					definition.Name,
					new Guid(definition.Id),
					new List<IDictionary<string, Value>> { annotation }));
		}
		else
		{
			AnnotationsGroups.Single(ag => ag.DefinitionId.ToString() == definition.Id).Annotations.Add(annotation);
		}
	}
}