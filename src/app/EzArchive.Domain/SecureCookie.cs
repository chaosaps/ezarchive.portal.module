using System;

namespace EzArchive.Domain;

public class SecureCookie
{
	public SecureCookieId Id { get; }
	public UserId UserId { get; }
	public PasswordGuid PasswordGuid { get; }
	public DateTime CreatedOn { get; }
	public DateTime? UsedOn { get; }

	public SecureCookie(
		SecureCookieId id,
		UserId userId,
		PasswordGuid passwordGuid,
		DateTime createdOn,
		DateTime? usedOn)
	{
		Id = id;
		UserId = userId;
		PasswordGuid = passwordGuid;
		CreatedOn = createdOn;
		UsedOn = usedOn;
	}

	public SecureCookie Use()
	{
		return new SecureCookie(
			Id,
			UserId,
			PasswordGuid,
			CreatedOn,
			DateTime.UtcNow);
	}
}