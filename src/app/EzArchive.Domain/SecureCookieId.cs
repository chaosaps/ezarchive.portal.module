using System;

namespace EzArchive.Domain;

public class SecureCookieId : ValueObject<Guid>
{
	public SecureCookieId(Guid value) : base(value)
	{
	}

	public SecureCookieId(string value) : this(new Guid(value))
	{
	}
}