namespace EzArchive.Domain;

public class File
{
	public FileId Id { get; }
	public string Url { get; }
	public string OriginalFilename { get; }
	public string MimeType { get; }

	public File(FileId id, string url, string originalFilename, string mimeType)
	{
		Id = id;
		Url = url;
		OriginalFilename = originalFilename;
		MimeType = mimeType;
	}
}