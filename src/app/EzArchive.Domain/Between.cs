using NodaTime;

namespace EzArchive.Domain;

public class Between
{
	public Between()
	{
		HasValue = false;
	}

	public Between(Instant from, Instant to)
	{
		From = @from;
		To = to;
		HasValue = true;
	}

	public bool HasValue { get; }
	public Instant From { get; }
	public Instant To { get; }
}