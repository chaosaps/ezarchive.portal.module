namespace EzArchive.Domain;

public class AuthKey
{
	public AuthToken.LevelTwo Token { get; }
	public UserId UserId { get; }
	public string Name { get; }

	public AuthKey(AuthToken.LevelTwo token, UserId userId, string name)
	{
		Token = token;
		UserId = userId;
		Name = name;
	}
}