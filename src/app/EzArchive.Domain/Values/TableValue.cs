﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EzArchive.Domain.Values;

public class TableValue : Value
{
	public List<List<string>> Value { get; set; }

	public TableValue()
	{
		Value = new List<List<string>>();
		Type = "Table";
	}

	public TableValue(string value)
	{
		Type = "Table";
		throw new NotImplementedException("Tables updates are not implemented");
	}

	public override string ToString()
	{
		return string.Join(",", RowToString());
	}

	private IEnumerable<string> RowToString()
	{
		return Value.Select(val => "[" + string.Join("] [", val) + "]");
	}
}