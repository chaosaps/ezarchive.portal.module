﻿using System;

namespace EzArchive.Domain.Values;

public abstract class Value
{
	public string Type { get; set; }

	public static Value Create(FieldDefinition fieldDefinition, string value) =>
		fieldDefinition.Type.ToLower() switch
		{
			"string" => new StringValue(value),
			"datetime" => new DateTimeValue(value),
			"table" => new TableValue(value),
			"boolean" => new BooleanValue(value.ToLower() == "true"),
			_ => throw new NotImplementedException($"FieldDefinition Type of '{fieldDefinition.Type}' not implemented")
		};
}