﻿namespace EzArchive.Domain.Values;

public class BooleanValue : Value
{
	public bool Value { get; set; }

	public BooleanValue(bool value)
	{
		Value = value;
		Type = "Boolean";
	}

	public override string ToString()
	{
		return Value ? "true" : "false";
	}
}