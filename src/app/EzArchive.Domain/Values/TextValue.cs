﻿namespace EzArchive.Domain.Values;

public class TextValue : Value
{
	public string Value { get; set; }

	public TextValue(string value)
	{
		Value = value;
		Type = "Text";
	}

	public override string ToString()
	{
		return Value;
	}
}