﻿namespace EzArchive.Domain.Values;

public class StringValue : Value
{
	public string Value { get; set; }

	public StringValue(string value)
	{
		Value = value ?? "";
		Type = "String";
	}

	public override string ToString()
	{
		return Value;
	}
}