namespace EzArchive.Domain.Values;

public class NullValue : Value
{
	public NullValue()
	{
		Type = "Null";
	}

	public override string ToString()
	{
		return "";
	}
}