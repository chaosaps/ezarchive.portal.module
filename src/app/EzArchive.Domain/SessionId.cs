using System;

namespace EzArchive.Domain;

public class SessionId : ValueObject<Guid>
{
	public SessionId(Guid value) : base(value)
	{
	}

	public SessionId(string value) : base(new Guid(value))
	{
	}
}