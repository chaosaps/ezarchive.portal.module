namespace EzArchive.Domain;

public class Folder
{
	public class _Id : ValueObject<uint>
	{
		public _Id(uint value) : base(value)
		{
		}
	}
}