namespace EzArchive.Domain;

public class LabelId : ValueObject<uint>
{
	public LabelId(uint value) : base(value)
	{
	}

	public LabelId(string value) : base(uint.Parse(value))
	{
	}
}