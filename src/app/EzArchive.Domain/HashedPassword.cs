using System;
using System.Linq;

namespace EzArchive.Domain
{
	public class HashedPassword : ValueObject<byte[]>
	{
		public HashedPassword(byte[] value) : base(value)
		{
		}

		public override string ToString() =>
			BitConverter.ToString(Value).Replace("-", "").ToLower();

		protected bool Equals(byte[] other)
		{
			if (other.Length != Value.Length) return false;

			return Value.Where((t, i) => t != other[i]).Any() == false;
		}
	}
}