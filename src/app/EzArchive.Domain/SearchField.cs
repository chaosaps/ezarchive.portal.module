﻿namespace EzArchive.Domain;

public class SearchField
{
	public SearchField(string key, string value)
	{
		Key = key;
		Value = value;
	}

	public string Key { get; }
	public string Value { get; }
}