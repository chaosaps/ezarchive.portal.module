using System;
using System.Collections.Generic;
using EzArchive.Domain.Tickets;

namespace EzArchive.Domain.Emails
{
	public class ResetPasswordEmail : Email
	{
		public ResetPasswordEmail(User user, ResetPasswordTicket ticket, Uri frontendUri)
			: base(
				"reset-password-en-1",
				new EmailAddress(user.Email), new Dictionary<string, string>
				{
					{"FULLNAME", user.Name},
					{"FIRSTNAME", FirstName(user.Name)},
					{"Url", new Uri(frontendUri, $"#resetpassword/{ticket.Id.Value}").ToString()}
				})
		{
		}

		private static string FirstName(string name)
		{
			if (string.IsNullOrWhiteSpace(name) || name.Contains(" ") == false)
				return "";

			return name.Split(" ")[0];
		}
	}
}