using System.Collections.Generic;

namespace EzArchive.Domain.Emails
{
	public abstract class Email
	{
		protected Email(string template, EmailAddress to, IDictionary<string, string> parameters)
		{
			From = new EmailAddress("support@ffw.io");
			To = to;
			Parameters = parameters;
			Template = template;
		}

		public EmailAddress From { get; }
		public EmailAddress To { get; }
		public IDictionary<string, string> Parameters { get; }
		public string Template { get; }
	}
}