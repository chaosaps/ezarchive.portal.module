using System.Security.Cryptography;
using System.Text;

namespace EzArchive.Domain;

public class Password : ValueObject<string>
{
	private const int Iterations = 1000;

	public Password(string value) : base(value)
	{
	}

	public HashedPassword GenerateHash(string salt) =>
		new(Hash(salt));

	public HashedPassword GenerateHash(UserId userId) =>
		new(Hash(userId.Value.ToString()));

	private byte[] Hash(string salt)
	{
		// Iterations is hardcoded to 1000, it can not be changed because then all passwords would stop working!
		return new Rfc2898DeriveBytes(Value, Encoding.UTF8.GetBytes(salt), Iterations).GetBytes(512);
	}

	public bool ConformsToPasswordPolicy() =>
		Value.Length >= 8;
}
