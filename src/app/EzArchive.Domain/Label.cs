namespace EzArchive.Domain;

public class Label
{
	public Label(LabelId id, string name, uint projectId)
	{
		Id = id;
		Name = name;
		ProjectId = projectId;
	}

	public LabelId Id { get; }
	public string Name { get; }
	public uint ProjectId { get; }
}