namespace EzArchive.Domain;

public class SessionUser
{
	public UserId UserId { get; }
	public uint SystemPermissions { get; }
	public UserName UserName { get; }

	public SessionUser(
		UserId userId,
		uint systemPermissions,
		UserName userName)
	{
		UserId = userId;
		SystemPermissions = systemPermissions;
		UserName = userName;
	}
}