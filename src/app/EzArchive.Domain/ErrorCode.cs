﻿namespace EzArchive.Domain
{
	public enum ErrorCode : uint
	{
		None = 0,
		InvalidInput = 1000,
		AssetNotFound = 2000,
		AnnotationNotFound = 2001,
		TicketNotFound = 10_001,
		TestError = 999999999
	}
}
