namespace EzArchive.Domain
{
	public class FormatId : ValueObject<uint>
	{
		public FormatId(uint value) : base(value)
		{
		}
	}
}