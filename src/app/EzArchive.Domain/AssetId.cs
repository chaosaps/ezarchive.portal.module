using System;

namespace EzArchive.Domain;

public class AssetId : ValueObject<Guid>
{
	public AssetId(Guid value) : base(value)
	{
	}

	public AssetId(string value) : base(new Guid(value))
	{
	}

	public static AssetId New() =>
		new AssetId(Guid.NewGuid());
}