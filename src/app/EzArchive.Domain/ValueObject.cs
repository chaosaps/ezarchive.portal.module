using System.Collections.Generic;

namespace EzArchive.Domain;

public class ValueObject<T>
{
	public T Value { get; }

	protected ValueObject(T value)
	{
		Value = value;
	}

	protected bool Equals(ValueObject<T> other) => EqualityComparer<T>.Default.Equals(Value, other.Value);

	public override bool Equals(object obj)
	{
		if (ReferenceEquals(null, obj)) return false;
		if (ReferenceEquals(this, obj)) return true;
		if (obj.GetType() != this.GetType()) return false;
		return Equals((ValueObject<T>)obj);
	}

	public override int GetHashCode() => EqualityComparer<T>.Default.GetHashCode(Value);

	public static bool operator ==(ValueObject<T> left, ValueObject<T> right) => Equals(left, right);

	public static bool operator !=(ValueObject<T> left, ValueObject<T> right) => !Equals(left, right);

	public override string ToString()
	{
		return Value.ToString();
	}
}

public class ValueObject<T1, T2>
{
	protected T1 Value1 { get; }
	protected T2 Value2 { get; }

	protected bool Equals(ValueObject<T1, T2> other)
	{
		return EqualityComparer<T1>.Default.Equals(Value1, other.Value1) &&
		       EqualityComparer<T2>.Default.Equals(Value2, other.Value2);
	}

	public override bool Equals(object obj)
	{
		if (ReferenceEquals(null, obj)) return false;
		if (ReferenceEquals(this, obj)) return true;
		if (obj.GetType() != this.GetType()) return false;
		return Equals((ValueObject<T1, T2>)obj);
	}

	public override int GetHashCode()
	{
		unchecked
		{
			return (EqualityComparer<T1>.Default.GetHashCode(Value1) * 397) ^
			       EqualityComparer<T2>.Default.GetHashCode(Value2);
		}
	}

	public static bool operator ==(ValueObject<T1, T2> left, ValueObject<T1, T2> right)
	{
		return Equals(left, right);
	}

	public static bool operator !=(ValueObject<T1, T2> left, ValueObject<T1, T2> right)
	{
		return !Equals(left, right);
	}

	protected ValueObject(T1 value1, T2 value2)
	{
		Value1 = value1;
		Value2 = value2;
	}
}