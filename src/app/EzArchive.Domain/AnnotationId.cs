using System;

namespace EzArchive.Domain;

public class AnnotationId : ValueObject<Guid>
{
	public AnnotationId(Guid value) : base(value)
	{
	}

	public AnnotationId(string value) : base(new Guid(value))
	{
	}

	public static AnnotationId New() =>
		new AnnotationId(Guid.NewGuid());
}