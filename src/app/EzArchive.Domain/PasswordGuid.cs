using System;

namespace EzArchive.Domain;

public class PasswordGuid : ValueObject<Guid>
{
	public PasswordGuid(Guid value) : base(value)
	{
	}

	public PasswordGuid(string value) : this(new Guid(value))
	{
	}
}