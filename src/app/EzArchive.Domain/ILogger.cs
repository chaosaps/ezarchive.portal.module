using System;

namespace EzArchive.Domain;

public interface ILogger
{
	void LogInformation(string name, string message, TimeSpan? executionDuration = null);

	void LogWarning(string name, string message, TimeSpan? executionDuration = null);

	void LogError(string name, string message, Exception exception, TimeSpan? executionDuration = null);

	void LogFatal(string name, string message, Exception exception = null, TimeSpan? executionDuration = null);
}