using System;

namespace EzArchive.Domain.Tickets
{
	public record TicketId(Guid Value)
	{
		public TicketId() : this(Guid.NewGuid())
		{
		}
	}
}