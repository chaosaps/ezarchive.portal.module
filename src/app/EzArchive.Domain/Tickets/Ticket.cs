using System;
using System.Collections.Generic;

namespace EzArchive.Domain.Tickets
{
	public abstract class Ticket
	{
		public TicketId Id { get; }
		public DateTime? Used_On { get; }

		protected Ticket(TicketId id)
		{
			Id = id;
		}

		protected Ticket(TicketId id, DateTime usedOn)
		{
			Id = id;
			Used_On = usedOn;
		}

		public abstract IDictionary<string, object> Data();
		public abstract Ticket Use();
	}
}