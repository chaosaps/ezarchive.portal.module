using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace EzArchive.Domain.Tickets
{
	public class ResetPasswordTicket : Ticket
	{
		public UserId UserId { get; }

		public ResetPasswordTicket(TicketId id, UserId userId) : base(id)
		{
			UserId = userId;
		}
		
		[JsonConstructor]
		public ResetPasswordTicket(TicketId id, DateTime usedOn, UserId userId) : base(id, usedOn)
		{
			UserId = userId;
		}

		public override IDictionary<string, object> Data()
		{
			return new Dictionary<string, object>
			{
				{"UserId", UserId.Value.ToString()}
			};
		}

		public override Ticket Use() => 
			new ResetPasswordTicket(Id, DateTime.UtcNow, UserId);
	}
}