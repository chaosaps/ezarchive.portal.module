﻿using System.Collections.Generic;
using System.Linq;
using EzArchive.Domain;

namespace EZArchive.Domain;

public class Search
{
	public Search(AssetId id, string typeId, IEnumerable<SearchField> fields, IEnumerable<string> tags)
	{
		Id = id;
		TypeId = typeId;
		Fields = fields.ToList();
		Tags = tags.ToList();
	}

	public AssetId Id { get; }
	public string TypeId { get; }
	public IList<SearchField> Fields { get; }
	public IList<string> Tags { get; }
}