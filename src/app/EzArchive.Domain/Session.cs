using System;

namespace EzArchive.Domain;

public class Session
{
	public Session(SessionId id, DateTime createdOn, DateTime modifiedOn, UserId userId, bool isAnonymous)
	{
		Id = id;
		CreatedOn = createdOn;
		ModifiedOn = modifiedOn;
		UserId = userId;
		IsAnonymous = isAnonymous;
	}

	public SessionId Id { get; }
	public UserId UserId { get; }
	public DateTime CreatedOn { get; }
	public DateTime ModifiedOn { get; }
	public bool IsAnonymous { get; }
}