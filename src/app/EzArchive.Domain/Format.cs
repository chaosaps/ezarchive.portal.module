namespace EzArchive.Domain
{
	public record Format(FormatId Id, string Extension);
}