namespace EzArchive.Domain;

public class FileId : ValueObject<uint>
{
	public FileId(uint value) : base(value)
	{
	}
}