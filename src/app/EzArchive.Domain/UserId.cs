using System;

namespace EzArchive.Domain;

public class UserId : ValueObject<Guid>
{
	public UserId(Guid value) : base(value)
	{
	}

	public UserId(string value) : base(new Guid(value))
	{
	}
}