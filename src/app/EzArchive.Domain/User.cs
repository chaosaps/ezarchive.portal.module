using System;

namespace EzArchive.Domain;

public class User
{
	public UserId Id { get; }
	public string Email { get; }
	public string Name { get; }
	public bool IsAdministrator => "Administrator".Equals(Permission, StringComparison.OrdinalIgnoreCase);
	public string Permission { get; }
	public string Preferences { get; }
	public string WayfAttributes { get; }

	public User(
		UserId id,
		string email,
		string name,
		string permission,
		string preferences,
		string wayfAttributes)
	{
		Id = id;
		Email = email;
		Name = name;
		Permission = permission;
		Preferences = preferences;
		WayfAttributes = wayfAttributes;
	}
}