namespace EzArchive.Domain;

public class Result
{
	public ErrorCode ErrorCode { get; } = ErrorCode.None;
	public bool HasError { get; }

	protected Result()
	{
	}

	public Result(ErrorCode errorCode)
	{
		ErrorCode = errorCode;
		HasError = true;
	}

	public static Result Ok() =>
		new Result();
}

public class Result<T> : Result
{
	public T Value { get; }

	public Result(T value)
	{
		Value = value;
	}

	public Result(ErrorCode errorCode) : base(errorCode)
	{
		Value = default;
	}
}