﻿namespace EzArchive.Domain;

public class AnnotationVisibility
{
	public static AnnotationVisibility Public { get; } = new AnnotationVisibility("Public");
	public static AnnotationVisibility Private { get; } = new AnnotationVisibility("Private");

	public AnnotationVisibility(string scope)
	{
		Scope = scope;
	}

	public string Scope { get; }

	public override string ToString() =>
		$"{Scope}";
}