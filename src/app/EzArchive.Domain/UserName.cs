namespace EzArchive.Domain;

public class UserName : ValueObject<string>
{
	public UserName(string value) : base(value)
	{
	}
}