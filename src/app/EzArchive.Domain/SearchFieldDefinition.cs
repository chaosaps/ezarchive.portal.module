namespace EzArchive.Domain;

public class SearchFieldDefinition
{
	public string DisplayName { get; }
	public string Type { get; }
	public string[] Ids { get; }
	public bool IsSortable { get; }
	public bool IsVisible { get; }

	public SearchFieldDefinition(string displayName, string[] ids, string type, bool isSortable, bool isVisible)
	{
		DisplayName = displayName;
		Ids = ids;
		Type = type;
		IsSortable = isSortable;
		IsVisible = isVisible;
	}
}