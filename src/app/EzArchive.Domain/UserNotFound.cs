using System;

namespace EzArchive.Domain;

public class UserNotFound : Exception
{
	public UserNotFound(UserId id) : base($"User with id: '{id.Value}' was not found")
	{
	}
}