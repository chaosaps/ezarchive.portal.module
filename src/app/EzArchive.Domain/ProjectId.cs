using System;

namespace EzArchive.Domain;

public class ProjectId : ValueObject<uint>
{
	public ProjectId(uint value) : base(value)
	{
	}

	public ProjectId(string value) : base(UInt32.Parse(value))
	{
	}
}