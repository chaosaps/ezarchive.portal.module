using System.Collections.Generic;
using System.Linq;

namespace EzArchive.Domain;

public class Project
{
	public Project(ProjectId id, string name, IEnumerable<Label> labels, IEnumerable<User> users)
	{
		Id = id;
		Name = name;
		Labels = labels.ToList().AsReadOnly();
		Users = users.ToList().AsReadOnly();
	}

	public ProjectId Id { get; }
	public string Name { get; }
	public IReadOnlyList<Label> Labels { get; }
	public IReadOnlyList<User> Users { get; }
}