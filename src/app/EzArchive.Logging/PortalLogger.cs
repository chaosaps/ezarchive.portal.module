using System;
using System.Text;
using Chaos.Portal.Core.Data;
using EzArchive.Domain;

namespace EzArchive.Logging;

public class PortalLogger : ILogger
{
	private readonly ILogRepository logRepository;

	public PortalLogger(ILogRepository logRepository)
	{
		this.logRepository = logRepository;
	}

	public void LogInformation(string name, string message, TimeSpan? executionDuration = null)
	{
		logRepository.Create(
			name,
			null,
			"INFO",
			executionDuration?.TotalMilliseconds ?? -1,
			message);
	}

	public void LogWarning(string name, string message, TimeSpan? executionDuration = null)
	{
		logRepository.Create(
			name,
			null,
			"WARNING",
			executionDuration?.TotalMilliseconds ?? -1,
			message);
	}

	public void LogError(string name, string message, Exception exception, TimeSpan? executionDuration = null)
	{
		logRepository.Create(
			name,
			null,
			"ERROR",
			executionDuration?.TotalMilliseconds ?? -1,
			$"{message}{CreateExceptionMessage(exception)}");
	}

	public void LogFatal(string name, string message, Exception exception = null, TimeSpan? executionDuration = null)
	{
		logRepository.Create(
			name,
			null,
			"FATAL",
			executionDuration?.TotalMilliseconds ?? -1,
			exception == null ? message : $"{message}{CreateExceptionMessage(exception)}");
	}

	private string CreateExceptionMessage(Exception exception)
	{
		var sb = new StringBuilder();

		sb.AppendLine("Exception details: ");
		sb.AppendLine($"{DateTime.UtcNow:O} - An exception occurred");
		sb.AppendLine($"	Type: {exception.GetType().Name}");
		sb.AppendLine($"	Message: {exception.Message}");
		sb.AppendLine($"	Stacktrace: {exception.StackTrace}");

		if (exception.InnerException != null)
		{
			sb.AppendLine("");
			sb.AppendLine("Inner Exception details: ");
			sb.AppendLine($"	Type: {exception.InnerException.GetType().Name}");
			sb.AppendLine($"	Message: {exception.InnerException.Message}");
			sb.AppendLine($"	Stacktrace: {exception.InnerException.StackTrace}");
		}

		return sb.ToString();
	}
}