using System;
using System.Collections.Generic;
using EzArchive.Domain;

namespace EzArchive.Logging;

public class InMemoryLogger : ILogger
{
	public List<LoggedMessage> LoggedMessages { get; }

	public InMemoryLogger(List<LoggedMessage> loggedMessages)
	{
		LoggedMessages = loggedMessages;
	}

	public InMemoryLogger()
	{
		LoggedMessages = new List<LoggedMessage>();
	}

	public void LogInformation(string name, string message, TimeSpan? executionDuration = null)
	{
		LoggedMessages.Add(
			new LoggedMessage(
				name,
				Level.Info,
				message));
	}

	public void LogWarning(string name, string message, TimeSpan? executionDuration = null)
	{
		LoggedMessages.Add(
			new LoggedMessage(
				name,
				Level.Warning,
				message));
	}

	public void LogError(string name, string message, Exception exception, TimeSpan? executionDuration = null)
	{
		LoggedMessages.Add(
			new LoggedMessage(
				name,
				Level.Error,
				message,
				exception));
	}

	public void LogFatal(string name, string message, Exception exception = null, TimeSpan? executionDuration = null)
	{
		LoggedMessages.Add(
			new LoggedMessage(
				name,
				Level.Fatal,
				message,
				exception));
	}

	public class LoggedMessage
	{
		public string Name { get; }

		public Level Level { get; }

		public string Message { get; }

		public Exception Exception { get; }

		public LoggedMessage(
			string name,
			Level level,
			string message,
			Exception exception = null)
		{
			Name = name;
			Level = level;
			Message = message;
			Exception = exception;
		}
	}

	public enum Level : uint
	{
		Info = 1,
		Warning = 2,
		Error = 3,
		Fatal = 4
	}
}