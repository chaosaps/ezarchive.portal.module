namespace EzArchive.Data.Exceptions;

public class DataDefinitionNotFound : DataNotFound
{
	public DataDefinitionNotFound(string id) : base($"No DataDefinition named ({id}) found")
	{
	}
}