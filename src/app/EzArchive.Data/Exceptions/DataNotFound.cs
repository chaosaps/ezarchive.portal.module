using System;

namespace EzArchive.Data.Exceptions;

public class DataNotFound : Exception
{
	public DataNotFound(string message) : base(message)
	{
	}
}