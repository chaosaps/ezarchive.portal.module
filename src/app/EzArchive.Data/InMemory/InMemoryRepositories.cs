using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Chaos.Authentication.Data.InMemory;
using Chaos.Mcm.Core.Model;
using Chaos.Mcm.Domain;
using Chaos.Portal.Core.Indexing;
using Chaos.Portal.Core.Indexing.Solr;
using Chaos.Portal.Core.Indexing.Solr.Response;
using EzArchive.Data.Portal;
using EzArchive.Domain;
using Folder = EzArchive.Domain.Folder;

namespace EzArchive.Data.InMemory;

public class InMemoryRepositories
{
	public readonly MetadataSchemaId ProfileMetadataSchemaId =
		new MetadataSchemaId("4a9078cf-eceb-43e1-bb46-8fb96fdd267b");

	public readonly MetadataSchemaId AnnotationMetadataSchemaId =
		new MetadataSchemaId("bf936c9d-370a-434a-98ae-2421c2030dcc");

	public readonly MetadataSchemaId AssetMetadataSchemaId =
		new MetadataSchemaId("92db8c0f-e03d-44e3-bcde-0e1b3cba309c");

	public readonly Guid AdminGroupId = new Guid("C1FE5DB7-B843-49DF-855B-CCA90744842F");
	public readonly Guid ContributorGroupId = new Guid("BB5FECA5-042C-477B-809A-09D097EC188A");
	public readonly Guid UserGroupId = new Guid("BAD6D658-2143-4D5F-B4CF-A36C933542D2");

	private static readonly UserId AnonymousUserId = new UserId("3F3F3F3F-527D-3F4F-5E3F-48373F3F5200");


	public InMemoryRepositories()
	{
		PortalRepositories = new Chaos.Portal.Core.Data.InMemory.InMemoryRepositories();
		PortalRepositories.User.Create(new Guid("D15F5D79-FB8D-420C-9A86-42D179F0695A"), "system");
		PortalRepositories.Group.Create(AdminGroupId, "Administrator",
			new Chaos.Portal.Core.UserId("D15F5D79-FB8D-420C-9A86-42D179F0695A"),
			uint.MaxValue);
		PortalRepositories.Group.Create(ContributorGroupId, "Contributor",
			new Chaos.Portal.Core.UserId("D15F5D79-FB8D-420C-9A86-42D179F0695A"), 1);
		PortalRepositories.Group.Create(UserGroupId, "User",
			new Chaos.Portal.Core.UserId("D15F5D79-FB8D-420C-9A86-42D179F0695A"), 0);

		McmRepositories = new Chaos.Mcm.Data.InMemory.InMemoryRepositories();
		Session = new SessionRepository(PortalRepositories.Session, AnonymousUserId);
		Password = new PasswordRepository();
		User = new UserRepository(
			PortalRepositories.User,
			McmRepositories.Object,
			McmRepositories.Metadata,
			PortalRepositories.Group,
			Password,
			new UserIndexStub(),
			ProfileMetadataSchemaId,
			AdminGroupId,
			ContributorGroupId,
			UserGroupId,
			50,
			100);
		SessionUser = new SessionUserGetter(PortalRepositories.User);
		DataDefinition = new DataDefinitionRepository(McmRepositories.MetadataSchema);
		SearchDefinition = new SearchDefinitionRepository();
		Project = new ProjectRepository(
			userId => User.Get(userId),
			projectId => Label.Get(projectId));
		McmRepositories.MetadataSchema.Create(
			new MetadataSchema(
				AssetMetadataSchemaId.Value,
				"EZA Asset",
				"<xml />",
				DateTime.UtcNow),
			Guid.Empty);
		McmRepositories.MetadataSchema.Create(
			new MetadataSchema(
				ProfileMetadataSchemaId.Value,
				"EZA Asset",
				"{\"Name\":\"EZA Asset\"}",
				DateTime.UtcNow),
			Guid.Empty);
		McmRepositories.MetadataSchema.Create(
			new MetadataSchema(
				AnnotationMetadataSchemaId.Value,
				"EZA Annotation",
				"{\"Name\":\"EZA Annotation\",\"Type\":1,\"Fields\":[{\"Id\":\"a1\",\"Type\":\"String\",\"IsRequired\": false}]}",
				DateTime.UtcNow),
			Guid.Empty);

		Asset = new AssetRepository(
			McmRepositories.Object,
			McmRepositories.Metadata,
			DataDefinition,
			User,
			new Folder._Id(1),
			AssetMetadataSchemaId);

		Label = new LabelRepository(McmRepositories.Label);

			File = new FileRepository(McmRepositories.File, McmRepositories.Object, 1);
			Format = new FormatRepository(McmRepositories.Format);
		}

	public Chaos.Portal.Core.Data.InMemory.InMemoryRepositories PortalRepositories { get; }
	public Chaos.Mcm.Data.InMemory.InMemoryRepositories McmRepositories { get; }
	public SearchDefinitionRepository SearchDefinition { get; }
	public DataDefinitionRepository DataDefinition { get; }
	public SessionUserGetter SessionUser { get; }
	public SessionRepository Session { get; }
	public UserRepository User { get; }
	public ProjectRepository Project { get; }
	public AssetRepository Asset { get; }
	public LabelRepository Label { get; }
	public FileRepository File { get; }

		public PasswordRepository Password;

		public FormatRepository Format { get; }
	}

public class UserIndexStub : IIndex
{
	public IIndexResponse<FlexibleResult> Query(IQuery query)
	{
		throw new NotImplementedException();
	}

	public void Index(IIndexable indexable)
	{
	}

	public void Index(IEnumerable<IIndexable> indexables)
	{
	}

	public void Commit(bool isSoftCommit = false)
	{
	}

	public void Optimize()
	{
	}

	public void Delete()
	{
	}

	public void Delete(string uniqueIdentifier)
	{
	}

	public Task<IIndexResponse<FlexibleResult>> QueryAsync(IQuery query)
	{
		throw new NotImplementedException();
	}

	public Task IndexAsync(IEnumerable<IIndexable> indexables)
	{
		return Task.CompletedTask;
	}

	public Task IndexAsync(IIndexable indexable)
	{
		return Task.CompletedTask;
	}
}
