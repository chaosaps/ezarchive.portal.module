using System;
using System.Collections.Generic;
using System.Linq;
using EzArchive.Application.UseCases.SearchDefinitions;
using EzArchive.Domain;

namespace EzArchive.Data.InMemory;

public class SearchDefinitionRepository : ISearchDefinitionRepository
{
	private readonly IList<SearchFieldDefinition> searchDefinitions;

	public SearchDefinitionRepository(IEnumerable<SearchFieldDefinition> searchDefinitions)
	{
		this.searchDefinitions = searchDefinitions.ToList();
	}

	public SearchDefinitionRepository()
	{
		searchDefinitions = new List<SearchFieldDefinition>();
	}

	public IReadOnlyList<SearchFieldDefinition> Get() =>
		searchDefinitions.ToList().AsReadOnly();

	public SearchFieldDefinition Get(string displayName)
	{
		var sfd = searchDefinitions.FirstOrDefault(item => item.DisplayName == displayName);

		if (sfd == null) throw new Exception("Invalid Field Name: " + displayName);

		return sfd;
	}

	public void Add(SearchFieldDefinition searchFieldDefinition)
	{
		searchDefinitions.Add(searchFieldDefinition);
	}
}