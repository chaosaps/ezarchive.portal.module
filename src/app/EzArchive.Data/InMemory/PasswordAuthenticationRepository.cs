using System.Collections.Generic;
using System.Linq;
using EzArchive.Application.UseCases.Passwords;
using EzArchive.Domain;

namespace EzArchive.Data.InMemory
{
	public class PasswordAuthenticationRepository : IPasswordAuthenticationRepository
	{
		private readonly IDictionary<UserId, HashedPassword> entities = new Dictionary<UserId, HashedPassword>();

		public bool UserHasPasswordHash(UserId userId, HashedPassword hashedPassword) =>
			entities.Any(itm => itm.Key == userId && itm.Value == hashedPassword);

		public void Create(UserId userId, HashedPassword hashedPassword)
		{
			entities.Add(userId, hashedPassword);
		}
	}
}