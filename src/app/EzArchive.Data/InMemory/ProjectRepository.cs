using System;
using System.Collections.Generic;
using System.Linq;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Domain;

namespace EzArchive.Data.InMemory;

public class ProjectRepository : IProjectRepository
{
	private readonly Func<UserId, User> getUser;
	private readonly Func<ProjectId, IEnumerable<Label>> getLabels;
	private uint lastInsertId = 0;

	private readonly IDictionary<ProjectId, Project> projects = new Dictionary<ProjectId, Project>();

	public ProjectRepository(
		Func<UserId, User> getUser,
		Func<ProjectId, IEnumerable<Label>> getLabels)
	{
		this.getUser = getUser;
		this.getLabels = getLabels;
	}

	public IReadOnlyList<Project> Get(UserId userId) =>
		Get()
			.Where(p => p.Users.Any(u => u.Id == userId))
			.ToList()
			.AsReadOnly();

	public Project Get(LabelId labelId) =>
		Get()
			.SingleOrDefault(p => p.Labels.Any(l => l.Id == labelId));

	public Project Set(ProjectId id, string name)
	{
		if (id == null)
		{
			var project = new Project(
				new ProjectId(++lastInsertId),
				name,
				new Label[0],
				new User[0]);
			Add(project);

			return project;
		}

		projects[id] = new Project(
			projects[id].Id,
			name,
			getLabels(id),
			projects[id].Users);

		return projects[id];
	}

	public void AddUser(ProjectId id, UserId userId)
	{
		projects[id] = new Project(
			projects[id].Id,
			projects[id].Name,
			projects[id].Labels,
			projects[id].Users.Append(getUser(userId)));
	}

	public Project Get(ProjectId id) =>
		projects.ContainsKey(id) == false
			? null
			: new Project(projects[id].Id, projects[id].Name, getLabels(id), projects[id].Users);

	public IEnumerable<Project> Get() =>
		projects.Values.Select(p => new Project(p.Id, p.Name, getLabels(p.Id), p.Users));

	public void Delete(ProjectId id) =>
		projects.Remove(id);

	public void RemoveUser(ProjectId id, UserId userId)
	{
		projects[id] = new Project(
			projects[id].Id,
			projects[id].Name,
			projects[id].Labels,
			projects[id].Users.Where(u => u.Id != userId));
	}

	public void Add(Project project) =>
		projects.Add(project.Id, project);
}