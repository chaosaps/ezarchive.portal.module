using System.Linq;
using Chaos.Portal.Core.Data;
using Chaos.Portal.Core.Data.Model;
using EzArchive.Application.UseCases.Sessions.Authenticate;
using EzArchive.Domain;

namespace EzArchive.Data.Portal;

public class SessionUserGetter : ISessionUserGetter
{
	private readonly IUserRepository userRepository;

	public SessionUserGetter(IUserRepository userRepository)
	{
		this.userRepository = userRepository;
	}

	public SessionUser Get(UserName userName)
	{
		var userInfo = userRepository.Get(userName.Value);

		return Map(userInfo);
	}

	public SessionUser Get(UserId userId)
	{
		var userInfo = userRepository.Get(userId.Value, null, null, null).First();
		return Map(userInfo);
	}

	private static SessionUser Map(UserInfo userInfo) =>
		new SessionUser(
			new UserId(userInfo.Guid),
			(uint)userInfo.SystemPermissions,
			new UserName(userInfo.Email));
}