using System.Collections.Generic;
using System.Linq;
using EzArchive.Application.UseCases.Labels;
using EzArchive.Domain;

namespace EzArchive.Data.Portal;

public class LabelRepository : ILabelRepository
{
	private readonly Chaos.Mcm.Data.ILabelRepository labelRepository;

	public LabelRepository(Chaos.Mcm.Data.ILabelRepository labelRepository)
	{
		this.labelRepository = labelRepository;
	}

	public IEnumerable<Label> Get(AssetId id)
	{
		return labelRepository
			.Get(objectId: id.Value)
			.Select(l =>
				new Label(
					new LabelId(l.Identifier.Value),
					l.Name,
					l.ProjectId));
	}

		public IEnumerable<Label> Get(ProjectId projectId) =>
			labelRepository
				.Get(projectId: projectId.Value)
				.Select(l =>
					new Label(new LabelId(l.Identifier.Value),
						l.Name,
						l.ProjectId));

		public void SetAssociation(LabelId id, AssetId assetId) =>
			labelRepository.AssociationWithObject(id.Value, assetId.Value);

		public void DeleteAssociation(LabelId id, AssetId assetId) =>
			labelRepository.DisassociationWithObject(id.Value, assetId.Value);

		public bool Delete(LabelId id) =>
			labelRepository.Delete(id.Value);

		public Label Set(ProjectId projectId, LabelId id, string name)
		{
			var label = labelRepository.Set(projectId.Value, new Chaos.Mcm.Core.Model.Label
			{
				Identifier = id == null ? (uint?)null : id.Value,
				Name = name
			});

		return new Label(
			new LabelId(label.Identifier.Value),
			label.Name,
			label.ProjectId);
	}
}
