using System.Linq;
using Chaos.Mcm.Data;
using EzArchive.Domain;

namespace EzArchive.Data.Portal
{
	public class FormatRepository : Application.UseCases.Formats.IFormatRepository
	{
		private readonly IFormatRepository formatRepository;

		public FormatRepository(IFormatRepository formatRepository)
		{
			this.formatRepository = formatRepository;
		}

		public Format Get(FormatId id)
		{
			return formatRepository
				.Get(id.Value)
				.Select(f => new Format(new FormatId(f.ID), f.Extension))
				.FirstOrDefault();
		}
	}
}