using System;
using System.Collections.Generic;
using System.Linq;
using Chaos.Mcm.Core.Model;
using Chaos.Mcm.Data;
using EzArchive.Application.UseCases.DataDefinitions;
using EzArchive.Data.Exceptions;
using EzArchive.Domain;
using Newtonsoft.Json;

namespace EzArchive.Data.Portal;

public class DataDefinitionRepository : IDataDefinitionRepository
{
	private readonly IMetadataSchemaRepository metadataSchemaRepository;
	private IList<DataDefinition> cache = new List<DataDefinition>();
	private DateTime cacheExpiration = new DateTime();

	public DataDefinitionRepository(IMetadataSchemaRepository metadataSchemaRepository)
	{
		this.metadataSchemaRepository = metadataSchemaRepository;
	}

	public IEnumerable<DataDefinition> GetAnnotations() =>
		GetDefinitions().Where(def => def.Type == DefinitionType.Annotation);

	public DataDefinition GetAnnotations(DataDefinition.__Id definitionId) =>
		GetAnnotations().Single(d => d.Id == definitionId.Value.ToString());

	public IReadOnlyList<DataDefinition> GetDataDefinitions()
	{
		return GetDefinitions()
			.Where(def => def.Type == DefinitionType.Metadata)
			.ToList()
			.AsReadOnly();
	}

	private IEnumerable<DataDefinition> GetDefinitions()
	{
		if (DateTime.Now.CompareTo(cacheExpiration) > 0)
		{
			lock (cache)
			{
				if (DateTime.Now.CompareTo(cacheExpiration) > 0)
				{
					var schemas = metadataSchemaRepository.Get();

					cache = MapDataDefinitions(schemas).ToList();
					cacheExpiration = DateTime.Now.AddMinutes(20);
				}
			}
		}

		return cache;
	}

	private static IEnumerable<DataDefinition> MapDataDefinitions(IList<MetadataSchema> schemas)
	{
		var definitions = new List<DataDefinition>();

		foreach (var schema in schemas)
		{
			try
			{
				var definition = schema.GetSchema<DataDefinition>();
				definition.Identifier = schema.Guid.ToString();

				definitions.Add(definition);
			}
			catch (JsonReaderException)
			{
			}
		}

		return definitions;
	}

	public bool IsTypeValid(uint typeId)
	{
		return GetDataDefinitions().Any(item => item.TypeId == typeId);
	}

	public DataDefinition GetDataDefinition(string id)
	{
		var schema = id.Split('.')[0];
		var definition = GetDataDefinitions().FirstOrDefault(i => i.Name == schema);

		if (definition == null)
			throw new DataDefinitionNotFound(id);

		return definition;
	}
}