using Chaos.Authentication.Data;
using EzArchive.Application.UseCases.Sessions;
using EzArchive.Domain;

namespace EzArchive.Data.Portal;

public class CredentialsValidator : ICredentialsValidator
{
	private readonly IPasswordRepository passwordRepository;

	public CredentialsValidator(IPasswordRepository passwordRepository)
	{
		this.passwordRepository = passwordRepository;
	}

	public bool AreCredentialsValid(UserId userId, Password password) =>
		passwordRepository.Get(userId.Value, password.GenerateHash(userId).ToString()) != null;
}
