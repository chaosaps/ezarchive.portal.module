using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Chaos.Mcm.Core.Model;
using Chaos.Mcm.Data;
using Chaos.Mcm.Domain;
using EzArchive.Application.Errors;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Application.UseCases.DataDefinitions;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;
using EzArchive.Domain.Values;
using Folder = EzArchive.Domain.Folder;
using Object = Chaos.Mcm.Core.Model.Object;

namespace EzArchive.Data.Portal;

public class AssetRepository : IAssetRepository
{
	private readonly IObjectRepository objectRepository;
	private readonly IMetadataRepository metadataRepository;
	private readonly IDataDefinitionRepository dataDefinitionRepository;
	private readonly IUserRepository userRepository;
	private readonly Folder._Id assetFolderId;
	private readonly MetadataSchemaId assetMetadataSchemaId;

	public AssetRepository(
		IObjectRepository objectRepository,
		IMetadataRepository metadataRepository,
		IDataDefinitionRepository dataDefinitionRepository,
		IUserRepository userRepository,
		Folder._Id assetFolderId,
		MetadataSchemaId assetMetadataSchemaId)
	{
		this.objectRepository = objectRepository;
		this.metadataRepository = metadataRepository;
		this.dataDefinitionRepository = dataDefinitionRepository;
		this.userRepository = userRepository;
		this.assetFolderId = assetFolderId;
		this.assetMetadataSchemaId = assetMetadataSchemaId;
	}

	public Asset Set(Asset asset)
	{
		var id = EnsureObjectExists(asset);
		var obj = objectRepository.Get(id, includeMetadata: true, includeFiles: true);

		if (obj == null)
		{
			objectRepository.Create(asset.Id.Value, asset.Type.Value, assetFolderId.Value);
			obj = objectRepository.Get(asset.Id.Value, includeMetadata: true, includeFiles: true);
		}

		SaveAssetXml(asset, obj, id);
		SaveFieldXml(asset, obj, id);
		SaveAnnotationXml(asset, obj, id);

		var updated = Get(new AssetId(id));

		return updated;

		Guid EnsureObjectExists(Asset asset)
		{
			if (asset.Id != null)
				return asset.Id.Value;

			var id = Guid.NewGuid();
			objectRepository.Create(id, asset.Type.Value, assetFolderId.Value);

			return id;
		}

		void SaveAssetXml(Asset asset, Object obj, Guid objectGuid)
		{
			var assetMetadata =
				obj.Metadatas.FirstOrDefault(
					meta => meta.MetadataSchemaId == assetMetadataSchemaId);
			var assetMetadataId = assetMetadata?.Id ?? new MetadataId(Guid.NewGuid());
			var assetXml = AssetXml();
			var revisionId = assetMetadata?.RevisionID ?? 0;
			metadataRepository.Set(
				new Metadata(
					assetMetadataId,
					null,
					assetMetadataSchemaId,
					new Chaos.Portal.Core.UserId(asset.LastChangedByUserId.Value),
					revisionId,
					assetXml,
					DateTime.UtcNow),
				objectGuid);

			XDocument AssetXml()
			{
				var xml = new XDocument();
				var root = new XElement("Chaos.Asset");
				var tags = new XElement("Tags");

				foreach (var tag in asset.Tags)
					tags.Add(new XElement("Tag", tag));

				root.Add(new XElement("DoFilesRequireLogin", asset.DoFilesRequireLogin));
				root.Add(tags);
				xml.Add(root);

				return xml;
			}
		}

		void SaveFieldXml(Asset asset, Object obj, Guid objectGuid)
		{
			foreach (var data in asset.Data)
			{
				var fieldMetadata =
					obj.Metadatas.FirstOrDefault(meta => meta.MetadataSchemaId.Value ==
					                                     Guid.Parse(data.DataDefinition.Id));
				var fieldMetadataId = fieldMetadata?.Id ?? new MetadataId(Guid.NewGuid());
				var fieldXml = CreateFieldXml(data);
				var revisionId = fieldMetadata == null ? 0 : fieldMetadata.RevisionID;
				metadataRepository.Set(
					new Metadata(
						fieldMetadataId,
						"da",
						new MetadataSchemaId(data.DataDefinition.Id),
						new Chaos.Portal.Core.UserId(asset.LastChangedByUserId.Value),
						revisionId,
						fieldXml,
						DateTime.UtcNow),
					objectGuid);
			}
		}
	}

	private XDocument CreateFieldXml(Asset._Data data)
	{
		var xml = new XDocument();
		var root = new XElement("Chaos.Data");
		var fieldsElement = new XElement("Fields");
		xml.Add(root);
		root.Add(fieldsElement);

		foreach (var field in data.Fields)
		{
			var f = data.DataDefinition.GetFieldDefinition(field.Key);

			if (f == null) continue;

			var fieldElement = CreateFieldXml(field);

			if (fieldElement == null) continue;

			var name = new XAttribute("Name", field.Key);
			var type = new XAttribute("Type", f.Type);

			fieldElement.Add(name);
			fieldElement.Add(type);
			fieldsElement.Add(fieldElement);
		}

		return xml;
	}

	private XElement CreateFieldXml(KeyValuePair<string, Value> field)
	{
		var tableValue = field.Value as TableValue;
		var stringValue = field.Value as StringValue;
		var dateTimeValue = field.Value as DateTimeValue;
		var textValue = field.Value as TextValue;
		var boolValue = field.Value as BooleanValue;
		var nullValue = field.Value as NullValue;

		if (stringValue != null)
			return new XElement("Field", field.Value.ToString());

		if (textValue != null)
			return new XElement("Field", field.Value.ToString());

		if (dateTimeValue != null)
			return new XElement("Field", field.Value.ToString());

		if (boolValue != null)
			return new XElement("Field", field.Value.ToString());

		if (nullValue != null)
			return null;

		if (tableValue != null)
		{
			var fieldElement = new XElement("Field");

			foreach (var row in tableValue.Value)
			{
				var rowElement = new XElement("Row");

				foreach (var cell in row)
					rowElement.Add(new XElement("Cell", cell));

				fieldElement.Add(rowElement);
			}

			return fieldElement;
		}

		throw new NotImplementedException($"Value type: '{field.Key}' not implemented in PersistentAssetRepository");
	}

	private void SaveAnnotationXml(Asset asset, Object obj, Guid objectGuid)
	{
		foreach (var annotationGroup in asset.AnnotationsGroups)
		{
			var annotationMetadata =
				obj.Metadatas.FirstOrDefault(meta => meta.MetadataSchemaId.Value == annotationGroup.DefinitionId);
			var annotationMetadataId = annotationMetadata?.Id ?? new MetadataId(Guid.NewGuid());
			var annotationXml = CreateAnnotationXml(annotationGroup);
			var revisionId = annotationMetadata == null ? 0 : annotationMetadata.RevisionID;

			metadataRepository.Set(
				new Metadata(
					annotationMetadataId,
					null,
					new MetadataSchemaId(annotationGroup.DefinitionId),
					new Chaos.Portal.Core.UserId(asset.LastChangedByUserId.Value),
					revisionId,
					annotationXml,
					DateTime.UtcNow),
				objectGuid);
		}
	}

	public XDocument CreateAnnotationXml(Asset._AnnotationGroup annotationGroup)
	{
		var xml = new XDocument();
		var root = new XElement("Annotations");

		foreach (var annotation in annotationGroup.Annotations)
		{
			var element = new XElement("Annotation");

			foreach (var pair in annotation)
			{
				var fieldXml = CreateFieldXml(pair);

				if (fieldXml == null) continue;

				fieldXml.Name = pair.Key;

				element.Add(fieldXml);
			}

			root.Add(element);
		}

		xml.Add(root);
		return xml;
	}

	public Asset Get(AssetId id)
	{
		var obj = objectRepository.Get(id.Value,
			includeMetadata: true,
			includeFiles: true);

		if (obj == null)
			return null;

		return Map(obj);
	}

	private Asset Map(Object obj)
	{
		var (filesRequireLogin, tags) = ExtractAssetData(obj);

		return new Asset(
			new AssetId(obj.Guid),
			new Asset._Type(obj.ObjectTypeID),
			LastChangedBy(obj),
			filesRequireLogin,
			tags, ExtractData(obj),
			ExtractAnnotations(obj),
			ExtractFiles(obj));
	}

	public IEnumerable<Asset> Get(uint? objectTypeId = null, uint pageIndex = 0u)
	{
		const uint pageSize = 1000u;

		while (true)
		{
			var objs = ObjectGet(objectTypeId, pageIndex, pageSize);

			foreach (var obj in objs)
				yield return Map(obj);

			if (objs.Count < pageSize)
				break;

			pageIndex++;
		}

		IList<Object> ObjectGet(uint? objectTypeId, uint pageIndex, uint pageSize, uint retries = 10)
		{
			try
			{
				return objectRepository.Get(
					assetFolderId.Value,
					includeMetadata: true,
					includeFiles: true,
					pageIndex: pageIndex,
					pageSize: pageSize,
					objectTypeId: objectTypeId);
			}
			catch (Exception)
			{
				if (retries > 0)
					return ObjectGet(objectTypeId, pageIndex, pageSize, --retries);

				throw;
			}
		}
	}

	public Task DeleteAsync(AssetId assetId)
	{
		objectRepository.Delete(assetId.Value);
		return Task.CompletedTask;
	}

	private (bool doFilesRequireLogin, IEnumerable<string> tags) ExtractAssetData(Object obj)
	{
		var fieldMetadata =
			obj.Metadatas.FirstOrDefault(
				item => item.MetadataSchemaId == assetMetadataSchemaId);

		if (fieldMetadata == null)
			return (false, new List<string>());

		var xml = fieldMetadata.MetadataXml;
		var doFilesRequireLogin = xml.Descendants("DoFilesRequireLogin").Any(e => e.Value.ToLower() == "true");
		var tags = xml.Descendants("Tag").Select(field => field.Value);

		return (doFilesRequireLogin, tags);
	}

	private UserId LastChangedBy(Object obj)
	{
		var userId = obj.Metadatas
			.OrderByDescending(m => m.RevisionID)
			.Select(m => m.EditingUserGuid)
			.FirstOrDefault();

		return userId != null
			? new UserId(userId.Value)
			: null;
	}

	private IEnumerable<Asset._Data> ExtractData(Object obj)
	{
		foreach (var fieldMetadata in obj.Metadatas)
		{
			var definition = dataDefinitionRepository.GetDataDefinitions()
				.SingleOrDefault(d => d.Id == fieldMetadata.MetadataSchemaId.Value.ToString());

			if (definition == null)
				continue;

			var fields = new Dictionary<string, Value>();

			foreach (var fieldDefinition in definition.Fields.Where(f => f.Type == "Boolean"))
			{
				if (!fields.ContainsKey(fieldDefinition.Id) && fieldDefinition.Options != null &&
				    fieldDefinition.Options.Any())
					fields.Add(fieldDefinition.Id, new BooleanValue(fieldDefinition.Options.First() == "true"));
			}

			foreach (var field in fieldMetadata.MetadataXml.Descendants("Field"))
			{
				var fieldName = field.Attribute("Name").Value;
				var fieldType = field.Attribute("Type").Value;
				var fieldValue = ExtractField(fieldType, field);

				if (fields.ContainsKey(fieldName))
					fields[fieldName] = fieldValue;
				else
					fields.Add(fieldName, fieldValue);
			}

			yield return new Asset._Data(fields, definition);
		}
	}

	private static Value ExtractField(string fieldType, XElement field)
	{
		switch (fieldType)
		{
			case "Text":
				return new StringValue(field.Value);
			case "String":
				return new StringValue(field.Value);
			case "MultipleChoice":
				return new StringValue(field.Value);
			case "Boolean":
				return new BooleanValue(field.Value.ToLower() == "true");
			case "Datetime":
			case "DateTime":
				return new DateTimeValue(field.Value);
			case "Table":
			{
				var table = new TableValue();

				foreach (var rowElement in field.Elements("Row"))
				{
					var row = new List<string>();

					foreach (var cellElement in rowElement.Elements("Cell"))
						row.Add(cellElement.Value);

					table.Value.Add(row);
				}

				return table;
			}
		}

		throw new NotImplementedException("Unsupported Field type: " + fieldType);
	}

	private IEnumerable<Asset._AnnotationGroup> ExtractAnnotations(Object obj)
	{
		foreach (var metadata in obj.Metadatas)
		{
			if (metadata.MetadataXml.Root.Name != "Annotations") continue;

			var dataDefinition = dataDefinitionRepository.GetAnnotations()
				.SingleOrDefault(ad => ad.Identifier == metadata.MetadataSchemaId.Value.ToString());

			if (dataDefinition == null)
				throw new UnhandledException(
					"Annotation Definition [" + metadata.MetadataSchemaId.Value + "] not found");

			var annotations = ExtractAnnotations(metadata).ToList();

			if (!annotations.Any()) continue;

			yield return new Asset._AnnotationGroup(
				dataDefinition.Name,
				Guid.Parse(dataDefinition.Identifier),
				annotations
			);
		}
	}

	private IEnumerable<IDictionary<string, Value>> ExtractAnnotations(Metadata metadata)
	{
		var definition = dataDefinitionRepository.GetAnnotations()
			.Single(ad => ad.Identifier == metadata.MetadataSchemaId.Value.ToString());

		foreach (var aElement in metadata.MetadataXml.Descendants("Annotation"))
		{
			var dict = new Dictionary<string, Value>
			{
				{ "__OwnerId", new StringValue("") },
				{ "__Owner", new StringValue("System") },
				{ "__Permission", new StringValue("0") },
				{ "__Visibility", new StringValue("Public") }
			};

			foreach (var field in aElement.Elements())
			{
				var name = field.Name.ToString();

				try
				{
					if (field.Name.ToString() == "Identifier")
						dict.Add(name, new StringValue(field.Value));
					else if (field.Name.ToString() == "__OwnerId")
					{
						if (Guid.TryParse(field.Value, out Guid id))
						{
							try
							{
								var ezUser = userRepository.Get(new UserId(id));
								dict["__OwnerId"] = new StringValue(ezUser.Id.Value.ToString());
								dict["__Owner"] = new StringValue(ezUser.Name ?? "-");
							}
							catch (Exception e)
							{
								Console.WriteLine(id);
								Console.WriteLine(e);
							}
						}
					}
					else if (field.Name.ToString() == "__Permission")
						dict["__Permission"] = new StringValue(field.Value);
					else if (field.Name.ToString() == "__Visibility")
						dict["__Visibility"] = new StringValue(field.Value);
					else
					{
						var type = definition.GetFieldDefinition(name).Type;

						if (dict.ContainsKey(name))
							Console.WriteLine(name);

						dict.Add(name, ExtractField(type, field));
					}
				}
				catch (InvalidOperationException)
				{
				}
			}

			yield return dict;
		}
	}

	private static IList<Asset._FileReference> ExtractFiles(Object obj)
	{
		var files = new Dictionary<uint, Asset._FileReference>();

		foreach (var fileInfo in obj.Files)
		{
			if (fileInfo.ParentID.HasValue)
				ExtractParentFile(files, fileInfo);
			else
				ExtractFile(files, fileInfo);
		}

		return files.Values.ToList();
	}

	private static void ExtractFile(Dictionary<uint, Asset._FileReference> files, FileInfo fileInfo)
	{
		if (files.ContainsKey(fileInfo.Identifier) == false)
		{
			var f = new Asset._FileReference(
				fileInfo.Identifier.ToString(),
				fileInfo.OriginalFilename,
				fileInfo.FormatType,
				new List<Asset._FileReference._Destination>());

			files.Add(fileInfo.Identifier, f);
		}

		var file = files[fileInfo.Identifier];
		file.Destinations.Add(new Asset._FileReference._Destination(
			fileInfo.MimeType,
			fileInfo.URL));
	}

	private static void ExtractParentFile(Dictionary<uint, Asset._FileReference> files, FileInfo fileInfo)
	{
		if (files.ContainsKey(fileInfo.ParentID.Value) == false)
		{
			var f = new Asset._FileReference(
				fileInfo.ParentID.Value.ToString(),
				fileInfo.OriginalFilename,
				fileInfo.FormatType,
				new List<Asset._FileReference._Destination>());

			files.Add(fileInfo.ParentID.Value, f);
		}

		var file = files[fileInfo.ParentID.Value];
		file.Destinations.Add(new Asset._FileReference._Destination(
			fileInfo.MimeType,
			fileInfo.URL));
	}
}