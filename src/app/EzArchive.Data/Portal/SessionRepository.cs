using System.Linq;
using Chaos.Portal.Core.Data;
using EzArchive.Domain;

namespace EzArchive.Data.Portal;

public class SessionRepository : Application.UseCases.Sessions.ISessionRepository
{
	private readonly ISessionRepository sessionRepository;
	private readonly UserId anonymousUserId;

	public SessionRepository(ISessionRepository sessionRepository, UserId anonymousUserId)
	{
		this.sessionRepository = sessionRepository;
		this.anonymousUserId = anonymousUserId;
	}

	public Session Create(UserId userId)
	{
		return Map(sessionRepository.Create(userId.Value));
	}

	public Session Get(SessionId sessionId)
	{
		var portalSession = sessionRepository.Get(sessionId.Value, null).FirstOrDefault();

		return portalSession != null
			? Map(portalSession)
			: null;
	}

	public void Update(SessionId sessionId, UserId userId) =>
		sessionRepository.Update(sessionId.Value, userId.Value);

	private Session Map(Chaos.Portal.Core.Data.Model.Session portalSession)
	{
		return new Session(
			new SessionId(portalSession.Id),
			portalSession.DateCreated,
			portalSession.DateModified ?? portalSession.DateCreated,
			new UserId(portalSession.UserId),
			portalSession.UserId == anonymousUserId.Value);
	}
}