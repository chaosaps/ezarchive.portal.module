using System.Linq;
using Chaos.Authentication.Data;
using Chaos.Portal.Core.Data;
using EzArchive.Application.Errors;
using EzArchive.Application.UseCases.Passwords;
using EzArchive.Domain;

namespace EzArchive.Data.Portal
{
	public class PasswordAuthenticationRepository : IPasswordAuthenticationRepository
	{
		private readonly IUserRepository userRepository;
		private readonly IPasswordRepository passwordRepository;

		public PasswordAuthenticationRepository(IUserRepository userRepository, IPasswordRepository passwordRepository)
		{
			this.userRepository = userRepository;
			this.passwordRepository = passwordRepository;
		}

		public bool UserHasPasswordHash(UserId userId, HashedPassword hashedPassword)
		{
			var user = userRepository.Get(userId.Value, null, null, null).FirstOrDefault();

			if (user == null)
				return false;

			var result = passwordRepository.Get(userId.Value, hashedPassword.ToString());

			return result != null;
		}

		public void Create(UserId userId, HashedPassword hashedPassword)
		{
			var rowsAffected = passwordRepository.Set(userId.Value, hashedPassword.ToString());

			if (rowsAffected == 0)
				throw new UnhandledException("Password was not updated");
		}
	}
}