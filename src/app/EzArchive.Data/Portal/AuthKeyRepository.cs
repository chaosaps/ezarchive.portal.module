using System.Linq;
using Chaos.Authentication.Data;
using EzArchive.Domain;

namespace EzArchive.Data.Portal;

public class AuthKeyRepository : Application.UseCases.AuthKeys.IAuthKeyRepository
{
	private readonly IAuthenticationRepository authenticationRepository;

	public AuthKeyRepository(IAuthenticationRepository authenticationRepository)
	{
		this.authenticationRepository = authenticationRepository;
	}

	public void Create(AuthKey authKey)
	{
		authenticationRepository.AuthKey.Create(
			authKey.Token.Value,
			authKey.UserId.Value,
			authKey.Name);
	}

	public AuthKey Get(AuthToken.LevelTwo token)
	{
		var authKey = authenticationRepository.AuthKey.Get(null, token.Value).SingleOrDefault();

		if (authKey == null)
			return null;

		return new AuthKey(
			AuthToken.LevelTwo.CreateWithoutHashing(authKey.Token),
			new UserId(authKey.UserGuid),
			authKey.Name);
	}
}