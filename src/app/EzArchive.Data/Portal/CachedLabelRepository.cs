﻿using System.Collections.Generic;
using EzArchive.Application.UseCases.Labels;
using EzArchive.Domain;

namespace EzArchive.Data.Portal
{
	public class CachedLabelRepository : ILabelRepository
	{
		private readonly CachedProjectRepository projectRepository;
		private readonly ILabelRepository labelRepository;

		public CachedLabelRepository(
			CachedProjectRepository projectRepository,
			ILabelRepository labelRepository)
		{
			this.projectRepository = projectRepository;
			this.labelRepository = labelRepository;
		}

		public IEnumerable<Label> Get(AssetId id)
		{
			return labelRepository.Get(id);
		}

		public void SetAssociation(LabelId id, AssetId assetId)
		{
			labelRepository.SetAssociation(id, assetId);
		}

		public void DeleteAssociation(LabelId id, AssetId assetId)
		{
			labelRepository.DeleteAssociation(id, assetId);
		}

		public bool Delete(LabelId id)
		{
			projectRepository.ClearCache();

			return labelRepository.Delete(id);
		}

		public Label Set(ProjectId projectId, LabelId id, string name)
		{
			projectRepository.ClearCache();

			return labelRepository.Set(projectId, id, name);
		}
	}
}
