using System.Collections.Generic;
using System.Linq;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;

namespace EzArchive.Data.Portal;

public class ProjectRepository : IProjectRepository
{
	private readonly Chaos.Mcm.Data.IProjectRepository projectRepository;
	private readonly IUserRepository userRepository;

		public ProjectRepository(
			Chaos.Mcm.Data.IProjectRepository projectRepository,
			IUserRepository userRepository)
		{
			this.projectRepository = projectRepository;
			this.userRepository = userRepository;
		}

		public IReadOnlyList<Project> Get(UserId userId) =>
			Map(projectRepository.Get(userId: userId.Value));

		public Project Get(ProjectId id)
		{
			var enumerable = projectRepository.Get(id: id.Value).ToList();
			return Map(enumerable).SingleOrDefault();
		}

	public Project Get(LabelId labelId) =>
		Map(projectRepository.Get(labelId: labelId.Value)).SingleOrDefault();

		public void Delete(ProjectId id) =>
			projectRepository.Delete(id.Value);

		public Project Set(ProjectId id, string name)
		{
			var result = projectRepository.Set(new Chaos.Mcm.Core.Model.Project
			{
				Identifier = id == null ? (uint?) null : id.Value,
				Name = name
			});

			return Get(new ProjectId(result.Identifier.Value));
		}

		public void AddUser(ProjectId id, UserId userId) =>
			projectRepository.AddUser(id.Value, userId.Value);

		public void RemoveUser(ProjectId id, UserId userId) =>
			projectRepository.RemoveUser(id.Value, userId.Value);

		private IReadOnlyList<Project> Map(IEnumerable<Chaos.Mcm.Core.Model.Project> projects) =>
			projects
				.Select(p => new Project(
					new ProjectId(p.Identifier.Value),
					p.Name,
					p.Labels.Select(l =>
							new Label(
								new LabelId(l.Identifier.Value),
								l.Name,
								l.ProjectId))
						.ToList()
						.AsReadOnly(),
					p.UserIds
						.Select(userid => GetUser(new UserId(userid)))
						.ToList()
						.AsReadOnly()))
				.ToList()
				.AsReadOnly();

		private User GetUser(UserId id) =>
			userRepository.Get(id);
	}
