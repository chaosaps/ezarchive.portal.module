using System;
using System.Collections.Generic;
using System.Linq;
using EzArchive.Application.UseCases.DataDefinitions;
using EzArchive.Application.UseCases.SearchDefinitions;
using EzArchive.Domain;
using EZArchive.Domain;
using EzArchive.Domain.Values;

namespace EzArchive.Data.Portal;

public class SearchBuilder
{
	private readonly ISearchDefinitionRepository searchDefinitionRepository;
	private readonly IDataDefinitionRepository dataDefinitionRepository;
	private readonly Value DEFAULT_VALUE_FOR_EMPTY_FIELD = new StringValue(null);
	private const string DEFAULT_TYPE_FOR_EMPTY_FIELD = "String";

	public SearchBuilder(
		ISearchDefinitionRepository searchDefinitionRepository,
		IDataDefinitionRepository dataDefinitionRepository)
	{
		this.searchDefinitionRepository = searchDefinitionRepository;
		this.dataDefinitionRepository = dataDefinitionRepository;
	}

	public Search Build(Asset asset)
	{
		return new Search(asset.Id, asset.Type.Value.ToString(), GetSearchFields(asset.Data), asset.Tags);
	}

	private IList<SearchField> GetSearchFields(IEnumerable<Asset._Data> data)
	{
		var searchFields = new List<SearchField>();

		foreach (var definition in searchDefinitionRepository.Get())
		{
			var field = GetFieldValue(data, definition);

			if (string.IsNullOrEmpty(field.Item1.ToString())) continue;

			searchFields.Add(
				new SearchField(
					definition.DisplayName,
					field.Item1.ToString()));
		}

		return searchFields;
	}

	private Tuple<Value, string> GetFieldValue(IEnumerable<Asset._Data> data, SearchFieldDefinition searchField)
	{
		foreach (var id in searchField.Ids)
		{
			var schema = id.Split('.')[0];
			var fieldName = id.Split('.')[1];

			var type = dataDefinitionRepository.GetDataDefinition(id).GetFieldDefinition(id).Type;

			var assetData = data.SingleOrDefault(item => item.DataDefinition.Name == schema);

			if (assetData != null &&
			    assetData.Fields.Any(f => f.Key == fieldName && !string.IsNullOrEmpty(f.Value.ToString())))
				return new Tuple<Value, string>(assetData.Fields.Single(f => f.Key == fieldName).Value, type);
		}

		return new Tuple<Value, string>(DEFAULT_VALUE_FOR_EMPTY_FIELD, DEFAULT_TYPE_FOR_EMPTY_FIELD);
	}

	// private string ExtractType(string schema, string fieldName)
	// {
	// 	var dataDefinition = dataDefinitionRepository.GetDataDefinition(schema);
	// 	var field = dataDefinition.Fields.Single(i => i.Id == fieldName);
	//
	// 	return field.Type;
	// }
}