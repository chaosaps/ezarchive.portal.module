using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Chaos.Portal.Core.Indexing;
using Chaos.Portal.Core.Indexing.Solr;
using Chaos.Portal.Core.Indexing.Solr.Response;
using EzArchive.Application;
using EzArchive.Application.UseCases.DataDefinitions;
using EzArchive.Application.UseCases.Labels;
using EzArchive.Application.UseCases.Search;
using EzArchive.Application.UseCases.SearchDefinitions;
using EzArchive.Domain;
using EZArchive.Domain;

namespace EzArchive.Data.Portal;

public class SearchRepository : ISearchRepository
{
	private readonly IIndex index;
	private readonly ISearchDefinitionRepository searchDefinitionRepository;
	private readonly IDataDefinitionRepository dataDefinitionRepository;
	private readonly ILabelRepository labelRepository;

	public SearchRepository(
		IIndex index,
		ISearchDefinitionRepository searchDefinitionRepository,
		IDataDefinitionRepository dataDefinitionRepository,
		ILabelRepository labelRepository)
	{
		this.index = index;
		this.searchDefinitionRepository = searchDefinitionRepository;
		this.dataDefinitionRepository = dataDefinitionRepository;
		this.labelRepository = labelRepository;
	}

	public void Set(Asset asset) =>
		Set(new[] { asset });

	public void Set(IEnumerable<Asset> assets, Action<int> progress = null)
	{
		var count = 0;

		Parallel.ForEach(
			Batchify(),
			new ParallelOptions { MaxDegreeOfParallelism = 16 },
			async batch =>
			{
				await index.IndexAsync(batch);
				progress?.Invoke(count += batch.Count);
			});

		IEnumerable<IList<IndexableSearchResult>> Batchify()
		{
			var indexableSearchResults = assets
				.Select(asset =>
					new IndexableSearchResult(
						searchDefinitionRepository,
						dataDefinitionRepository,
						asset,
						labelRepository.Get(asset.Id)));

			const int takes = 100;
			var skip = 0;

			while (true)
			{
				var searchResults = indexableSearchResults.Skip(skip).Take(takes).ToList();

				if (searchResults.Any() == false)
					yield break;

				yield return searchResults;
				skip += takes;
			}
		}
	}

	public void Delete()
	{
		index.Delete();
	}

	public void Delete(AssetId assetId)
	{
		index.Delete(assetId.Value.ToString());
	}

	public async Task<Page<Search>> SearchAsync(IQuery query)
	{
		var indexResponse = await index.QueryAsync(query);
		return ToPage(indexResponse.QueryResult);
	}

	public async Task<FacetResponse> FacetAsync(IQuery query) =>
		(await index.QueryAsync(query)).FacetResponse;

	private Page<Search> ToPage(IQueryResult<FlexibleResult> queryResult)
	{
		var searchFieldDefinitions = searchDefinitionRepository.Get();

		return new Page<Search>(
			queryResult.FoundCount,
			queryResult.StartIndex,
			queryResult.Results.Select(r =>
				new Search(
					new AssetId(r.Id),
					r.Fields["s_document_typeid"][0],
					r.Fields.Where(f => Contains(f.Key))
						.Select(f => new SearchField(ToDisplayName(f.Key), f.Value.FirstOrDefault())),
					Tags(r))));

		bool Contains(string key) =>
			searchFieldDefinitions
				.Select(sf => GetFormattedSearchKey(sf.Type, sf.DisplayName))
				.Any(searchKey => searchKey == key);

		string ToDisplayName(string key)
		{
			var definition = searchFieldDefinitions
				.FirstOrDefault(sfd => GetFormattedSearchKey(sfd.Type, sfd.DisplayName) == key);

			if (definition == null)
				throw new Exception($"key '{key}' does not match a SearchFieldDefinition");

			return definition.DisplayName;
		}

		IList<string> Tags(FlexibleResult r) =>
			r.Fields.ContainsKey("sm_tags")
				? r.Fields["sm_tags"]
				: new List<string>();
	}

	public static string GetFormattedSearchKey(string type, string key)
	{
		return GetFormattedKey(type, "search", key);

		string GetFormattedKey(string type, string schema, string field)
		{
			if (type == "Datetime")
				return ("d_" + schema + "_" + field).Replace(" ", "_").ToLower();
			if (type == "Text")
				return ("t_" + schema + "_" + field).Replace(" ", "_").ToLower();
			if (type == "Table")
				return ("sm_" + schema + "_" + field).Replace(" ", "_").ToLower();

			return ("s_" + schema + "_" + field).Replace(" ", "_").ToLower();
		}
	}

	private class IndexableSearchResult : IIndexable
	{
		private readonly ISearchDefinitionRepository searchDefinitionRepository;
		private readonly IDataDefinitionRepository dataDefinitionRepository;
		private readonly Asset asset;
		private readonly IEnumerable<Label> labels;

		public IndexableSearchResult(
			ISearchDefinitionRepository searchDefinitionRepository,
			IDataDefinitionRepository dataDefinitionRepository,
			Asset asset,
			IEnumerable<Label> labels)
		{
			this.searchDefinitionRepository = searchDefinitionRepository;
			this.dataDefinitionRepository = dataDefinitionRepository;
			this.asset = asset;
			this.labels = labels;
		}

		public IEnumerable<KeyValuePair<string, string>> GetIndexableFields()
		{
			yield return UniqueIdentifier;

			// Index Asset data
			foreach (var assetData in asset.Data)
			{
				foreach (var field in assetData.Fields)
				{
					var id = assetData.DataDefinition.Name + "." + field.Key;
					IEnumerable<string> values = new List<string>();

					try
					{
						values = IndexHelper.GetIndexValue(id, field.Value);
					}
					catch (InvalidOperationException)
					{
					}

					foreach (var value in values)
					{
						if (!string.IsNullOrEmpty(value))
							yield return new KeyValuePair<string, string>(IndexHelper.GetIndexKey(id), value);
					}
				}
			}

			// Index search fields
			var dto = new SearchBuilder(searchDefinitionRepository, dataDefinitionRepository).Build(asset);
			foreach (var searchField in dto.Fields)
			{
				var sfd = searchDefinitionRepository.Get(searchField.Key);

				var formattedValue = IndexHelper.GetFormattedValue(searchField.Value, sfd.Type);

				yield return
					new KeyValuePair<string, string>(IndexHelper.GetFormattedSearchKey(sfd.Type, searchField.Key),
						formattedValue);

				if (sfd.IsSortable)
					yield return new KeyValuePair<string, string>(
						IndexHelper.GetFormattedSortKey(sfd.Type, searchField.Key),
						formattedValue);
			}

			// Index annotations
			foreach (var annotationGroup in asset.AnnotationsGroups)
			foreach (var annotation in annotationGroup.Annotations)
			foreach (var pair in annotation)
			{
				var key = annotationGroup.Name.Replace(" ", "_").Replace(".", "_");
				key += "_" + pair.Key.Replace(" ", "_").Replace(".", "_");
				yield return new KeyValuePair<string, string>("sm_an_" + key, pair.Value.ToString());
			}

			// Index document standard values
			yield return new KeyValuePair<string, string>("s_document_typeid", dto.TypeId);

			// Index tags
			foreach (var tag in asset.Tags)
				yield return new KeyValuePair<string, string>("sm_tags", tag.ToLower());

			// Index labels
			foreach (var label in labels)
				yield return new KeyValuePair<string, string>("sm_labels", label.Id.Value.ToString());

			// Index project Ids
			foreach (var projectId in labels.Select(l => l.ProjectId).Distinct())
				yield return new KeyValuePair<string, string>("sm_projectids", projectId.ToString());
		}

		public KeyValuePair<string, string> UniqueIdentifier =>
			new KeyValuePair<string, string>("Id", asset.Id.Value.ToString());
	}
}