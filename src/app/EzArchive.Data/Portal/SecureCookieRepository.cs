using System.Linq;
using Chaos.Authentication.Data;
using EzArchive.Application.UseCases.SecureCookies.Create;
using EzArchive.Domain;
using ISecureCookieRepository = EzArchive.Application.UseCases.SecureCookies.ISecureCookieRepository;

namespace EzArchive.Data.Portal;

public class SecureCookieRepository : ISecureCookieRepository
{
	private readonly IAuthenticationRepository authenticationRepository;

	public SecureCookieRepository(IAuthenticationRepository authenticationRepository)
	{
		this.authenticationRepository = authenticationRepository;
	}

	public void Create(CreateSecureCookieRequest createSecureCookieRequest) =>
		authenticationRepository.SecureCookie.Create(
			createSecureCookieRequest.Id.Value,
			createSecureCookieRequest.UserId.Value,
			createSecureCookieRequest.PasswordGuid.Value,
			createSecureCookieRequest.SessionId.Value);

	public SecureCookie Get(SecureCookieId id, UserId userId, PasswordGuid passwordGuid) =>
		authenticationRepository.SecureCookie.Get(
				id.Value, userId.Value, passwordGuid.Value)
			.Select(Map)
			.First();

	public SecureCookie Get(SecureCookieId id, PasswordGuid passwordGuid) =>
		authenticationRepository.SecureCookie.Get(
				id.Value, null, passwordGuid.Value)
			.Select(Map)
			.FirstOrDefault();

	private SecureCookie Map(Chaos.Authentication.Core.Model.SecureCookie sc) =>
		new SecureCookie(
			new SecureCookieId(sc.Guid),
			new UserId(sc.UserGuid),
			new PasswordGuid(sc.PasswordGuid),
			sc.DateCreated,
			sc.DateUsed);

	public void Use(SecureCookieId id, UserId userId) =>
		authenticationRepository.SecureCookie.Use(id.Value, userId.Value, null);
}