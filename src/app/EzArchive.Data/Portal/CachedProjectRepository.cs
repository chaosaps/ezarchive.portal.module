﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Domain;

namespace EzArchive.Data.Portal
{
	public class CachedProjectRepository : IProjectRepository
	{
		private readonly IProjectRepository projectRepository;
		private IDictionary<UserId, (DateTime expiration, IReadOnlyList<Project> projects)> crudeCache =
			new ConcurrentDictionary<UserId, (DateTime, IReadOnlyList<Project>)>();

		public CachedProjectRepository(IProjectRepository projectRepository)
		{
			this.projectRepository = projectRepository;
		}
		
		public IReadOnlyList<Project> Get(UserId userId)
		{
			lock (crudeCache)
				if (crudeCache.ContainsKey(userId) && crudeCache[userId].expiration.CompareTo(DateTime.Now) > 0)
					return crudeCache[userId].projects;

			var projects = projectRepository.Get(userId);
			
			lock (crudeCache)
			{
				ClearUserCache(userId);
				crudeCache.Add(userId, (DateTime.Now.AddMinutes(10), projects));
			}
			
			return projects;
		}

		public Project Get(ProjectId id)
		{
			return projectRepository.Get(id);
		}

		public Project Get(LabelId labelId)
		{
			return projectRepository.Get(labelId);
		}

		public Project Set(ProjectId id, string name)
		{
			return projectRepository.Set(id, name);
		}

		public void AddUser(ProjectId id, UserId userId)
		{
			ClearUserCache(userId);
			
			projectRepository.AddUser(id, userId);
		}

		public void Delete(ProjectId id)
		{
			ClearCache();
			
			projectRepository.Delete(id);
		}

		public void RemoveUser(ProjectId id, UserId userId)
		{
			ClearUserCache(userId);
			
			projectRepository.RemoveUser(id, userId);
		}
		
		private void ClearUserCache(UserId userId)
		{
			lock (crudeCache)
				if (crudeCache.ContainsKey(userId))
					crudeCache.Remove(userId);
		}

		public void ClearCache()
		{
			lock (crudeCache)
			{
				crudeCache = new ConcurrentDictionary<UserId, (DateTime expiration, IReadOnlyList<Project> projects)>();
			}
		}
	}
}