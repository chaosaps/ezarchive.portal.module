using System;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Passwords;
using EzArchive.Domain;
using EzArchive.Domain.Tickets;
using Newtonsoft.Json;

namespace EzArchive.Data.Portal
{
	public class TicketRepository : ITicketRepository
	{
		private readonly Chaos.Portal.Core.Data.ITicketRepository ticketRepository;

		public TicketRepository(
			Chaos.Portal.Core.Data.ITicketRepository ticketRepository)
		{
			this.ticketRepository = ticketRepository;
		}

		public void Create(Ticket ticket)
		{
			ticketRepository.Create(new Chaos.Portal.Core.Data.Model.Ticket
			{
				Id = ticket.Id.Value,
				TypeId = MapTicketType(ticket),
				Data = JsonConvert.SerializeObject(ticket.Data())
			});
		}

		public ResetPasswordTicket UseResetPasswordTicket(TicketId ticketId)
		{
			var result = GetResetPasswordTicket(ticketId);

			if (result != null)
				ticketRepository.Use(ticketId.Value);

			return result;
		}

		private ResetPasswordTicket GetResetPasswordTicket
			(TicketId ticketId)
		{
			try
			{
				var ticket = ticketRepository.Get(ticketId.Value);
				var data = JsonConvert.DeserializeObject<ResetPasswordTicketData>(ticket.Data);

				if (ticket.DateUsed.HasValue)
					return null;

				if (ticket.TypeId != 1) // todo move type id out
					throw new Exception($"Ticket has to have TypeId '1' but was '{ticket.TypeId}'");

				return new ResetPasswordTicket(ticketId, new UserId(data.UserId));
			}
			catch (ChaosDatabaseException)
			{
				return null;
			}
		}

		private record ResetPasswordTicketData(Guid UserId);

		private uint MapTicketType(Ticket ticket)
		{
			return ticket switch
			{
				ResetPasswordTicket _ => 1u,
				_ => throw new ArgumentException($"Unable to map ticket of type '{ticket.GetType().FullName}'")
			};
		}
	}
}