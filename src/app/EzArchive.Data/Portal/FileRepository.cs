using System.Collections.Generic;
using System.Linq;
using Chaos.Mcm.Data;
using EzArchive.Domain;
using IFileRepository = EzArchive.Application.UseCases.Files.IFileRepository;

namespace EzArchive.Data.Portal;

public class FileRepository : IFileRepository
{
	private readonly Chaos.Mcm.Data.IFileRepository fileRepository;
	private readonly IObjectRepository objectRepository;
	private readonly uint uploadDestinationId;

	public FileRepository(
		Chaos.Mcm.Data.IFileRepository fileRepository,
		IObjectRepository objectRepository,
		uint uploadDestinationId)
	{
		this.fileRepository = fileRepository;
		this.uploadDestinationId = uploadDestinationId;
		this.objectRepository = objectRepository;
	}

	public FileId Set(
		AssetId assetId,
		uint? parentId,
		string filename,
		string originalFilename,
		string folderPath,
		uint formatId,
		FileId id = null)
	{
		return new FileId(
			fileRepository.Create(
				assetId.Value,
				parentId,
				uploadDestinationId,
				filename,
				originalFilename,
				folderPath,
				formatId));
	}

	public void Set(FileId id, string newFileName)
	{
		var file = fileRepository.Get(id.Value);
		file.OriginalFilename = newFileName;

		fileRepository.Set(file);
	}

	public IEnumerable<Asset._FileReference._Destination> Get(FileId id)
	{
		var parent = fileRepository.Get(id.Value);
		yield return NewDestination(parent);

		foreach (var file in fileRepository.Get(parentId: id.Value))
			yield return NewDestination(file);

		Asset._FileReference._Destination NewDestination(Chaos.Mcm.Core.Model.File file) =>
			new Asset._FileReference._Destination(
				"",
				file.FolderPath + "/" + file.Filename);
	}

	public File GetFile(FileId id, string token)
	{
		var file = fileRepository.Get(id.Value);
		var o = objectRepository.Get(file.ObjectGuid, false, true);

		return o.Files
			.Where(f => f.Identifier == id.Value && f.Token == token)
			.Select(f => new File(
				new FileId(f.Identifier),
				f.URL,
				f.OriginalFilename,
				f.MimeType))
			.FirstOrDefault();
	}

	public void Delete(FileId id) =>
		fileRepository.Delete(id.Value);
}