using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Chaos.Authentication.Data;
using Chaos.Mcm.Core.Model;
using Chaos.Mcm.Data;
using Chaos.Mcm.Domain;
using Chaos.Portal.Core.Data;
using Chaos.Portal.Core.Data.Model;
using Chaos.Portal.Core.Indexing.Solr;
using Chaos.Portal.Core.Indexing.Solr.Request;
using EzArchive.Domain;
using IUserRepository = EzArchive.Application.UseCases.Users.IUserRepository;
using User = EzArchive.Domain.User;

namespace EzArchive.Data.Portal;

public class UserRepository : IUserRepository
{
	private readonly Guid adminGroupId;
	private readonly Guid contributorGroupId;
	private readonly IGroupRepository groupRepository;
	private readonly IPasswordRepository passwordRepository;
	private readonly IObjectRepository objectRepository;
	private readonly IMetadataRepository metadataRepository;
	private readonly MetadataSchemaId profileMetadataSchemaId;
	private readonly Guid userGroupId;
	private readonly uint profileObjectTypeId;
	private readonly uint userFolderId;
	private readonly Chaos.Portal.Core.Data.IUserRepository userRepository;
	private readonly IIndex index;

	public UserRepository(
		Chaos.Portal.Core.Data.IUserRepository userRepository,
		IObjectRepository objectRepository,
		IMetadataRepository metadataRepository,
		IGroupRepository groupRepository,
		IPasswordRepository passwordRepository,
		IIndex index,
		MetadataSchemaId profileMetadataSchemaId,
		Guid adminGroupId,
		Guid contributorGroupId,
		Guid userGroupId,
		uint profileObjectTypeId,
		uint userFolderId)
	{
		this.userRepository = userRepository;
		this.objectRepository = objectRepository;
		this.metadataRepository = metadataRepository;
		this.groupRepository = groupRepository;
		this.passwordRepository = passwordRepository;
		this.index = index;
		this.profileMetadataSchemaId = profileMetadataSchemaId;
		this.adminGroupId = adminGroupId;
		this.contributorGroupId = contributorGroupId;
		this.userGroupId = userGroupId;
		this.profileObjectTypeId = profileObjectTypeId;
		this.userFolderId = userFolderId;
	}

	public IEnumerable<User> Get()
	{
		var userInfos = GetAllUsers();

		return Get(userInfos);

		IEnumerable<UserInfo> GetAllUsers()
		{
			var users = new Dictionary<Guid, UserInfo>();
			var userInfoGet = userRepository.Get(null, null, null, null);

			// Ensure users are only added once (we dont care about sessions)
			foreach (var userInfo in userInfoGet.Where(userInfo => !users.ContainsKey(userInfo.Guid)))
				users.Add(userInfo.Guid, userInfo);

			return users.Values;
		}
	}

	public User Get(UserId userId)
	{
		var user = userRepository.Get(new Chaos.Portal.Core.UserId(userId.Value));

		if (user == null)
			throw new UserNotFound(userId);

		return Get(new[] { user }).Single();
	}

	public IEnumerable<User> Get(IEnumerable<UserInfo> userInfos)
	{
		var profiles = objectRepository.Get(userInfos.Select(id => id.Guid), includeMetadata: true);

		foreach (var userInfo in userInfos)
		{
			var profile = profiles.SingleOrDefault(u => u != null && u.Guid == userInfo.Guid);
			Metadata profileData = null;

			if (profile != null)
			{
				profileData =
					profile?.Metadatas.FirstOrDefault(item => item.MetadataSchemaId == profileMetadataSchemaId);

				if (profileData?.MetadataXml?.Root == null)
					throw new UserNotFound(new UserId(userInfo.Guid));
			}

			yield return CreateEzUser(userInfo, profileData?.MetadataXml?.Root);
		}
	}

	public User Set(User user)
	{
		var userId = CreateIfNew();
		UpdateUserProfile();

		return Get(userId);

		UserId CreateIfNew()
		{
			var userId = user.Id == null
				? new UserId(Guid.NewGuid())
				: user.Id;

			try
			{
				var usr = userRepository.Get(user.Email);

				if (usr != null && usr.Guid != user.Id?.Value)
					throw new Exception("Email already in use");
			}
			catch (ArgumentException)
			{
			}

			if (user.Id == null)
			{
				userRepository.Create(userId.Value, user.Email);
				objectRepository.Create(userId.Value, profileObjectTypeId, userFolderId);
			}
			else if (objectRepository.Get(user.Id.Value) == null)
				objectRepository.Create(user.Id.Value, profileObjectTypeId, userFolderId);

			return userId;
		}

		void UpdateUserProfile()
		{
			var xml = ToXML(user);
			var profileObject = objectRepository.Get(userId.Value, includeMetadata: true);
			var profileMetadata =
				profileObject.Metadatas.SingleOrDefault(m =>
					m.MetadataSchemaId == profileMetadataSchemaId);

			metadataRepository.Set(
				new Metadata(
					new MetadataId(userId.Value),
					null,
					profileMetadataSchemaId,
					new Chaos.Portal.Core.UserId(userId.Value),
					profileMetadata?.RevisionID ?? 0,
					xml,
					DateTime.UtcNow),
				userId.Value);

			userRepository.Set(userId.Value, user.Email);

			var groups = groupRepository.Get(new Chaos.Portal.Core.UserId(userId.Value));

			if (!WasPermissionChanged(user.Permission))
				return;

			foreach (var group in groups)
				groupRepository.RemoveUser(group.Guid, userId.Value, null);

			if (user.Permission == "Administrator")
				groupRepository.AddUser(adminGroupId, userId.Value, 0, null);

			if (user.Permission == "Contributor")
				groupRepository.AddUser(contributorGroupId, userId.Value, 0, null);

			if (user.Permission == "User")
				groupRepository.AddUser(userGroupId, userId.Value, 0, null);

			bool WasPermissionChanged(string newPermission)
			{
				if (newPermission == "Administrator" && groups.Any(group => @group.Guid == adminGroupId))
					return false;
				if (newPermission == "Contributor" && groups.Any(group => @group.Guid == contributorGroupId))
					return false;
				if (newPermission == "User" && groups.Any(group => @group.Guid == userGroupId))
					return false;

				return true;
			}
		}

		XDocument ToXML(User user)
		{
			return new XDocument(
				new XElement("EZArchive.Portal.Module.Core.EzUser",
					new XElement("Identifier", userId.Value.ToString()),
					new XElement("Email", user.Email),
					new XElement("Name", user.Name),
					new XElement("IsAdministrator", user.IsAdministrator),
					new XElement("Permission", user.Permission),
					new XElement("Preferences", user.Preferences),
					new XElement("WayfAttributes", user.WayfAttributes)));
		}
	}

	private User CreateEzUser(UserInfo user, XElement profile)
	{
		var userId = new UserId(user.Guid);
		var email = user.Email;
		string name = null;
		string preferences = null;
		string wayfAttributes = null;

		if (profile != null)
		{
			name = profile.Element("Name")?.Value;
			preferences = profile.Element("Preferences")?.Value;
			wayfAttributes = profile.Element("WayfAttributes")?.Value;
		}

		return new User(
			userId,
			email,
			name,
			DeterminePermission(user),
			preferences,
			wayfAttributes);
	}

	private string DeterminePermission(UserInfo user)
	{
		var groups = groupRepository.Get(new Chaos.Portal.Core.UserId(user.Guid));

		if (groups.Any(group => @group.Guid == adminGroupId))
			return "Administrator";
		if (groups.Any(group => @group.Guid == contributorGroupId))
			return "Contributor";
		if (groups.Any(group => @group.Guid == userGroupId))
			return "User";

		return null;
	}

	public User Get(string email)
	{
		return Get(new[] { userRepository.Get(email) }).Single();
	}

		public void SetPassword(string email, HashedPassword password)
		{
			var user = Get(new[] {userRepository.Get(email)}).Single();
			passwordRepository.Set(user.Id.Value, password.ToString());
		}

	public void Delete(UserId userId)
	{
		objectRepository.Delete(userId.Value);
		passwordRepository.Delete(userId.Value);
		userRepository.Delete(userId.Value);
	}

	public async Task<IEnumerable<(UserId id, string name)>> SearchAsync(SolrQuery query)
	{
		var queryResult = await index.QueryAsync(query);
		return queryResult.QueryResult.Results
			.Select(r => (new UserId(r.Id), r.Fields["t_name"].FirstOrDefault()));
	}

	public void Index(User user) =>
		Index(new[] { user });

	public void Index(IEnumerable<User> users)
	{
		Parallel.ForEach(
			Batchify(),
			new ParallelOptions { MaxDegreeOfParallelism = 8 },
			async batch => await index.IndexAsync(batch));

		IEnumerable<IList<UserIndex>> Batchify()
		{
			var userIndices = users
				.Where(user => user.Name != null)
				.Select(user => new UserIndex(user)).ToList();

			const int takes = 100;
			var skip = 0;

			while (true)
			{
				var searchResults = userIndices.Skip(skip).Take(takes).ToList();

				if (searchResults.Any() == false)
					yield break;

				yield return searchResults;
				skip += takes;
			}
		}
	}

	public void IndexDelete()
	{
		index.Delete();
	}

	private class UserIndex : IIndexable
	{
		private readonly User user;

		public UserIndex(User user)
		{
			this.user = user;
		}

		public IEnumerable<KeyValuePair<string, string>> GetIndexableFields()
		{
			yield return UniqueIdentifier;
			yield return new KeyValuePair<string, string>("t_name", user.Name);
		}

		public KeyValuePair<string, string> UniqueIdentifier =>
			new KeyValuePair<string, string>("Id", user.Id.Value.ToString());
	}
}
