﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EzArchive.Application;
using EzArchive.Domain.Emails;
using Mandrill;
using Mandrill.Models;
using Mandrill.Requests.Messages;
using Mandrill.Requests.Templates;

namespace EzArchive.Mandrill
{
	public class MandrillEmailSender : IEmailSender
	{
		private readonly IMandrillApi mandrillApi;

		public MandrillEmailSender(IMandrillApi mandrillApi)
		{
			this.mandrillApi = mandrillApi;
		}
		
		public async Task SendAsync(ResetPasswordEmail email)
		{
			var result = await SendTemplateApiMail(
				email.From,
				email.To,
				null,
				null,
				email.Parameters,
				email.Template);

			foreach (var emailResult in result)
				Console.WriteLine(emailResult.RejectReason);
		}
		
		private  async Task<IList<EmailResult>> SendTemplateApiMail(
			Domain.EmailAddress fromEmail, 
			Domain.EmailAddress toEmail, 
			string bccAddress,
			string subject, 
			IDictionary<string, string> globalVariables, 
			string templateName)
		{
			if (string.IsNullOrEmpty(templateName))
				throw new Exception("SendTemplateApiMail method requires a template name");

			try
			{
				// Create mail message
				var message = new EmailMessage
				{
					FromEmail = fromEmail.Value,
					To = new[] {new EmailAddress(toEmail.Value)},
					Merge = true,
				};

				//If a subject provided
				if (!string.IsNullOrEmpty(subject))
					message.Subject = subject;

				//If a bcc address provided
				if (!string.IsNullOrEmpty(bccAddress))
					message.BccAddress = bccAddress;

				// Add global variables. If any. 
				if (globalVariables != null)
				{
					foreach (var variable in globalVariables)
					{
						message.AddGlobalVariable(variable.Key, variable.Value);
					}
				}

				// Send message to Mandrill
				var request = new SendMessageTemplateRequest(message, templateName, new List<TemplateContent>());
				var result = await mandrillApi.SendMessageTemplate(request);

				// If not send. Throw error
				if (result.First().Status == EmailResultStatus.Invalid)
					throw new Exception($"Email not send - status: {result.First().Status}");

				// Otherwise return result
				return result;
			}
			catch (AggregateException ex)
			{
				//Error finding
				if (!DoTemplateExist(templateName))
					throw new Exception($"Mandrill template: {templateName} not found", ex);
			}

			return null;
		}

		private  bool DoTemplateExist(string templateName)
		{
			try
			{
				var request = new TemplateInfoRequest(templateName);
				mandrillApi.TemplateInfo(request);

				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		
	}
}