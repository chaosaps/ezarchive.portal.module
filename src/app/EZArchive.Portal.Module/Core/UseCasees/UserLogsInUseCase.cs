﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace EZArchive.Portal.Module.Core.UseCasees
{
	public class UserLogsInUseCase
	{
		public void EnsureUserHasARole(Guid id)
		{
			var user = Context.Repository.EzUser.Get(id);

			if(string.IsNullOrEmpty(user.Permission))
				user.Permission =  "Contributor";

			Context.Repository.EzUser.Set(user);
		}

		public void EnsureUserHasWayfAttributes(Guid id, IDictionary<string, IList<string>> attributesObject)
		{
			var user = Context.Repository.EzUser.Get(id);

			user.WayfAttributes = JsonConvert.SerializeObject(attributesObject);

			if (string.IsNullOrEmpty(user.Permission))
				user.Permission = "Contributor";

			Context.Repository.EzUser.Set(user);
		}
	}
}