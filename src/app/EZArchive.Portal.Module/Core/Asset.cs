﻿using System;
using System.Collections.Generic;
using System.Linq;
using EZArchive.Portal.Module.Core.Configuration;
using Newtonsoft.Json;

namespace EZArchive.Portal.Module.Core
{
  public class Asset : IIdentifiable
  {
    public Asset()
    {
      Files = new List<FileReference>();
      Annotations = new List<AnnotationGroup>();
      Tags = new string[0];
      Data = new List<AssetData>();
    }

    public string Identifier { get; set; }
    public uint TypeId { get; set; }
    public IList<AssetData> Data { get; set; }
    public IList<FileReference> Files { get; set; }
    public IList<AnnotationGroup> Annotations { get; set; }
    public Guid LastChangedByUserId { get; set; }
    public string[] Tags { get; set; }
    public bool DoFilesRequireLogin { get; set; }

    public void AddAnnotations(Guid definitionId, IDictionary<string, Value> annotation)
    {
      var annotationGroup = Annotations.SingleOrDefault(ag => ag.DefinitionId == definitionId);

      if (annotationGroup == null)
      {
        var definition = Context.Repository.Definition.GetAnnotations().SingleOrDefault(ad => ad.Identifier == definitionId.ToString());
        
        if(definition == null) throw new System.Exception("DefinitionId " + definitionId + "not a valid Annotation Definition");

        annotationGroup = new AnnotationGroup
          {
            DefinitionId = definitionId,
            Name = definition.Name
          };

        Annotations.Add(annotationGroup);
      }

      annotationGroup.Annotations.Add(annotation);
    }

    public void RemoveAnnotation(Guid id)
    {
      var annotationGroup = Annotations.SingleOrDefault(ag => ag.Annotations.Any(a => a.ContainsKey("Identifier") && a["Identifier"].ToString() == id.ToString()));
      var annotation = annotationGroup.Annotations.SingleOrDefault(a => a.ContainsKey("Identifier") && a["Identifier"].ToString() == id.ToString());

      annotationGroup.Annotations.Remove(annotation);
    }

    public class FileReference
    {
      public FileReference()
      {
        Destinations = new List<Destination>();
      }

      public string Identifier { get; set; }
			public string Name { get; set; }
			public string Type { get; set; }

      public IList<Destination> Destinations { get; set; }


      public class Destination
      {
        public string Type { get; set; }

        public string Url { get; set; }
      }
    }

    public class AnnotationGroup
    {
      public AnnotationGroup()
      {
        Annotations = new List<IDictionary<string, Value>>();
      }

      public string Name { get; set; }
      public Guid DefinitionId { get; set; }
      public IList<IDictionary<string, Value>> Annotations { get; set; } 
    }

    public Value FindField(string name, string id)
    {
      return Data.Single(item => item.Name == name).Fields.Single(item => item.Key == id).Value;
    }

    public IEnumerable<Datum> GetFields()
    {
      foreach (var field in Data.SelectMany(assetData => assetData.Fields))
        yield return new Datum {Id = field.Key, Value = field.Value};
    }

    public void SetField(string key, Value value)
    {
			if(!key.Contains('.'))
				throw new ArgumentException("Key does not contain a definition name, the format is (<DefinitionName.FieldName>)");

      var definitionName = key.Split('.')[0];
      var field = key.Split('.')[1];
      var data = Data.SingleOrDefault(d => d.Name == definitionName);

      if (data == null)
      {
        data = new AssetData(Context.Repository.Definition.GetDataDefinition(definitionName));
        Data.Add(data);
      }

      if (data.Fields.ContainsKey(field))
        data.Fields[field] = value;
      else
        data.Fields.Add(field, value);
    }
  }

  public class Datum
  {
    public string Id { get; set; }
    public Value Value { get; set; }
  }

  public class AssetData
  {
    public string Name
    {
      get { return DataDefinition.Name; }
      set { DataDefinition = Context.Repository.Definition.GetDataDefinition(value); }
    }

    public IDictionary<string, Value> Fields { get; set; }

    [JsonIgnore]
    public DataDefinition DataDefinition { get; private set; }

    public AssetData(DataDefinition dataDefinition)
    {
      DataDefinition = dataDefinition;
      Fields = new Dictionary<string, Value>();
    }
  }
}