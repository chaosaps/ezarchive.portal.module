﻿namespace EZArchive.Portal.Module.Core
{
  public class SearchField
  {
    public string Key { get; set; }
    public string Value { get; set; }
  }
}