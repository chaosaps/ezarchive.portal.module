namespace EZArchive.Portal.Module.Core.Configuration
{
	public enum FacetType
	{
		Field = 0,
		Range = 1
	}
}