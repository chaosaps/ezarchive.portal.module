namespace EZArchive.Portal.Module.Core.Configuration
{
	public enum DefinitionType
	{
		Metadata = 0,
		Annotation = 1
	}
}