using System;

namespace EZArchive.Portal.Module.Core.Configuration
{
  public class McmConfig
  {
    public uint ProfileObjectTypeId { get; set; }
    public uint AssetFolderId { get; set; }
    public uint UserFolderId { get; set; }
    public uint UploadDestinationId { get; set; }
    public Guid ProfileMetadataSchemaId { get; set; }
    public Guid AssetMetadataSchemaId { get; set; }
  }
}