namespace EZArchive.Portal.Module.Core.Configuration
{
	public class FacetSetting
	{
		public FacetSetting()
		{
			Fields = new FacetSpecification[0];
		}

		public string Header { get; set; }
		public string Position { get; set; }
		public FacetSpecification[] Fields { get; set; }
	}
}