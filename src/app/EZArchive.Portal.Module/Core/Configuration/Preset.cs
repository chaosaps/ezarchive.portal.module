namespace EZArchive.Portal.Module.Core.Configuration
{
  public class Preset
  {
    public string Id { get; set; }
    public string Extension { get; set; }
    public uint FormatId { get; set; }
  }
}