namespace EZArchive.Portal.Module.Core.Configuration
{
	public class FacetSpecification
	{
		public string Key { get; set; }
		public FacetType Type { get; set; }
	}
}