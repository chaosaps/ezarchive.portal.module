using System.Linq;

namespace EZArchive.Portal.Module.Core.Configuration
{
	public class SearchFieldDefinition
	{
		public string DisplayName { get; set; }
		public string[] Ids { get; set; }
		public bool IsSortable { get; set; }
		public bool IsVisible { get; set; }

		public SearchFieldDefinition()
		{
			IsVisible = true;
		}

		public string Type
		{
			get
			{
				if (!Ids.Any())
					return "String";

				var firstId = Ids.First();

				return Context.Repository.Definition.GetDataDefinition(firstId).GetFieldDefinition(firstId).Type;
			}
		}
	}
}