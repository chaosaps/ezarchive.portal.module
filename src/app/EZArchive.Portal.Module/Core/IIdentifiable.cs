namespace EZArchive.Portal.Module.Core
{
  public interface IIdentifiable
  {
    string Identifier { get; set; }
  }
}