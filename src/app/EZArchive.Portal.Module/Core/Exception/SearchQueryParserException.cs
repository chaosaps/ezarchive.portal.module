﻿namespace EZArchive.Portal.Module.Core.Exception
{
  public class SearchQueryParserException : System.Exception
  {
    public SearchQueryParserException(string message) : base(message)
    {
    }
  }
}