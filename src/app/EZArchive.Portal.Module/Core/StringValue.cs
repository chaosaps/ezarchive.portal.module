﻿namespace EZArchive.Portal.Module.Core
{
  public class StringValue : Value
  {
    public string Value { get; set; }

    public StringValue(string value)
    {
      Value = value;
      Type = "String";
    }

    public override string ToString()
    {
      return Value;
    }
  }
  
  public class NullValue : Value
  {
    public NullValue()
    {
      Type = "Null";
    }

    public override string ToString()
    {
      return "";
    }
  }
}