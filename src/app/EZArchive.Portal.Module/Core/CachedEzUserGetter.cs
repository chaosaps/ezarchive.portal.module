﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using EZArchive.Portal.Module.Core.Gateway;

namespace EZArchive.Portal.Module.Core
{
  public class CachedEzUserGetter : IEzUserGetter
  {
    private readonly IEzUserGetter _userGetter;
    private IDictionary<Guid, EzUser> _cache;
    
    public CachedEzUserGetter(IEzUserGetter userGetter)
    {
      _cache = new ConcurrentDictionary<Guid, EzUser>();
      _userGetter = userGetter;
    }

    public EzUser Get(Guid id)
    {
      if (_cache.ContainsKey(id))
        return _cache[id];

      var user = _userGetter.Get(id);
      
      _cache.Add(id, user);

      return user;
    }
  }
}