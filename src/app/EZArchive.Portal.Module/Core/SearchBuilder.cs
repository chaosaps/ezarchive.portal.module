﻿using System;
using System.Collections.Generic;
using System.Linq;
using EZArchive.Portal.Module.Core.Configuration;

namespace EZArchive.Portal.Module.Core
{
  public class SearchBuilder
  {
    private readonly Value DEFAULT_VALUE_FOR_EMPTY_FIELD = new StringValue(null);
    private const string DEFAULT_TYPE_FOR_EMPTY_FIELD = "String";

    public Search Build(Asset asset)
    {
      return new Search
        {
          Identifier = asset.Identifier,
					TypeId = asset.TypeId.ToString(),
          Fields = GetSearchFields(asset.Data),
          Tags = new List<string>(asset.Tags)
        };
    }

    private IList<SearchField> GetSearchFields(IEnumerable<AssetData> data)
    {
      var searchFields = new List<SearchField>();
			
      foreach (var definition in Context.Config.SearchDefinition)
      {
        var field = GetFieldValue(data, definition);

        if (string.IsNullOrEmpty(field.Item1.ToString())) continue;

        var searchField = new SearchField
          {
            Key = definition.DisplayName,
            Value = field.Item1.ToString()
          };

        searchFields.Add(searchField);
      }

      return searchFields;
    }

    private Tuple<Value, string> GetFieldValue(IEnumerable<AssetData> data, SearchFieldDefinition searchField)
    {
      foreach (var id in searchField.Ids)
      {
        var schema = id.Split('.')[0];
        var fieldName = id.Split('.')[1];

        var type = Context.Repository.Definition.GetDataDefinition(id).GetFieldDefinition(id).Type;

        var assetData = data.SingleOrDefault(item => item.DataDefinition.Name == schema);
				
        if (assetData != null && assetData.Fields.Any(f => f.Key == fieldName && !string.IsNullOrEmpty(f.Value.ToString())))
          return new Tuple<Value, string>(assetData.Fields.Single(f => f.Key == fieldName).Value, type);
      }

      return new Tuple<Value, string>(DEFAULT_VALUE_FOR_EMPTY_FIELD, DEFAULT_TYPE_FOR_EMPTY_FIELD);
    }

    private static string ExtractType(string schema, string fieldName)
    {
      var dataDefinition = Context.Repository.Definition.GetDataDefinition(schema);
      var field = dataDefinition.Fields.Single(i => i.Id == fieldName);

      return field.Type;
    }
  }
}