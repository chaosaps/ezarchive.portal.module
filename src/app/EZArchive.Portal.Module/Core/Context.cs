﻿using EZArchive.Portal.Module.Core.Configuration;
using EZArchive.Portal.Module.Core.Gateway;

namespace EZArchive.Portal.Module.Core
{
  public static class Context
  {
    static Context()
    {
      Config = new Config();
    }

    public static IEzArchiveRepository Repository { get; set; }
    public static Config Config { get; set; }
  }
}