﻿using System;
using System.Globalization;

namespace EZArchive.Portal.Module.Core
{
  public class DateTimeValue : Value
  {
    public DateTime Value { get; private set; }

    public DateTimeValue(string value)
    {
      if (string.IsNullOrEmpty(value)) return;

	    DateTime dt;
	    if (DateTime.TryParseExact(value, new[] { "yyyy'-'MM'-'dd'T'HH':'mm':'ss.fff'Z'", "yyyy'-'MM'-'dd'T'HH':'mm':'ss", "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'", "dd'-'MM'-'yyyy' 'HH':'mm':'ss" }, null, DateTimeStyles.AdjustToUniversal, out dt))
		    Value = dt;
			else
		    try
		    {
			    Value = DateTime.Parse(value, null, DateTimeStyles.AdjustToUniversal);
		    }
		    catch (System.Exception)
		    {
			    throw new System.Exception(value);
		    }
    }

    public override string ToString()
    {
      return Value.ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss.fff'Z'");
    }
  }
}