using System;
using System.Collections.Generic;

namespace EZArchive.Portal.Module.Core.Gateway
{
  public interface IAssetRepository
  {
    Asset Set(Asset asset);
    Asset Get(Guid id);
    bool Delete(Guid id);
  }
}