using System.Collections.Generic;

namespace EZArchive.Portal.Module.Core.Gateway
{
	public interface IProjectRepository
	{
		Project Set(Project project);
		IEnumerable<Project> Get(string id = null, string userId = null, string labelId = null);
	}
}