using System.Collections.Generic;

namespace EZArchive.Portal.Module.Core.Gateway
{
	public interface ILabelRepository
	{
		void SetAssociation(string id, string assetId);
		void DeleteAssociation(string id, string assetId);
		IEnumerable<Label> Get(string assetId);
	}
}