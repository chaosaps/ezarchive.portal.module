using System;
using System.Collections.Generic;

namespace EZArchive.Portal.Module.Core.Gateway
{
  public interface IEzUserRepository : IEzUserGetter
  {
    EzUser Set(EzUser user);
	  IEnumerable<EzUser> Search(string s, uint pageSize);
  }

  public interface IEzUserGetter
  {
    EzUser Get(Guid id);
  }
}