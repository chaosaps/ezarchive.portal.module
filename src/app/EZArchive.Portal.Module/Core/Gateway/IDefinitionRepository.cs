using System.Collections.Generic;
using EZArchive.Portal.Module.Core.Configuration;

namespace EZArchive.Portal.Module.Core.Gateway
{
  public interface IDefinitionRepository
  {
    IEnumerable<DataDefinition> GetAnnotations();
    IEnumerable<DataDefinition> GetDataDefinitions();

    DataDefinition GetDataDefinition(string id);
    void SetDataDefinition(DataDefinition definition);
  }
}