﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using EZArchive.Portal.Module.Core.Exception;
using EZArchive.Portal.Module.Data;

namespace EZArchive.Portal.Module.Core
{
  public class SearchQuery
  {
    public const string DEFAULT_QUERY = "*:*";
		private List<string> Queries = new List<string>();

    public static SearchQuery AllQuery()
    {
      return Parse(DEFAULT_QUERY);
    }

    public static SearchQuery Parse(string query)
    {
      if (string.IsNullOrEmpty(query)) query = DEFAULT_QUERY;
	    if (query.StartsWith("AND", StringComparison.InvariantCultureIgnoreCase))
	    {
		    var sq = new SearchQuery();
		    foreach (var subQuery in UnWrap(query.Substring(3)))
		    {
			    sq.AndQuery(subQuery);
		    }

		    return sq;
	    }
	    else
	    {
		    var sq = new SearchQuery();
		    sq.AndQuery(query);

		    return sq;
	    }
    }

	  private static IEnumerable<string> UnWrap(string query)
	  {
		  query = query.Trim();
		  var split = query.Split(' ');
		  var currentQuery = "";

		  foreach (var s in split)
		  {
			  if (s.Contains(":") && currentQuery != "" && !IsDateTime(s))
			  {
				  yield return currentQuery.TrimEnd();
				  
				  currentQuery = "";
			  }

			  currentQuery += s + " ";
		  }
		  
		  yield return currentQuery;
	  }

	  private static bool IsDateTime(string candidate)
	  {
		  return DateTime.TryParse(candidate, out _);
	  }

	  public override string ToString()
    {
			if(Queries.Count == 1)
				return Queries.Single();

			return "(" + string.Join(")AND(", Queries) + ")";
    }

		public void AndQuery(string query, bool parseQuery = true)
		{
			if (parseQuery) Queries.Add(ParseSingleQuery(query.Trim()));
			else
			{
				if (string.IsNullOrEmpty(query)) return;

				Queries.Add(query.Trim());
			};
		}
	  
	  private static string ParseSingleQuery(string query)
	  {
		  if (query == "*:*") return query;

		  var colonIndex = query.IndexOf(':');

		  if (colonIndex == -1) return query.Replace("+", "%2B");
		  if (colonIndex == query.Length - 1) throw new SearchQueryParserException("Query must be in the format");

		  var field = query.Substring(0, colonIndex);
		  var value = query.Substring(colonIndex + 1);
		  var formattedValue = GetFormattedValue(value);

		  var key = IndexHelper.GetIndexKey(field);

		  if (string.IsNullOrEmpty(key))
			  return formattedValue;
		  
		  return $"{key}:{formattedValue}";
	  }

	  private static string GetFormattedValue(string value)
	  {
		  if (value.ToLower().StartsWith("between_fields"))
		  {
			  var from = value.Split(' ')[1];
			  var to = value.Split(' ')[2];

			  return $"[{from} TO {to}]";
		  }
		  
		  if (value.ToLower().StartsWith("between"))
		  {
			  var from = value.Split(' ')[1];
			  var to = value.Split(' ')[2];

			  return $"[{from} TO {to}]";
		  }
      
		  if (value.ToLower().StartsWith("exact"))
		  {
			  var val = value.Substring(value.IndexOf(' ')+1);

			  return $"\"{val}\"";
		  }

		  if (value.ToLower().StartsWith("or"))
		  {
			  var val = value.Split(' ').Skip(1);

			  return $"({string.Join(" OR ", val)})";
		  }

		  return value;
	  }
	}
}