﻿namespace Chaos.Portal.Module.Larmfm.Domain
{
  using System.IO;

  public interface IStorage
  {
    void Write(string key, Stream stream, string contentType = "application/octet-stream");
    void Delete(string key);
  }
}