﻿using System.Collections.Generic;
using System.Linq;

namespace EZArchive.Portal.Module.Core
{
  public class TableValue : Value
  {
    public List<List<string>> Value { get; set; }

    public TableValue()
    {
      Value = new List<List<string>>();
      Type = "Table";
    }

    public override string ToString()
    {
      return string.Join(",", RowToString());
    }

    private IEnumerable<string> RowToString()
    {
      return Value.Select(val => "[" + string.Join("] [", val) + "]");
    }
  }
}