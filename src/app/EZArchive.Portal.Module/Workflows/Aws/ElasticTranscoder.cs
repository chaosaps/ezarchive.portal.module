﻿using Amazon;
using Amazon.ElasticTranscoder.Model;
using Chaos.Portal.Module.Larmfm.Domain;
using EZArchive.Portal.Module.Core;

namespace EZArchive.Portal.Module.Workflows.Aws
{
	public class ElasticTranscoder : ITranscoder
	{
		public void Transcode(string inputKey, string outputKey, string presetId, bool createThumbnail)
		{
			using (var et = AWSClientFactory.CreateAmazonElasticTranscoderClient(Context.Config.Aws.AccessKey,
				Context.Config.Aws.SecretAccessKey,
				RegionEndpoint.EUWest1))
			{
				var jobOutput = new CreateJobOutput
				{
					PresetId = presetId,
					Key = outputKey
				};

				if (createThumbnail)
					jobOutput.ThumbnailPattern = outputKey.Substring(0, outputKey.LastIndexOf(".")) + "_{count}";

				var request = new CreateJobRequest
				{
					PipelineId = Context.Config.Aws.PipelineId,
					Input = new JobInput
					{
						Key = inputKey
					},
					Output = jobOutput
				};
				et.CreateJob(request);
			}
		}

		public void Transcode(string inputKey, string outputKey, string presetId)
		{
			Transcode(inputKey, outputKey, presetId, false);
		}
	}
}