﻿using System.IO;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Chaos.Portal.Core.Exceptions;
using Chaos.Portal.Module.Larmfm.Domain;
using EZArchive.Portal.Module.Core;

namespace EZArchive.Portal.Module.Workflows.Aws
{
  public class S3 : IStorage
  {
    public void Write(string key, Stream stream, string contentType = "application/octet-stream")
    {
      try
      {
        using (var s3 = AWSClientFactory.CreateAmazonS3Client(Context.Config.Aws.AccessKey,
                                                         Context.Config.Aws.SecretAccessKey, RegionEndpoint.EUWest1))
        {
          stream.Position = 0;

          var request = new PutObjectRequest
            {
              BucketName = Context.Config.Aws.UploadBucket,
              CannedACL = S3CannedACL.PublicRead,
              InputStream = stream,
              AutoCloseStream = true,
              ContentType = contentType,
              Key = key
            };

          s3.PutObject(request);
        }
      }
      catch (AmazonS3Exception e)
      {
        throw new UnhandledException("Upload failed", e);
      }
    }

    public void Delete(string key)
    {
      try
      {
        using (var s3 = AWSClientFactory.CreateAmazonS3Client(Context.Config.Aws.AccessKey, Context.Config.Aws.SecretAccessKey, RegionEndpoint.EUWest1))
        {
          var request = new DeleteObjectRequest
          {
            BucketName = Context.Config.Aws.UploadBucket,
            Key = key
          };

          s3.DeleteObject(request);
        }
      }
      catch (AmazonS3Exception)
      {
       // throw new UnhandledException("Deletion failed on file: " + key, e);
      }
    }
  }
}