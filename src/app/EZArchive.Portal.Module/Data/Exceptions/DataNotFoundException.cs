﻿using System;

namespace EZArchive.Portal.Module.Data.Exceptions
{
  public class DataNotFoundException : Exception
  {
    public DataNotFoundException(string message) : base(message)
    {
    }
  }
}