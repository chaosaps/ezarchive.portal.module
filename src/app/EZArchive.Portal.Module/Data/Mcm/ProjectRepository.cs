﻿using System;
using System.Collections.Generic;
using System.Linq;
using Chaos.Mcm.Data;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Core.Gateway;
using IProjectRepository = EZArchive.Portal.Module.Core.Gateway.IProjectRepository;

namespace EZArchive.Portal.Module.Data.Mcm
{
	class ProjectRepository : IProjectRepository
	{
		public IMcmRepository Mcm { get; set; }
		public IEzUserRepository UserRepository { get; set; }

		public ProjectRepository(IMcmRepository mcm, IEzUserRepository userRepository)
		{
			Mcm = mcm;
			UserRepository = userRepository;
		}

		public Project Set(Project project)
		{
			var result = Mcm.Project.Set(new Chaos.Mcm.Data.Dto.Project
			{
				Identifier = project.Identifier == null ? (uint?) null : uint.Parse(project.Identifier),
				Name = project.Name
			});

			return new Project
			{
				Identifier = result.Identifier.ToString(),
				Name = result.Name
			};
		}

		public IEnumerable<Project> Get(string id = null, string userId = null, string labelId = null)
		{
			var results = Mcm.Project.Get(id == null ? (uint?) null : uint.Parse(id),
				userId == null ? (Guid?) null : Guid.Parse(userId),
				labelId == null ? (uint?) null : uint.Parse(labelId));

			foreach (var project in results)
			{
				var labels = Mcm.Label.Get(project.Identifier.Value);
				var users = GetUsers(project);

				yield return new Project
				{
					Identifier = project.Identifier.ToString(),
					Name = project.Name,
					Users = users.ToList(),
					Labels = labels.Select(l => new Label
					{
						Identifier = l.Identifier.ToString(),
						Name = l.Name
					}).ToList()
				};
			}
		}

		private IEnumerable<EzUser> GetUsers(Chaos.Mcm.Data.Dto.Project project)
		{
			var lst = new List<EzUser>();

			foreach (var id in project.UserIds)
			{
				try
				{
					lst.Add(UserRepository.Get(id));
				}
				catch (Exception)
				{
					continue;
				}
			}

			return lst;
		}
	}
}