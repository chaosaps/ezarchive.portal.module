﻿using Chaos.Mcm.Data;
using Chaos.Portal.Authentication.Data;
using Chaos.Portal.Core.Data;
using Chaos.Portal.Core.Indexing.View;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Core.Gateway;
using EZArchive.Portal.Module.Data.View;
using ILabelRepository = EZArchive.Portal.Module.Core.Gateway.ILabelRepository;
using IProjectRepository = EZArchive.Portal.Module.Core.Gateway.IProjectRepository;

namespace EZArchive.Portal.Module.Data.Mcm
{
  public class EzMcmRepository : IEzArchiveRepository
  {
    public IAssetRepository Asset { get; set; }
    public IEzUserRepository EzUser { get; set; }
    public IEzFileRepository EzFile { get; set; }
    public IDefinitionRepository Definition { get; set; }
	  public IProjectRepository Project { get; set; }
	  public ILabelRepository Label { get; set; }

	  public EzMcmRepository(IPortalRepository portal, IMcmRepository mcm, IAuthenticationRepository authentication,
                           IViewManager viewManager)
    {
			EzUser = new PersistentUserRepository(portal, mcm, authentication, viewManager);
      Asset = new PersistentAssetRepository(mcm, viewManager, new CachedEzUserGetter(EzUser));
      EzFile = new PersistentEzFileRepository(mcm);
      Definition = new DefinitionRepository(mcm);
			Project = new ProjectRepository(mcm, EzUser);
			Label = new PersistentLabelRepository(mcm);
    }
  }

}