﻿using System;
using System.Collections.Generic;
using System.Linq;
using Chaos.Mcm.Data;
using Chaos.Mcm.Data.Dto;
using EZArchive.Portal.Module.Core.Configuration;
using EZArchive.Portal.Module.Core.Gateway;
using EZArchive.Portal.Module.Data.Exceptions;
using Newtonsoft.Json;

namespace EZArchive.Portal.Module.Data.Mcm
{
	public class DefinitionRepository : IDefinitionRepository
	{
		private IList<DataDefinition> _cache = new List<DataDefinition>();
		private DateTime _cacheExpiration = new DateTime();
		public IMcmRepository Mcm { get; set; }

		public DefinitionRepository(IMcmRepository mcm)
		{
			Mcm = mcm;
		}

		public IEnumerable<DataDefinition> GetAnnotations()
		{
			return GetDefinitions().Where(def => def.Type == DefinitionType.Annotation);
		}

		public IEnumerable<DataDefinition> GetDataDefinitions()
		{
			return GetDefinitions().Where(def => def.Type == DefinitionType.Metadata);
		}

		private IEnumerable<DataDefinition> GetDefinitions()
		{
			if (DateTime.Now.CompareTo(_cacheExpiration) > 0)
			{
				lock (_cache)
				{
					if (DateTime.Now.CompareTo(_cacheExpiration) > 0)
					{
						var schemas = Mcm.MetadataSchemaGet();

						_cache = MapDataDefinitions(schemas).ToList();
						_cacheExpiration = DateTime.Now.AddMinutes(20);
					}
				}
			}

			return _cache;
		}

		private static IEnumerable<DataDefinition> MapDataDefinitions(IList<MetadataSchema> schemas)
		{
			var definitions = new List<DataDefinition>();

			foreach (var schema in schemas)
			{
				try
				{
					var definition = schema.GetSchema<DataDefinition>();
					definition.Identifier = schema.Guid.ToString();

					definitions.Add(definition);
				}
				catch (JsonReaderException)
				{
				}
			}

			return definitions;
		}

		public DataDefinition GetDataDefinition(string id)
		{
			var schema = id.Split('.')[0];

			var definition = GetDataDefinitions().FirstOrDefault(i => i.Name == schema);

			if (definition == null) throw new DataNotFoundException(string.Format("No DataDefinition named ({0}) found", id));

			return definition;
		}

		public void SetDataDefinition(DataDefinition definition)
		{
			throw new NotImplementedException();
		}
	}
}