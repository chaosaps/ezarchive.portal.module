﻿using EZArchive.Portal.Module.Core.Gateway;

namespace EZArchive.Portal.Module.Data.InMemory
{
  public class EzInMemoryRepository : IEzArchiveRepository
  {
    public IAssetRepository Asset { get; set; }
    public IEzUserRepository EzUser { get; set; }
    public IEzFileRepository EzFile { get; set; }
    public IDefinitionRepository Definition { get; set; }
	  public IProjectRepository Project { get; set; }
	  public ILabelRepository Label { get; set; }

	  public EzInMemoryRepository()
    {
      EzUser = new EzUserRepository();
      EzFile = new EzFileRepository();
      Asset = new AssetRepository((EzFileRepository) EzFile);
      Definition = new DefinitionRepository();
		  Project = new ProjectRepository();
		  Label = new LabelRepository();
    }
  }
}