﻿using System;
using System.Collections.Generic;
using System.Linq;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Core.Gateway;
using EZArchive.Portal.Module.Data.Exceptions;

namespace EZArchive.Portal.Module.Data.InMemory
{
	public class AssetRepository : EntityRepository<Asset>, IAssetRepository
	{
		private EzFileRepository EzFile { get; set; }

		public AssetRepository(EzFileRepository ezFile)
		{
			EzFile = ezFile;
		}

		public Asset Set(Asset asset)
		{
			try
			{
				if (asset.Identifier == null)
					Store(asset);
				else
				{
					var old = Get(Guid.Parse(asset.Identifier));
					old.Data = asset.Data;
					old.Annotations = asset.Annotations;
					old.Tags = asset.Tags;
					old.DoFilesRequireLogin = asset.DoFilesRequireLogin;
					old.LastChangedByUserId = asset.LastChangedByUserId;

					Store(old);
				}
			}
			catch (DataNotFoundException)
			{
				Store(asset);
			}

			return asset;
		}

		public Asset Get(Guid id)
		{
			if (id == Guid.Empty)
				throw new DataNotFoundException("All arguments cannot be null");

			var firstOrDefault = _store.Values.Select(Deserialize).FirstOrDefault();

			if (firstOrDefault == null)
				throw new DataNotFoundException("FileId not Found");

			foreach (var file in EzFile.Get(id).Where(f => firstOrDefault.Files.All(ff => ff.Identifier != f.Identifier)))
				firstOrDefault.Files.Add(file);

			return firstOrDefault;
		}

		public bool Delete(Guid id)
		{
			return Delete(id.ToString());
		}

		public IEnumerable<Asset> Get(uint? objectTypeId = null, uint index = 0)
		{
			var firstOrDefault = _store.Values.Select(Deserialize).FirstOrDefault();

			if (firstOrDefault == null)
				throw new DataNotFoundException("FileId not Found");

			yield return firstOrDefault;
		}

		public IEnumerable<Asset> Get(uint? objectTypeId = null)
		{
			var firstOrDefault = _store.Values.Select(Deserialize).FirstOrDefault();

			if (firstOrDefault == null)
				throw new DataNotFoundException("FileId not Found");

			yield return firstOrDefault;
		}
	}
}