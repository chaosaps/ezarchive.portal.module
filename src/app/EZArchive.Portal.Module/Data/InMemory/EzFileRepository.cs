﻿using System;
using System.Collections.Generic;
using System.Linq;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Core.Gateway;
using EZArchive.Portal.Module.Data.Exceptions;

namespace EZArchive.Portal.Module.Data.InMemory
{
  public class EzFileRepository : EntityRepository<AssetFileReference>, IEzFileRepository
  {
    private uint _CurrentId = 0;

    public uint Set(Guid assetId, uint? parentId, string filename, string originalFilename, string folderpath,
                       uint formatId, uint id = 0)
    {
      var formatInfo = GetFormatInfo(formatId);

      var fileReference = new Asset.FileReference
        {
          Identifier = NextId().ToString(),
          Type = formatInfo.FormatCategory,
					Name = originalFilename,
          Destinations = new List<Asset.FileReference.Destination>
            {
              new Asset.FileReference.Destination
                {
                  Type = formatInfo.MimeType,
                  Url = string.Format("{0}/{1}", folderpath, System.IO.Path.GetFileName(filename))
                }
            }
        };

      try
      {
        var join = Retrieve(assetId.ToString());
        join.Files.Add(fileReference);

        Store(join);
      }
      catch (DataNotFoundException)
      {
        Store(new AssetFileReference
          {
            Identifier = assetId.ToString(),
            Files = new[] {fileReference}
          });
      }

      return uint.Parse(fileReference.Identifier);
    }

    public bool Delete(uint id)
    {
      foreach (var key in _store.Keys)
      {
        var asset = Retrieve(key);

        for (var i = 0; i < asset.Files.Count; i++)
        {
          var fileReference = asset.Files[i];

          if (fileReference.Identifier == id.ToString())
          {
            asset.Files.RemoveAt(i);

            return true;
          }
        }
      }

      return false;
    }

    public IEnumerable<Asset.FileReference.Destination> GetIncludingChildren(uint id)
    {
	    foreach (var fileReference in Retrieve())
		    foreach (var d in fileReference.Files.Where(file => file.Identifier == id.ToString()).SelectMany(file => file.Destinations))
			    yield return d;
    }

	  public uint Set(uint id, string originalFilename)
	  {
		  throw new NotImplementedException();
	  }

	  private FormatInfo GetFormatInfo(uint formatId)
    {
      switch (formatId)
      {
        case 1:
          return new FormatInfo {FormatCategory = "Video Source", MimeType = "application/octet-stream"};
        case 2:
          return new FormatInfo {FormatCategory = "Audio Source", MimeType = "application/octet-stream"};
        case 3:
          return new FormatInfo {FormatCategory = "Image Source", MimeType = "application/octet-stream"};
        case 4:
          return new FormatInfo {FormatCategory = "Other Source", MimeType = "application/octet-stream"};
        case 6:
          return new FormatInfo {FormatCategory = "Video Preview", MimeType = "video/webm"};
        case 7:
          return new FormatInfo {FormatCategory = "Video Preview", MimeType = "video/mp4"};
        case 11:
          return new FormatInfo {FormatCategory = "Audio Preview", MimeType = "Audio/mp4"};
        default:
          return new FormatInfo {FormatCategory = "Unknown", MimeType = "application/octet-stream"};
      }
    }

    private class FormatInfo
    {
      public string FormatCategory { get; set; }
      public string MimeType { get; set; }
    }

    private uint NextId()
    {
      return ++_CurrentId;
    }

    public IList<Asset.FileReference> Get(Guid assetId)
    {
      try
      {
        return Retrieve(assetId.ToString()).Files;
      }
      catch (DataNotFoundException)
      {
        return new List<Asset.FileReference>();
      }
    }
  }

  public class AssetFileReference : IIdentifiable
  {
    public AssetFileReference()
    {
      Files = new List<Asset.FileReference>();
    }

    public string Identifier { get; set; }
    public IList<Asset.FileReference> Files { get; set; }
  }
}