﻿using System;
using System.Collections.Generic;
using System.Linq;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Core.Gateway;

namespace EZArchive.Portal.Module.Data.InMemory
{
  public class EzUserRepository : EntityRepository<EzUser>, IEzUserRepository
  {
    public EzUser Set(EzUser user)
    {
      return Store(user);
    }

    public EzUser Get(Guid id)
    {
      return Retrieve(id.ToString());
    }

    public IEnumerable<EzUser> Get()
    {
      return _store.Keys.Select(Retrieve);
    }

	  public IEnumerable<EzUser> Search(string s, uint pageSize)
	  {
		  return Get().Where(u => u.Name.StartsWith(s)).Take((int) pageSize);
	  }
  }
}