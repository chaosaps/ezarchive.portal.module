﻿using System;
using System.Collections.Generic;
using System.Linq;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Data.Exceptions;
using Newtonsoft.Json;

namespace EZArchive.Portal.Module.Data.InMemory
{
  public class EntityRepository<T> where T : IIdentifiable
  {
    protected readonly IDictionary<string, string> _store = new Dictionary<string, string>();

    public T Store(T entity)
    {
      if (entity.Identifier == null)
        entity.Identifier = Guid.NewGuid().ToString();

      var json = JsonConvert.SerializeObject(entity);

      if (_store.ContainsKey(entity.Identifier))
        _store[entity.Identifier] = json;
      else
        _store.Add(entity.Identifier, json);

      return entity;
    }

    public IEnumerable<T> Retrieve()
    {
      return _store.Select(item => Deserialize(item.Value));
    }

    public T Retrieve(string id)
    {
      if (id == null || !_store.ContainsKey(id))
        throw new DataNotFoundException(typeof(T).Name + " not found, index: " + id);

      var json = _store[id];

      return Deserialize(json);
    }

    public static T Deserialize(string json)
    {
      return JsonConvert.DeserializeObject<T>(json);
    }

    public bool Delete(string id)
    {
      if (id == null || !_store.ContainsKey(id))
        return false;

      _store.Remove(id);

      return true;
    }
  }
}