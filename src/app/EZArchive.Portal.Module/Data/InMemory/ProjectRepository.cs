﻿using System;
using System.Collections.Generic;
using System.Linq;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Core.Gateway;

namespace EZArchive.Portal.Module.Data.InMemory
{
	public class ProjectRepository : EntityRepository<Project>, IProjectRepository
	{
		public Project Set(Project project)
		{
			foreach (var label in project.Labels)
				if (label.Identifier == null)
					label.Identifier = Guid.NewGuid().ToString();

			return Store(project);
		}

		public IEnumerable<Project> Get(string id = null, string userId = null, string labelId = null)
		{
			return Retrieve().Where(p => (userId == null || p.Users.Any(u => u.Identifier == userId)) &&
			                             (labelId == null || p.Labels.Any(l => l.Identifier == labelId)) &&
			                             (id == null || id == p.Identifier));
		}
	}
}