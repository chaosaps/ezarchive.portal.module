﻿using System.Collections.Generic;
using System.Linq;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Core.Gateway;

namespace EZArchive.Portal.Module.Data.InMemory
{
	public class LabelRepository : EntityRepository<LabelRepository.LabelAssetAssociation>, ILabelRepository
	{
		public void SetAssociation(string id, string assetId)
		{
			Store(new LabelAssetAssociation(id, assetId));
		}

		public void DeleteAssociation(string id, string assetId)
		{
			Delete(id);
		}

		public IEnumerable<Label> Get(string assetId)
		{
			return Retrieve().Where(la => la.AssetId == assetId).Select(la => new Label{Identifier = la.Identifier});
		}

		public new bool Delete(string id)
		{
			var projects = Context.Repository.Project.Get(labelId: id).ToList();

			foreach (var project in projects)
			{
				var lbl = project.Labels.FirstOrDefault(l => l.Identifier == id);
				if (lbl == null) continue;

				project.Labels.Remove(lbl);

				Context.Repository.Project.Set(project);
			}

			base.Delete(id);

			return true;
		}

		public class LabelAssetAssociation : IIdentifiable
		{
			public LabelAssetAssociation() { }

			public LabelAssetAssociation(string id, string assetId)
			{
				Identifier = id;
				AssetId = assetId;
			}

			public string AssetId { get; set; }
			public string Identifier { get; set; }
		}
	}
}