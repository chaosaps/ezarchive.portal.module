﻿using System.Collections.Generic;
using System.Linq;
using EZArchive.Portal.Module.Core.Configuration;
using EZArchive.Portal.Module.Core.Gateway;

namespace EZArchive.Portal.Module.Data.InMemory
{
  public class DefinitionRepository : IDefinitionRepository
  {
    public EntityRepository<DataDefinition> _dataDefinitions = new EntityRepository<DataDefinition>();

    public IEnumerable<DataDefinition> GetAnnotations()
    {
      return _dataDefinitions.Retrieve().Where(item => item.Type == DefinitionType.Annotation);
    }

    public IEnumerable<DataDefinition> GetDataDefinitions()
    {
      return _dataDefinitions.Retrieve().Where(item => item.Type == DefinitionType.Metadata);
    }

    public DataDefinition GetDataDefinition(string id)
    {
      var schema = id.Split('.')[0];

      return GetDataDefinitions().First(i => i.Name == schema);
    }

    public void SetDataDefinition(DataDefinition definition)
    {
	    var def = _dataDefinitions.Retrieve().SingleOrDefault(d => d.Name == definition.Name);

	    if (def != null)
				definition.Identifier = def.Identifier;
				
			_dataDefinitions.Store(definition);
    }
  }
}