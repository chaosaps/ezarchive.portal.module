﻿using System.Collections.Generic;
using Chaos.Mcm.Configuration;
using Chaos.Mcm.Data;
using Chaos.Mcm.Extension.v6;
using Chaos.Portal.Authentication;
using Chaos.Portal.Core;
using Chaos.Portal.Core.Module;
using EZArchive.Portal.Module.Api.Bindings;
using EZArchive.Portal.Module.Api.Endpoints;
using EZArchive.Portal.Module.Api.Results;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Core.Configuration;
using EZArchive.Portal.Module.Core.UseCasees;
using EZArchive.Portal.Module.Data.InMemory;
using EZArchive.Portal.Module.Data.Mcm;
using EZArchive.Portal.Module.Data.View;
using EZArchive.Portal.Module.Workflows.Aws;
using Newtonsoft.Json;
using Label = EZArchive.Portal.Module.Api.Endpoints.Label;

namespace EZArchive.Portal.Module
{
	public class EzArchiveModule : IModuleConfig
	{
		public void Load(IPortalApplication portalApplication)
		{
			Context.Config = portalApplication.GetSettings<Config>("EZ-Archive");

			if (Context.Config.IsSandbox)
			{
				Context.Repository = new EzInMemoryRepository();

				portalApplication.MapRoute("/v6/EZFile", () => new EzFileExtension(portalApplication, null, null));
				portalApplication.MapRoute("/v6/EZAsset", () => new AssetExtension(portalApplication, null));
			}
			else
			{
				portalApplication.OnModuleLoaded += (sender, args) =>
				{
					var authModule = args.Module as IAuthenticationModule;

					if (authModule == null) return;

					var mcmConfig = portalApplication.GetSettings<McmModuleConfiguration>("MCM");

					var portal = portalApplication.PortalRepository;
					var mcm = new McmRepository().WithConfiguration(mcmConfig.ConnectionString);

					Context.Repository = new EzMcmRepository(portal, mcm, authModule.AuthenticationRepository,
						portalApplication.ViewManager);

					authModule.OnUserLoggedIn += (o, ra) => new UserLogsInUseCase().EnsureUserHasARole(ra.Request.User.Guid);
					authModule.OnWayfUserLoggedIn += (o, pa) =>
						new UserLogsInUseCase().EnsureUserHasWayfAttributes(pa.UserId, pa.AttributesObject);
					portalApplication.MapRoute("/v6/EZDownload", () => new Download(portalApplication, mcm, mcmConfig));
				};

				var storage = new S3();
				var transcoder = new ElasticTranscoder();

				portalApplication.MapRoute("/v6/EZFile", () => new EzFileExtension(portalApplication, storage, transcoder));
				portalApplication.MapRoute("/v6/EZAsset", () => new AssetExtension(portalApplication, storage));
			}

			portalApplication.MapRoute("/v6/EZAnnotation", () => new AnnotationExtension(portalApplication));
			portalApplication.MapRoute("/v6/EZUser", () => new EzUserExtension(portalApplication));
			portalApplication.MapRoute("/v6/EZAnnotationDefinition", () => new AnnotationDefinition(portalApplication));
			portalApplication.MapRoute("/v6/EZLabel", () => new Label(portalApplication));

			portalApplication.AddBinding(typeof(AnnotationVisibility),
				new JsonParameterBinding<AnnotationVisibility>(optional: true));
			portalApplication.AddBinding(typeof(Dictionary<string, string>),
				new JsonParameterBinding<Dictionary<string, string>>());
			portalApplication.AddBinding(typeof(IDictionary<string, string>),
				new JsonParameterBinding<Dictionary<string, string>>());
			portalApplication.AddBinding(typeof(IDictionary<string, Value>), new FieldValuesParameterBinding());
			portalApplication.AddBinding(typeof(Dictionary<string, Value>), new FieldValuesParameterBinding());
			portalApplication.AddBinding(typeof(SearchQuery), new SearchQueryParameterBinding());
			portalApplication.AddBinding(typeof(AnnotationPermission), new EnumParameterBinding<AnnotationPermission>());

			portalApplication.AddView(new UserView(), Context.Config.UserView);

			var settings = new JsonSerializerSettings();
			settings.Converters.Add(new ValueConverter());

			JsonConvert.DefaultSettings = () => settings;
		}
	}
}