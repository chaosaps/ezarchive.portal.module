﻿using System.Collections.Generic;
using CHAOS.Serialization;
using Chaos.Portal.Core.Data.Model;

namespace EZArchive.Portal.Module.Api.Results
{
  public class DataDefinitionResult : AResult
  {
    public DataDefinitionResult()
    {
      Fields = new Dictionary<string, FieldDefinition>();
    }
    
    [Serialize]
    public string Identifier { get; set; }

    [Serialize]
    public string Name { get; set; }

    [Serialize]
    public string Type { get; set; }

    [Serialize]
		public string TypeId { get; set; }

		[Serialize]
		public bool IsEditable { get; set; }

    [Serialize]
    public string Description { get; set; }

    [Serialize]
    public IDictionary<string, FieldDefinition> Fields { get; set; }
  }

	[Serialize("FieldDefinition")]
  public class FieldDefinition
  {
    [Serialize]
    public string DisplayName { get; set; }

    [Serialize]
    public string Type { get; set; }

    [Serialize]
    public string[] Options { get; set; }

    [Serialize]
    public bool IsRequired { get; set; }

    [Serialize]
    public string Placeholder { get; set; }
  }
}