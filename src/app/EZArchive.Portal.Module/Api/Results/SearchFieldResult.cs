﻿using CHAOS.Serialization;

namespace EZArchive.Portal.Module.Api.Results
{
	[Serialize("SearchFieldResult")]
  public class SearchFieldResult
  {
    [Serialize]
    public string Key { get; set; }

    [Serialize]
    public string Value { get; set; }
  }
}
