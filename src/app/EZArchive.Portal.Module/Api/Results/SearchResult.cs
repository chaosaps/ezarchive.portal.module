﻿using System.Collections.Generic;
using CHAOS.Serialization;
using Chaos.Portal.Core.Data.Model;

namespace EZArchive.Portal.Module.Api.Results
{
  public class SearchResult : AResult
  {
    public SearchResult()
    {
      Fields = new List<SearchFieldResult>();
    }

    [Serialize]
    public string Identifier { get; set; }

		[Serialize]
		public string TypeId { get; set; }

    [Serialize]
    public IList<SearchFieldResult> Fields { get; set; }
  }
}