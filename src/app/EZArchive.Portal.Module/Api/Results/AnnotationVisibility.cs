﻿namespace EZArchive.Portal.Module.Api.Results
{
	public class AnnotationVisibility
	{
		public static AnnotationVisibility Public { get; }
		public static AnnotationVisibility Private { get; }

		static AnnotationVisibility()
		{
			Public = new AnnotationVisibility
			{
				Scope = "Public"
			};
			Private = new AnnotationVisibility
			{
				Scope = "Private"
			};
		}

		public string Scope { get; set; }

		public override string ToString()
		{
			return $"{Scope}";
		}
	}
}