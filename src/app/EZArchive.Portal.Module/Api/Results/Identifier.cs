﻿using CHAOS.Serialization;
using Chaos.Portal.Core.Data.Model;

namespace EZArchive.Portal.Module.Api.Results
{
	public class IdentifierResult : AResult
	{
		[Serialize]
		public string Identifier { get; set; }

		public IdentifierResult(string identifier)
		{
			Identifier = identifier;
		}
	}
}