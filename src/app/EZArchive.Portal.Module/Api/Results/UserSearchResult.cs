﻿using Chaos.Portal.Core.Data.Model;
using CHAOS.Serialization;

namespace EZArchive.Portal.Module.Api.Results
{
	public class UserSearchResult : AResult
	{
		[Serialize]
		public string Identifier { get; set; }

		[Serialize]
		public string Name { get; set; }
	}
}