﻿using System;
using Chaos.Portal.Core.Request;

namespace EZArchive.Portal.Module.Api.Endpoints
{
  internal interface IWorkflow
  {
    void Invoke(Guid assetId, FileStream file);
  }
}