using System;
using System.IO;
using Chaos.Portal.Module.Larmfm.Domain;
using EZArchive.Portal.Module.Core;
using FileStream = Chaos.Portal.Core.Request.FileStream;

namespace EZArchive.Portal.Module.Api.Endpoints.Workflow
{
  public class VideoWorkflow : IWorkflow
  {
    private IStorage Storage { get; set; }
    private ITranscoder Transcoder { get; set; }

    public VideoWorkflow(IStorage storage, ITranscoder transcoder)
    {
      Storage = storage;
      Transcoder = transcoder;
    }

    public void Invoke(Guid assetId, FileStream file)
    {
      var folderpath = "Objects/" + assetId.ToString();
      var filename = Guid.NewGuid().ToString();
      var sourceFilename = String.Format("{0}{1}", filename, Path.GetExtension(file.FileName));

      var parentId = Context.Repository.EzFile.Set(assetId, null, sourceFilename, file.FileName, folderpath, 1);

      var sourceKey = String.Format("{0}/{1}", folderpath, sourceFilename);
      Storage.Write(sourceKey, file.InputStream);

      foreach (var preset in Context.Config.Aws.VideoPresets)
      {
        var outputFilename = Guid.NewGuid() + "." + preset.Extension;
        var outputKey = String.Format("{0}/{1}", folderpath, outputFilename);
        Transcoder.Transcode(sourceKey, outputKey, preset.Id);

        Context.Repository.EzFile.Set(assetId, parentId, outputFilename, file.FileName, folderpath, preset.FormatId);
      }
    }
  }
}