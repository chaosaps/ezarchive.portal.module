using System;
using System.IO;
using Chaos.Portal.Module.Larmfm.Domain;
using EZArchive.Portal.Module.Core;
using FileStream = Chaos.Portal.Core.Request.FileStream;

namespace EZArchive.Portal.Module.Api.Endpoints.Workflow
{
  public class OtherWorkflow : IWorkflow
  {
    private IStorage Storage { get; set; }

    public OtherWorkflow(IStorage storage)
    {
      Storage = storage;
    }

    public void Invoke(Guid assetId, FileStream file)
    {
      var folderpath = "Objects/" + assetId.ToString();
      var filename = Guid.NewGuid().ToString();
      var sourceFilename = String.Format("{0}{1}", filename, Path.GetExtension(file.FileName));
	    var formatId = FindFormat(file.FileName);

			Context.Repository.EzFile.Set(assetId, null, sourceFilename, file.FileName, folderpath, formatId);

      var sourceKey = String.Format("{0}/{1}", folderpath, sourceFilename);
      Storage.Write(sourceKey, file.InputStream);
    }

	  private static uint FindFormat(string filename)
	  {
		  if (filename.EndsWith(".pdf")) return 12;
		  if (filename.EndsWith(".doc")) return 13;
		  if (filename.EndsWith(".docx")) return 13;

		  return 4;
	  }
  }
}