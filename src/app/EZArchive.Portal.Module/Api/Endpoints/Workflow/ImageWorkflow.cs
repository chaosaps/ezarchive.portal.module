using System;
using System.IO;
using Chaos.Portal.Module.Larmfm.Domain;
using EZArchive.Portal.Module.Core;
using FileStream = Chaos.Portal.Core.Request.FileStream;

namespace EZArchive.Portal.Module.Api.Endpoints.Workflow
{
  public class ImageWorkflow : IWorkflow
  {
    private IStorage Storage { get; set; }

    public ImageWorkflow(IStorage storage)
    {
      Storage = storage;
    }

    public void Invoke(Guid assetId, FileStream file)
    {
      var folderpath = "Objects/" + assetId.ToString();
      var filename = Guid.NewGuid().ToString();
      var sourceFilename = String.Format("{0}{1}", filename, Path.GetExtension(file.FileName));

      Context.Repository.EzFile.Set(assetId, null, sourceFilename, file.FileName, folderpath, 3);

      var sourceKey = String.Format("{0}/{1}", folderpath, sourceFilename);
      Storage.Write(sourceKey, file.InputStream, GetContentType(file.FileName));
    }

	  private string GetContentType(string filename)
	  {
		  var extension = Path.GetExtension(filename);

		  switch (extension)
		  {
				case ".jpeg":
				case ".jpg":
				  return "image/jpeg";
				case ".png":
				  return "image/png";
				default:
				  return "application/octet-stream";

		  }
	  }
  }
}