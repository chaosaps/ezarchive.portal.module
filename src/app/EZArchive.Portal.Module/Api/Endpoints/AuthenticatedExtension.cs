﻿using System.Linq;
using Chaos.Portal.Core;
using Chaos.Portal.Core.Exceptions;
using Chaos.Portal.Core.Extension;
using EZArchive.Portal.Module.Core;

namespace EZArchive.Portal.Module.Api.Endpoints
{
	public abstract class AuthenticatedExtension : AExtension
	{
		protected AuthenticatedExtension(IPortalApplication portalApplication) : base(portalApplication)
		{
		}

		/// <summary>
		/// Throws and exception if user is not authenticated
		/// <exception cref="InsufficientPermissionsException"></exception>
		/// </summary>
		protected void RequiresAuthentication()
		{
			if (Request.IsAnonymousUser)
				throw new InsufficientPermissionsException("User not logged in");
		}

		protected void Requires(string[] orPermissions)
		{
			var me = GetCurrentUser();

			if (!UserIs(orPermissions))
				throw new InsufficientPermissionsException("The current user has insufficient permissions");
		}

		protected bool UserIs(string[] orPermissions)
		{
			var me = GetCurrentUser();

			return orPermissions.Any(perm => perm == me.Permission);
		}

		protected EzUser GetCurrentUser()
		{
			var userId = Request.User.Guid;
			var user = Context.Repository.EzUser.Get(userId);

			return user;
		}
	}
}