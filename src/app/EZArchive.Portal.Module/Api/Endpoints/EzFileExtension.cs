﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Chaos.Portal.Core;
using Chaos.Portal.Module.Larmfm.Domain;
using Chaos.Portal.v5.Extension.Result;
using EZArchive.Portal.Module.Api.Endpoints.Workflow;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Data.Exceptions;

namespace EZArchive.Portal.Module.Api.Endpoints
{
	public class EzFileExtension : AuthenticatedExtension
	{
		public IStorage Storage { get; set; }
		private readonly IDictionary<uint, IWorkflow> _Workflows = new Dictionary<uint, IWorkflow>();

		public EzFileExtension(IPortalApplication portalApplication, IStorage storage, ITranscoder transcoder) : base(
			portalApplication)
		{
			Storage = storage;
			_Workflows.Add(4, new OtherWorkflow(storage));
			_Workflows.Add(3, new ImageWorkflow(storage));
			_Workflows.Add(2, new AudioWorkflow(storage, transcoder));
			_Workflows.Add(1, new VideoWorkflow(storage, transcoder));
		}

		public EndpointResult Upload(Guid assetId, uint type = 4)
		{
			RequiresAuthentication();
			Requires(new[] {"Administrator", "Contributor"});

			var file = Request.Files.FirstOrDefault();
			if (file == null) throw new IOException("No data found");

			_Workflows[type].Invoke(assetId, file);

			return EndpointResult.Success();
		}

		public EndpointResult Delete(uint id)
		{
			RequiresAuthentication();
			Requires(new[] {"Administrator", "Contributor"});

			var files = Context.Repository.EzFile.GetIncludingChildren(id);

			foreach (var destination in files)
				Storage.Delete(destination.Url);

			Context.Repository.EzFile.Delete(id);

			return EndpointResult.Success();
		}

		public EndpointResult Set(uint id, string name)
		{
			RequiresAuthentication();
			Requires(new[] {"Administrator", "Contributor"});

			var files = Context.Repository.EzFile.GetIncludingChildren(id);
			if (!files.Any()) throw new DataNotFoundException("");

			var result = Context.Repository.EzFile.Set(id, name);

			return new EndpointResult {WasSuccess = result > 0};
		}
	}
}