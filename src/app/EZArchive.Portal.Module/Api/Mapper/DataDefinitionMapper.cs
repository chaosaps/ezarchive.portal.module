using System.Linq;
using EZArchive.Portal.Module.Api.Results;
using EZArchive.Portal.Module.Core.Configuration;
using FieldDefinition = EZArchive.Portal.Module.Core.Configuration.FieldDefinition;

namespace EZArchive.Portal.Module.Api.Mapper
{
  public class DataDefinitionMapper
  {
    public static DataDefinitionResult Map(DataDefinition dataDefinition)
    {
      return new DataDefinitionResult
        {
          Identifier = dataDefinition.Identifier,
          Name = dataDefinition.Name,
          Type = dataDefinition.Type.ToString(),
          TypeId = dataDefinition.TypeId.ToString(),
          Description = dataDefinition.Description,
          Fields = dataDefinition.Fields.ToDictionary<FieldDefinition, string, Results.FieldDefinition>(field => field.Id, MapType)
        };
    }

    private static Results.FieldDefinition MapType(FieldDefinition fieldDefinition)
    {
      return new Results.FieldDefinition
        {
          Type = fieldDefinition.Type,
          Options = fieldDefinition.Options,
          IsRequired = fieldDefinition.IsRequired,
          Placeholder = fieldDefinition.Placeholder,
          DisplayName = fieldDefinition.DisplayName
        };
    }
  }
}