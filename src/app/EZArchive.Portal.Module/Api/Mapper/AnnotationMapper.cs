﻿using System.Collections.Generic;
using EZArchive.Portal.Module.Core;

namespace EZArchive.Portal.Module.Api.Mapper
{
  public static class AnnotationMapper
  {
    public static IDictionary<string, object> Map(IDictionary<string, Value> annotation)
    {
      var dict = new Dictionary<string, object>();

      foreach (var field in annotation)
      {
        if (field.Value.Type == "Table")
          dict.Add(field.Key, (field.Value as TableValue).Value);
        else dict.Add(field.Key, field.Value.ToString());
      }

      return dict;
    }
  }
}