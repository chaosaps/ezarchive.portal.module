﻿using System.Collections.Generic;
using System.Linq;
using EZArchive.Portal.Module.Api.Results;
using EZArchive.Portal.Module.Core;

namespace EZArchive.Portal.Module.Api.Mapper
{
  public static class SearchMapper
  {
    public static SearchResult Map(Search search)
    {
      return new SearchResult
        {
          Identifier = search.Identifier,
					TypeId = search.TypeId,
          Fields = Copy(search.Fields)
        };
    }

    private static IList<SearchFieldResult> Copy(IEnumerable<SearchField> input)
    {
      var sfrs = new List<SearchFieldResult>();

      foreach (var field in input)
      {
        var def = Context.Config.SearchDefinition.SingleOrDefault(definition => definition.DisplayName == field.Key);

        if(def != null && !def.IsVisible) continue;
        
        sfrs.Add(new SearchFieldResult
        {
          Key = field.Key,
          Value = field.Value
        });
      }

      return sfrs;
    }
  }
}