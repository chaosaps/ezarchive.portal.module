﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Chaos.Portal.Core.Bindings;
using Chaos.Portal.Core.Exceptions;

namespace EZArchive.Portal.Module.Api.Bindings
{
  public class EnumParameterBinding<T> : IParameterBinding where T : struct, IConvertible
  {
    public object Bind(IDictionary<string, string> parameters, ParameterInfo parameterInfo)
    {
      var name = parameterInfo.Name;

      if (parameters.ContainsKey(name))
        return Enum.Parse(typeof(T), parameters[name], true);

      throw new ParameterBindingMissingException(string.Format("No parameter named: {0} was found", name));
    }
  }
}