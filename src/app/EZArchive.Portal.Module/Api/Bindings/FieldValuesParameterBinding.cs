﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Chaos.Portal.Core.Bindings;
using EZArchive.Portal.Module.Core;
using Newtonsoft.Json;

namespace EZArchive.Portal.Module.Api.Bindings
{
  public class FieldValuesParameterBinding : IParameterBinding
  {
    public object Bind(IDictionary<string, string> parameters, ParameterInfo parameterInfo)
    {
      string name = parameterInfo.Name;

      if (!parameters.ContainsKey(name))
        throw new ArgumentException("Parameter not given", parameterInfo.Name);

      if (parameters[name] == null) 
        throw new NotImplementedException();

      return JsonConvert.DeserializeObject<Dictionary<string, Value>>(parameters[name], new ValueConverter());
    }
  }

  public class ValueConverter : JsonConverter
  {
    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
      var dict = (Dictionary<string, Value>) value;
      writer.WriteStartObject();

      foreach (var pair in dict)
      {
        var stringValue = pair.Value as StringValue;
        var dateTimeValue = pair.Value as DateTimeValue;
        var tableValue = pair.Value as TableValue;
        var booleanValue = pair.Value as BooleanValue;

        if (stringValue != null)
        {
          writer.WritePropertyName(pair.Key);
          writer.WriteValue(stringValue.Value);
        }
        else if (dateTimeValue != null)
        {
          writer.WritePropertyName(pair.Key);
          writer.WriteValue(dateTimeValue.Value);
        }
        else if (booleanValue != null)
        {
          writer.WritePropertyName(pair.Key);
          writer.WriteValue(booleanValue.Value);
        }
        else if (tableValue != null)
        {
          writer.WritePropertyName(pair.Key);
          writer.WriteStartArray();

          foreach (var row in tableValue.Value)
          {
            writer.WriteStartArray();

            foreach (var cell in row)
              writer.WriteValue(cell);

            writer.WriteEnd();
          }

          writer.WriteEnd();
        }
      }

      writer.WriteEndObject();
    }

    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
      var dict = new Dictionary<string, Value>();

      while (reader.Read())
      {
        if (reader.TokenType == JsonToken.EndObject) break;

        if (reader.TokenType == JsonToken.PropertyName)
        {
          var key = reader.Value.ToString();
          
          reader.Read();

          if (reader.TokenType == JsonToken.String)
          {
            dict.Add(key, new StringValue(reader.Value.ToString()));

            continue;
          }
          
          if (reader.TokenType == JsonToken.Null)
          {
            dict.Add(key, new NullValue());

            continue;
          }

          if (reader.TokenType == JsonToken.Date)
          {
            dict.Add(key, new DateTimeValue(reader.Value.ToString()));

            continue;
          }

          if (reader.TokenType == JsonToken.StartArray)
          {
            reader.Read();
            
            if (reader.TokenType == JsonToken.StartArray)
            {
              var table = new TableValue();
              var row = new List<string>();

              while (reader.Read())
              {
                if (reader.TokenType == JsonToken.EndArray)
                  break;

                if (reader.TokenType == JsonToken.String)
                {
                  for (; reader.TokenType == JsonToken.String; reader.Read())
                    row.Add(reader.Value.ToString());
                  
                  if (reader.TokenType == JsonToken.EndArray)
                    table.Value.Add(row);
                }
                if (reader.TokenType == JsonToken.StartArray)
                  row = new List<string>();
              }

              dict.Add(key, table);
            }
          }
        }
      }

      return dict;
    }

    public override bool CanConvert(Type objectType)
    {
      return objectType == typeof(IDictionary<string, Value>) || objectType == typeof(Dictionary<string, Value>);
    }
  }
}