﻿using System.Collections.Generic;
using System.Reflection;
using Chaos.Portal.Core.Bindings;
using EZArchive.Portal.Module.Core;

namespace EZArchive.Portal.Module.Api.Bindings
{
  public class SearchQueryParameterBinding : IParameterBinding
  {
    public object Bind(IDictionary<string, string> parameters, ParameterInfo parameterInfo)
    {
      return parameters.ContainsKey(parameterInfo.Name) ? SearchQuery.Parse(parameters[parameterInfo.Name]) : SearchQuery.AllQuery();
    }
  }
}