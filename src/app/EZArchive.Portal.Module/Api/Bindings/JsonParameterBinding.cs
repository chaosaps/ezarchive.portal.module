﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Newtonsoft.Json;
using Chaos.Portal.Core.Bindings;

namespace EZArchive.Portal.Module.Api.Bindings
{
  // TODO: Promote this class to be part of Portal
  public class JsonParameterBinding<T> : IParameterBinding
  {
    private readonly bool _optional;

    public JsonParameterBinding(bool optional = false)
    {
      _optional = optional;
    }
    
    public object Bind(IDictionary<string, string> parameters, ParameterInfo parameterInfo)
    {
      var name = parameterInfo.Name;

      if(!parameters.ContainsKey(name))
        if (_optional)
          return null;
        else
          throw new ArgumentException("Parameter not given", parameterInfo.Name);

      return JsonConvert.DeserializeObject<T>(parameters[name]);
    }
  }
}
