using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Data.Tests;

[TestFixture]
public class SearchDefinitionRepositoryTests
{
	private readonly SearchFieldDefinition searchFieldDefinition
		= new SearchFieldDefinition("dn", new string[0], "t", true, true);

	private SearchDefinitionRepository sut;

	[SetUp]
	public void SetUp()
	{
		sut = new SearchDefinitionRepository();
	}
		
	[Test]
	public void Getting_NoDefinitions_ReturnEmpty()
	{
		// act
		var results = sut.Get();

		// assert
		results.ShouldBeEmpty();
	}

	[Test]
	public void Getting_DefinitionsExist_Return()
	{
		// arrange
		sut.Add(searchFieldDefinition);
			
		// act
		var results = sut.Get();

		// assert
		results.ShouldNotBeEmpty();
	}
}
