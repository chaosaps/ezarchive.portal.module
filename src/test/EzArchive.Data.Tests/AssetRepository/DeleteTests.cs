using System.Threading.Tasks;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Data.Tests.AssetRepository;

[TestFixture]
public class DeleteTests
{
	private InMemoryRepositories repos;
	private Portal.AssetRepository sut;
	private Asset asset;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		sut = repos.Asset;
		asset = repos.Asset.Set(new Asset(
			AssetId.New(),
			new Asset._Type(1),
			new UserId("d88c3717-8612-4fe6-9341-ead35fa318aa"),
			true,
			new string[0],
			new Asset._Data[0],
			new Asset._AnnotationGroup[0],
			new Asset._FileReference[0]));
	}

	[Test]
	public async Task DeleteObject()
	{
		// act
		await sut.DeleteAsync(asset.Id);

		// assert
		repos.Asset.Get(asset.Id).ShouldBeNull();
	}
}