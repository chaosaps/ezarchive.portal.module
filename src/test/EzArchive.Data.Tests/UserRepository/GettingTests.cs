using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;
using PortalUserRepository = Chaos.Portal.Core.Data.InMemory.UserRepository;
using PortalGroupRepository = Chaos.Portal.Core.Data.InMemory.GroupRepository;

namespace EzArchive.Data.Tests.UserRepository;

[TestFixture]
public class GettingTests : _UserRepositoryTests
{
	[Test]
	public void GettingByUserId_UserDoesntExist_ThrowException()
	{
		// act
		void Act() => Sut.Get(UserId);

		// assert
		Should.Throw<UserNotFound>(Act);
	}

	[Test]
	public void GettingByUserId_UserExists_Return()
	{
		// arrange
		UserRepository.Create(UserId.Value, "john@example.com");
		McmInMemoryRepos.Object.Create(UserId.Value, 1, 1);
		McmInMemoryRepos.Metadata.Set(Metadata, UserId.Value);

		// act
		var result = Sut.Get(UserId);

		// assert
		result.Id.ShouldBe(UserId);
		result.Email.ShouldBe("john@example.com");
	}
}