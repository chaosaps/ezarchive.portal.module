using System.Xml.Linq;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Data.Tests.UserRepository;

[TestFixture]
public class CreatingTests : _UserRepositoryTests
{
	[Test]
	public void Creating_DoesntAlreadyExist_CreateInPortalAndMcm()
	{
		// arrange


		// act
		var user = Sut.Set(
			new User(
				null,
				"john@example.com",
				"John Doe",
				"Administrator",
				"",
				null));

		// assert
		var o = McmInMemoryRepos.Object.Get(user.Id.Value, includeMetadata: true);
		o.Guid.ShouldBe(user.Id.Value);
		o.Metadatas[0].MetadataSchemaId.ShouldBe(ProfileMetadataSchemaId);
		o.Metadatas[0].MetadataXml.ToString(SaveOptions.DisableFormatting).ShouldBe(
			"<EZArchive.Portal.Module.Core.EzUser>" +
			$"<Identifier>{user.Id}</Identifier>" +
			"<Email>john@example.com</Email>" +
			"<Name>John Doe</Name>" +
			"<IsAdministrator>true</IsAdministrator>" +
			"<Permission>Administrator</Permission>" +
			"<Preferences></Preferences>" +
			"<WayfAttributes />" +
			"</EZArchive.Portal.Module.Core.EzUser>");
	}
}