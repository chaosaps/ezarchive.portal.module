using System;
using System.Xml.Linq;
using Chaos.Authentication.Data.InMemory;
using Chaos.Mcm.Core.Model;
using Chaos.Mcm.Data.InMemory;
using Chaos.Mcm.Domain;
using EzArchive.Application.Tests.UseCases.Users;
using EzArchive.Domain;
using NUnit.Framework;
using PortalUserRepository = Chaos.Portal.Core.Data.InMemory.UserRepository;
using PortalGroupRepository = Chaos.Portal.Core.Data.InMemory.GroupRepository;

namespace EzArchive.Data.Tests.UserRepository;

[TestFixture]
public class _UserRepositoryTests
{
	protected Portal.UserRepository Sut;
	protected PortalUserRepository UserRepository;
	protected InMemoryRepositories McmInMemoryRepos;
	protected Metadata Metadata;
	protected UserId UserId;
	protected PortalGroupRepository GroupRepository;
	protected MetadataSchemaId ProfileMetadataSchemaId;

	[SetUp]
	public void Setup()
	{
		McmInMemoryRepos = new InMemoryRepositories();
		var portalRepos = new Chaos.Portal.Core.Data.InMemory.InMemoryRepositories();
		UserRepository = portalRepos.User;
		ProfileMetadataSchemaId = new MetadataSchemaId("b9234b1e-8de6-43e1-a6b4-95b1eff24952");
		GroupRepository = portalRepos.Group;
		UserId = new UserId("c2714990-8252-43ff-9d44-09966e7df398");
		var adminGroupId = new Guid("b4efd2a0-7f8d-4cdd-afb4-f0d330be6403");
		var contributorGroupId = new Guid("7475a63c-6614-4f58-bf71-400c40b3c9b6");
		var userGroupId = new Guid("4dfd7752-1569-4d26-b6a3-23f19f9221bb");
		GroupRepository.Create(adminGroupId, "Admin", new Chaos.Portal.Core.UserId(UserId.Value), uint.MaxValue);
		GroupRepository.Create(contributorGroupId, "Contributor", new Chaos.Portal.Core.UserId(UserId.Value), 0);
		GroupRepository.Create(userGroupId, "User", new Chaos.Portal.Core.UserId(UserId.Value), 0);
		Sut = new Portal.UserRepository(
			UserRepository,
			McmInMemoryRepos.Object,
			McmInMemoryRepos.Metadata,
			GroupRepository,
			new PasswordRepository(),
			new UserIndexSpy(),
			ProfileMetadataSchemaId,
			adminGroupId,
			contributorGroupId,
			userGroupId,
			1,
			1);
		Metadata = new Metadata(
			new MetadataId("3f6ab323-88ef-4bd9-ae7b-c13ba58e2660"),
			"da",
			ProfileMetadataSchemaId,
			new Chaos.Portal.Core.UserId(UserId.Value),
			1,
			XDocument.Parse(
				"<EZArchive.Portal.Module.Core.EzUser><Identifier>c2714990-8252-43ff-9d44-09966e7df398</Identifier><Email>john@example.com</Email><Name>Normal</Name><IsAdministrator>False</IsAdministrator><Permission>User</Permission></EZArchive.Portal.Module.Core.EzUser>"),
			DateTime.UtcNow);
	}
}