﻿using System;
using Chaos.Portal.Core.Extension;
using EZArchive.Portal.Module.Core.Configuration;
using EZArchive.Portal.Module.Test.Api.Endpoints;
using Moq;
using NUnit.Framework;

namespace EZArchive.Portal.Module.Test
{
  [TestFixture, Category("unittest"), Parallelizable(ParallelScope.Fixtures)]
  public class EzArchiveModuleTest : ExtensionTestBase
  {
     [Test]
     public void Load_EndpointsAreMapped()
     {
       var module = new EzArchiveModule();
       _portal.Setup(m => m.GetSettings<Config>("EZ-Archive")).Returns(new Config{IsSandbox = true});

       module.Load(_portal.Object);

       _portal.Verify(m => m.MapRoute("/v6/EZAsset", It.IsAny<Func<IExtension>>()));
       _portal.Verify(m => m.MapRoute("/v6/EZSearch", It.IsAny<Func<IExtension>>()));
       _portal.Verify(m => m.MapRoute("/v6/EZSettings", It.IsAny<Func<IExtension>>()));
       _portal.Verify(m => m.MapRoute("/v6/EZAnnotation", It.IsAny<Func<IExtension>>()));
       _portal.Verify(m => m.MapRoute("/v6/EZFile", It.IsAny<Func<IExtension>>()));
       _portal.Verify(m => m.MapRoute("/v6/EZUser", It.IsAny<Func<IExtension>>()));
       _portal.Verify(m => m.MapRoute("/v6/EZSystem", It.IsAny<Func<IExtension>>()));
       _portal.Verify(m => m.MapRoute("/v6/EZDataDefinition", It.IsAny<Func<IExtension>>()));
       _portal.Verify(m => m.MapRoute("/v6/EZFacet", It.IsAny<Func<IExtension>>()));
       _portal.Verify(m => m.MapRoute("/v6/EZSearchDefinition", It.IsAny<Func<IExtension>>()));
       _portal.Verify(m => m.MapRoute("/v6/EZAnnotationDefinition", It.IsAny<Func<IExtension>>()));
       _portal.Verify(m => m.MapRoute("/v6/EZProject", It.IsAny<Func<IExtension>>()));
       _portal.Verify(m => m.MapRoute("/v6/EZLabel", It.IsAny<Func<IExtension>>()));
     }
  }
}