﻿using System;
using System.Linq;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Data.View;
using NUnit.Framework;

namespace EZArchive.Portal.Module.Test.Data.View
{
	[TestFixture, Category("unittest")]
	public class UserViewTest
	{
		[Test]
		public void Index_GivenWrongType_ReturnEmptyList()
		{
			var view = new UserView();

			var results = view.Index(new object());

			Assert.That(results, Is.Empty);
		}

		[Test]
		public void Index_GivenNameIsNull_ReturnListWithOneUserViewData()
		{
			var view = new UserView();

			var results = view.Index(new EzUser());

			Assert.That(results, Is.Empty);
		}

		[Test]
		public void Index_GivenOneUser_ReturnListWithOneUserViewData()
		{
			var view = new UserView();

			var results = view.Index(new EzUser{Name = "name"});

			Assert.That(results.Count, Is.EqualTo(1));
		}

		[Test]
		public void GetIndexableFields()
		{
			var u = TestResources.Make_EzUserUser();
			var data = new UserViewData(u);

			var result = data.GetIndexableFields();

			Func<string, string> getValue = key => result.Single(pair => pair.Key == key).Value;
			Assert.That(getValue("Id"), Is.EqualTo(u.Identifier));
			Assert.That(getValue("t_name"), Is.EqualTo(u.Name));
		}
	}
}
