﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Chaos.Mcm.Data.Dto;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Core.Configuration;
using EZArchive.Portal.Module.Core.Gateway;
using EZArchive.Portal.Module.Data.InMemory;
using EZArchive.Portal.Module.Data.Mcm;
using Moq;
using NUnit.Framework;

namespace EZArchive.Portal.Module.Test.Data.Mcm
{
  [TestFixture, Category("unittest")]
  public class PersistentAssetRepositoryTest
  {
    [Test]
    public void CreateFieldXml_DefaultCase()
    {
      Context.Repository = new EzInMemoryRepository();
      var definition = new DataDefinition
        {
          Name = "DD1",
          Fields = new List<FieldDefinition>
            {
              new FieldDefinition {Id = "F1", Type = "String"},
              new FieldDefinition {Id = "F2", Type = "Table"}
            }
        };
      var data = new AssetData(definition)
        {
          Fields = new Dictionary<string, Value>
            {
              {"F1", new StringValue("V1")},
              {"F2", new TableValue {Value = new List<List<string>> {new List<string> {"v2", "v3"}}}}
            }
        };
      Context.Repository.Definition.SetDataDefinition(definition);

      var xml = PersistentAssetRepository.CreateFieldXml(data);

      var fs = xml.Element("Chaos.Data").Element("Fields");
      var f = fs.Element("Field");
      Assert.That(f.Attribute("Name").Value, Is.EqualTo("F1"));
      Assert.That(f.Attribute("Type").Value, Is.EqualTo("String"));
      Assert.That(f.Value, Is.EqualTo("V1"));
      var l = fs.Elements("Field").Last();
      Assert.That(l.Attribute("Name").Value, Is.EqualTo("F2"));
      Assert.That(l.Attribute("Type").Value, Is.EqualTo("Table"));
      Assert.That(l.Elements().Count(), Is.EqualTo(1));
      Assert.That(l.Elements().First().Elements().Count(), Is.EqualTo(2));
      Assert.That(l.Elements().First().Elements().First().Value, Is.EqualTo("v2"));
    }

    [Test]
    public void CreateAssetXml_DefaultCase()
    {
      var asset = new Asset
        {
          DoFilesRequireLogin = true,
          Tags = new[] {"T1", "T2"}
        };

      var xml = PersistentAssetRepository.CreateAssetXml(asset);

      Assert.That(xml.Element("Chaos.Asset").Element("DoFilesRequireLogin").Value, Is.EqualTo("true"));
      Assert.That(xml.Element("Chaos.Asset").Element("Tags").Elements("Tag").First().Value, Is.EqualTo("T1"));
      Assert.That(xml.Element("Chaos.Asset").Element("Tags").Elements("Tag").Last().Value, Is.EqualTo("T2"));
    }

    [Test]
    public void CreateAnnotationXml_DefaultCase()
    {
      var annotationGroup = new Asset.AnnotationGroup
        {
          Name = "test",
          DefinitionId = Guid.Parse("00000000-0000-0000-0000-000000000001"),
          Annotations = new[]
            {
              new Dictionary<string, Value>
                {
                  {"Identifier", new StringValue("00000000-0000-0000-0000-000000000011")}
                }
            }
        };

      var xml = PersistentAssetRepository.CreateAnnotationXml(annotationGroup);

      Assert.That(xml.Root.Element("Annotation").Element("Identifier").Value, Is.EqualTo("00000000-0000-0000-0000-000000000011"));
      Assert.That(xml.Root.Element("Annotation").Element("Identifier").HasElements, Is.False);
    }

    [Test]
    public void ExtractAnnotations_DefaultCase()
    {
      var metadata = new Metadata();
      metadata.MetadataSchemaGuid = Guid.NewGuid();
      metadata.MetadataXml = XDocument.Parse("<Annotations><Annotation><Identifier>00000000-0000-0000-0000-000000000011</Identifier><NotInDefinition>Garbage</NotInDefinition></Annotation></Annotations>");
      Context.Repository = new EzInMemoryRepository();
      Context.Repository.Definition.SetDataDefinition(new DataDefinition
        {
          Identifier = metadata.MetadataSchemaGuid.ToString(), 
          Type = DefinitionType.Annotation,
          Fields = new []{new FieldDefinition{Id = "Identifier", Type = "String"}}
        });

      var results = new PersistentAssetRepository(null, null, new Mock<IEzUserGetter>().Object).ExtractAnnotations(metadata).First();

      Assert.That(results["Identifier"].ToString(), Is.EqualTo("00000000-0000-0000-0000-000000000011"));
    }
  }
}