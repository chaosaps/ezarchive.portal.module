﻿using System.Linq;
using EZArchive.Portal.Module.Core.Configuration;
using NUnit.Framework;

namespace EZArchive.Portal.Module.Test.Core
{
  [TestFixture, Category("unittest"), Parallelizable(ParallelScope.Fixtures)]
  public class ConfigTest
  {
    [Test]
    public void SearchFields_GivenNoneSpecified_ReturnEmptyList()
    {
      var config = new Config();

      var result = config.SearchDefinition;

      Assert.That(result, Is.Empty);
    }

    [Test]
    public void SearchFields_GivenOneSpecified_ReturnListOfOne()
    {
      var config = Make_Config();

      var result = config.SearchDefinition;

      Assert.That(result.Count(), Is.EqualTo(1));
      Assert.That(result.First().IsSortable, Is.False);
    }

    private static Config Make_Config()
    {
      return new Config
        {
          SearchDefinition = new []
            {
              new SearchFieldDefinition()
                {
                  DisplayName = "dn", 
                  Ids = new[] {"ddt.f1"}
                }
            }
        };
    }
  }
}
