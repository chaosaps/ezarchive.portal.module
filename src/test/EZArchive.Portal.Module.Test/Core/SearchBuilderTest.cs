﻿using System.Collections.Generic;
using System.Linq;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Core.Configuration;
using EZArchive.Portal.Module.Data.InMemory;
using NUnit.Framework;

namespace EZArchive.Portal.Module.Test.Core
{
  [TestFixture, Category("unittest")]
  public class SearchBuilderTest
  {
    private SearchBuilder _builder;

    [SetUp]
    public void SetUp()
    {
      Context.Repository = new EzInMemoryRepository();
      Context.Config = new Config
        {
          SearchDefinition = new[]
            {
              new SearchFieldDefinition
                {
                  DisplayName = "DN1",
                  Ids = new[] {"a b.f1"}
                },
              new SearchFieldDefinition
                {
                  DisplayName = "DN2",
                  Ids = new[] {"a b.f2"}
                },
              new SearchFieldDefinition
                {
                  DisplayName = "DN3",
                  Ids = new[] {"a b.f3"},
                  IsVisible = false
                }
            }
        };
      Context.Repository.Definition.SetDataDefinition(new DataDefinition
        {
          Name = "a b",
          Fields = new[]
            {
              new FieldDefinition {Id = "f1", Type = "Integer"},
              new FieldDefinition {Id = "f2", Type = "String"},
              new FieldDefinition {Id = "f3", Type = "String"}
            }
        });

      _builder = new SearchBuilder();
    }

    [Test]
    public void Build_GivenAssetWithNoField_ReturnNoSearchFields()
    {
      var asset = new Asset();

      var result = _builder.Build(asset);

      Assert.That(result.Fields.Count, Is.EqualTo(0));
    }

    [Test]
    public void Build_GivenFieldIsFoundInSearchFieldsConfig_ReturnSearchResultWithTheField()
    {
      var asset = new Asset
        {
          Data = new List<AssetData>
            {
              new AssetData(new DataDefinition{Name = "a b"})
                {
                  Fields = new Dictionary<string, Value>() {{"f1", new StringValue("v1")}}
                }
            }
        };

      var result = _builder.Build(asset);

      Assert.That(result.Fields.First(f => f.Key == "DN1").Value, Is.EqualTo("v1"));
    }
  }
}