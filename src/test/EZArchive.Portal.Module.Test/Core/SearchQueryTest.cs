﻿using System.Collections.Generic;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Core.Configuration;
using EZArchive.Portal.Module.Core.Exception;
using EZArchive.Portal.Module.Data.InMemory;
using NUnit.Framework;

namespace EZArchive.Portal.Module.Test.Core
{
	[TestFixture, Category("unittest")]
  public class SearchQueryTest
  {
    [Test]
    public void Parse_GivenNUll_AllSearch()
    {
      var q = SearchQuery.Parse(null);

      Assert.That(q.ToString(), Is.EqualTo("*:*"));
    }

    [Test]
    public void Parse_GivenAllQuery_AllSearch()
    {
      var q = SearchQuery.AllQuery();

      Assert.That(q.ToString(), Is.EqualTo("*:*"));
    }

    [Test]
    public void Parse_GivenEmptyString_AllSearch()
    {
      var q = SearchQuery.Parse("");

      Assert.That(q.ToString(), Is.EqualTo("*:*"));
    }

    [Test]
    public void Parse_GivenQueryWithNoColon_QueryDefaultField()
    {
      var q = SearchQuery.Parse("value");

      Assert.That(q.ToString(), Is.EqualTo("value"));
    }
	  
	  [Test]
	  public void Parse_GivenQueryWithPlus_EscapePlus()
	  {
		  var q = SearchQuery.Parse("+value");

		  Assert.That(q.ToString(), Is.EqualTo("%2Bvalue"));
	  }

    [Test]
    public void Parse_GivenKeyButNoValue_Throw()
    {
      
	    void Act() => SearchQuery.Parse("key:");
      
	    Assert.That(Act, Throws.TypeOf<SearchQueryParserException>());
    }

    [Test]
    public void Parse_GivenKeyButNoValueWithSpaces_Throw()
    {
	    void Act() => SearchQuery.Parse(" key:    ");
      
	    Assert.That(Act, Throws.TypeOf<SearchQueryParserException>());
    }

    [Test]
    public void Parse_GivenLabel_Throw()
    {
      var q = SearchQuery.Parse("{Label}:EXACT p1.l1");

			Assert.That(q.ToString(), Is.EqualTo("sm_labels:\"p1.l1\""));
    }

		[Test]
		public void Parse_GivenOrList_Throw()
		{
			var q = SearchQuery.Parse("{Projects}:OR 1 2 3");

			Assert.That(q.ToString(), Is.EqualTo("sm_projectids:(1 OR 2 OR 3)"));
		}

		[Test]
    public void Parse_GivenBetweenDateRange_ReturnQueryContainingDateRange()
    {
      Context.Repository = new EzInMemoryRepository();
      Context.Repository.Definition.SetDataDefinition(new DataDefinition
      {
        Name = "DD",
        Fields = new List<FieldDefinition>
                {
                  new FieldDefinition{Id = "F1", Type = "Datetime"}
                }
      });

      var q = SearchQuery.Parse("DD.F1:BETWEEN 2000-01-01T00:00:00Z 2001-12-31T23:59:59.999Z");

      Assert.That(q.ToString(), Is.EqualTo("d_dd_f1:[2000-01-01T00:00:00Z TO 2001-12-31T23:59:59.999Z]"));
    }

		[Test]
		public void AddQuery_GivenMulipleAdds_CombineWithAND()
		{
			var q = SearchQuery.Parse("{Tags}:v1");
			q.AndQuery("{Label}:EXACT v2");
			q.AndQuery("{Label}:EXACT v3");

			Assert.That(q.ToString(), Is.EqualTo("(sm_tags:v1)AND(sm_labels:\"v2\")AND(sm_labels:\"v3\")"));
		}
	  
	  [Test]
	  public void AddQuery_GivenSingleAddQuery_CombineWithAND()
	  {
		  var q = SearchQuery.Parse("AND {Tags}:v1 {Label}:EXACT v2 {Label}:EXACT v3");

		  Assert.That(q.ToString(), Is.EqualTo("(sm_tags:v1)AND(sm_labels:\"v2\")AND(sm_labels:\"v3\")"));
	  }
	  
	  [Test]
	  public void AddQuery_GivenSingleAddQueryWithDefault_CombineWithAND()
	  {
		  var q = SearchQuery.Parse("AND {Document}:v1 v1 {Document}.TypeId:25 {Label}:EXACT v3");

		  Assert.That(q.ToString(), Is.EqualTo("(v1 v1)AND(s_document_typeid:25)AND(sm_labels:\"v3\")"));
	  }
	  
	  [Test]
	  public void AddQuery_GivenBetweenTwoDateFields_CreateQueryWithTwoRanges()
	  {
		  Context.Repository = new EzInMemoryRepository();
		  Context.Repository.Definition.SetDataDefinition(new DataDefinition
		  {
			  Name = "DD",
			  Fields = new List<FieldDefinition>
			  {
				  new FieldDefinition{Id = "F1", Type = "Datetime"},
				  new FieldDefinition{Id = "F2", Type = "Datetime"}
			  }
		  });
		  var q = SearchQuery.Parse("AND DD.F1:BETWEEN * 2018-06-06T00:00:00Z DD.F2:BETWEEN 2018-06-06T00:00:00Z *");

		  Assert.That(q.ToString(), Is.EqualTo("(d_dd_f1:[* TO 2018-06-06T00:00:00Z])AND(d_dd_f2:[2018-06-06T00:00:00Z TO *])"));
	  }
	}
}
