﻿using System;
using System.Collections.Generic;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Core.UseCasees;
using NUnit.Framework;

namespace EZArchive.Portal.Module.Test.Core.UseCases
{
	[TestFixture, Category("unittest")]
	public class UserLogsInUseCaseTest : TestBase
	{
		private EzUser _user;

		private Guid UserId
		{
			get { return Guid.Parse(_user.Identifier); }
		}

		[SetUp]
		public void SetUp()
		{
			_user = new EzUser {Identifier = "00000000-0000-0000-0000-000000000001", Permission = "Contributor"};
		}

		[Test]
		public void EnsureUserHasARole_GivenContributor_DontChangePermission()
		{
			Context.Repository.EzUser.Set(_user);

			new UserLogsInUseCase().EnsureUserHasARole(UserId);

			var actual = Context.Repository.EzUser.Get(UserId);
			Assert.That(actual.Permission, Is.EqualTo(_user.Permission));
		}

		[Test]
		public void EnsureUserHasARole_GivenNoPermission_ChangeToUser()
		{
			_user.Permission = null;
			Context.Repository.EzUser.Set(_user);

			new UserLogsInUseCase().EnsureUserHasARole(UserId);

			var actual = Context.Repository.EzUser.Get(UserId);
			Assert.That(actual.Permission, Is.EqualTo("Contributor"));
		}

		[Test]
		public void EnsureUserHasWayfAttributes_GivenAttributes_SetOnUser()
		{
			_user.Permission = null;
			Context.Repository.EzUser.Set(_user);

			new UserLogsInUseCase().EnsureUserHasWayfAttributes(UserId, new Dictionary<string, IList<string>> { { "eduPersonTargetedID", new List<string> { "1" } } });

			var actual = Context.Repository.EzUser.Get(UserId);
			Assert.That(actual.WayfAttributes, Is.EqualTo("{\"eduPersonTargetedID\":[\"1\"]}"));
		}
	}
}