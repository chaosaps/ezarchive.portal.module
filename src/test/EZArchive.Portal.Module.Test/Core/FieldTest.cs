using EZArchive.Portal.Module.Core.Configuration;
using NUnit.Framework;

namespace EZArchive.Portal.Module.Test.Core
{
  [TestFixture, Category("unittest"), Parallelizable(ParallelScope.Fixtures)]
  public class FieldTest
  {
    [Test]
    public void DisplayName_GivenDisplayNameNotSet_ReturnSameValueAsId()
    {
      var field = new FieldDefinition {Id = "Field.Id"};

      var result = field.DisplayName;

      Assert.That(result, Is.EqualTo(field.Id));
    }

    [Test]
    public void DisplayName_Set_CanSetDisplayNameIndependantOfId()
    {
      var field = new FieldDefinition {Id = "Field.Id", DisplayName = "DisplayName"};

      var result = field.DisplayName;

      Assert.That(result, Is.Not.EqualTo(field.Id));
      Assert.That(result, Is.EqualTo("DisplayName"));
    }
  }
}