﻿using EZArchive.Portal.Module.Core.Configuration;
using NUnit.Framework;

namespace EZArchive.Portal.Module.Test.Core
{
	[TestFixture, Category("unittest"), Parallelizable(ParallelScope.Fixtures)]
	public class DateDefinitionTest
	{
		[Test]
		public void CanEdit_GivenNoRole_False()
		{
			var dd = new DataDefinition();

			var result = dd.CanEdit(null);

			Assert.That(result, Is.False);
		}

		[Test]
		public void CanEdit_GivenRoleNotAllowed_False()
		{
			var dd = new DataDefinition
			{
				CanWrite = new[] { "Administrator" }
			};

			var result = dd.CanEdit("not allowed");

			Assert.That(result, Is.False);
		}

		[Test]
		public void CanEdit_GivenRoleThatIsAllowed_True()
		{
			var dd = new DataDefinition
			{
				CanWrite = new[] { "Administrator" }
			};

			var result = dd.CanEdit("Administrator");

			Assert.That(result, Is.True);
		}

		[Test]
		public void CanEdit_GivenRoleWithMultipleAllowed_True()
		{
			var dd = new DataDefinition
			{
				CanWrite = new[] { "Administrator", "Contributor" }
			};

			var result = dd.CanEdit("Contributor");

			Assert.That(result, Is.True);
		}

		[Test]
		public void CanEdit_DefaultAllowAccess_True()
		{
			var dd = new DataDefinition();

			var result = dd.CanEdit("Administrator");

			Assert.That(result, Is.True);
		}
	}
}