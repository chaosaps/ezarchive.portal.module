﻿using System;
using System.Collections.Generic;
using System.Linq;
using Chaos.Portal.Core.Exceptions;
using EZArchive.Portal.Module.Api.Endpoints;
using EZArchive.Portal.Module.Api.Results;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Data.Exceptions;
using NUnit.Framework;

namespace EZArchive.Portal.Module.Test.Api.Endpoints
{
  [TestFixture, Category("unittest")]
  public class AnnotationExtensionTest : ExtensionTestBase
  {
    private AnnotationExtension _extension;

    [SetUp]
    public void Setup()
    {
      _extension = (AnnotationExtension) new AnnotationExtension(_portal.Object).WithPortalRequest(_request.Object);
    }

    [Test]
    public void Set_UserNotLoggedIn_Throw()
    {
      SetupAnonymousUser();

      void Act() => _extension.Set(Guid.Empty, Guid.Empty, null);
      
      Assert.That(Act, Throws.TypeOf<InsufficientPermissionsException>().And.Message.EqualTo("User not logged in"));
    }

    [Test]
    public void Set_UserLoggedInButNotAdministrator_Throw()
    {
      SetupNormalUser();

      void Act() => _extension.Set(Guid.Empty, Guid.Empty, null);
      
      Assert.That(Act, Throws.TypeOf<InsufficientPermissionsException>());
    }


    [Test]
    public void Set_AssetDoesntExist_ReturnFailure()
    {
      SetupAdministratorUser();

      void Act() => _extension.Set(Guid.Empty, Guid.Empty, new Dictionary<string, Value>());
      
      Assert.That(Act, Throws.TypeOf<DataNotFoundException>());
    }

    [Test]
    public void Set_AssetExists_UpdateAssetWithNewAnnotation()
    {
			var asset = TestResources.Make_VideoAsset(DataDefinition, SetupAdministratorUser());
      Context.Repository.Asset.Set(asset);
      var annotation = new Dictionary<string, Value>
      { { "StartTime", new StringValue("00:00:00") },
        { "EndTime", new StringValue("01:00:00") },
        { "Body", new StringValue("some text") }
      };

      var result = _extension.Set(Guid.Parse(asset.Identifier), Guid.Empty, annotation, AnnotationVisibility.Public);

      Assert.That(result.Identifier, Is.Not.Null);
      var a = Context.Repository.Asset.Get(Guid.Parse(asset.Identifier));
      var an = a.Annotations.First().Annotations.Last();
      Assert.That(an["StartTime"].ToString(), Is.EqualTo(annotation["StartTime"].ToString()));
      Assert.That(an["EndTime"].ToString(), Is.EqualTo(annotation["EndTime"].ToString()));
      Assert.That(an["EndTime"].ToString(), Is.EqualTo(annotation["EndTime"].ToString()));
      Assert.That(an["__Visibility"].ToString(), Is.EqualTo("Public"));
    }

    [Test]
    public void Delete_AnnotationDoesntExist_ReturnFailure()
    {
      SetupAdministratorUser();

      var result = _extension.Delete(Guid.Empty, Guid.Empty);

      Assert.That(result.WasSuccess, Is.False);
    }

    [Test]
    public void Delete_AnnotationExists_RemoveFromAsset()
    {
			var asset = TestResources.Make_VideoAsset(DataDefinition, SetupAdministratorUser());
      var annotation = asset.Annotations.First().Annotations.First();
      Context.Repository.Asset.Set(asset);

      var result = _extension.Delete(Guid.Parse(asset.Identifier), Guid.Parse(annotation["Identifier"].ToString()));

      Assert.That(result.WasSuccess, Is.True);
      var a = Context.Repository.Asset.Get(Guid.Parse(asset.Identifier));
      Assert.That(a.Annotations.First().Annotations.First()["Identifier"].ToString(), Is.EqualTo("00000000-0000-0000-0000-000000000002"));
      Assert.That(a.Annotations.Count, Is.EqualTo(1));
    }

    [Test]
    public void SetPermission_UserIsNotOwner_Throw()
    {
      SetupUserUser();
      var asset = TestResources.Make_VideoAsset(DataDefinition, TestResources.Make_EzUserAdmin());
      var annotation = asset.Annotations.First().Annotations.First();
      Context.Repository.Asset.Set(asset);
      
      void Act() => _extension.SetPermission(Guid.Parse(asset.Identifier), annotation["Identifier"].ToString(), (uint) AnnotationPermission.User);
      
      Assert.That(Act, Throws.TypeOf<InsufficientPermissionsException>());
    }
    
    [Test]
    public void SetPermission_UserIsOwner_ScopeIsChanged()
    {
      var user = SetupUserUser();
      var asset = TestResources.Make_VideoAsset(DataDefinition, user);
      var annotation = asset.Annotations.First().Annotations.First();
      Context.Repository.Asset.Set(asset);
      
      var result = _extension.SetPermission(Guid.Parse(asset.Identifier), annotation["Identifier"].ToString(), (uint) AnnotationPermission.User);

      Assert.That(result.WasSuccess, Is.True);
      var actualAsset = Context.Repository.Asset.Get(Guid.Parse(asset.Identifier));
      var actualAnnotation = actualAsset.Annotations.SelectMany(ag => ag.Annotations).Single(a => a["Identifier"].ToString() == annotation["Identifier"].ToString());
      Assert.That(actualAnnotation["__Permission"].ToString(), Is.EqualTo("0"));
    }
  }
}