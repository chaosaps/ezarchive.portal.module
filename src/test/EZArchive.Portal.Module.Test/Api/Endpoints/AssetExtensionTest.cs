﻿using System;
using System.Collections.Generic;
using System.Linq;
using Chaos.Portal.Core.Data.Model;
using Chaos.Portal.Core.Exceptions;
using Chaos.Portal.Core.Logging.Database;
using Chaos.Portal.Module.Larmfm.Domain;
using EZArchive.Portal.Module.Api.Endpoints;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Data.Exceptions;
using Moq;
using NUnit.Framework;

namespace EZArchive.Portal.Module.Test.Api.Endpoints
{
  [TestFixture, Category("unittest")]
  public class AssetExtensionTest : ExtensionTestBase
  {
	  private AssetExtension Make_AssetExtension()
    {
      return (AssetExtension) new AssetExtension(_portal.Object, new Mock<IStorage>().Object).WithPortalRequest(_request.Object);
    }

    [Test]
    public void Delete_AssetNotThere_ReturnFailure()
    {
      SetupAdministratorUser();
      var extension = Make_AssetExtension();

      var result = extension.Delete(Guid.Empty);

      Assert.That(result.WasSuccess, Is.False);
    }

    [Test]
    public void Delete_AssetDoesExist_ReturSuccess()
    {
      var extension = Make_AssetExtension();
			var asset = TestResources.Make_VideoAsset(DataDefinition, SetupAdministratorUser());
      Context.Repository.Asset.Set(asset);

      var result = extension.Delete(Guid.Parse(asset.Identifier));

      Assert.That(result.WasSuccess, Is.True);
    }

    [Test]
    public void Delete_GivenContributor_ReturnSuccess()
    {
      var extension = Make_AssetExtension();
			var asset = TestResources.Make_VideoAsset(DataDefinition, SetupContributorUser());
      Context.Repository.Asset.Set(asset);

      var result = extension.Delete(Guid.Parse(asset.Identifier));

      Assert.That(result.WasSuccess, Is.True);
    }

    [Test]
    public void Delete_GivenUserPermission_Throw()
    {
      var extension = Make_AssetExtension();
      SetupUserUser();

      void Act() => extension.Delete(Guid.Empty);
      
      Assert.That(Act, Throws.TypeOf<InsufficientPermissionsException>());
    }
  }
}