﻿using System.Linq;
using EZArchive.Portal.Module.Api.Endpoints;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Core.Configuration;
using EZArchive.Portal.Module.Data.InMemory;
using NUnit.Framework;

namespace EZArchive.Portal.Module.Test.Api.Endpoints
{
  [TestFixture, Category("unittest")]
  public class AnnotationDefinitionTest : ExtensionTestBase
  {
    [Test]
    public void Get_GivenNoDefinitions_ReturnEmptyList()
    {
      var ext = Make_AnnotationDefinitionEndpoint();

      var result = ext.Get();

      Assert.That(result, Is.Empty);
    }

    [Test]
    public void Get_OneDefinition_ReturnListOfOne()
    {
      var ext = Make_AnnotationDefinitionEndpoint();
      var repository = (DefinitionRepository) Context.Repository.Definition;
      repository.SetDataDefinition(new DataDefinition{Type = DefinitionType.Annotation});

      var result = ext.Get();

      Assert.That(result.Count(), Is.EqualTo(1));
    }

    private AnnotationDefinition Make_AnnotationDefinitionEndpoint()
    {
      return (AnnotationDefinition) new AnnotationDefinition(_portal.Object).WithPortalRequest(_request.Object);
    }
  }
}
