﻿using System;
using System.Collections.Generic;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Core.Configuration;

public class TestResources
{
  public static Asset Make_VideoAsset(DataDefinition dd, EzUser owner)
	{
    return new Asset
      {
        Identifier = "10000000-0000-0000-0000-000000000001",
        TypeId = 1,
        Data = new List<AssetData>
          {
            new AssetData(dd)
              {
                Fields = new Dictionary<string, Value>
                  {
                    {"Title", new StringValue("Yo mama")},
                    {"Pet", new StringValue("Dinosaurs")},
                    {"Films", new StringValue("Meet the feebles,Avatar,Casablanca")}
                  }
              }
          },
        Files = new[]
          {
            new Asset.FileReference
              {
                Identifier = "1",
                Type = "Video",
                Destinations = new[]
                  {
                    new Asset.FileReference.Destination
                      {
                        Type = "http",
                        Url = "https://s3-eu-west-1.amazonaws.com/arc-ftvl/Gollu_Aur_Pappu&session={SESSION_GUID_MISSING}&wayf={WAYF_ATTRIBUTES}.mp4"
                      },
                    new Asset.FileReference.Destination
                      {
                        Type = "http",
                        Url = "https://s3-eu-west-1.amazonaws.com/arc-ftvl/Gollu_Aur_Pappu.webm"
                      }
                  }
              }
          },
        Annotations = new List<Asset.AnnotationGroup>
          {
            new Asset.AnnotationGroup
              {
                Annotations = new List<IDictionary<string, Value>>
                  {
                    new Dictionary<string, Value>
                      {
                        {"__OwnerId", new StringValue(owner.Identifier)},
                        {"__Visibility", new StringValue("Private")},
                        {"Identifier", new StringValue("00000000-0000-0000-0000-000000000001")},
                        {"Field", new StringValue("Value")}
                      },
                    new Dictionary<string, Value>
                      {
                        {"__OwnerId", new StringValue(owner.Identifier)},
                        {"__Visibility", new StringValue("Public")},
                        {"Identifier", new StringValue("00000000-0000-0000-0000-000000000002")},
                        {"Field", new StringValue("Value2")}
                      }
                  }
              }
          },
        Tags = new[]
          {
            "tag 666"
          }
      };
  }

  public static EzUser Make_EzUserAdmin()
  {
    return new EzUser
      {
        Identifier = "00000000-0000-0000-0000-000000000001",
        Email = "me@example.com",
        Name = "John Doe",
        Permission = "Administrator"
      };
  }

  public static EzUser Make_EzUserUser()
  {
    return new EzUser
      {
        Identifier = "00000000-0000-0000-0000-000000000002",
        Email = "other@example.com",
        Name = "Jane Doe",
        Permission = "User"
      };
  }

  public static EzUser Make_EzContributorUser()
  {
    return new EzUser
      {
        Identifier = "00000000-0000-0000-0000-000000000003",
        Email = "contributor@example.com",
        Name = "Jimbo",
        Permission = "Contributor"
      };
  }

  public static Asset Make_VideoAssetWithNoFiles(DataDefinition dd)
  {
    return new Asset
      {
        Identifier = "20000000-0000-0000-0000-000000000002",
        TypeId = 1,
        Data = new List<AssetData>
          {
            new AssetData(dd)
              {
                Fields = new Dictionary<string, Value>
                  {
                    {"Title", new StringValue("Yo mama")},
                    {"Pet", new StringValue("Dinosaurs")},
                    {"Films", new StringValue("Meet the feebles,Avatar,Casablanca")}
                  }
              }
          }
      };
  }
}