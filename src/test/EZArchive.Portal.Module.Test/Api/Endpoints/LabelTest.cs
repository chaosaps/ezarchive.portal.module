﻿using System;
using System.Collections.Generic;
using Chaos.Portal.Core.Exceptions;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Core.Gateway;
using Moq;
using NUnit.Framework;
using Label = EZArchive.Portal.Module.Api.Endpoints.Label;

namespace EZArchive.Portal.Module.Test.Api.Endpoints
{
	[TestFixture, Category("unittest")]
	public class LabelTest : ExtensionTestBase
	{
		private Label Make_LabelExtension()
		{
			return (Label) new Label(_portal.Object).WithPortalRequest(_request.Object);
		}

		[Test]
		public void DisassociateWith_GivenInvalidLabelId_Throw()
		{
			var ext = Make_LabelExtension();

			void Act() => ext.DisassociateWith("missing id", "missing id");

			Assert.That(Act, Throws.TypeOf<InsufficientPermissionsException>());
		}

		[Test]
		public void DisassociateWith_GivenValidIds_SetAssociation()
		{
			var ext = Make_LabelExtension();
			SetupUserUser();
			var labelSpy = new LabelRepositorySpy();
			Context.Repository.Label = labelSpy;
			Context.Repository.Project.Set(new Project
			{
				Identifier = "p1",
				Users = new[] {TestResources.Make_EzUserUser()},
				Labels = new[] {new Module.Core.Label {Identifier = "l1"}}
			});
			Context.Repository.Asset.Set(new Asset {Identifier = "00000000-0000-0000-0000-000000000001"});

			var result = ext.DisassociateWith("l1", "00000000-0000-0000-0000-000000000001");

			Assert.That(result.WasSuccess, Is.True);
			Assert.That(labelSpy.LabelId, Is.EqualTo("l1"));
			Assert.That(labelSpy.AssetId, Is.EqualTo("00000000-0000-0000-0000-000000000001"));
			_viewManager.Verify(m => m.Index(It.IsAny<object>()));
		}
	}

	public class LabelRepositorySpy : ILabelRepository
	{
		public string LabelId;
		public string AssetId;

		public void SetAssociation(string id, string assetId)
		{
			AssetId = assetId;
			LabelId = id;
		}

		public void DeleteAssociation(string id, string assetId)
		{
			AssetId = assetId;
			LabelId = id;
		}

		public IEnumerable<Module.Core.Label> Get(string assetId)
		{
			throw new NotImplementedException();
		}

		public bool Delete(string id)
		{
			throw new NotImplementedException();
		}
	}
}