﻿using System;
using System.IO;
using System.Linq;
using Chaos.Portal.Core.Exceptions;
using Chaos.Portal.Module.Larmfm.Domain;
using EZArchive.Portal.Module.Api.Endpoints;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Core.Configuration;
using EZArchive.Portal.Module.Data.Exceptions;
using Moq;
using NUnit.Framework;
using FileStream = Chaos.Portal.Core.Request.FileStream;

namespace EZArchive.Portal.Module.Test.Api.Endpoints
{
	[TestFixture, Category("unittest")]
  public class EzFileExtensionTest : ExtensionTestBase
  {
    private EzFileExtension sut;
    private Mock<IStorage> _storage;
    private Mock<ITranscoder> _transcoder;

    [SetUp]
    public void SetUp()
    {
      _storage = new Mock<IStorage>();
      _transcoder = new Mock<ITranscoder>();
      sut = (EzFileExtension)new EzFileExtension(_portal.Object, _storage.Object, _transcoder.Object).WithPortalRequest(_request.Object);
    }

    [Test]
    public void Upload_AnonymousUser_Throw()
    {
      SetupAnonymousUser();

	    void Act() => sut.Upload(Guid.Empty);
      
			Assert.That(Act, Throws.TypeOf<InsufficientPermissionsException>().And.Message.EqualTo("User not logged in"));
    }

    [Test]
    public void Upload_NormalUser_Throw()
    {
      SetupNormalUser();

	    void Act() => sut.Upload(Guid.Empty);
      
		  Assert.That(Act, Throws.TypeOf<InsufficientPermissionsException>());
    }

    [Test]
    public void Upload_FileNotPresent_Throw()
    {
      SetupAdministratorUser();

	    void Act() => sut.Upload(Guid.Empty);
      
		  Assert.That(Act, Throws.TypeOf<IOException>());
    }

    [Test]
    public void Upload_GivenGenericFile_WriteSourceToStorage()
    {
      var asset = TestResources.Make_VideoAssetWithNoFiles(DataDefinition);
      Context.Repository.Asset.Set(asset);
      SetupAdministratorUser();
      _request.Setup(m => m.Files).Returns(new[] {new FileStream(null, "", null, 0)});

      var result = sut.Upload(new Guid(asset.Identifier), 4);

      Assert.That(result.WasSuccess, Is.True);
      _storage.Verify(m => m.Write(It.Is<string>(i => i.StartsWith("Objects/20000000-0000-0000-0000-000000000002/")), null, It.IsAny<string>()));
      var file = Context.Repository.Asset.Get(new Guid(asset.Identifier)).Files.First();
      Assert.That(file.Type, Is.EqualTo("Other Source"));
      Assert.That(file.Identifier, Is.EqualTo("1"));
      Assert.That(file.Destinations.First().Url, Is.SubPathOf("Objects/20000000-0000-0000-0000-000000000002/"));
    }
    
    [Test]
    public void Upload_GivenImageFile_WriteSourceToStorage()
    {
			var asset = TestResources.Make_VideoAssetWithNoFiles(DataDefinition);
      Context.Repository.Asset.Set(asset);
      SetupAdministratorUser();
      _request.Setup(m => m.Files).Returns(new[] {new FileStream(null, "something.jpg", null, 0)});

      var result = sut.Upload(new Guid(asset.Identifier), 3);

      Assert.That(result.WasSuccess, Is.True);
      _storage.Verify(m => m.Write(It.Is<string>(i => i.StartsWith("Objects/20000000-0000-0000-0000-000000000002/")), null, "image/jpeg"));
      var file = Context.Repository.Asset.Get(new Guid(asset.Identifier)).Files.First();
      Assert.That(file.Type, Is.EqualTo("Image Source"));
      Assert.That(file.Identifier, Is.EqualTo("1"));
      Assert.That(file.Destinations.First().Url, Is.SubPathOf("Objects/20000000-0000-0000-0000-000000000002/"));
    }
    
    [Test]
    public void Upload_GivenAudioFile_WriteSourceToStorageAndStartTranscodeJob()
    {
      Context.Config.Aws.AudioPresets = new []{new Preset{Extension = "mp4", FormatId = 11}};
			var asset = TestResources.Make_VideoAssetWithNoFiles(DataDefinition);
      Context.Repository.Asset.Set(asset);
      SetupAdministratorUser();
      _request.Setup(m => m.Files).Returns(new[] {new FileStream(null, null, null, 0)});

      var result = sut.Upload(new Guid(asset.Identifier), 2);

      Assert.That(result.WasSuccess, Is.True);
      _storage.Verify(m => m.Write(It.Is<string>(i => i.StartsWith("Objects/20000000-0000-0000-0000-000000000002/")), null, It.IsAny<string>()));
      _transcoder.Verify(m => m.Transcode(It.Is<string>(i => i.StartsWith("Objects/20000000-0000-0000-0000-000000000002/")), It.Is<string>(i => i.StartsWith("Objects/20000000-0000-0000-0000-000000000002/") && i.EndsWith(".mp4")), It.IsAny<string>()));
      var sourceFile = Context.Repository.Asset.Get(new Guid(asset.Identifier)).Files.First();
      Assert.That(sourceFile.Type, Is.EqualTo("Audio Source"));
      Assert.That(sourceFile.Identifier, Is.EqualTo("1"));
      Assert.That(sourceFile.Destinations.First().Url, Is.SupersetOf("Objects/20000000-0000-0000-0000-000000000002/"));
      var previewFile = Context.Repository.Asset.Get(new Guid(asset.Identifier)).Files.Last();
      Assert.That(previewFile.Type, Is.EqualTo("Audio Preview"));
      Assert.That(previewFile.Identifier, Is.EqualTo("2"));
      Assert.That(previewFile.Destinations.First().Url, Is.SupersetOf("Objects/20000000-0000-0000-0000-000000000002/"));

    }
    
    [Test]
    public void Upload_GivenVideoFile_WriteSourceToStorageAndStartTranscodeJob()
    {
      Context.Config.Aws.VideoPresets = new []{new Preset{Extension = "mp4", FormatId = 7}, new Preset{Extension = "webm", FormatId = 7}};
			var asset = TestResources.Make_VideoAssetWithNoFiles(DataDefinition);
      Context.Repository.Asset.Set(asset);
      SetupAdministratorUser();
      _request.Setup(m => m.Files).Returns(new[] {new FileStream(null, null, null, 0)});

      var result = sut.Upload(new Guid(asset.Identifier), 1);

      Assert.That(result.WasSuccess, Is.True);
      _storage.Verify(m => m.Write(It.Is<string>(i => i.StartsWith("Objects/20000000-0000-0000-0000-000000000002/")), null, It.IsAny<string>()));
      _transcoder.Verify(m => m.Transcode(It.Is<string>(i => i.StartsWith("Objects/20000000-0000-0000-0000-000000000002/")), It.Is<string>(i => i.StartsWith("Objects/20000000-0000-0000-0000-000000000002/") && i.EndsWith(".mp4")), It.IsAny<string>()));
      _transcoder.Verify(m => m.Transcode(It.Is<string>(i => i.StartsWith("Objects/20000000-0000-0000-0000-000000000002/")), It.Is<string>(i => i.StartsWith("Objects/20000000-0000-0000-0000-000000000002/") && i.EndsWith(".webm")), It.IsAny<string>()));
      var files = Context.Repository.Asset.Get(new Guid(asset.Identifier)).Files.ToList();
      var sourceFile = files[0];
      Assert.That(sourceFile.Type, Is.EqualTo("Video Source"));
      Assert.That(sourceFile.Identifier, Is.EqualTo("1"));
      Assert.That(sourceFile.Destinations.First().Url, Is.SupersetOf("Objects/20000000-0000-0000-0000-000000000002/"));
      var preview1File = files[1];
      Assert.That(preview1File.Type, Is.EqualTo("Video Preview"));
      Assert.That(preview1File.Identifier, Is.EqualTo("2"));
      Assert.That(preview1File.Destinations.First().Url, Is.SupersetOf("Objects/20000000-0000-0000-0000-000000000002/"));
      var preview2File = files[2];
      Assert.That(preview2File.Type, Is.EqualTo("Video Preview"));
      Assert.That(preview2File.Identifier, Is.EqualTo("3"));
      Assert.That(preview2File.Destinations.First().Url, Is.SupersetOf("Objects/20000000-0000-0000-0000-000000000002/"));

    }

    [Test]
    public void Delete_AnonymousUser_Throw()
    {
      SetupAnonymousUser();

	    void Act() => sut.Delete(0);
      
	    Assert.That(Act, Throws.TypeOf<InsufficientPermissionsException>().And.Message.EqualTo("User not logged in"));
    }

    [Test]
    public void Delete_NormalUser_Throw()
    {
      SetupNormalUser();

	    void Act() => sut.Delete(0);
      
	    Assert.That(Act, Throws.TypeOf<InsufficientPermissionsException>());
    }

		[Test]
		public void Set_AnonymousUser_Throw()
		{
			SetupAnonymousUser();

			void Act() => sut.Set(0, "newname");
      
			Assert.That(Act, Throws.TypeOf<InsufficientPermissionsException>().And.Message.EqualTo("User not logged in"));
		}

		[Test]
		public void Set_NormalUser_Throw()
		{
			SetupNormalUser();

			void Act() => sut.Set(0, "newname");
      
			Assert.That(Act, Throws.TypeOf<InsufficientPermissionsException>());
		}

		[Test]
		public void Set_GivenFileDoesntExists_Throw()
		{
			SetupContributorUser();

			void Act() => sut.Set(0, "newname");
      
			Assert.That(Act, Throws.TypeOf<DataNotFoundException>());
		}
	}
}