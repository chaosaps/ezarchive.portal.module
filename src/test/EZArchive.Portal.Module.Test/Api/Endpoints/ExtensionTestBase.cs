﻿using System;
using Chaos.Portal.Core;
using Chaos.Portal.Core.Data.Model;
using Chaos.Portal.Core.Indexing.View;
using Chaos.Portal.Core.Request;
using EZArchive.Portal.Module.Core;
using Moq;
using NUnit.Framework;

namespace EZArchive.Portal.Module.Test.Api.Endpoints
{
  public class ExtensionTestBase : TestBase
  {
    [SetUp]
    public void BaseExtensionSetup()
    {
      _portal = new Mock<IPortalApplication>();
      _request = new Mock<IPortalRequest>();
			_viewManager = new Mock<IViewManager>();
	    _portal.Setup(p => p.ViewManager).Returns(_viewManager.Object);
    }

    protected Mock<IPortalApplication> _portal;
    protected Mock<IPortalRequest> _request;
	  protected Mock<IViewManager> _viewManager;

	  protected EzUser SetupNormalUser()
    {
		  var user = TestResources.Make_EzUserUser();
		  _request.Setup(p => p.User).Returns(new UserInfo { Guid = new Guid(user.Identifier) });
      Context.Repository.EzUser.Set(user);

		  return user;
    }

    protected void SetupAnonymousUser()
    {
      _request.Setup(p => p.IsAnonymousUser).Returns(true);
    }

    protected EzUser SetupAdministratorUser()
    {
      _request.Setup(p => p.User).Returns(new UserInfo {Guid = new Guid(TestResources.Make_EzUserAdmin().Identifier)});
      return Context.Repository.EzUser.Set(TestResources.Make_EzUserAdmin());
    }
    
    protected EzUser SetupContributorUser()
    {
      _request.Setup(p => p.User).Returns(new UserInfo { Guid = new Guid(TestResources.Make_EzContributorUser().Identifier) });
      return Context.Repository.EzUser.Set(TestResources.Make_EzContributorUser());
    }
    
    protected EzUser SetupUserUser()
    {
      _request.Setup(p => p.User).Returns(new UserInfo {Guid = new Guid(TestResources.Make_EzUserUser().Identifier)});
      return Context.Repository.EzUser.Set(TestResources.Make_EzUserUser());
    }
  }
}