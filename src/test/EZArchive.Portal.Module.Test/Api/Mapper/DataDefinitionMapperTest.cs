﻿using System.Linq;
using EZArchive.Portal.Module.Api.Mapper;
using EZArchive.Portal.Module.Core.Configuration;
using NUnit.Framework;

namespace EZArchive.Portal.Module.Test.Api.Mapper
{
  [TestFixture, Category("unittest")]
  public class DataDefinitionMapperTest
  {
    [Test]
    public void Map_GivenDataDefinition_ReturnResult()
    {
      var dd = new DataDefinition
        {
          Id = "00000000-0000-0000-0000-000000000001",
          Name = "name",
          Description = "description",
					TypeId = 1,
          Fields = new []{new FieldDefinition
            {
              Id = "id",
              DisplayName = "dn",
              IsRequired = true,
              Note = "note",
              Options = new string[]{"o1"},
              Placeholder = "placeholder",
              Type = "type"
            }}
        };

      var result = DataDefinitionMapper.Map(dd);

      Assert.That(result.Identifier, Is.EqualTo("00000000-0000-0000-0000-000000000001"));
      Assert.That(result.Name, Is.EqualTo("name"));
      Assert.That(result.Description, Is.EqualTo("description"));
			Assert.That(result.TypeId, Is.EqualTo("1"));
      Assert.That(result.Fields, Is.Not.Empty);
      var field = result.Fields.Values.First();
      Assert.That(field.DisplayName, Is.EqualTo("dn"));
      Assert.That(field.IsRequired, Is.True);
      Assert.That(field.Placeholder, Is.EqualTo("placeholder"));
      Assert.That(field.Options, Is.Not.Empty);
      Assert.That(field.Type, Is.EqualTo("type"));
    }
  }
}