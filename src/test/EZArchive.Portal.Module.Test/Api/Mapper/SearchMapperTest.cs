﻿using System;
using System.Collections.Generic;
using System.Linq;
using EZArchive.Portal.Module.Api.Mapper;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Core.Configuration;
using NUnit.Framework;

namespace EZArchive.Portal.Module.Test.Api.Mapper
{
  [TestFixture, Category("unittest")]
  public class SearchMapperTest
  {
    [Test]
    public void Map_GivenSearchDataWithFieldInvisble_DontMapField()
    {
      var s = new Search
        {
          Fields = new List<SearchField>()
            {
              new SearchField {Key = "DN1", Value = "V1"},
              new SearchField {Key = "DN2", Value = "V2"}
            }
        };
      Context.Config.SearchDefinition = new []
        {
          new SearchFieldDefinition { DisplayName = "DN1", IsVisible = true },
          new SearchFieldDefinition { DisplayName = "DN2", IsVisible = false }
        };

      var result = SearchMapper.Map(s);

      foreach (var searchFieldResult in result.Fields)
      {
        Console.WriteLine($"{searchFieldResult.Key}");
      }
      
      Assert.That(result.Fields.Any(item => item.Key == "DN1"), Is.True);
      Assert.That(result.Fields.Any(item => item.Key == "DN2"), Is.False);
    }
  }
}
