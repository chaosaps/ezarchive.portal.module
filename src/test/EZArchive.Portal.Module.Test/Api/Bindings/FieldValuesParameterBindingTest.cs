﻿using System;
using System.Collections.Generic;
using System.Linq;
using EZArchive.Portal.Module.Api.Bindings;
using EZArchive.Portal.Module.Core;
using NUnit.Framework;
using Newtonsoft.Json;

namespace EZArchive.Portal.Module.Test.Api.Bindings
{
  [TestFixture, Category("unittest"), Parallelizable(ParallelScope.Fixtures)]
  public class FieldValuesParameterBindingTest : TestBase
  {
    [SetUp]
    public void SetUp()
    {
      _method = fields => { };
    }

    private FieldValuesMethod _method;

    private delegate void FieldValuesMethod(IDictionary<string, Value> fields);

    [Test]
    public void Bind_GivenEmptyField_Throw()
    {
      var binding = new FieldValuesParameterBinding();
      var dict = new Dictionary<string, string> {{"fields", "{\"F\":\"\"}"}};

      var result = binding.Bind(dict, _method.Method.GetParameters()[0]) as IDictionary<string, Value>;

      Assert.That(result.Values.First().Type, Is.EqualTo("String"));
    }

    [Test]
    public void Bind_GivenNull_Throw()
    {
      var binding = new FieldValuesParameterBinding();
      var dict = new Dictionary<string, string> {{"fields", null}};

      void Act() => binding.Bind(dict, _method.Method.GetParameters()[0]);
      
      Assert.That(Act, Throws.TypeOf<NotImplementedException>());
    }

    [Test]
    public void CanConvert_IsValidType_True()
    {
      var conv = new ValueConverter();

      bool result = conv.CanConvert(typeof (Dictionary<string, Value>));

      Assert.That(result, Is.True);
    }

    [Test]
    public void DeserializeObject_GivenFieldWithArrayOfArrayWithMultipleColumns_ReturnTableValue()
    {
      string json = "{\"F\":[[\"Some Val 1.1\", \"Some Val 1.2\"],[\"Some Val 2.1\", \"Some Val 2.2\"]]}";

      var result = JsonConvert.DeserializeObject<Dictionary<string, Value>>(json).Values.First() as TableValue;

      Assert.That(result.Value.Count(), Is.EqualTo(2));
      Assert.That(result.Value.First().Count(), Is.EqualTo(2));
      Assert.That(result.Value.Last().Count(), Is.EqualTo(2));
    }

    [Test]
    public void DeserializeObject_GivenFieldWithArrayOfArray_ReturnTableValue()
    {
      string json = "{\"F\":[[\"Some Val 1\"],[\"Some Val 2\"]]}";

      var result = JsonConvert.DeserializeObject<Dictionary<string, Value>>(json).Values.First() as TableValue;

      Assert.That(result.Value.Count(), Is.EqualTo(2));
      Assert.That(result.Value.First().First(), Is.EqualTo("Some Val 1"));
    }

    [Test]
    public void DeserializeObject_GivenFieldWithEmptyString_ReturnStringValue()
    {
      string json = "{\"F\":\"\"}";
      var conv = new ValueConverter();

      var result = JsonConvert.DeserializeObject<Dictionary<string, Value>>(json, conv).Values.First() as StringValue;

      Assert.That(result.Value, Is.Empty);
    }

    [Test]
    public void DeserializeObject_GivenFieldWithString_ReturnStringValue()
    {
      string json = "{\"F\":\"Some Val\"}";

      var result = JsonConvert.DeserializeObject<Dictionary<string, Value>>(json).Values.First() as StringValue;

      Assert.That(result.Value, Is.EqualTo("Some Val"));
    }

    [Test]
    public void DeserializeObject_GivenFieldWithTableAndString_ReturnStringValue()
    {
      var json = "{\"F\":[[\"Val 1.0\",\"Val 1.1\"]],\"Identifier\":\"123\"}";

      var results = JsonConvert.DeserializeObject<Dictionary<string, Value>>(json).Values.ToList();

      Assert.That((results[0] as TableValue).Value[0][0], Is.EqualTo("Val 1.0"));
      Assert.That((results[1] as StringValue).Value, Is.EqualTo("123"));
    }

    [Test]
    public void Serialize_GivenEmptyStringValue_ReturnJson()
    {
      var dict = new Dictionary<string, Value> {{"F", new StringValue("")}};

      string result = JsonConvert.SerializeObject(dict);

      Assert.That(result, Is.EqualTo("{\"F\":\"\"}"));
    }

    [Test]
    public void Serialize_GivenStringValue_ReturnJson()
    {
      var dict = new Dictionary<string, Value> {{"F", new StringValue("Val")}};

      string result = JsonConvert.SerializeObject(dict);

      Assert.That(result, Is.EqualTo("{\"F\":\"Val\"}"));
    }

    [Test]
    public void Serialize_GivenTableAndString_ReturnJson()
    {
      var dict = new Dictionary<string, Value>
        {
          {
            "F", new TableValue {Value = new List<List<string>> {new List<string> {"Val 1.0", "Val 1.1"}}}
          },
          {
            "Identifier", new StringValue("123")
          }
        }
        ;

      var result = JsonConvert.SerializeObject(dict);

      Assert.That(result, Is.EqualTo("{\"F\":[[\"Val 1.0\",\"Val 1.1\"]],\"Identifier\":\"123\"}"));
    }

    [Test]
    public void Serialize_GivenTableValueWithMultipleValues_ReturnJson()
    {
      var dict = new Dictionary<string, Value>
        {
          {
            "F", new TableValue
              {
                Value = new List<List<string>>
                  {
                    new List<string> {"Val 1.0", "Val 1.1"},
                    new List<string> {"Val 2.0", "Val 2.1"}
                  }
              }
          }
        };

      string result = JsonConvert.SerializeObject(dict);

      Assert.That(result, Is.EqualTo("{\"F\":[[\"Val 1.0\",\"Val 1.1\"],[\"Val 2.0\",\"Val 2.1\"]]}"));
    }

    [Test]
    public void Serialize_GivenTableValue_ReturnJson()
    {
      var dict = new Dictionary<string, Value>
        {
          {"F", new TableValue {Value = new List<List<string>> {new List<string> {"Val 1"}}}}
        };

      string result = JsonConvert.SerializeObject(dict);

      Assert.That(result, Is.EqualTo("{\"F\":[[\"Val 1\"]]}"));
    }
  }
}