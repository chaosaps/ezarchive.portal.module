﻿using System.Collections.Generic;
using EZArchive.Portal.Module.Api.Bindings;
using EZArchive.Portal.Module.Core;
using NUnit.Framework;

namespace EZArchive.Portal.Module.Test.Api.Bindings
{
  [TestFixture, Category("unittest"), Parallelizable(ParallelScope.Fixtures)]
  public class SearchQueryParameterBindingTest
  {
    private delegate void SearchQueryMethod(SearchQuery q);
    private SearchQueryMethod _method;
    private SearchQueryParameterBinding _binding;

    [SetUp]
    public void SetUp()
    {
      _method = q => { };
      _binding = new SearchQueryParameterBinding();
    }

    [Test]
    public void Bind_GivenNoParameter_ReturnAllQuery()
    {
      var result = _binding.Bind(new Dictionary<string, string>(), _method.Method.GetParameters()[0]);

      Assert.That(result.ToString(), Is.EqualTo("*:*"));
    }

    [Test]
    public void Bind_GivenNullParameter_ReturnAllQuery()
    {
      var result = _binding.Bind(new Dictionary<string, string> { { "q", null } }, _method.Method.GetParameters()[0]);

      Assert.That(result.ToString(), Is.EqualTo("*:*"));
    }
  }
}