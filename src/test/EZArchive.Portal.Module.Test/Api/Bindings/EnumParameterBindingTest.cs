﻿using System.Collections.Generic;
using System.Linq;
using EZArchive.Portal.Module.Api.Bindings;
using NUnit.Framework;

namespace EZArchive.Portal.Module.Test.Api.Bindings
{
  [TestFixture, Category("unittest")]
  public class EnumParameterBindingTest
  {
    [TestCase("1", "Val1", 1u)]
    [TestCase("2", "Val2", 2u)]
    [TestCase("Val1", "Val1", 1u)]
    [TestCase("Val2", "Val2", 2u)]
    [TestCase("Val1, Val2", "3", 3u)]
    public void Bind_Value(string parameterValue, string name, uint value)
    {
      var binding = new EnumParameterBinding<TestEnum>();
      var dict = new Dictionary<string, string> {{"parameter", parameterValue}};

      var methodInfo = GetType().GetMethod("Method");
      var result = (TestEnum) binding.Bind(dict, methodInfo.GetParameters().First());
      
      Assert.That(result.ToString(), Is.EqualTo(name));
      Assert.That((uint)result, Is.EqualTo(value));
    }

    public void Method(TestEnum parameter) {}
  }

  public enum TestEnum{
    Val1 = 1, Val2 =2    
  }
  
}