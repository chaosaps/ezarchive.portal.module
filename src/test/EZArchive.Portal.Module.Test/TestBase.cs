﻿using System.Collections.Generic;
using EZArchive.Portal.Module.Api.Bindings;
using EZArchive.Portal.Module.Core;
using EZArchive.Portal.Module.Core.Configuration;
using EZArchive.Portal.Module.Data.InMemory;
using NUnit.Framework;
using Newtonsoft.Json;

namespace EZArchive.Portal.Module.Test
{
  [TestFixture]
  public class TestBase
  {
		protected DataDefinition DataDefinition;

    [SetUp]
    public void BaseSetUp()
    {
      var settings = new JsonSerializerSettings();
      settings.Converters.Add(new ValueConverter());

      JsonConvert.DefaultSettings = () => settings;

			Context.Repository = new EzInMemoryRepository();
			Context.Config = new Config();

			DataDefinition = new DataDefinition
			{
				Name = "DD1",
				TypeId = 1,
				Fields = new List<FieldDefinition>
					  {
						  new FieldDefinition {Id = "Title", Type = "String"},
						  new FieldDefinition {Id = "Type", Type = "String"}
					  }
			};

			Context.Repository.Definition.SetDataDefinition(DataDefinition);
    }
  }
}
