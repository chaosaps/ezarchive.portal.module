using System;
using EzArchive.Application.UseCases.DataDefinitions;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.DataDefinitions;

[TestFixture]
public class GetDataDefinitionUseCaseTests
{
	private GetDataDefinitionUseCase sut;
	private InMemoryRepositories repos;
	private Session session;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();

		sut = new GetDataDefinitionUseCase(repos.DataDefinition, repos.User);
		var user = repos.User.Set(new User(null, "email", "name", "Contributor", null, null));

		session = repos.Session.Create(user.Id);
	}

	[Test]
	public void NoDataDefinitions_ReturnEmpty()
	{
		// act
		var result = sut.Invoke(session);

		// assert
		result.Value.Count.ShouldBe(1);
	}

	[Test]
	public void DataDefinitionExists_Return()
	{
		// arrange
		repos.McmRepositories.MetadataSchema.Create("name", "{}", Guid.Empty, new Guid());

		// act
		var result = sut.Invoke(session);

		// assert
		result.Value.Count.ShouldBe(2);
	}
}