using System.Collections.Generic;
using System.Threading.Tasks;
using Chaos.Portal.Core.Indexing.Solr.Response;
using EzArchive.Application.UseCases.Tags;
using EzArchive.Data.InMemory;
using EzArchive.Data.Portal;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;
using FacetFieldsResult = Chaos.Portal.Core.Indexing.Responses.FacetFieldsResult;

namespace EzArchive.Application.Tests.UseCases.Tags;

[TestFixture]
public class GetTagsUseCaseTests
{
	private InMemoryRepositories repos;
	private Session session;
	private IndexSpy indexSpy;
	private User user;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		user = repos.User.Set(new User(null, "email", "name", "Contributor", null, null));

		session = repos.Session.Create(user.Id);
		indexSpy = new IndexSpy();
	}

	[Test]
	public async Task NoQuery_DontAddPrefixInSolrQuery()
	{
		// arrange
		var sut = new GetTagsUseCase(
			new SearchRepository(
				indexSpy,
				repos.SearchDefinition,
				repos.DataDefinition,
				repos.Label),
			repos.Project,
			false);
		indexSpy.QueryResponse = new IndexSpy.ResponseWithFacet(
			new FacetFieldsResult("val", new[] { new Facet("string", "facet", 1), }));

		// act
		var result = await sut.InvokeAsync(session, "");

		// assert
		result.Value.ShouldNotBeEmpty();
		indexSpy.WasCalledWithQuery.ToString().ShouldBe(
			"fl=score, *&wt=xml&q=*:*&sort=&start=0&rows=0&fq=&facet=true&facet.field=sm_tags&facet.mincount=1&facet.prefix=&group=false&group.limit=0&group.field=&group.offset=0");
	}

	[Test]
	public async Task PermissionsAreHandledThroughProjects_DontAddPrefixInSolrQuery()
	{
		// arrange
		repos.Project.Add(new Project(
			new ProjectId(1),
			"project name",
			new List<Label> { new Label(new LabelId(1), "label", 1) },
			new[] { user }));
		repos.Project.Add(new Project(
			new ProjectId(2),
			"project name 2",
			new List<Label> { new Label(new LabelId(2), "label", 2) },
			new[] { user })
		);

		var sut = new GetTagsUseCase(
			new SearchRepository(
				indexSpy,
				repos.SearchDefinition,
				repos.DataDefinition,
				repos.Label),
			repos.Project,
			true);
		indexSpy.QueryResponse = new IndexSpy.ResponseWithFacet(
			new FacetFieldsResult("val", new[] { new Facet("string", "facet", 1), }));

		// act
		var result = await sut.InvokeAsync(session, "");

		// assert
		result.Value.ShouldNotBeEmpty();
		indexSpy.WasCalledWithQuery.ToString().ShouldBe(
			"fl=score, *&wt=xml&q=*:*&sort=&start=0&rows=0&fq=sm_projectids:(1 OR 2)&facet=true&facet.field=sm_tags&facet.mincount=1&facet.prefix=&group=false&group.limit=0&group.field=&group.offset=0");
	}

	[Test]
	public async Task HasQuery_DontAddPrefixInSolrQuery()
	{
		// arrange
		var sut = new GetTagsUseCase(
			new SearchRepository(
				indexSpy,
				repos.SearchDefinition,
				repos.DataDefinition,
				repos.Label),
			repos.Project,
			false);
		indexSpy.QueryResponse = new IndexSpy.ResponseWithFacet(
			new FacetFieldsResult("val", new[] { new Facet("string", "facet", 1), }));

		// act
		var result = await sut.InvokeAsync(session, "query");

		// assert
		result.Value.ShouldNotBeEmpty();
		indexSpy.WasCalledWithQuery.ToString().ShouldBe(
			"fl=score, *&wt=xml&q=*:*&sort=&start=0&rows=0&fq=&facet=true&facet.field=sm_tags&facet.mincount=1&facet.prefix=query&group=false&group.limit=0&group.field=&group.offset=0");
	}
}