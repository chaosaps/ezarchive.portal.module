using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Files;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Files;

[TestFixture]
public class CreateFileReferenceUseCaseTests
{
	private InMemoryRepositories repos;
	private CreateFileReferenceUseCase sut;
	private Session userSession;
	private Session adminSession;
	private Asset asset;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		sut = new CreateFileReferenceUseCase(repos.File, repos.User);
		var user = repos.User.Set(new User(null, "User", "name", "User", null, null));
		var admin = repos.User.Set(new User(null, "Administrator", "name", "Administrator", null, null));
		userSession = repos.Session.Create(user.Id);
		adminSession = repos.Session.Create(admin.Id);
		asset = repos.Asset.Set(
			new Asset(
				AssetId.New(),
				new Asset._Type(1),
				admin.Id,
				true,
				new string[0],
				new Asset._Data[0],
				new Asset._AnnotationGroup[0],
				new Asset._FileReference[0]));
	}

	[Test]
	public void CurrentUserIsUser_Throw()
	{
		// act
		void Act() => sut.Invoke(userSession, asset.Id, "file", "file", "/", 1, null);

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void CurrentUserIsAdmin_CrateFileReferenceInRepository()
	{
		// act
		var result = sut.Invoke(adminSession, asset.Id, "file", "file", "/", 1, null);

		// assert
		result.Value.Value.ShouldBe(1u);
		repos.McmRepositories.File.Get(result.Value.Value).Filename.ShouldBe("file");
	}
}