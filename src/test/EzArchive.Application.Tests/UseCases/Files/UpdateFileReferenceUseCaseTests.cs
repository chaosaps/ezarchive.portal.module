using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Files;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Files;

[TestFixture]
public class UpdateFileReferenceUseCaseTests
{
	private InMemoryRepositories repos;
	private UpdateFileReferenceUseCase sut;
	private Session userSession;
	private Session adminSession;
	private Asset asset;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		sut = new UpdateFileReferenceUseCase(repos.File, repos.User);
		var user = repos.User.Set(new User(null, "User", "name", "User", null, null));
		var admin = repos.User.Set(new User(null, "Administrator", "name", "Administrator", null, null));
		userSession = repos.Session.Create(user.Id);
		adminSession = repos.Session.Create(admin.Id);
		asset = repos.Asset.Set(
			new Asset(
				AssetId.New(),
				new Asset._Type(1),
				admin.Id,
				true,
				new string[0],
				new Asset._Data[0],
				new Asset._AnnotationGroup[0],
				new Asset._FileReference[0]));
	}

	[Test]
	public void CurrentUserIsUser_Throw()
	{
		// arrange
		var fileId = repos.File.Set(asset.Id, null, "org", "org", "", 1);

		// act
		void Act() => sut.Invoke(userSession, fileId, "file");

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void UserHasPermission_UpdateFileNameInRepository()
	{
		var fileId = repos.File.Set(asset.Id, null, "org", "org", "", 1);

		// act
		var result = sut.Invoke(adminSession, fileId, "updated");

		// assert
		repos.Asset.Get(asset.Id).Files[0].Name.ShouldBe("updated");
		result.HasError.ShouldBeFalse();
	}
}