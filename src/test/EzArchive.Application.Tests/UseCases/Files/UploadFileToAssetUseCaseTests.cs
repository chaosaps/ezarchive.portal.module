using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.Tests.UseCases.Assets;
using EzArchive.Application.UseCases.Files;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Files;

[TestFixture]
public class UploadFileToAssetUseCaseTests
{
	private InMemoryRepositories repos;
	private UploadFileToAssetUseCase sut;
	private Session userSession;
	private Session adminSession;
	private Asset asset;
	private StorageSpy storageSpy;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		storageSpy = new StorageSpy();
		sut = new UploadFileToAssetUseCase(repos.File, repos.User, storageSpy, 1);
		var user = repos.User.Set(new User(null, "User", "name", "User", null, null));
		var admin = repos.User.Set(new User(null, "Administrator", "name", "Administrator", null, null));
		userSession = repos.Session.Create(user.Id);
		adminSession = repos.Session.Create(admin.Id);
		asset = repos.Asset.Set(
			new Asset(
				AssetId.New(),
				new Asset._Type(1),
				admin.Id,
				true,
				new string[0],
				new Asset._Data[0],
				new Asset._AnnotationGroup[0],
				new Asset._FileReference[0]));
	}

	[Test]
	public async Task CurrentUserIsUser_Throw()
	{
		// act
		Task Act() => sut.InvokeAsync(userSession, asset.Id, "file.png", new MemoryStream());

		// assert
		await Should.ThrowAsync<InsufficientPermissionsException>(Act);
	}

	[Test]
	public async Task CurrentUserIsAdmin_AddToRepositoryAndStorage()
	{
		// act
		var result = await sut.InvokeAsync(adminSession, asset.Id, "file.png", new MemoryStream());

		// assert
		result.HasError.ShouldBeFalse();
		storageSpy.WrittenKeys.Any(f => f.EndsWith(".png")).ShouldBeTrue();
		repos.Asset.Get(asset.Id).Files.ShouldNotBeEmpty();
	}
}