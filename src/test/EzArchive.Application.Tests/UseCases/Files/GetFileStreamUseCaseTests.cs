using System.Threading.Tasks;
using EzArchive.Application.Tests.UseCases.Assets;
using EzArchive.Application.UseCases.Files;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Files;

[TestFixture]
public class GetFileStreamUseCaseTests
{
	private InMemoryRepositories repos;
	private GetFileStreamUseCase sut;
	private Session userSession;
	private Session adminSession;
	private Asset asset;
	private StorageSpy storageSpy;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		storageSpy = new StorageSpy();
		sut = new GetFileStreamUseCase(repos.File, storageSpy);
		var user = repos.User.Set(new User(null, "User", "name", "User", null, null));
		var admin = repos.User.Set(new User(null, "Administrator", "name", "Administrator", null, null));
		userSession = repos.Session.Create(user.Id);
		adminSession = repos.Session.Create(admin.Id);
		asset = repos.Asset.Set(
			new Asset(
				AssetId.New(),
				new Asset._Type(1),
				admin.Id,
				true,
				new string[0],
				new Asset._Data[0],
				new Asset._AnnotationGroup[0],
				new Asset._FileReference[0]));
	}


	[Ignore("Cannot be tested until ObjectRepository File Token is dynamic (has to be S3)")]
	public async Task UserIsAdmin_ReturnStream()
	{
		// arrange
		var fileId = repos.File.Set(asset.Id, null, "org", "org", "", 1);

		// act
		var result = await sut.InvokeAsync(adminSession, fileId);

		// assert
		result.Value.fileName.ShouldBe("org");
		result.Value.contentType.ShouldBe("video/mp4");
		result.Value.stream.Length.ShouldNotBe(0);
	}
}