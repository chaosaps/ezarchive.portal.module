using System.Threading.Tasks;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.Tests.UseCases.Assets;
using EzArchive.Application.UseCases.Files;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Files;

[TestFixture]
public class DeleteFileUseCaseTests
{
	private InMemoryRepositories repos;
	private DeleteFileUseCase sut;
	private Session userSession;
	private Session adminSession;
	private Asset asset;
	private StorageSpy storageSpy;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		storageSpy = new StorageSpy();
		sut = new DeleteFileUseCase(repos.File, repos.User, storageSpy);
		var user = repos.User.Set(new User(null, "User", "name", "User", null, null));
		var admin = repos.User.Set(new User(null, "Administrator", "name", "Administrator", null, null));
		userSession = repos.Session.Create(user.Id);
		adminSession = repos.Session.Create(admin.Id);
		asset = repos.Asset.Set(
			new Asset(
				AssetId.New(),
				new Asset._Type(1),
				admin.Id,
				true,
				new string[0],
				new Asset._Data[0],
				new Asset._AnnotationGroup[0],
				new Asset._FileReference[0]));
	}

	[Test]
	public async Task CurrentUserIsUser_Throw()
	{
		// arrange
		var fileId = repos.File.Set(asset.Id, null, "org", "org", "", 1);

		// act
		Task Act() => sut.InvokeAsync(userSession, fileId);

		// assert
		await Should.ThrowAsync<InsufficientPermissionsException>(Act);
	}

	[Test]
	public async Task UserIsContributor_SaveTags()
	{
		// arrange
		var fileId = repos.File.Set(asset.Id, null, "org", "org", "", 1);

		// act
		var result = await sut.InvokeAsync(adminSession, fileId);

		// assert
		result.HasError.ShouldBeFalse();
		repos.Asset.Get(asset.Id).Files.ShouldBeEmpty();
		storageSpy.DeletedKeys.ShouldContain(k => k == "/org");
	}
}