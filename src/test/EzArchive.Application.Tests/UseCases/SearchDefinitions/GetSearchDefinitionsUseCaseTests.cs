using EzArchive.Application.UseCases.SearchDefinitions;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.SearchDefinitions;

[TestFixture]
public class GetSearchDefinitionsUseCaseTests
{
	private GetSearchDefinitionsUseCase sut;
	private InMemoryRepositories repos;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		sut = new GetSearchDefinitionsUseCase(repos.SearchDefinition);
	}

	[Test]
	public void NoSearchDefinitionSet_ReturnEmpty()
	{
		// act
		var result = sut.Invoke();

		// assert
		result.ErrorCode.ShouldBe(ErrorCode.None);
	}

	[Test]
	public void SearchDefinitionIsSet_Return()
	{
		// arrange
		repos.SearchDefinition.Add(
			new SearchFieldDefinition("DN", new[] { "a.f1", "b.f1" }, "String", true, true));

		// act
		var result = sut.Invoke();

		// assert
		result.ErrorCode.ShouldBe(ErrorCode.None);
		result.Value[0].ShouldSatisfyAllConditions(
			() => result.Value[0].DisplayName.ShouldBe("DN"),
			() => result.Value[0].Type.ShouldBe("String"),
			() => result.Value[0].IsSortable.ShouldBeTrue());
	}
}