using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using File = EzArchive.Domain.File;

namespace EzArchive.Application.Tests.UseCases.Assets;

public class StorageSpy : IStorage
{
	public readonly IList<string> DeletedKeys = new List<string>();
	public readonly IList<string> WrittenKeys = new List<string>();

	public Task WriteAsync(string key, Stream stream, string contentType = "application/octet-stream")
	{
		WrittenKeys.Add(key);
		return Task.CompletedTask;
	}

	public Task DeleteAsync(string key)
	{
		DeletedKeys.Add(key);
		return Task.CompletedTask;
	}

	public Task<Stream> ReadAsync(File file)
	{
		var stream = new MemoryStream(Encoding.UTF8.GetBytes("content")) as Stream;
		stream.Position = 0;
		return Task.FromResult(stream);
	}
}