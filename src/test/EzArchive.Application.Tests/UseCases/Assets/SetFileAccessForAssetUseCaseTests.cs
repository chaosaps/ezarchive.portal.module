using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Assets;

[TestFixture]
public class SetFileAccessForAssetUseCaseTests
{
	private InMemoryRepositories repos;
	private SetFileAccessForAssetUseCase sut;
	private Session userSession;
	private Session adminSession;
	private Asset asset;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		sut = new SetFileAccessForAssetUseCase(repos.Asset, repos.User);
		var user = repos.User.Set(new User(null, "User", "name", "User", null, null));
		var admin = repos.User.Set(new User(null, "Administrator", "name", "Administrator", null, null));
		userSession = repos.Session.Create(user.Id);
		adminSession = repos.Session.Create(admin.Id);
		asset = repos.Asset.Set(
			new Asset(
				AssetId.New(),
				new Asset._Type(1),
				admin.Id,
				true,
				new string[0],
				new Asset._Data[0],
				new Asset._AnnotationGroup[0],
				new Asset._FileReference[0]));
	}

	[Test]
	public void CurrentUseIsUser_Throw()
	{
		// act
		void Act() => sut.Invoke(userSession, asset.Id, false);

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void CurrentUserIsAdmin_SetFileAccessOnAsset()
	{
		// act
		var result = sut.Invoke(adminSession, asset.Id, false);

		// assert
		result.HasError.ShouldBeFalse();
		repos.Asset.Get(asset.Id).DoFilesRequireLogin.ShouldBeFalse();
	}
}