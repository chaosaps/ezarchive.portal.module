using System;
using System.Collections.Generic;
using System.Linq;
using Chaos.Mcm.Core.Model;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.Tests.UseCases.Search;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Data.InMemory;
using EzArchive.Data.Portal;
using EzArchive.Domain;
using EzArchive.Domain.Values;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Assets;

[TestFixture]
public class ReIndexAssetsUseCaseTests
{
	private ReIndexAssetsUseCase sut;
	private InMemoryRepositories repos;
	private Session userSession;
	private DataDefinition dataDefinition;
	private Asset asset;
	private DataDefinition annotationDefinition;
	private SearchUseCaseTests.SearchIndexSpy indexSpy;
	private Session adminSession;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		var user = repos.User.Set(
			new User(
				null,
				"email 1",
				"name",
				"User",
				null,
				null));
		userSession = repos.Session.Create(user.Id);
		var admin = repos.User.Set(
			new User(
				null,
				"email 2",
				"name",
				"Administrator",
				null,
				null));
		adminSession = repos.Session.Create(admin.Id);
		repos.McmRepositories.MetadataSchema.Create(
			new MetadataSchema(
				new Guid("20903220-e714-4cdd-b49a-8107723f6f0d"),
				"name",
				"{" +
				"  \"Name\":\"name\"," +
				"  \"Fields\": [{" +
				"    \"Id\": \"f1\"," +
				"    \"Type\": " +
				"    \"String\"," +
				"    \"Required\": false," +
				"    \"Placeholder\": \"\"," +
				"    \"Note\": null" +
				"  }]" +
				"}",
				DateTime.UtcNow),
			user.Id.Value);
		repos.McmRepositories.MetadataSchema.Create(
			new MetadataSchema(
				new Guid("59464896-f114-413e-b2bb-a572061d6873"),
				"annotation",
				"{" +
				"  \"Name\":\"annotation\"," +
				"  \"Type\":1," +
				"  \"Fields\": [{" +
				"    \"Id\": \"a1\"," +
				"    \"Type\": " +
				"    \"String\"," +
				"    \"Required\": false," +
				"    \"Placeholder\": \"\"," +
				"    \"Note\": null" +
				"  }]" +
				"}",
				DateTime.UtcNow),
			user.Id.Value);

		dataDefinition = repos.DataDefinition.GetDataDefinitions()
			.Single(dd => dd.Id == "20903220-e714-4cdd-b49a-8107723f6f0d");
		annotationDefinition = repos.DataDefinition.GetAnnotations()
			.Single(dd => dd.Id == "59464896-f114-413e-b2bb-a572061d6873");

		asset = repos.Asset.Set(new Asset(
			null,
			new Asset._Type(1),
			userSession.UserId,
			true,
			new[] { "t1" },
			new[]
			{
				new Asset._Data(
					new Dictionary<string, Value> { { "f1", new StringValue("v1") } },
					dataDefinition)
			},
			new List<Asset._AnnotationGroup>
			{
				new Asset._AnnotationGroup(
					"annotation",
					new Guid(annotationDefinition.Id),
					new[]
					{
						new Dictionary<string, Value>
						{
							{ "__Visibility", new StringValue("Private") },
							{ "__OwnerId", new StringValue(userSession.UserId.Value.ToString()) },
							{ "a1", new StringValue("v3") }
						}
					}
				)
			},
			new List<Asset._FileReference>()));

		repos.SearchDefinition.Add(new SearchFieldDefinition("Title", new[] { "name.f1" }, "string", true, true));

		indexSpy = new SearchUseCaseTests.SearchIndexSpy();
		new IndexHelper(repos.SearchDefinition, repos.DataDefinition);

		sut = new ReIndexAssetsUseCase(
			repos.Asset,
			repos.User,
			new SearchRepository(
				indexSpy,
				repos.SearchDefinition,
				repos.DataDefinition,
				repos.Label));
	}

	[Test]
	public void CurrentUserIsUser_Throw()
	{
		// act
		void Act() => sut.Invoke(userSession, true);

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void CurrentUserIsAdministrator_IndexAllAssets()
	{
		// act
		var result = sut.Invoke(adminSession, true);

		// assert
		result.HasError.ShouldBeFalse();
		indexSpy.WasIndexedWith.ShouldNotBeEmpty();
		var indexedFields = indexSpy.WasIndexedWith.First().GetIndexableFields().ToList();
		indexedFields.Single(f => "id".Equals(f.Key, StringComparison.OrdinalIgnoreCase)).Value
			.ShouldBe(asset.Id.Value.ToString());
		indexedFields.Single(f => "s_search_title".Equals(f.Key, StringComparison.OrdinalIgnoreCase)).Value
			.ShouldBe("v1");
		indexSpy.WasDeleted.ShouldBeTrue();
	}
}