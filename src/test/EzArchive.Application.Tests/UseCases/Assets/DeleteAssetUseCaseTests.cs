using System.Linq;
using System.Threading.Tasks;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.Tests.UseCases.Search;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Data.InMemory;
using EzArchive.Data.Portal;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Assets;

[TestFixture]
public class DeleteAssetUseCaseTests
{
	private InMemoryRepositories repos;
	private DeleteAssetUseCase sut;
	private Session userSession;
	private Session contributorSession;
	private Session adminSession;
	private Asset asset;
	private StorageSpy storageSpy;
	private SearchUseCaseTests.SearchIndexSpy searchIndexSpy;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		storageSpy = new StorageSpy();
		searchIndexSpy = new SearchUseCaseTests.SearchIndexSpy();
		sut = new DeleteAssetUseCase(
			repos.Asset,
			repos.User,
			new SearchRepository(
				searchIndexSpy,
				repos.SearchDefinition,
				repos.DataDefinition,
				repos.Label),
			storageSpy
		);
		var user = repos.User.Set(new User(null, "User", "name", "User", null, null));
		var contributor = repos.User.Set(new User(null, "Contributor", "name", "Contributor", null, null));
		var admin = repos.User.Set(new User(null, "Administrator", "name", "Administrator", null, null));
		userSession = repos.Session.Create(user.Id);
		contributorSession = repos.Session.Create(contributor.Id);
		adminSession = repos.Session.Create(admin.Id);
		asset = repos.Asset.Set(new Asset(
			AssetId.New(),
			new Asset._Type(1),
			admin.Id,
			true,
			new string[0],
			new Asset._Data[0],
			new Asset._AnnotationGroup[0],
			new Asset._FileReference[0]));
		repos.File.Set(asset.Id, null, "filename.ext", "filename.ext", "path/to/file", 1);
	}

	[Test]
	public void UserIsUser_Throw()
	{
		// act
		Task Act() => sut.InvokeAsync(userSession, asset.Id);

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public async Task UserIsContributor_SaveTags()
	{
		// act
		var result = await sut.InvokeAsync(contributorSession, asset.Id);

		// assert
		result.HasError.ShouldBeFalse();
		repos.Asset.Get(asset.Id).ShouldBeNull();
		storageSpy.DeletedKeys
			.Any(key => "basePath/path/to/file/filename.ext?{SESSION_GUID_MISSING}".Equals(key))
			.ShouldBeTrue();
		searchIndexSpy.WasDeleted.ShouldBeTrue();
	}
}