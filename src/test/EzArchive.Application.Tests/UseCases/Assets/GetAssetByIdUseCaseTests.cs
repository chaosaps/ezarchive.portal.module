using System;
using System.Collections.Generic;
using System.Linq;
using Chaos.Mcm.Core.Model;
using EzArchive.Application.Errors;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using EzArchive.Domain.Values;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Assets;

[TestFixture]
public class GetAssetByIdUseCaseTests
{
	private GetAssetByIdUseCase sut;
	private InMemoryRepositories repos;
	private Session session;
	private DataDefinition dataDefinition;
	private Asset asset;
	private DataDefinition annotationDefinition;
	private Session anonymousSession;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		var user = repos.User.Set(
			new User(
				null,
				"email",
				"name",
				"Contributor",
				null,
				"{\"eduPersonPrincipalName\":[\"eduPersonPrincipalName@domain.dk\"],\"eduPersonPrimaryAffiliation\":[\"student\"],\"eduPersonScopedAffiliation\":[\"student@domain.dk\"],\"schacHomeOrganization\":[\"domain.dk\"],\"eduPersonTargetedID\":[\"WAYF-DK-eduPersonTargetedID\"],\"groups\":[\"groups-domain.dk\",\"users\",\"members\"]}"));
		session = repos.Session.Create(user.Id);
		anonymousSession = repos.Session.Create(repos.User.Set(
			new User(
				null,
				"Anonymous",
				"Anonymous",
				"User",
				null,
				null)).Id);
		repos.McmRepositories.MetadataSchema.Create(
			new MetadataSchema(
				new Guid("20903220-e714-4cdd-b49a-8107723f6f0d"),
				"name",
				"{" +
				"  \"Name\":\"name\"," +
				"  \"Fields\": [{" +
				"    \"Id\": \"f1\"," +
				"    \"Type\": " +
				"    \"String\"," +
				"    \"Required\": false," +
				"    \"Placeholder\": \"\"," +
				"    \"Note\": null" +
				"  }]" +
				"}",
				DateTime.UtcNow),
			user.Id.Value);
		repos.McmRepositories.MetadataSchema.Create(
			new MetadataSchema(
				new Guid("59464896-f114-413e-b2bb-a572061d6873"),
				"annotation",
				"{" +
				"  \"Name\":\"annotation\"," +
				"  \"Type\":1," +
				"  \"Fields\": [{" +
				"    \"Id\": \"a1\"," +
				"    \"Type\": " +
				"    \"String\"," +
				"    \"Required\": false," +
				"    \"Placeholder\": \"\"," +
				"    \"Note\": null" +
				"  }]" +
				"}",
				DateTime.UtcNow),
			user.Id.Value);

		dataDefinition = repos.DataDefinition.GetDataDefinitions()
			.Single(dd => dd.Id == "20903220-e714-4cdd-b49a-8107723f6f0d");
		annotationDefinition = repos.DataDefinition.GetAnnotations()
			.Single(dd => dd.Id == "59464896-f114-413e-b2bb-a572061d6873");

		asset = repos.Asset.Set(new Asset(
			null,
			new Asset._Type(1),
			session.UserId,
			true,
			new[] { "t1" },
			new[]
			{
				new Asset._Data(
					new Dictionary<string, Value> { { "f1", new StringValue("v1") } },
					dataDefinition)
			},
			new List<Asset._AnnotationGroup>
			{
				new Asset._AnnotationGroup(
					"annotation",
					new Guid(annotationDefinition.Id),
					new[]
					{
						new Dictionary<string, Value>
						{
							{ "__Visibility", new StringValue("Public") },
							{ "__OwnerId", new StringValue(anonymousSession.UserId.Value.ToString()) },
							{ "a1", new StringValue("v1") }
						},
						new Dictionary<string, Value>
						{
							{ "__Visibility", new StringValue("Private") },
							{ "__OwnerId", new StringValue(anonymousSession.UserId.Value.ToString()) },
							{ "a1", new StringValue("v2") },
						},
						new Dictionary<string, Value>
						{
							{ "__Visibility", new StringValue("Private") },
							{ "__OwnerId", new StringValue(session.UserId.Value.ToString()) },
							{ "a1", new StringValue("v3") }
						}
					}
				)
			},
			new List<Asset._FileReference>()));
		repos.McmRepositories.File.Create(asset.Id.Value, null, 1, "{WAYF_ATTRIBUTES}_filename.mp4",
			"filename.mp4", "path/to", 1);

		sut = new GetAssetByIdUseCase(repos.Asset, repos.User);
	}

	[Test]
	public void NoAssetWithId_Throw()
	{
		// act
		void Act() => sut.Invoke(session, new AssetId("c74cb530-3a8b-4852-8225-23b451420e47"));

		// assert
		Should.Throw<DataNotFoundException>(Act);
	}

	[Test]
	public void AssetWithIdExists_Return()
	{
		// act
		var result = sut.Invoke(session, asset.Id);

		// assert
		result.Value.Id.ShouldNotBeNull();
		result.Value.Type.Value.ShouldBe(1u);
		result.Value.FindField("name", "f1").ToString().ShouldBe("v1");
		result.Value.DoFilesRequireLogin.ShouldBeTrue();
		result.Value.Tags[0].ShouldBe("t1");
	}

	[Test]
	public void AssetWithAnnotation_ReturnWithPrivateAnnotationsExcludedForOtherUsers()
	{
		// act
		var result = sut.Invoke(session, asset.Id);

		// assert
		result.Value.ShouldSatisfyAllConditions(
			() => result.Value.DoFilesRequireLogin.ShouldBeTrue(),
			() => result.Value.AnnotationsGroups[0].Annotations[0]["a1"].ToString().ShouldBe("v1"),
			() => result.Value.AnnotationsGroups[0].Annotations[1]["a1"].ToString().ShouldBe("v3"),
			() => result.Value.AnnotationsGroups[0].Annotations.Count.ShouldBe(2));
	}

	[Test]
	public void AssetWithFile_ReturnWithPlaceholdersReplacedInPaths()
	{
		// arrange


		// act
		var result = sut.Invoke(session, asset.Id);

		// assert
		result.Value.Files.ShouldNotBeEmpty();
		result.Value.Files.First().Destinations.First().Url
			.ShouldBe(
				"basePath/path/to/eyAgImVkdVBlcnNvblRhcmdldGVkSUQiOlsiV0FZRi1ESy1lZHVQZXJzb25UYXJnZXRlZElEIl0sICAiZWR1UGVyc29uUHJpbmNpcGxlTmFtZSI6WyIiXSwgICJlZHVQZXJzb25QcmltYXJ5QWZmaWxpYXRpb24iOlsic3R1ZGVudCJdLCAgInNjaGFjSG9tZU9yZ2FuaXphdGlvbiI6WyJkb21haW4uZGsiXSwgICJlZHVQZXJzb25TY29wZWRBZmZpbGlhdGlvbiI6WyJzdHVkZW50QGRvbWFpbi5kayJdfQ==_filename.mp4?" +
				session.Id.Value);
	}

	[Test]
	public void UserNotLoggedIn_ReturnWithFallbackString()
	{
		// arrange


		// act
		var result = sut.Invoke(anonymousSession, asset.Id);

		// assert
		result.Value.Files.ShouldNotBeEmpty();
		result.Value.Files.First().Destinations.First().Url
			.ShouldBe(
				"basePath/path/to/e1dBWUZfQVRUUklCVVRFU19NSVNTSU5HfQ==_filename.mp4?" +
				anonymousSession.Id.Value);
	}
}