using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.Tests.UseCases.Search;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Data.InMemory;
using EzArchive.Data.Portal;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Assets;

[TestFixture]
public class SetTagAssetUseCaseTests
{
	private InMemoryRepositories repos;
	private SetTagAssetUseCase sut;
	private Session userSession;
	private Session contributorSession;
	private Session adminSession;
	private Asset asset;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		sut = new SetTagAssetUseCase(
			repos.Asset,
			repos.User,
			new SearchRepository(
				new SearchUseCaseTests.SearchIndexSpy(),
				repos.SearchDefinition,
				repos.DataDefinition,
				repos.Label));
		var user = repos.User.Set(new User(null, "User", "name", "User", null, null));
		var contributor = repos.User.Set(new User(null, "Contributor", "name", "Contributor", null, null));
		var admin = repos.User.Set(new User(null, "Administrator", "name", "Administrator", null, null));
		userSession = repos.Session.Create(user.Id);
		contributorSession = repos.Session.Create(contributor.Id);
		adminSession = repos.Session.Create(admin.Id);
		asset = repos.Asset.Set(new Asset(
			AssetId.New(),
			new Asset._Type(1),
			admin.Id,
			true,
			new string[0],
			new Asset._Data[0],
			new Asset._AnnotationGroup[0],
			new Asset._FileReference[0]));
	}

	[Test]
	public void UserIsUser_Throw()
	{
		// act
		void Act() => sut.Invoke(userSession, asset.Id, "tag");

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void UserIsContributor_SaveTags()
	{
		// act
		var result = sut.Invoke(contributorSession, asset.Id, "tag");

		// assert
		result.HasError.ShouldBeFalse();
		repos.Asset.Get(asset.Id).Tags.Count.ShouldBe(1);
		repos.Asset.Get(asset.Id).Tags[0].ShouldBe("tag");
	}

	[Test]
	public void UserIsAdministrator_SaveTags()
	{
		// act
		var result = sut.Invoke(adminSession, asset.Id, "tag");

		// assert
		result.HasError.ShouldBeFalse();
		repos.Asset.Get(asset.Id).Tags.Count.ShouldBe(1);
		repos.Asset.Get(asset.Id).Tags[0].ShouldBe("tag");
	}

	[Test]
	public void MultipleTags_SaveTags()
	{
		// act
		var result = sut.Invoke(adminSession, asset.Id, "tag1", "tag2");

		// assert
		result.HasError.ShouldBeFalse();
		repos.Asset.Get(asset.Id).Tags.Count.ShouldBe(2);
		repos.Asset.Get(asset.Id).Tags[0].ShouldBe("tag1");
		repos.Asset.Get(asset.Id).Tags[1].ShouldBe("tag2");
	}
}