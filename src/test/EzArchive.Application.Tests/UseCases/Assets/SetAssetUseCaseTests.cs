using System;
using System.Collections.Generic;
using System.Linq;
using Chaos.Mcm.Core.Model;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.Tests.UseCases.Search;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Data.InMemory;
using EzArchive.Data.Portal;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Assets;

[TestFixture]
public class SetAssetUseCaseTests
{
	private InMemoryRepositories repos;
	private Session userSession;
	private Session contributorSession;
	private SetAssetUseCase sut;
	private SearchUseCaseTests.SearchIndexSpy searchIndexSpy;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		var contributor = repos.User.Set(
			new User(
				null,
				"contributor",
				"contributor",
				"Contributor",
				null,
				"{\"eduPersonPrincipalName\":[\"eduPersonPrincipalName@domain.dk\"],\"eduPersonPrimaryAffiliation\":[\"student\"],\"eduPersonScopedAffiliation\":[\"student@domain.dk\"],\"schacHomeOrganization\":[\"domain.dk\"],\"eduPersonTargetedID\":[\"WAYF-DK-eduPersonTargetedID\"],\"groups\":[\"groups-domain.dk\",\"users\",\"members\"]}"));

		var user = repos.User.Set(
			new User(
				null,
				"user",
				"user",
				"User",
				null,
				"{\"eduPersonPrincipalName\":[\"eduPersonPrincipalName@domain.dk\"],\"eduPersonPrimaryAffiliation\":[\"student\"],\"eduPersonScopedAffiliation\":[\"student@domain.dk\"],\"schacHomeOrganization\":[\"domain.dk\"],\"eduPersonTargetedID\":[\"WAYF-DK-eduPersonTargetedID\"],\"groups\":[\"groups-domain.dk\",\"users\",\"members\"]}"));

		repos.McmRepositories.MetadataSchema.Create(
			new MetadataSchema(
				new Guid("20903220-e714-4cdd-b49a-8107723f6f0d"),
				"name",
				"{" +
				"  \"Name\":\"DD1\"," +
				"  \"TypeId\": 1," +
				"  \"Fields\": [{" +
				"    \"Id\": \"Title\"," +
				"    \"Type\": \"String\"," +
				"    \"Required\": false," +
				"    \"Placeholder\": \"\"," +
				"    \"Note\": null" +
				"  },{" +
				"    \"Id\": \"Title2\"," +
				"    \"Type\": \"String\"," +
				"    \"Required\": false," +
				"    \"Placeholder\": \"\"," +
				"    \"Note\": null" +
				"  },{" +
				"    \"Id\": \"Title3\"," +
				"    \"Type\": \"String\"," +
				"    \"Required\": false," +
				"    \"Placeholder\": \"\"," +
				"    \"Note\": null" +
				"  }" +
				"]" +
				"}",
				DateTime.UtcNow),
			contributor.Id.Value);

		repos.SearchDefinition.Add(new SearchFieldDefinition("Title", new[] { "DD1.Title" }, "string", true, true));

		userSession = repos.Session.Create(user.Id);
		contributorSession = repos.Session.Create(contributor.Id);

		searchIndexSpy = new SearchUseCaseTests.SearchIndexSpy();
		sut = new SetAssetUseCase(
			repos.User,
			repos.Asset,
			repos.DataDefinition,
			new SearchRepository(
				searchIndexSpy,
				repos.SearchDefinition,
				repos.DataDefinition,
				repos.Label));

		new IndexHelper(repos.SearchDefinition, repos.DataDefinition);
	}

	[Test]
	public void IsUser_Throw()
	{
		// act
		void Act() => sut.Invoke(
			userSession, new Dictionary<string, string>());

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void NewAsset_SetOnRepository()
	{
		// act
		var res = sut.Invoke(
			contributorSession,
			new Dictionary<string, string> { { "TypeId", "1" }, { "DD1.Title", "val" } });

		// assert
		var result = repos.Asset.Get(res.Value.Id);
		res.ShouldSatisfyAllConditions(
			() => result.Id.ShouldBe(res.Value.Id),
			() => result.FindField("DD1", "Title").ToString().ShouldBe("val"),
			() => result.LastChangedByUserId.ShouldBe(contributorSession.UserId));
	}

	[Test]
	public void NewAssetWithNullValue_SetOnRepository()
	{
		// act
		var res = sut.Invoke(
			contributorSession,
			new Dictionary<string, string> { { "TypeId", "1" }, { "DD1.Title", null } });

		// assert
		var result = repos.Asset.Get(res.Value.Id);
		res.ShouldSatisfyAllConditions(
			() => result.Id.ShouldBe(res.Value.Id),
			() => result.FindField("DD1", "Title").ToString().ShouldBe(""),
			() => result.LastChangedByUserId.ShouldBe(contributorSession.UserId));
	}

	[Test]
	public void NewAssetWithSpecifiedId_SetOnRepository()
	{
		// act
		var res = sut.Invoke(
			contributorSession,
			new Dictionary<string, string>
			{
				{ "Identifier", "5e5ddf99-8bcb-431b-8b72-493a973db628" }, { "TypeId", "1" }, { "DD1.Title", "val" }
			});

		// assert
		var result = repos.Asset.Get(res.Value.Id);
		res.ShouldSatisfyAllConditions(
			() => result.Id.Value.ToString().ShouldBe("5e5ddf99-8bcb-431b-8b72-493a973db628"),
			() => result.FindField("DD1", "Title").ToString().ShouldBe("val"),
			() => result.LastChangedByUserId.ShouldBe(contributorSession.UserId));
	}


	[Test]
	public void ExistingAsset_SetOnRepository()
	{
		// arrange
		var asset = sut.Invoke(
			contributorSession,
			new Dictionary<string, string>
				{ { "TypeId", "1" }, { "DD1.Title", "val" }, { "DD1.Title2", "other val" } });

		// act
		var res = sut.Invoke(
			contributorSession,
			new Dictionary<string, string>
			{
				{ "Identifier", asset.Value.Id.Value.ToString() },
				{ "DD1.Title", "val 2" },
				{ "DD1.Title3", "val" }
			});

		// assert
		res.ShouldSatisfyAllConditions(
			() => res.Value.Id.ShouldBe(res.Value.Id),
			() => res.Value.FindField("DD1", "Title").ToString().ShouldBe("val 2"),
			() => res.Value.FindField("DD1", "Title2").ToString().ShouldBe("other val"),
			() => res.Value.FindField("DD1", "Title3").ToString().ShouldBe("val"),
			() => res.Value.LastChangedByUserId.ShouldBe(contributorSession.UserId));
		var result = repos.Asset.Get(res.Value.Id);
		result.ShouldSatisfyAllConditions(
			() => result.Id.ShouldBe(res.Value.Id),
			() => result.FindField("DD1", "Title").ToString().ShouldBe("val 2"),
			() => result.FindField("DD1", "Title2").ToString().ShouldBe("other val"),
			() => result.FindField("DD1", "Title3").ToString().ShouldBe("val"),
			() => result.LastChangedByUserId.ShouldBe(contributorSession.UserId));
		var indexedFields = searchIndexSpy.WasIndexedWith.First().GetIndexableFields().ToList();
		indexedFields.Single(f => "id".Equals(f.Key, StringComparison.OrdinalIgnoreCase)).Value
			.ShouldBe(asset.Value.Id.Value.ToString());
		indexedFields.Single(f => "s_search_title".Equals(f.Key, StringComparison.OrdinalIgnoreCase)).Value
			.ShouldBe("val 2");
	}
}