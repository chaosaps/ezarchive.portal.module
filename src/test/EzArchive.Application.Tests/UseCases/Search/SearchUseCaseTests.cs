using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Chaos.Portal.Core.Indexing;
using Chaos.Portal.Core.Indexing.Solr;
using Chaos.Portal.Core.Indexing.Solr.Response;
using EzArchive.Application.UseCases.Search;
using EzArchive.Data.InMemory;
using EzArchive.Data.Portal;
using EzArchive.Domain;
using NodaTime;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Search;

[TestFixture]
public class SearchUseCaseTests
{
	private InMemoryRepositories repos;
	private Session session;
	private SearchIndexSpy searchIndexSpy;
	private User user;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		searchIndexSpy = new SearchIndexSpy();


		repos.SearchDefinition.Add(
			new SearchFieldDefinition("SearchField", new[] { "default_sort_field" }, "string", true, true));

		user = repos.User.Set(new User(null, "email", "name", "Contributor", null, null));

		session = repos.Session.Create(user.Id);
	}

	private SearchUseCase MakeSystemUnderTest(bool permissionsAreHandledThroughProjects = false) =>
		new SearchUseCase(
			new SearchRepository(
				searchIndexSpy,
				repos.SearchDefinition,
				repos.DataDefinition,
				repos.Label),
			repos.SearchDefinition,
			repos.Project,
			"default_sort_field desc",
			permissionsAreHandledThroughProjects);

	[Test]
	public async Task NoSearchResults_ReturnEmptyPage()
	{
		// arrange
		var sut = MakeSystemUnderTest();
		searchIndexSpy.QueryResponse = new EmptyResponse();

		// act
		var result = await sut.InvokeAsync(
			session,
			SearchQuery.AllQuery(),
			new SearchQuery(),
			new List<string>(),
			null,
			null,
			new Between(),
			new PageRequest(0, 10));

		// assert
		result.Value.FoundCount.ShouldBe(0u);
		result.Value.StartIndex.ShouldBe(0u);
		result.Value.Results.ShouldBeEmpty();
	}

	[Test]
	public async Task HasSearchResults_ReturnPage()
	{
		// arrange
		var sut = MakeSystemUnderTest();
		searchIndexSpy.QueryResponse = new Response(
			new FlexibleResult(
				"CD5F261A-0F9C-4D59-BC91-2A10FE84D4D1",
				1f,
				new Dictionary<string, IList<string>>
				{
					{ "s_document_typeid", new[] { "1" } }
				}));

		// act
		var result = await sut.InvokeAsync(
			session,
			SearchQuery.AllQuery(),
			new SearchQuery(),
			new List<string>(),
			null,
			null,
			new Between(),
			new PageRequest(0, 10));

		// assert
		result.Value.ShouldSatisfyAllConditions(
			() => result.Value.FoundCount.ShouldBe(1u),
			() => result.Value.StartIndex.ShouldBe(0u),
			() => result.Value.Results.ShouldNotBeEmpty(),
			() => result.Value.Results.First().Id.ShouldBe(new AssetId("CD5F261A-0F9C-4D59-BC91-2A10FE84D4D1")),
			() => result.Value.Results.First().TypeId.ShouldBe("1"));
	}

	[Test]
	public async Task QueryAll_CallIndexWithAllQuery()
	{
		// arrange
		var sut = MakeSystemUnderTest();
		searchIndexSpy.QueryResponse = new EmptyResponse();

		// act
		await sut.InvokeAsync(
			session,
			SearchQuery.AllQuery(),
			new SearchQuery(),
			new List<string>(),
			null,
			null,
			new Between(),
			new PageRequest(0, 10));

		// assert
		searchIndexSpy.WasCalledWithQuery.ToString()
			.ShouldBe(
				"fl=score, *&wt=xml&q=*:*&sort=default_sort_field desc&start=0&rows=10&fq=&qf=Fulltext&defType=edismax&facet=false");
	}

	[Test]
	public async Task HasFilter_CallIndexWithFilter()
	{
		// arrange
		var sut = MakeSystemUnderTest();
		searchIndexSpy.QueryResponse = new EmptyResponse();
		var filter = new SearchQuery();
		filter.AndQuery("{document}.TypeId:test");

		// act
		await sut.InvokeAsync(
			session,
			SearchQuery.AllQuery(),
			filter,
			new List<string>(),
			null,
			null,
			new Between(),
			new PageRequest(0, 10));

		// assert
		searchIndexSpy.WasCalledWithQuery.ToString()
			.ShouldBe(
				"fl=score, *&wt=xml&q=*:*&sort=default_sort_field desc&start=0&rows=10&fq=s_document_typeid:test&qf=Fulltext&defType=edismax&facet=false");
	}

	[Test]
	public async Task HasSort_CallIndexWithSort()
	{
		// arrange
		var sut = MakeSystemUnderTest();
		searchIndexSpy.QueryResponse = new EmptyResponse();

		// act
		await sut.InvokeAsync(
			session,
			SearchQuery.AllQuery(),
			new SearchQuery(),
			new List<string>(),
			"SearchField asc",
			null,
			new Between(),
			new PageRequest(0, 10));

		// assert
		searchIndexSpy.WasCalledWithQuery.ToString()
			.ShouldBe(
				"fl=score, *&wt=xml&q=*:*&sort=s_sort_searchfield asc&start=0&rows=10&fq=&qf=Fulltext&defType=edismax&facet=false");
	}

	[Test]
	public async Task HasTag_CallIndexFilter()
	{
		// arrange
		var sut = MakeSystemUnderTest();
		searchIndexSpy.QueryResponse = new EmptyResponse();

		// act
		await sut.InvokeAsync(
			session,
			SearchQuery.AllQuery(),
			new SearchQuery(),
			new List<string>(),
			null,
			"MyTag",
			new Between(),
			new PageRequest(0, 10));

		// assert
		searchIndexSpy.WasCalledWithQuery.ToString()
			.ShouldBe(
				"fl=score, *&wt=xml&q=*:*&sort=default_sort_field desc&start=0&rows=10&fq=sm_tags:\"MyTag\"&qf=Fulltext&defType=edismax&facet=false");
	}

	[Test]
	public async Task PermissionsAreHandledThroughProjects_CallIndexFilter()
	{
		// arrange
		repos.Project.Add(new Project(
			new ProjectId(1),
			"project name",
			new List<Label> { new Label(new LabelId(1), "label", 1) },
			new[] { user }));
		repos.Project.Add(new Project(
			new ProjectId(2),
			"project name 2",
			new List<Label> { new Label(new LabelId(2), "label", 1) },
			new[] { user })
		);
		var sut = MakeSystemUnderTest(true);
		searchIndexSpy.QueryResponse = new EmptyResponse();

		// act
		await sut.InvokeAsync(
			session,
			SearchQuery.AllQuery(),
			new SearchQuery(),
			new List<string>(),
			null,
			null,
			new Between(),
			new PageRequest(0, 10));

		// assert
		searchIndexSpy.WasCalledWithQuery.ToString()
			.ShouldBe(
				"fl=score, *&wt=xml&q=*:*&sort=default_sort_field desc&start=0&rows=10&fq=sm_projectids:(1 OR 2)&qf=Fulltext&defType=edismax&facet=false");
	}

	[Test]
	public async Task HasFacets_CallIndexFilter()
	{
		// arrange
		var sut = MakeSystemUnderTest();
		searchIndexSpy.QueryResponse = new EmptyResponse();

		// act
		await sut.InvokeAsync(
			session,
			SearchQuery.AllQuery(),
			new SearchQuery(),
			new List<string> { "{document}.TypeId:1" },
			null,
			null,
			new Between(),
			new PageRequest(0, 10));

		// assert
		searchIndexSpy.WasCalledWithQuery.ToString()
			.ShouldBe(
				"fl=score, *&wt=xml&q=*:*&sort=default_sort_field desc&start=0&rows=10&fq=s_document_typeid:\"1\"&qf=Fulltext&defType=edismax&facet=false");
	}

	[Test]
	public async Task HasBetween_CallIndexFilter()
	{
		// arrange
		var sut = MakeSystemUnderTest();
		searchIndexSpy.QueryResponse = new EmptyResponse();

		// act
		await sut.InvokeAsync(
			session,
			SearchQuery.AllQuery(),
			new SearchQuery(),
			new List<string>(),
			null,
			null,
			new Between(Instant.MinValue, Instant.MaxValue),
			new PageRequest(0, 10));

		// assert
		searchIndexSpy.WasCalledWithQuery.ToString()
			.ShouldBe(
				"fl=score, *&wt=xml&q=*:*&sort=default_sort_field desc&start=0&rows=10&fq=((d_search_udsendelsesdato:[-9998-01-01T00:00:00Z TO 9999-12-31T23:59:59Z])OR(d_programoversigt_larm_metadata_startdate:[-9998-01-01T00:00:00Z TO 9999-12-31T23:59:59Z])OR(d_programoversigt_larm_metadata_enddate:[-9998-01-01T00:00:00Z TO 9999-12-31T23:59:59Z]))OR((d_programoversigt_larm_metadata_startdate:[* TO -9998-01-01T00:00:00Z])AND(d_programoversigt_larm_metadata_enddate:[9999-12-31T23:59:59Z TO *]))&qf=Fulltext&defType=edismax&facet=false");
	}

	[Test]
	public async Task HasPaging_CallIndexFilter()
	{
		// arrange
		var sut = MakeSystemUnderTest();
		searchIndexSpy.QueryResponse = new EmptyResponse();

		// act
		await sut.InvokeAsync(
			session,
			SearchQuery.AllQuery(),
			new SearchQuery(),
			new List<string>(),
			null,
			null,
			new Between(),
			new PageRequest(1, 20));

		// assert
		searchIndexSpy.WasCalledWithQuery.ToString()
			.ShouldBe(
				"fl=score, *&wt=xml&q=*:*&sort=default_sort_field desc&start=20&rows=20&fq=&qf=Fulltext&defType=edismax&facet=false");
	}


	public class SearchIndexSpy : IIndex
	{
		public IIndexResponse<FlexibleResult> Query(IQuery query)
		{
			WasCalledWithQuery = query;
			return QueryResponse;
		}

		public Task<IIndexResponse<FlexibleResult>> QueryAsync(IQuery query)
		{
			WasCalledWithQuery = query;
			return Task.FromResult(QueryResponse);
		}

		public Task IndexAsync(IEnumerable<IIndexable> indexables)
		{
			WasIndexedWith = indexables;
			return Task.CompletedTask;
		}

		public Task IndexAsync(IIndexable indexable)
		{
			WasIndexedWith = new[] { indexable };
			return Task.CompletedTask;
		}

		public IIndexResponse<FlexibleResult> QueryResponse { get; set; }

		public IQuery WasCalledWithQuery { get; set; }

		public IEnumerable<IIndexable> WasIndexedWith { get; set; }

		public bool WasDeleted { get; set; }

		public void Index(IIndexable indexable) =>
			WasIndexedWith = new[] { indexable };

		public void Index(IEnumerable<IIndexable> indexables) =>
			WasIndexedWith = indexables;

		public void Commit(bool isSoftCommit = false)
		{
			throw new NotImplementedException();
		}

		public void Optimize()
		{
			throw new NotImplementedException();
		}

		public void Delete()
		{
			WasDeleted = true;
		}

		public void Delete(string uniqueIdentifier)
		{
			WasDeleted = true;
		}
	}

	public class Response : IIndexResponse<FlexibleResult>
	{
		public Header Header { get; }
		public FacetResponse FacetResponse { get; }
		public IList<IQueryResultGroup<FlexibleResult>> QueryResultGroups { get; }
		public IQueryResult<FlexibleResult> QueryResult => QueryResultGroups[0].Groups[0];

		public Response(params FlexibleResult[] results)
		{
			QueryResultGroups = new List<IQueryResultGroup<FlexibleResult>>
			{
				new QueryResultGroup<FlexibleResult>(
					"name",
					1u,
					new List<IQueryResult<FlexibleResult>>
					{
						new QueryResult<FlexibleResult>(results) { FoundCount = (uint)results.Length }
					})
			};
		}
	}

	public class EmptyResponse : IIndexResponse<FlexibleResult>
	{
		public Header Header { get; }
		public FacetResponse FacetResponse { get; }

		public IList<IQueryResultGroup<FlexibleResult>> QueryResultGroups =>
			new List<IQueryResultGroup<FlexibleResult>>
			{
				new QueryResultGroup<FlexibleResult>(
					"name",
					0,
					new List<IQueryResult<FlexibleResult>>
					{
						new QueryResult<FlexibleResult>(new FlexibleResult[0])
					})
			};

		public IQueryResult<FlexibleResult> QueryResult => QueryResultGroups[0].Groups[0];
	}
}