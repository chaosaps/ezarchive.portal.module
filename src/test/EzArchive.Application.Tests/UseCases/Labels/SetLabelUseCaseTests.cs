using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Labels;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Labels;

[TestFixture]
public class SetLabelUseCaseTests
{
	private SetLabelUseCase sut;
	private InMemoryRepositories repos;
	private User user;
	private Session session;
	private Project project;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		sut = new SetLabelUseCase(repos.Project, repos.Label);

		user = repos.User.Set(
			new User(new UserId("b8828194-d434-465f-8dc3-3a45ae4467aa"), "email", "name", "User", null, null));

		session = repos.Session.Create(user.Id);
		project = repos.Project.Set(null, "p");
	}

	[Test]
	public void NotMemberOfProject_Throw()
	{
		// act
		void Act() => sut.Invoke(session, project.Id, null, "l1");

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void IsMemberOfProject_AssociateLabelAndProject()
	{
		// arrange
		repos.Project.AddUser(project.Id, user.Id);

		// act
		var result = sut.Invoke(session, project.Id, null, "l1");

		// assert
		result.Value.Id.Value.ShouldBe(1u);
		repos.Label.Get(project.Id).ShouldNotBeEmpty();
	}
}