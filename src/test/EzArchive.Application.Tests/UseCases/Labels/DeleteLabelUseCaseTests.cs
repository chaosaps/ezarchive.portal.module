using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Labels;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Labels;

[TestFixture]
public class DeleteLabelUseCaseTests
{
	private DeleteLabelUseCase sut;
	private InMemoryRepositories repos;
	private User user;
	private Session session;
	private Project project;
	private Label label;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		sut = new DeleteLabelUseCase(repos.Project, repos.Label);

		user = repos.User.Set(
			new User(new UserId("b8828194-d434-465f-8dc3-3a45ae4467aa"), "email", "name", "User", null, null));

		session = repos.Session.Create(user.Id);
		project = repos.Project.Set(null, "p");
		label = repos.Label.Set(project.Id, null, "l");
	}

	[Test]
	public void NotMemberOfProject_Throw()
	{
		// act
		void Act() => sut.Invoke(session, label.Id);

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void IsMemberOfProject_DeleteLabelFromProject()
	{
		// arrange
		repos.Project.AddUser(project.Id, session.UserId);

		// act
		var result = sut.Invoke(session, label.Id);

		// assert
		result.HasError.ShouldBeFalse();
		repos.Label.Get(project.Id).ShouldBeEmpty();
	}
}