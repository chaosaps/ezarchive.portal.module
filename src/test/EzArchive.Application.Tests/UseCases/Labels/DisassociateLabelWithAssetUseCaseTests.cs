using System.Linq;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.Tests.UseCases.Search;
using EzArchive.Application.UseCases.Labels;
using EzArchive.Data.InMemory;
using EzArchive.Data.Portal;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Labels;

[TestFixture]
public class DisassociateLabelWithAssetUseCaseTests
{
	private DisassociateLabelWithAssetUseCase sut;
	private InMemoryRepositories repos;
	private User user;
	private Session session;
	private Project project;
	private Label label;
	private Asset asset;
	private SearchUseCaseTests.SearchIndexSpy searchIndexSpy;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		searchIndexSpy = new SearchUseCaseTests.SearchIndexSpy();
		sut = new DisassociateLabelWithAssetUseCase(
			repos.Project,
			repos.Label,
			repos.Asset,
			new SearchRepository(
				searchIndexSpy, repos.SearchDefinition, repos.DataDefinition, repos.Label));

		user = repos.User.Set(
			new User(new UserId("b8828194-d434-465f-8dc3-3a45ae4467aa"), "email", "name", "User", null, null));

		session = repos.Session.Create(user.Id);
		project = repos.Project.Set(null, "p");
		label = repos.Label.Set(project.Id, null, "l");
		asset = repos.Asset.Set(
			new Asset(
				AssetId.New(),
				new Asset._Type(1),
				user.Id,
				true,
				new string[0],
				new Asset._Data[0],
				new Asset._AnnotationGroup[0],
				new Asset._FileReference[0]));
	}

	[Test]
	public void UserDoesNotHaveAccessToProject_Throw()
	{
		// act
		void Act() => sut.Invoke(session, label.Id, asset.Id);

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void UserIsMemberOfProject_MakeAssociation()
	{
		// arrange
		repos.Project.AddUser(project.Id, user.Id);
		repos.Label.SetAssociation(label.Id, asset.Id);

		// act
		var result = sut.Invoke(session, label.Id, asset.Id);

		// assert
		result.HasError.ShouldBeFalse();
		repos.Label.Get(asset.Id).ShouldBeEmpty();
		searchIndexSpy.WasIndexedWith
			.First()
			.GetIndexableFields()
			.Where(f => f.Key == "sm_labels")
			.ShouldBeEmpty();
	}
}