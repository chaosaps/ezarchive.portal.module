using System.Threading.Tasks;
using System.Xml.Linq;
using EzArchive.Application.UseCases.Formats;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Formats
{
	[TestFixture]
	public class GetFormatByIdUseCaseTests
	{
		private GetFormatByIdUseCase sut;
		private InMemoryRepositories repos;

		[SetUp]
		public void SetUp()
		{
			repos = new InMemoryRepositories();
			sut = new GetFormatByIdUseCase(repos.Format);
		}

		[Test]
		public async Task FormatExists_Return()
		{
			// arrange
			var formatId = repos.McmRepositories.Format.Create(1, "name", new XDocument(), "image/png", ".png");

			// act
			var result = await sut.InvokeAsync(new FormatId(formatId));

			// assert
			result.Value.Id.Value.ShouldBe(formatId);
			result.Value.Extension.ShouldBe(".png");
		}
	}
}