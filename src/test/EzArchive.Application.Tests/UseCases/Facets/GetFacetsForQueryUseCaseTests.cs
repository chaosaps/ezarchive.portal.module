using System.Collections.Generic;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Facets;
using EzArchive.Data.InMemory;
using EzArchive.Data.Portal;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Facets;

[TestFixture]
public class GetFacetsForQueryUseCaseTests
{
	private InMemoryRepositories repos;
	private Session session;
	private IndexSpy indexSpy;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		indexSpy = new IndexSpy();

		repos.SearchDefinition.Add(
			new SearchFieldDefinition("SearchField", new[] { "default_sort_field" }, "string", true, true));

		var user = repos.User.Set(new User(null, "email", "name", "Contributor", null, null));

		session = repos.Session.Create(user.Id);
	}

	private GetFacetsForQueryUseCase MakeSystemUnderTest(bool permissionsAreHandledThroughProjects = false) =>
		new GetFacetsForQueryUseCase(
			new SearchRepository(
				indexSpy,
				repos.SearchDefinition,
				repos.DataDefinition,
				repos.Label),
			repos.Project,
			permissionsAreHandledThroughProjects,
			new[]
			{
				new FacetSetting("Header", "Left side", new[]
				{
					new FacetSetting.FacetSpecification("{document}.TypeId", FacetSetting.FacetType.Field),
				}),
			});

	[Test]
	public async Task NoSearchResults_ReturnEmptyPage()
	{
		// arrange
		var sut = MakeSystemUnderTest();
		indexSpy.QueryResponse = new IndexSpy.EmptyResponse();

		// act
		await sut.InvokeAsync(
			session,
			SearchQuery.AllQuery(),
			new SearchQuery(),
			new List<string>(),
			null,
			new Between(),
			null);

		// assert
		indexSpy.WasCalledWithQuery.ToString()
			.ShouldBe(
				"fl=score, *&wt=xml&q=*:*&sort=&start=0&rows=0&fq=&qf=Fulltext&defType=edismax&facet=true&facet.mincount=0&facet.field=s_document_typeid");
	}

	[Test]
	public async Task QueryAll_CallIndexWithAllQuery()
	{
		// arrange
		var sut = MakeSystemUnderTest();
		indexSpy.QueryResponse = new IndexSpy.Response();

		// act
		await sut.InvokeAsync(
			session,
			SearchQuery.AllQuery(),
			new SearchQuery(),
			new List<string>(),
			null,
			new Between(),
			null);

		// assert
		indexSpy.WasCalledWithQuery.ToString()
			.ShouldBe(
				"fl=score, *&wt=xml&q=*:*&sort=&start=0&rows=0&fq=&qf=Fulltext&defType=edismax&facet=true&facet.mincount=0&facet.field=s_document_typeid");
	}
}