using System.Collections.Generic;
using System.Threading.Tasks;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.Tests.UseCases.Search;
using EzArchive.Application.UseCases.Annotations;
using EzArchive.Data.InMemory;
using EzArchive.Data.Portal;
using EzArchive.Domain;
using EzArchive.Domain.Values;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Annotations;

[TestFixture]
public class DeleteAnnotationUseCaseTests
{
	private InMemoryRepositories repos;
	private DeleteAnnotationUseCase sut;
	private Session userSession;
	private Session contributorSession;
	private Session adminSession;
	private Asset asset;
	private SearchUseCaseTests.SearchIndexSpy searchIndexSpy;
	private AnnotationId annotationsId;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		searchIndexSpy = new SearchUseCaseTests.SearchIndexSpy();
		sut = new DeleteAnnotationUseCase(
			repos.Asset,
			repos.User,
			new SearchRepository(
				searchIndexSpy,
				repos.SearchDefinition,
				repos.DataDefinition,
				repos.Label)
		);
		var user = repos.User.Set(new User(null, "User", "name", "User", null, null));
		var contributor = repos.User.Set(new User(null, "Contributor", "name", "Contributor", null, null));
		var admin = repos.User.Set(new User(null, "Administrator", "name", "Administrator", null, null));
		userSession = repos.Session.Create(user.Id);
		contributorSession = repos.Session.Create(contributor.Id);
		adminSession = repos.Session.Create(admin.Id);
		annotationsId = new AnnotationId(repos.AnnotationMetadataSchemaId.Value);
		asset = repos.Asset.Set(new Asset(
			AssetId.New(),
			new Asset._Type(1),
			admin.Id,
			true,
			new string[0],
			new Asset._Data[0],
			new List<Asset._AnnotationGroup>
			{
				new Asset._AnnotationGroup(
					"Annotations",
					repos.AnnotationMetadataSchemaId.Value,
					new List<IDictionary<string, Value>>
					{
						new Dictionary<string, Value>
						{
							{ "Identifier", new StringValue(annotationsId.ToString()) },
							{ "a1", new BooleanValue(true) }
						}
					})
			},
			new Asset._FileReference[0]));
		repos.File.Set(asset.Id, null, "filename.ext", "filename.ext", "path/to/file", 1);
	}

	[Test]
	public void UserIsUser_Throw()
	{
		// act
		async Task Act() => await sut.InvokeAsync(userSession, asset.Id, annotationsId);

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public async Task AssetDoesntExist_Error()
	{
		// act
		var result = await sut.InvokeAsync(contributorSession, AssetId.New(), annotationsId);

		// assert
		result.ErrorCode.ShouldBe(ErrorCode.AssetNotFound);
	}

	[Test]
	public async Task AnnotationDoesNotExist_Error()
	{
		// act
		var result = await sut.InvokeAsync(contributorSession, asset.Id, AnnotationId.New());

		// assert
		result.ErrorCode.ShouldBe(ErrorCode.AnnotationNotFound);
	}

	[Test]
	public async Task AssetAndAnnotationExists_Return()
	{
		// act
		var result = await sut.InvokeAsync(contributorSession, asset.Id, annotationsId);

		// assert
		result.HasError.ShouldBeFalse();
		repos.Asset.Get(asset.Id).GetAnnotation(annotationsId).ShouldBeNull();
		searchIndexSpy.WasIndexedWith.ShouldNotBeEmpty();
	}
}