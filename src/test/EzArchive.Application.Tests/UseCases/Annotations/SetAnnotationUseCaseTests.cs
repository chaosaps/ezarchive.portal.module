using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Annotations;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using EzArchive.Domain.Values;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Annotations;

[TestFixture]
public class SetAnnotationUseCaseTests
{
	private SetAnnotationUseCase sut;
	private InMemoryRepositories repos;
	private Session userSession;
	private Session contributorSession;
	private Asset asset;
	private DataDefinition annotationDefinition;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		sut = new SetAnnotationUseCase(repos.User, repos.Asset, repos.DataDefinition);

		var contributor = repos.User.Set(
			new User(
				null,
				"contributor",
				"contributor",
				"Contributor",
				null,
				"{\"eduPersonPrincipalName\":[\"eduPersonPrincipalName@domain.dk\"],\"eduPersonPrimaryAffiliation\":[\"student\"],\"eduPersonScopedAffiliation\":[\"student@domain.dk\"],\"schacHomeOrganization\":[\"domain.dk\"],\"eduPersonTargetedID\":[\"WAYF-DK-eduPersonTargetedID\"],\"groups\":[\"groups-domain.dk\",\"users\",\"members\"]}"));

		var user = repos.User.Set(
			new User(
				null,
				"user",
				"user",
				"User",
				null,
				"{\"eduPersonPrincipalName\":[\"eduPersonPrincipalName@domain.dk\"],\"eduPersonPrimaryAffiliation\":[\"student\"],\"eduPersonScopedAffiliation\":[\"student@domain.dk\"],\"schacHomeOrganization\":[\"domain.dk\"],\"eduPersonTargetedID\":[\"WAYF-DK-eduPersonTargetedID\"],\"groups\":[\"groups-domain.dk\",\"users\",\"members\"]}"));

		userSession = repos.Session.Create(user.Id);
		contributorSession = repos.Session.Create(contributor.Id);

		asset = repos.Asset.Set(new Asset(
			AssetId.New(),
			new Asset._Type(1),
			contributor.Id,
			true,
			new string[0],
			new Asset._Data[0],
			new Asset._AnnotationGroup[0],
			new Asset._FileReference[0]));

		annotationDefinition = repos.DataDefinition.GetAnnotations().First();
	}

	[Test]
	public async Task UserIsUser_ThrowError()
	{
		// act
		Task Act() => sut.InvokeAsync(
			userSession,
			asset.Id,
			new DataDefinition.__Id(annotationDefinition.Id),
			new Dictionary<string, string>(),
			AnnotationVisibility.Public);

		// assert
		await Should.ThrowAsync<InsufficientPermissionsException>(Act);
	}

	[Test]
	public async Task NoIdentifierProvided_CreateNewAnnotation()
	{
		// arrange


		// act
		var result = await sut.InvokeAsync(
			contributorSession,
			asset.Id,
			new DataDefinition.__Id(annotationDefinition.Id),
			new Dictionary<string, string> { { "a1", "val" } },
			AnnotationVisibility.Public);

		// assert
		result.Value.ShouldNotBeNull();
		repos.Asset.Get(asset.Id).AnnotationsGroups.ShouldNotBeEmpty();
		repos.Asset.Get(asset.Id).AnnotationsGroups[0].Annotations.ShouldNotBeEmpty();
		repos.Asset.Get(asset.Id).AnnotationsGroups[0].Annotations[0]["a1"].ToString().ShouldBe("val");
	}

	[Test]
	public async Task IdentifierProvidedAndAnnotationExists_UpdateExistingAnnotation()
	{
		// arrange
		var annotationId = new Guid("387ef0f7-8517-46e8-8d34-6dda7beaa780").ToString();
		asset.SetAnnotation(
			repos.DataDefinition.GetAnnotations().First(),
			new Dictionary<string, Value>
			{
				{ "Identifier", new StringValue(annotationId) },
				{ "a1", new StringValue("origianl") },
			});
		repos.Asset.Set(asset);

		// act
		var result = await sut.InvokeAsync(
			contributorSession,
			asset.Id,
			new DataDefinition.__Id(annotationDefinition.Id),
			new Dictionary<string, string> { { "Identifier", annotationId }, { "a1", "updated" } },
			AnnotationVisibility.Public);

		// assert
		result.Value.ShouldNotBeNull();
		repos.Asset.Get(asset.Id).AnnotationsGroups.ShouldNotBeEmpty();
		repos.Asset.Get(asset.Id).AnnotationsGroups[0].Annotations[0]["a1"].ToString().ShouldBe("updated");
	}
}