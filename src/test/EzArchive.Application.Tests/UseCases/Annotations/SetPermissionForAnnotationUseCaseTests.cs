using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.Errors;
using EzArchive.Application.UseCases.Annotations;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using EzArchive.Domain.Values;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Annotations;

[TestFixture]
public class SetPermissionForAnnotationUseCaseTests
{
	private InMemoryRepositories repos;
	private SetPermissionForAnnotationUseCase sut;
	private Session userSession;
	private Session contributorSession;
	private AnnotationId annotationId;
	private Asset asset;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		sut = new SetPermissionForAnnotationUseCase(repos.User, repos.Asset);

		var contributor = repos.User.Set(
			new User(
				null,
				"contributor",
				"contributor",
				"Contributor",
				null,
				"{\"eduPersonPrincipalName\":[\"eduPersonPrincipalName@domain.dk\"],\"eduPersonPrimaryAffiliation\":[\"student\"],\"eduPersonScopedAffiliation\":[\"student@domain.dk\"],\"schacHomeOrganization\":[\"domain.dk\"],\"eduPersonTargetedID\":[\"WAYF-DK-eduPersonTargetedID\"],\"groups\":[\"groups-domain.dk\",\"users\",\"members\"]}"));

		var user = repos.User.Set(
			new User(
				null,
				"user",
				"user",
				"User",
				null,
				"{\"eduPersonPrincipalName\":[\"eduPersonPrincipalName@domain.dk\"],\"eduPersonPrimaryAffiliation\":[\"student\"],\"eduPersonScopedAffiliation\":[\"student@domain.dk\"],\"schacHomeOrganization\":[\"domain.dk\"],\"eduPersonTargetedID\":[\"WAYF-DK-eduPersonTargetedID\"],\"groups\":[\"groups-domain.dk\",\"users\",\"members\"]}"));

		userSession = repos.Session.Create(user.Id);
		contributorSession = repos.Session.Create(contributor.Id);

		asset = repos.Asset.Set(new Asset(
			AssetId.New(),
			new Asset._Type(1),
			contributor.Id,
			true,
			new string[0],
			new Asset._Data[0],
			new Asset._AnnotationGroup[0],
			new Asset._FileReference[0]));

		annotationId = AnnotationId.New();
		asset.SetAnnotation(
			repos.DataDefinition.GetAnnotations().First(),
			new Dictionary<string, Value>
			{
				{ "Identifier", new StringValue(annotationId.ToString()) },
				{ "a1", new StringValue("v1") },
			});
		repos.Asset.Set(asset);
	}


	[Test]
	public async Task UserIsUser_ThrowError()
	{
		// act
		Task Act() => sut.InvokeAsync(userSession, asset.Id, annotationId, AnnotationPermission.User);

		// assert
		await Should.ThrowAsync<InsufficientPermissionsException>(Act);
	}

	[Test]
	public async Task AssetDoesntExist_Throw()
	{
		// act
		Task Act() => sut.InvokeAsync(contributorSession, AssetId.New(), annotationId, AnnotationPermission.User);

		// assert
		await Should.ThrowAsync<DataNotFoundException>(Act);
	}

	[Test]
	public async Task AnnotationDoesntExist_Throw()
	{
		// act
		Task Act() => sut.InvokeAsync(contributorSession, asset.Id, AnnotationId.New(), AnnotationPermission.User);

		// assert
		await Should.ThrowAsync<DataNotFoundException>(Act);
	}

	[Test]
	public async Task AnnotationHasNoOwner_SetPermission()
	{
		// act
		await sut.InvokeAsync(contributorSession, asset.Id, annotationId, AnnotationPermission.Contributor);

		// assert
		var updated = repos.Asset.Get(asset.Id);
		var annotation = updated.GetAnnotation(annotationId);
	}
}