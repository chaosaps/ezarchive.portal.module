using System.Threading.Tasks;
using EzArchive.Application.UseCases.Annotations;
using EzArchive.Data.InMemory;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Annotations;

[TestFixture]
public class GetAnnotationDefinitionUseCaseTests
{
	private InMemoryRepositories repos;
	private GetAnnotationDefinitionsUseCase sut;

	public GetAnnotationDefinitionUseCaseTests()
	{
		repos = new InMemoryRepositories();
		sut = new GetAnnotationDefinitionsUseCase(repos.DataDefinition);
	}

	[Test]
	public async Task AnnotationDefinitionsExist_ReturnList()
	{
		// act
		var result = await sut.InvokeAsync();

		// assert
		result.Value.ShouldNotBeEmpty();
	}
}