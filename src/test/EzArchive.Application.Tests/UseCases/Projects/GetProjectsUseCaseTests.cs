using System.Linq;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Projects;

[TestFixture]
public class GetProjectsUseCaseTests
{
	private InMemoryRepositories repos;
	private GetProjectsUseCase sut;
	private User user;
	private Session session;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		sut = new GetProjectsUseCase(repos.Project);

		user = repos.User.Set(
			new User(new UserId("b8828194-d434-465f-8dc3-3a45ae4467aa"), "email", "name", "User", null, null));

		session = repos.Session.Create(user.Id);

		repos.Project.Add(new Project(new ProjectId(1), "p", new Label[0], new[] { user }));
	}

	[Test]
	public void ReturnListOfProjects()
	{
		// act
		var result = sut.Invoke(session);

		// assert
		result.Value.First().Id.Value.ShouldBe(1u);
	}
}