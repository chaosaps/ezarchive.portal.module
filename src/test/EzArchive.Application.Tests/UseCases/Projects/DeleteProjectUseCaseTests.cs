using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Projects;

[TestFixture]
public class DeleteProjectUseCaseTests
{
	private DeleteProjectUseCase sut;
	private InMemoryRepositories repos;
	private User user;
	private Session session;
	private Project project;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		sut = new DeleteProjectUseCase(repos.Project);

		user = repos.User.Set(
			new User(new UserId("b8828194-d434-465f-8dc3-3a45ae4467aa"), "email", "name", "User", null, null));

		session = repos.Session.Create(user.Id);
		project = repos.Project.Set(null, "p");
	}

	[Test]
	public void UserNotMemberOfProject_Throw()
	{
		// act
		void Act() => sut.Invoke(session, project.Id);

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void ProjectHasLabels_Throw()
	{
		// arrange
		repos.Project.AddUser(project.Id, session.UserId);
		repos.Label.Set(project.Id, null, "l");

		// act
		void Act() => sut.Invoke(session, project.Id);

		// assert
		Should.Throw<ActionNotAllowedException>(Act);
	}

	[Test]
	public void ProjectHasNoLabels_DeleteInRepository()
	{
		// arrange
		repos.Project.AddUser(project.Id, session.UserId);

		// act
		var result = sut.Invoke(session, project.Id);

		// assert
		result.HasError.ShouldBeFalse();
		repos.Project.Get(project.Id).ShouldBeNull();
	}
}