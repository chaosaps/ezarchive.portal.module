using EzArchive.Application.UseCases.Projects;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Projects;

[TestFixture]
public class SetProjectsUseCaseTests
{
	private SetProjectsUseCase sut;
	private InMemoryRepositories repos;
	private User user;
	private Session session;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		sut = new SetProjectsUseCase(repos.Project);

		user = repos.User.Set(
			new User(new UserId("b8828194-d434-465f-8dc3-3a45ae4467aa"), "email", "name", "User", null, null));

		session = repos.Session.Create(user.Id);
	}

	[Test]
	public void NotGivingId_CreateNewProjectAndAssociateWithUser()
	{
		// arrange


		// act
		var result = sut.Invoke(session, null, "p");

		// assert
		result.Value.Id.Value.ShouldBe(1u);
		repos.Project.Get(session.UserId)[0].Id.ShouldBe(result.Value.Id);
		result.Value.Users.Count.ShouldBe(1);
	}
}