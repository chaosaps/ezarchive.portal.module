using System.Linq;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Projects;

[TestFixture]
public class AssociateProjectWithUserUseCaseTests
{
	private AssociateProjectWithUserUseCase sut;
	private InMemoryRepositories repos;
	private User otherUser;
	private Session session;
	private Project project;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		sut = new AssociateProjectWithUserUseCase(repos.Project);

		var user = repos.User.Set(
			new User(new UserId("b8828194-d434-465f-8dc3-3a45ae4467aa"), "email", "name", "User", null, null));

		otherUser = repos.User.Set(
			new User(new UserId("42c3fd18-7aea-4975-9957-524214da1b24"), "email1", "name", "User", null, null));

		session = repos.Session.Create(user.Id);
		project = repos.Project.Set(null, "p");
	}

	[Test]
	public void UserNotMemberOfProject_Throw()
	{
		// act
		void Act() => sut.Invoke(session, project.Id, otherUser.Id);

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void UserIsMember_AssociateOtherUserWithProject()
	{
		// arrange
		repos.Project.AddUser(project.Id, session.UserId);

		// act
		var result = sut.Invoke(session, project.Id, otherUser.Id);

		// assert
		result.HasError.ShouldBeFalse();
		repos.Project.Get(otherUser.Id).ShouldNotBeEmpty();
		repos.Project.Get(otherUser.Id).First().Id.ShouldBe(project.Id);
	}
}