using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Projects;

[TestFixture]
public class DisassociateProjectWithUserUseCaseTests
{
	private DisassociateProjectWithUserUseCase sut;
	private InMemoryRepositories repos;
	private User otherUser;
	private Session session;
	private Project project;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		sut = new DisassociateProjectWithUserUseCase(repos.Project);

		var user = repos.User.Set(
			new User(new UserId("b8828194-d434-465f-8dc3-3a45ae4467aa"), "email", "name", "User", null, null));

		otherUser = repos.User.Set(
			new User(new UserId("42c3fd18-7aea-4975-9957-524214da1b24"), "email1", "name", "User", null, null));

		session = repos.Session.Create(user.Id);
		project = repos.Project.Set(null, "p");

		repos.Project.AddUser(project.Id, otherUser.Id);
	}

	[Test]
	public void UserNotMemberOfProject_Throw()
	{
		// act
		void Act() => sut.Invoke(session, project.Id, otherUser.Id);

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void UserIsMember_DisassociateOtherUserWithProject()
	{
		// arrange
		repos.Project.AddUser(project.Id, session.UserId);

		// act
		var result = sut.Invoke(session, project.Id, otherUser.Id);

		// assert
		result.HasError.ShouldBeFalse();
		repos.Project.Get(otherUser.Id).ShouldBeEmpty();
	}
}