using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Users;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Users;

[TestFixture]
public class DeleteUserUseCaseTests
{
	private InMemoryRepositories repos;
	private DeleteUserUseCase sut;
	private Session adminSession;
	private Session contributorSession;
	private Session userSession;
	private User existingUser;
	private UserId anonymousUserId = new UserId("75fc6081-03d8-4e62-9247-51f9f81c1cbf");

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		repos.User.Set(
			new User(anonymousUserId, "anonymous", "name", "User", null, null));
		var admin = repos.User.Set(new User(null, "email 1", "name", "Administrator", null, null));
		var contributor = repos.User.Set(new User(null, "email 2", "name", "Contributor", null, null));
		var user = repos.User.Set(new User(null, "email 3", "name", "User", null, null));
		existingUser = repos.User.Set(new User(null, "email 4", "name", "User", null, null));

		adminSession = repos.Session.Create(admin.Id);
		contributorSession = repos.Session.Create(contributor.Id);
		userSession = repos.Session.Create(user.Id);

		sut = new DeleteUserUseCase(repos.User, repos.Project);
	}

	[Test]
	public void CurrentUserIsUser_Throw()
	{
		// act
		void Act() => sut.Invoke(userSession, existingUser.Id);

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void CurrentUserIsContributor_Throw()
	{
		// act
		void Act() => sut.Invoke(contributorSession, existingUser.Id);

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void CurrentUserIsAdministratorDeletingSelf_Throw()
	{
		// act
		void Act() => sut.Invoke(adminSession, adminSession.UserId);

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void CurrentUserIsAdministratorDeletingOtherUser_DeleteInRepository()
	{
		// act
		var result = sut.Invoke(adminSession, existingUser.Id);

		// assert
		result.HasError.ShouldBeFalse();
		Should.Throw<UserNotFound>(() => repos.User.Get(existingUser.Id));
	}

	[Test]
	public void CurrentUserIsAdministratorDeletingOtherUser_RemovedFromProjects()
	{
		// arrange
		repos.Project.Add(new Project(new ProjectId(1), "project", new Label[0], new[] { existingUser }));

		// act
		var result = sut.Invoke(adminSession, existingUser.Id);

		// assert
		result.HasError.ShouldBeFalse();
		Should.Throw<UserNotFound>(() => repos.User.Get(existingUser.Id));
		repos.Project.Get(existingUser.Id).ShouldBeEmpty();
	}
}