using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Users;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Users;

[TestFixture]
public class SetUserUseCaseTests
{
	private InMemoryRepositories repos;
	private SetUserUseCase sut;
	private Session adminSession;
	private Session contributorSession;
	private Session userSession;
	private User existingUser;
	private UserId anonymousUserId = new UserId("75fc6081-03d8-4e62-9247-51f9f81c1cbf");

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		repos.User.Set(
			new User(anonymousUserId, "anonymous", "name", "User", null, null));
		var admin = repos.User.Set(new User(null, "email 1", "name", "Administrator", null, null));
		var contributor = repos.User.Set(new User(null, "email 2", "name", "Contributor", null, null));
		var user = repos.User.Set(new User(null, "email 3", "name", "User", null, null));
		existingUser = repos.User.Set(new User(null, "email 4", "name", "User", null, null));

		adminSession = repos.Session.Create(admin.Id);
		contributorSession = repos.Session.Create(contributor.Id);
		userSession = repos.Session.Create(user.Id);

		sut = new SetUserUseCase(repos.User, anonymousUserId);
	}

	[Test]
	public void UserIsCreatingNewUser_Throw()
	{
		// act
		void Act() => sut.Invoke(userSession, new SetUserRequest(null, "new email", "name", null, null, null));

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void UserIsUpdatingHimSelf_UpdateInRepository()
	{
		// act
		var result = sut.Invoke(userSession,
			new SetUserRequest(userSession.UserId, "new email", "new name", null, "new preferences", null));

		// assert
		result.Value.Email.ShouldBe("new email");
		result.Value.Name.ShouldBe("new name");
		result.Value.Preferences.ShouldBe("new preferences");
		var storedUser = repos.User.Get(result.Value.Id);
		result.Value.Email.ShouldBe(storedUser.Email);
		result.Value.Name.ShouldBe(storedUser.Name);
		result.Value.Preferences.ShouldBe(storedUser.Preferences);
	}

	[Test]
	public void UserIsUpdatingOwnPassword_UpdateInRepository()
	{
		// act
		sut.Invoke(userSession,
			new SetUserRequest(userSession.UserId, "new email", "new name", null, "new preferences",
				new Password("newPassword12")));

			// assert
			repos.Password.Get(userSession.UserId.Value,
					new Password("newPassword12")
						.GenerateHash(userSession.UserId).ToString())
				.ShouldNotBeNull();
		}

	[Test]
	public void UserIsUpdatingOtherUser_Throw()
	{
		// act
		void Act() => sut.Invoke(userSession, new SetUserRequest(null, "new email", "name", null, null, null));

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void ContributorIsUpdatingOtherUser_Throw()
	{
		// act
		void Act() => sut.Invoke(contributorSession,
			new SetUserRequest(existingUser.Id, "new email", "name", null, null, null));

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void ContributorIsCreatingNewUser_Throw()
	{
		// act
		void Act() => sut.Invoke(contributorSession, new SetUserRequest(null, "new email", "name", null, null, null));

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}


	[Test]
	public void ContributorIsUpdatingHimSelf_UpdateInRepository()
	{
		// act
		var result = sut.Invoke(contributorSession,
			new SetUserRequest(contributorSession.UserId, "new email", "new name", null, "new preferences", null));

		// assert
		result.Value.Email.ShouldBe("new email");
		result.Value.Name.ShouldBe("new name");
		result.Value.Preferences.ShouldBe("new preferences");
		var storedUser = repos.User.Get(result.Value.Id);
		result.Value.Email.ShouldBe(storedUser.Email);
		result.Value.Name.ShouldBe(storedUser.Name);
		result.Value.Preferences.ShouldBe(storedUser.Preferences);
	}

	[Test]
	public void ContributorIsUpdatingOwnPermission_Throw()
	{
		// act
		void Act() => sut.Invoke(contributorSession,
			new SetUserRequest(contributorSession.UserId, "new email", "name", "Administrator", null, null));

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void UserIsUpdatingOwnPermission_Throw()
	{
		// act
		void Act() => sut.Invoke(userSession,
			new SetUserRequest(userSession.UserId, "new email", "name", "Administrator", null, null));

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void AdministratorIsUpdatingOwnPermission_Throw()
	{
		// act
		void Act() => sut.Invoke(adminSession,
			new SetUserRequest(adminSession.UserId, "new email", "name", "User", null, null));

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void AdministratorIsUpdatingAnonymousUser_Throw()
	{
		// act
		void Act() =>
			sut.Invoke(adminSession, new SetUserRequest(anonymousUserId, "new email", "name", null, null, null));

		// assert
		Should.Throw<InsufficientPermissionsException>(Act);
	}

	[Test]
	public void AdministratorIsCreatingNewUser_CreateInRepository()
	{
		// act
		var result = sut.Invoke(adminSession,
			new SetUserRequest(null, "new email", "name", "Administrator", "preferences", null));

		// assert
		result.Value.Email.ShouldBe("new email");
		result.Value.Name.ShouldBe("name");
		result.Value.Preferences.ShouldBe("preferences");
		var storedUser = repos.User.Get(result.Value.Id);
		result.Value.Email.ShouldBe(storedUser.Email);
		result.Value.Name.ShouldBe(storedUser.Name);
		result.Value.Preferences.ShouldBe(storedUser.Preferences);
	}

	[Test]
	public void AdministratorIsUpdatingOtherUser_UpdateInRepository()
	{
		// act
		var result = sut.Invoke(adminSession,
			new SetUserRequest(userSession.UserId, "email", "new name", "Administrator", "new preferences", null));

		// assert
		result.Value.Email.ShouldBe("email");
		result.Value.Name.ShouldBe("new name");
		result.Value.Permission.ShouldBe("Administrator");
		result.Value.Preferences.ShouldBe("new preferences");
		var storedUser = repos.User.Get(result.Value.Id);
		result.Value.Email.ShouldBe(storedUser.Email);
		result.Value.Name.ShouldBe(storedUser.Name);
		result.Value.Permission.ShouldBe(storedUser.Permission);
		result.Value.Preferences.ShouldBe(storedUser.Preferences);
	}
}
