using System.Threading.Tasks;
using EzArchive.Application.UseCases.Users;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Users;

[TestFixture]
public class SearchUsersUseCaseTests
{
	private InMemoryRepositories repos;
	private SearchUsersUseCase sut;
	private User user;
	private Session session;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		user = repos.User.Set(new User(null, "email 2", "name", "User", null, null));
		session = repos.Session.Create(user.Id);

		sut = new SearchUsersUseCase(repos.User);
	}

	[Ignore("Need to implement proper test of user search")]
	public async Task METHOD()
	{
		// arrange


		// act
		var result = await sut.InvokeAsync(session, "", 10);

		// assert
		result.Value.ShouldNotBeEmpty();
	}
}