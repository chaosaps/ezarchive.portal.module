using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Chaos.Portal.Core.Indexing;
using Chaos.Portal.Core.Indexing.Solr;
using Chaos.Portal.Core.Indexing.Solr.Response;

namespace EzArchive.Application.Tests.UseCases.Users;

public class UserIndexSpy : IIndex
{
	public IIndexResponse<FlexibleResult> Query(IQuery query)
	{
		throw new NotImplementedException();
	}

	public void Index(IIndexable indexable)
	{
		throw new System.NotImplementedException();
	}

	public void Index(IEnumerable<IIndexable> indexables)
	{
		throw new System.NotImplementedException();
	}

	public void Commit(bool isSoftCommit = false)
	{
		throw new System.NotImplementedException();
	}

	public void Optimize()
	{
		throw new System.NotImplementedException();
	}

	public void Delete()
	{
		throw new System.NotImplementedException();
	}

	public void Delete(string uniqueIdentifier)
	{
		throw new System.NotImplementedException();
	}

	public Task<IIndexResponse<FlexibleResult>> QueryAsync(IQuery query)
	{
		throw new System.NotImplementedException();
	}

	public Task IndexAsync(IEnumerable<IIndexable> indexables)
	{
		throw new System.NotImplementedException();
	}

	public Task IndexAsync(IIndexable indexable)
	{
		throw new System.NotImplementedException();
	}
}