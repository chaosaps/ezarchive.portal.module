using System.Linq;
using Chaos.Portal.Core.Exceptions;
using EzArchive.Application.UseCases.Users.GetAll;
using EzArchive.Data.InMemory;
using EzArchive.Domain;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests.UseCases.Users;

[TestFixture]
public class GetUsersUseCaseTests
{
	private InMemoryRepositories repos;
	private GetUsersUseCase sut;
	private User admin;
	private User user;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		repos.User.Set(
			new User(new UserId("75fc6081-03d8-4e62-9247-51f9f81c1cbf"), "anonymous", "name", "User", null, null));
		admin = repos.User.Set(new User(null, "email 1", "name", "Administrator", null, null));
		user = repos.User.Set(new User(null, "email 2", "name", "User", null, null));

		sut = new GetUsersUseCase(repos.User, new UserId("75fc6081-03d8-4e62-9247-51f9f81c1cbf"));
	}

	[Test]
	public void UserIsNotAdmin_ThrowError()
	{
		// act
		void Act() => sut.Invoke(user.Id);

		// assert
		Should.Throw<InsufficientPermissionsException>(Act)
			.Message.ShouldBe("This action requires Administrator permission");
	}

	[Test]
	public void ReturnListOfUsers()
	{
		// act
		var result = sut.Invoke(admin.Id).Value.ToList();

		// assert
		result[1].ShouldSatisfyAllConditions(
			() => result[1].Email.ShouldBe("email 1"));
		result[2].ShouldSatisfyAllConditions(
			() => result[2].Email.ShouldBe("email 2"));
	}
}