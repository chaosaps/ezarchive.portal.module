﻿using System;
using EzArchive.Application.Errors;
using EzArchive.Data.InMemory;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.Application.Tests;

[TestFixture, Category("unittest")]
public class SearchQueryTest
{
	private InMemoryRepositories repos;
	private IndexHelper indexHelper;

	[SetUp]
	public void SetUp()
	{
		repos = new InMemoryRepositories();
		indexHelper = new IndexHelper(repos.SearchDefinition, repos.DataDefinition);
	}

	[Test]
	public void Parse_GivenNUll_AllSearch()
	{
		// act
		var q = SearchQuery.Parse(null);

		// assert
		q.ToString().ShouldBe("*:*");
	}

	[Test]
	public void Parse_GivenAllQuery_AllSearch()
	{
		// act
		var q = SearchQuery.AllQuery();

		// assert
		q.ToString().ShouldBe("*:*");
	}

	[Test]
	public void Parse_GivenEmptyString_AllSearch()
	{
		// act
		var q = SearchQuery.Parse("");

		// assert
		q.ToString().ShouldBe("*:*");
	}

	[Test]
	public void Parse_GivenQueryWithNoColon_QueryDefaultField()
	{
		// act
		var q = SearchQuery.Parse("value");

		// assert
		q.ToString().ShouldBe("value");
	}

	[Test]
	public void Parse_GivenQueryWithPlus_EscapePlus()
	{
		// act
		var q = SearchQuery.Parse("+value");

		// assert
		q.ToString().ShouldBe("%2Bvalue");
	}

	[Test]
	public void Parse_GivenKeyButNoValue_Throw()
	{
		// act
		void Act() => SearchQuery.Parse("key:");

		// assert
		Should.Throw<SearchQueryParserException>(Act);
	}

	[Test]
	public void Parse_GivenKeyButNoValueWithSpaces_Throw()
	{
		// act
		void Act() => SearchQuery.Parse(" key:    ");

		// assert
		Should.Throw<SearchQueryParserException>(Act);
	}

	[Test]
	public void Parse_GivenLabel_Throw()
	{
		// act
		var q = SearchQuery.Parse("{Label}:EXACT p1.l1");

		// assert
		q.ToString().ShouldBe("sm_labels:\"p1.l1\"");
	}

	[Test]
	public void Parse_GivenOrList_Throw()
	{
		// act
		var q = SearchQuery.Parse("{Projects}:OR 1 2 3");

		// assert
		q.ToString().ShouldBe("sm_projectids:(1 OR 2 OR 3)");
	}

	[Test]
	public void Parse_GivenBetweenDateRange_ReturnQueryContainingDateRange()
	{
		// arrange
		repos.McmRepositories.MetadataSchema.Create("name", "{" +
		                                                    "\"Name\": \"DD\"," +
		                                                    "\"CanWrite\": [\"Administrator\"]," +
		                                                    "\"Type\": 0," +
		                                                    "\"TypeId\": 1," +
		                                                    "\"Description\": \"Create a new asset with metadata\"," +
		                                                    "\"Fields\": [" +
		                                                    "  {" +
		                                                    "    \"Id\": \"F1\"," +
		                                                    "    \"DisplayName\": \"F1\"," +
		                                                    "    \"Type\": \"Datetime\"," +
		                                                    "    \"Required\": false," +
		                                                    "    \"Placeholder\": \"\"," +
		                                                    "    \"Note\": null" +
		                                                    "  }]" +
		                                                    "}", Guid.Empty, new Guid());

		// act
		var q = SearchQuery.Parse("DD.F1:BETWEEN 2000-01-01T00:00:00Z 2001-12-31T23:59:59.999Z");

		// assert
		q.ToString().ShouldBe("d_dd_f1:[2000-01-01T00:00:00Z TO 2001-12-31T23:59:59.999Z]");
	}

	[Test]
	public void AddQuery_GivenMulipleAdds_CombineWithAND()
	{
		// arrange
		var q = SearchQuery.Parse("{Tags}:v1");
		q.AndQuery("{Label}:EXACT v2");

		// act
		q.AndQuery("{Label}:EXACT v3");

		// assert
		q.ToString().ShouldBe("(sm_tags:v1)AND(sm_labels:\"v2\")AND(sm_labels:\"v3\")");
	}

	[Test]
	public void AddQuery_GivenSingleAddQuery_CombineWithAND()
	{
		// act
		var q = SearchQuery.Parse("AND {Tags}:v1 {Label}:EXACT v2 {Label}:EXACT v3");

		// assert
		q.ToString().ShouldBe("(sm_tags:v1)AND(sm_labels:\"v2\")AND(sm_labels:\"v3\")");
	}

	[Test]
	public void AddQuery_GivenSingleAddQueryWithDefault_CombineWithAND()
	{
		// act
		var q = SearchQuery.Parse("AND {Document}:v1 v1 {Document}.TypeId:25 {Label}:EXACT v3");

		// assert
		q.ToString().ShouldBe("(v1 v1)AND(s_document_typeid:25)AND(sm_labels:\"v3\")");
	}

	[Test]
	public void AddQuery_GivenBetweenTwoDateFields_CreateQueryWithTwoRanges()
	{
		repos.McmRepositories.MetadataSchema.Create("name", "{" +
		                                                    "\"Name\": \"DD\"," +
		                                                    "\"CanWrite\": [\"Administrator\"]," +
		                                                    "\"Type\": 0," +
		                                                    "\"TypeId\": 1," +
		                                                    "\"Description\": \"Create a new asset with metadata\"," +
		                                                    "\"Fields\": [" +
		                                                    "  {" +
		                                                    "    \"Id\": \"F1\"," +
		                                                    "    \"DisplayName\": \"F1\"," +
		                                                    "    \"Type\": \"Datetime\"," +
		                                                    "    \"Required\": false," +
		                                                    "    \"Placeholder\": \"\"," +
		                                                    "    \"Note\": null" +
		                                                    "  },{" +
		                                                    "    \"Id\": \"F2\"," +
		                                                    "    \"DisplayName\": \"F2\"," +
		                                                    "    \"Type\": \"Datetime\"," +
		                                                    "    \"Required\": false," +
		                                                    "    \"Placeholder\": \"\"," +
		                                                    "    \"Note\": null" +
		                                                    "  }]" +
		                                                    "}", Guid.Empty, new Guid());
		// act
		var q = SearchQuery.Parse("AND DD.F1:BETWEEN * 2018-06-06T00:00:00Z DD.F2:BETWEEN 2018-06-06T00:00:00Z *");

		// assert
		q.ToString().ShouldBe("(d_dd_f1:[* TO 2018-06-06T00:00:00Z])AND(d_dd_f2:[2018-06-06T00:00:00Z TO *])");
	}
}