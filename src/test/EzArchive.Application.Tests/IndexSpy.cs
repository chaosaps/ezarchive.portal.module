using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Chaos.Portal.Core.Indexing;
using Chaos.Portal.Core.Indexing.Responses;
using Chaos.Portal.Core.Indexing.Solr;
using Chaos.Portal.Core.Indexing.Solr.Response;

namespace EzArchive.Application.Tests;

public class IndexSpy : IIndex
{
	public IIndexResponse<FlexibleResult> Query(IQuery query)
	{
		WasCalledWithQuery = query;
		return QueryResponse;
	}

	public Task<IIndexResponse<FlexibleResult>> QueryAsync(IQuery query)
	{
		WasCalledWithQuery = query;
		return Task.FromResult(QueryResponse);
	}

	public Task IndexAsync(IEnumerable<IIndexable> indexables)
	{
		throw new NotImplementedException();
	}

	public Task IndexAsync(IIndexable indexable)
	{
		throw new NotImplementedException();
	}

	public IIndexResponse<FlexibleResult> QueryResponse { get; set; }

	public IQuery WasCalledWithQuery { get; set; }

	public void Index(IIndexable indexable)
	{
		throw new NotImplementedException();
	}

	public void Index(IEnumerable<IIndexable> indexables)
	{
		throw new NotImplementedException();
	}

	public void Commit(bool isSoftCommit = false)
	{
		throw new NotImplementedException();
	}

	public void Optimize()
	{
		throw new NotImplementedException();
	}

	public void Delete()
	{
		throw new NotImplementedException();
	}

	public void Delete(string uniqueIdentifier)
	{
		throw new NotImplementedException();
	}

	public class EmptyResponse : IIndexResponse<FlexibleResult>
	{
		public Header Header { get; }
		public FacetResponse FacetResponse { get; }

		public IList<IQueryResultGroup<FlexibleResult>> QueryResultGroups =>
			new List<IQueryResultGroup<FlexibleResult>>
			{
				new QueryResultGroup<FlexibleResult>(
					"name",
					0,
					new List<IQueryResult<FlexibleResult>>
					{
						new QueryResult<FlexibleResult>(new FlexibleResult[0])
					})
			};

		public IQueryResult<FlexibleResult> QueryResult => QueryResultGroups[0].Groups[0];
	}


	public class Response : IIndexResponse<FlexibleResult>
	{
		public Header Header { get; }
		public FacetResponse FacetResponse { get; }
		public IList<IQueryResultGroup<FlexibleResult>> QueryResultGroups { get; }
		public IQueryResult<FlexibleResult> QueryResult => QueryResultGroups[0].Groups[0];

		public Response(params FlexibleResult[] results)
		{
			QueryResultGroups = new List<IQueryResultGroup<FlexibleResult>>
			{
				new QueryResultGroup<FlexibleResult>(
					"name",
					1u,
					new List<IQueryResult<FlexibleResult>>
					{
						new QueryResult<FlexibleResult>(results) { FoundCount = (uint)results.Length }
					})
			};
		}
	}

	public class ResponseWithFacet : IIndexResponse<FlexibleResult>
	{
		public Header Header { get; }
		public FacetResponse FacetResponse { get; }
		public IList<IQueryResultGroup<FlexibleResult>> QueryResultGroups { get; }
		public IQueryResult<FlexibleResult> QueryResult => QueryResultGroups[0].Groups[0];

		public ResponseWithFacet(params IFacetFieldsResult[] fields)
		{
			FacetResponse = new FacetResponse
			{
				FacetFieldsResult = fields
			};
		}
	}
}