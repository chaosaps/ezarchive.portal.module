using System;
using System.Threading.Tasks;
using EzArchive.Domain;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;

namespace EzArchive.WebApi.Tests;

public class AuthenticatedTaskExecutorSpy :
	IAuthenticatedTaskExecutor,
	IAuthenticatedResultTaskExecutor,
	IAuthenticatedAsyncResultTaskExecutor,
	IAuthenticatedAsyncTaskExecutor
{
	public bool WasExecuted { get; private set; }

	public IActionResult Execute(
		SessionId sessionId,
		Func<Session, Result> task,
		Func<IActionResult> onSuccess)
	{
		WasExecuted = true;
		return new OkResult();
	}

	public IActionResult Execute<T>(
		SessionId sessionid,
		Func<Session, Result<T>> task,
		Func<T, IActionResult> onSuccess)
	{
		WasExecuted = true;
		return new OkResult();
	}

	public async Task<IActionResult> ExecuteAsync<T>(
		SessionId sessionId,
		Func<Session, Task<Result<T>>> task,
		Func<T, IActionResult> onSuccess)
	{
		WasExecuted = true;
		return await Task.FromResult(new OkResult());
	}

	public async Task<IActionResult> ExecuteAsync(
		SessionId sessionId,
		Func<Session, Task<Result>> task,
		Func<IActionResult> onSuccess)
	{
		WasExecuted = true;
		return await Task.FromResult(new OkResult());
	}
}