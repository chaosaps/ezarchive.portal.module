using System;
using System.Threading.Tasks;
using EzArchive.Domain;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;

namespace EzArchive.WebApi.Tests;

public class AnonymousTaskExecutorSpy : IAnonymousTaskExecutor
{
	public bool WasExecuted { get; private set; }

	public IActionResult Execute<T>(Func<Result<T>> task, Func<T, IActionResult> onSuccess)
	{
		WasExecuted = true;
		return new OkResult();
	}

	public IActionResult Execute<T>(SessionId sessionId, Func<Session, Result<T>> task,
		Func<T, IActionResult> onSuccess)
	{
		WasExecuted = true;
		return new OkResult();
	}

	public Task<IActionResult> ExecuteAsync<T>(Func<Task<Result<T>>> task, Func<T, IActionResult> onSuccess)
	{
		WasExecuted = true;
		return Task.FromResult(new OkResult() as IActionResult);
	}

	public Task<IActionResult> ExecuteAsync(
		Func<Task<Result>> task,
		Func<IActionResult> onSuccess)
	{
		WasExecuted = true;
		return Task.FromResult(new OkResult() as IActionResult);
	}
}