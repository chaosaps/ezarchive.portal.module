using System;
using Chaos.Portal.Core.Data.InMemory;
using EzArchive.Application.UseCases.Sessions;
using EzArchive.Domain;
using EzArchive.WebApi.TaskExecutors;
using SessionRepository = EzArchive.Data.Portal.SessionRepository;

namespace EzArchive.WebApi.Tests;

public class Test
{
	private static readonly UserId AnonymousUserId = new UserId("74d226e0-47e8-435d-9bdc-25f79e8e7b06");

	public static IAnonymousTaskExecutor AnonymousTaskExecutor(ISessionRepository sessionRepository) =>
		new AnonymousTaskExecutor(sessionRepository, sessionRepository);

	public static AuthenticatedResultTaskExecutor
		AuthenticatedResultTaskExecutor(ISessionRepository sessionRepository) =>
		new AuthenticatedResultTaskExecutor(sessionRepository, sessionRepository, AnonymousUserId);

	public static AuthenticatedTaskExecutor AuthenticatedTaskExecutor(ISessionRepository sessionRepository) =>
		new AuthenticatedTaskExecutor(sessionRepository, sessionRepository, AnonymousUserId);

	public static (ISessionRepository, SessionId) SetupUserWithSession()
	{
		var repos = new InMemoryRepositories();
		var userId = new Guid("11970905-5d33-4457-92f6-cdf9b007dbc6");
		repos.User.Create(userId, "email");
		var session = repos.Session.Create(new Guid("c71f0576-de39-49a1-bc6e-f6153ce64536"));

		return (new SessionRepository(repos.Session, AnonymousUserId), new SessionId(session.Id));
	}
}