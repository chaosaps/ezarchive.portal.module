using System;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.Endpoints.EzUser;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;
using Session = EzArchive.Domain.Session;

namespace EzArchive.WebApi.Tests.Endpoints.EzUser;

public class SetEndpointTests
{
	[SetUp]
	public void Setup()
	{
	}

	[Test]
	public void EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = new SetEndpoint(taskExecutorSpy, null);

		// act
		var result = sut.Invoke(
			new Guid("6f4b74b4-b80a-4742-9207-e709c983c246"),
			new Dtos.Inputs.SetUserRequest(null, "email", "name", null, null),
			null);

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public void EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedResultTaskExecutor(sessionRepository);
		var sut = new SetEndpoint(taskExecutor, new UseCaseSpy());

		// act
		var result = sut.Invoke(sessionId.Value,
			new Dtos.Inputs.SetUserRequest(null, "email", "name", null, null),
			null);

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		var wrappedResult = (result as OkObjectResult).Value as ResultWrapper<Dtos.Results.EzUser>;
		wrappedResult.Body.Results[0].Identifier.ShouldBe("0febfc92-5d66-42c8-b796-56491d5af744");
	}

	private class UseCaseSpy : ISetUserUseCase
	{
		public Result<User> Invoke(Session session, SetUserRequest request)
		{
			return new Result<User>(
				new User(
					new UserId("0febfc92-5d66-42c8-b796-56491d5af744"),
					"email",
					"name",
					"Administrators",
					"",
					"")
			);
		}
	}
}