using System;
using Chaos.Portal.Core.Data.Model;
using Chaos.Portal.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.Endpoints.User;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;
using Session = Chaos.Portal.Core.Data.Model.Session;
using UserInfo = Chaos.Portal.Core.Data.Model.UserInfo;

namespace EzArchive.WebApi.Tests.Endpoints.EzUser;

public class GetCurrentEndpointTests
{
	[SetUp]
	public void Setup()
	{
	}

	[Test]
	public void EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = new GetCurrentEndpoint(taskExecutorSpy, null);

		// act
		var result = sut.Invoke(new Guid("6f4b74b4-b80a-4742-9207-e709c983c246"));

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public void EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedResultTaskExecutor(sessionRepository);
		var sut = new GetCurrentEndpoint(taskExecutor, new UseCaseSpy());

		// act
		var result = sut.Invoke(sessionId.Value);

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		var wrappedResult = (result as OkObjectResult).Value as ResultWrapper<Dtos.Results.UserInfo>;
		wrappedResult.Body.Results[0].Guid.ShouldBe(new Guid("7f230f44-e68a-45c5-88b7-a79824b6774e"));
	}

	public class UseCaseSpy : IGetCurrentUserUseCase
	{
		public Result<UserInfo> Invoke(SessionId sessionId)
		{
			return new Result<UserInfo>(
				new UserInfo(
					new User(
						new Guid("0febfc92-5d66-42c8-b796-56491d5af744"),
						"email",
						DateTime.UtcNow),
					new Session(
						new Guid("7f230f44-e68a-45c5-88b7-a79824b6774e"),
						new Guid("0febfc92-5d66-42c8-b796-56491d5af744"),
						DateTime.UtcNow,
						DateTime.UtcNow)));
		}
	}
}