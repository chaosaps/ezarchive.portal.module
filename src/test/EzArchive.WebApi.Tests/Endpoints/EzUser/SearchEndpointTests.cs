using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Users;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.Endpoints.EzUser;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;
using Session = EzArchive.Domain.Session;

namespace EzArchive.WebApi.Tests.Endpoints.EzUser;

[TestFixture]
public class SearchEndpointTests
{
	[Test]
	public async Task EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = new SearchEndpoint(taskExecutorSpy, null);

		// act
		await sut.InvokeAsync(new Guid(), "", 10);

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public async Task EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedResultTaskExecutor(sessionRepository);
		var useCaseSpy = new UseCaseSpy();
		var sut = new SearchEndpoint(taskExecutor, useCaseSpy);

		// act
		var result = await sut.InvokeAsync(sessionId.Value, "", 10);

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		var wrappedResult = (result as OkObjectResult).Value as ResultWrapper<SearchUser>;
		wrappedResult.Body.Results[0].Identifier.ShouldBe("4813d90f-3d9a-4b8d-99f6-bf39c6c0a1ad");
		wrappedResult.Body.Results[0].Name.ShouldBe("name");
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	public class UseCaseSpy : ISearchUsersUseCase
	{
		public bool WasCalled { get; private set; }

		public Task<Result<IEnumerable<(UserId id, string name)>>> InvokeAsync(Session session, string query,
			uint pageSize)
		{
			WasCalled = true;
			return Task.FromResult(
				new Result<IEnumerable<(UserId id, string name)>>(
					new[]
					{
						(
							new UserId("4813d90f-3d9a-4b8d-99f6-bf39c6c0a1ad"), "name")
					}));
		}
	}
}