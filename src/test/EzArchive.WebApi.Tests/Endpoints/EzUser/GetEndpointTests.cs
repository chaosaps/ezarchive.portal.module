using System;
using System.Collections.Generic;
using EzArchive.Application.UseCases.Users.GetAll;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.Endpoints.EzUser;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.WebApi.Tests.Endpoints.EzUser;

public class GetEndpointTests
{
	[SetUp]
	public void Setup()
	{
	}

	[Test]
	public void EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = new GetEndpoint(taskExecutorSpy, null);

		// act
		var result = sut.Invoke(new Guid("6f4b74b4-b80a-4742-9207-e709c983c246"));

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public void EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedResultTaskExecutor(sessionRepository);
		var sut = new GetEndpoint(taskExecutor, new UseCaseSpy());

		// act
		var result = sut.Invoke(sessionId.Value);

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		var wrappedResult = (result as OkObjectResult).Value as ResultWrapper<Dtos.Results.EzUser>;
		wrappedResult.Body.Results[0].Identifier.ShouldBe("0febfc92-5d66-42c8-b796-56491d5af744");
	}

	public class UseCaseSpy : IGetUsersUseCase
	{
		public Result<IEnumerable<User>> Invoke(UserId userId)
		{
			return new Result<IEnumerable<User>>(new[]
			{
				new User(
					new UserId("0febfc92-5d66-42c8-b796-56491d5af744"),
					"email",
					"name",
					"Administrators",
					"",
					"")
			});
		}
	}
}