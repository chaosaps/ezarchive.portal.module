using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EzArchive.Application;
using EzArchive.Application.UseCases.Search;
using EzArchive.Domain;
using EZArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.Endpoints.EzSearch;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;
using Session = EzArchive.Domain.Session;

namespace EzArchive.WebApi.Tests.Endpoints.EzSearch;

[TestFixture]
public class GetEndpointTests
{
	[Test]
	public async Task EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = new GetEndpoint(taskExecutorSpy, null);

		// act
		await sut.Invoke(new Guid());

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public async Task EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedResultTaskExecutor(sessionRepository);
		var useCaseSpy = new UseCaseSpy();
		var sut = new GetEndpoint(taskExecutor, useCaseSpy);

		// act
		var result = await sut.Invoke(sessionId.Value);

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		var wrappedResult = (result as OkObjectResult).Value as ResultWrapper<SearchResult>;
		wrappedResult.Body.Results.Count().ShouldBe(1);
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	public class UseCaseSpy : ISearchUseCase
	{
		public bool WasCalled { get; private set; }

		public Task<Result<Page<Search>>> InvokeAsync(Session session, SearchQuery searchQuery,
			SearchQuery searchQueryFilter,
			IList<string> facetsList, string sort, string tag,
			Between between,
			PageRequest pageRequest)
		{
			WasCalled = true;
			var search = new Search(new AssetId("995F134B-36B3-4F92-A7A5-D5F8522868AA"), "1",
				new[] { new SearchField("key", "val") }, new string[0]);
			return Task.FromResult(new Result<Page<Search>>(new Page<Search>(1, 0, new[] { search })));
		}
	}
}