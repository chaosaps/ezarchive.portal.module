using System;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Inputs;
using EzArchive.WebApi.Endpoints.EzProject;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.WebApi.Tests.Endpoints.EzProject;

[TestFixture]
public class SetEndpointTests
{
	[Test]
	public void EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = new SetEndpoint(taskExecutorSpy, null);

		// act
		sut.Invoke(new Guid(), new SetProjectRequest(null, "name"));

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public void EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedResultTaskExecutor(sessionRepository);
		var useCaseSpy = new UseCaseSpy();
		var sut = new SetEndpoint(taskExecutor, useCaseSpy);

		// act
		var result = sut.Invoke(sessionId.Value, new SetProjectRequest(null, "name"));

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	public class UseCaseSpy : ISetProjectsUseCase
	{
		public bool WasCalled { get; private set; }

		public Result<Project> Invoke(Session session, ProjectId id, string name)
		{
			WasCalled = true;
			return new Result<Project>(new Project(new ProjectId(1), "", new Label[0], new User[0]));
		}
	}
}