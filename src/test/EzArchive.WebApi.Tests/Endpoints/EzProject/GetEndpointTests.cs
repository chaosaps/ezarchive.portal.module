using System;
using System.Collections.Generic;
using EzArchive.Application.UseCases.Projects;
using EzArchive.Domain;
using EzArchive.WebApi.Endpoints.EzProject;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.WebApi.Tests.Endpoints.EzProject;

[TestFixture]
public class GetEndpointTests
{
	[Test]
	public void EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = new GetEndpoint(taskExecutorSpy, null);

		// act
		sut.Invoke(new Guid());

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public void EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedResultTaskExecutor(sessionRepository);
		var useCaseSpy = new UseCaseSpy();
		var sut = new GetEndpoint(taskExecutor, useCaseSpy);

		// act
		var result = sut.Invoke(sessionId.Value);

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	public class UseCaseSpy : IGetProjectsUseCase
	{
		public bool WasCalled { get; private set; }

		public Result<IEnumerable<Project>> Invoke(Session session)
		{
			WasCalled = true;
			return new Result<IEnumerable<Project>>(new List<Project>());
		}
	}
}