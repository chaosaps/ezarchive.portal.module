using System;
using System.Collections.Generic;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.Endpoints.EzAsset;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;
using Session = EzArchive.Domain.Session;

namespace EzArchive.WebApi.Tests.Endpoints.EzAsset;

[TestFixture]
public class SetEndpointTests
{
	[Test]
	public void EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = new SetEndpoint(taskExecutorSpy, null);

		// act
		sut.Invoke(new Guid(), null);

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public void EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedResultTaskExecutor(sessionRepository);
		var useCaseSpy = new UseCaseSpy();
		var sut = new SetEndpoint(taskExecutor, useCaseSpy);

		// act
		var result = sut.Invoke(sessionId.Value, null);

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		var wrappedResult = (result as OkObjectResult)?.Value as ResultWrapper<Dtos.Results.EzAsset>;
		wrappedResult?.Body.Results[0].Identifier
			.ShouldBe("b55d20dc-a33d-40a0-abce-2481983b287e", StringCompareShould.IgnoreCase);
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	public class UseCaseSpy : ISetAssetUseCase
	{
		public bool WasCalled { get; private set; }

		public Result<Asset> Invoke(Session session, IDictionary<string, string> data)
		{
			WasCalled = true;
			return new Result<Asset>(
				new Asset(
					new AssetId("b55d20dc-a33d-40a0-abce-2481983b287e"),
					new Asset._Type(1u),
					session.UserId,
					true,
					new List<string>(),
					new List<Asset._Data>(),
					new List<Asset._AnnotationGroup>(),
					new List<Asset._FileReference>()));
		}
	}
}