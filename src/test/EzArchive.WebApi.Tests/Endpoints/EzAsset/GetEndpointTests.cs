using System;
using System.Collections.Generic;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.Endpoints.EzAsset;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;
using Session = EzArchive.Domain.Session;

namespace EzArchive.WebApi.Tests.Endpoints.EzAsset;

[TestFixture]
public class GetEndpointTests
{
	[Test]
	public void EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AnonymousTaskExecutorSpy();
		var sut = new GetEndpoint(taskExecutorSpy, null);

		// act
		sut.Invoke(new Guid(), new Guid("9625d002-458f-4693-863f-87d97eabee62"));

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public void EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AnonymousTaskExecutor(sessionRepository);
		var useCaseSpy = new UseCaseSpy();
		var sut = new GetEndpoint(taskExecutor, useCaseSpy);

		// act
		var result = sut.Invoke(sessionId.Value, new Guid("9625d002-458f-4693-863f-87d97eabee62"));

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		var wrappedResult = (result as OkObjectResult)?.Value as ResultWrapper<Dtos.Results.EzAsset>;
		wrappedResult?.Body.Results[0].Identifier
			.ShouldBe("9625d002-458f-4693-863f-87d97eabee62", StringCompareShould.IgnoreCase);
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	public class UseCaseSpy : IGetAssetByIdUseCase
	{
		public bool WasCalled { get; private set; }

		public Result<Asset> Invoke(Session session, AssetId id)
		{
			WasCalled = true;
			return new Result<Asset>(
				new Asset(
					id,
					new Asset._Type(1u),
					session.UserId,
					true,
					new List<string>(),
					new List<Asset._Data>(),
					new List<Asset._AnnotationGroup>(),
					new List<Asset._FileReference>()));
		}
	}
}