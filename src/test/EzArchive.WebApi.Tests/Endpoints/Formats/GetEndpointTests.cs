using System.Threading.Tasks;
using EzArchive.Application.UseCases.Formats;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.Endpoints.Formats;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;
using Format = EzArchive.Domain.Format;

namespace EzArchive.WebApi.Tests.Endpoints.Formats
{
	[TestFixture]
	public class GetEndpointTests
	{
		[Test]
		public async Task EnsureTaskExecutorIsUsed()
		{
			// arrange
			var taskExecutorSpy = new AnonymousTaskExecutorSpy();
			var sut = new GetEndpoint(taskExecutorSpy, null);

			// act
			await sut.Invoke(1);

			// assert
			taskExecutorSpy.WasExecuted.ShouldBeTrue();
		}

		[Test]
		public async Task EnsureUseCaseIsUsed()
		{
			// arrange
			var (sessionRepository, sessionId) = Test.SetupUserWithSession();
			var taskExecutor = Test.AnonymousTaskExecutor(sessionRepository);
			var useCaseSpy = new UseCaseSpy();
			var sut = new GetEndpoint(taskExecutor, useCaseSpy);

			// act
			var result = await sut.Invoke(1);

			// assert
			result.ShouldBeOfType<OkObjectResult>();
			var formatResult = (ResultWrapper<Dtos.Results.Format>) (result as OkObjectResult).Value;
			formatResult.Body.Results[0].Id.ShouldBe(1u);
			formatResult.Body.Results[0].Extension.ShouldBe(".png");
			useCaseSpy.WasCalled.ShouldBeTrue();
		}

		public class UseCaseSpy : IGetFormatByIdUseCase
		{
			public bool WasCalled { get; private set; }

			public Task<Result<Format>> InvokeAsync(FormatId id)
			{
				WasCalled = true;
				return Task.FromResult(new Result<Format>(new Format(id, ".png")));
			}
		}
	}
}