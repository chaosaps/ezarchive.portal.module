using System.Threading.Tasks;
using EzArchive.Application.UseCases.Passwords;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Inputs;
using EzArchive.WebApi.Endpoints.Passwords;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.WebApi.Tests.Endpoints.Passwords
{
	[TestFixture]
	public class RequestResetPasswordEndpointTests
	{
		[Test]
		public async Task Invoking_EmailNotProvided_ReturnError()
		{
			// arrange
			var sut = new RequestResetPasswordEndpoint(new AnonymousTaskExecutorSpy(), null);

			// act
			var actionResult = await sut.Invoke(new RequestResetPassword(""));

			// assert
			actionResult.ShouldBeOfType<BadRequestObjectResult>();
		}

		[Test]
		public async Task EnsureTaskExecutorIsUsed()
		{
			// arrange
			var taskExecutorSpy = new AnonymousTaskExecutorSpy();
			var sut = new RequestResetPasswordEndpoint(taskExecutorSpy, null);

			// act
			await sut.Invoke(new RequestResetPassword("john@example.com"));

			// assert
			taskExecutorSpy.WasExecuted.ShouldBeTrue();
		}

		[Test]
		public async Task Invoking_UseCaseReturnsSuccessfully_MapToDto()
		{
			// arrange
			var (sessionRepo, _) = Test.SetupUserWithSession();
			var useCaseSpy = new UseCaseSpy();
			var sut = new RequestResetPasswordEndpoint(Test.AnonymousTaskExecutor(sessionRepo), useCaseSpy);

			// act
			var actionResult = await sut.Invoke(new RequestResetPassword("john@example.com"));

			// assert
			Assert.That(useCaseSpy.WasInvoked, Is.True);
			Assert.That(actionResult, Is.InstanceOf<OkObjectResult>());
		}

		private class UseCaseSpy : IRequestResetPasswordUseCase
		{
			public bool WasInvoked { get; private set; }

			public Task<Result> InvokeAsync(EmailAddress emailAddress)
			{
				WasInvoked = true;
				return Task.FromResult(Result.Ok());
			}
		}
	}
}