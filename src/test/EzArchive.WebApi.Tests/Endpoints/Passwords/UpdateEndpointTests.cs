using System;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Passwords;
using EzArchive.Domain;
using EzArchive.Domain.Tickets;
using EzArchive.WebApi.Dtos.Inputs;
using EzArchive.WebApi.Endpoints.Passwords;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.WebApi.Tests.Endpoints.Passwords
{
	[TestFixture]
	public class UpdateEndpointTests
	{
		[Test]
		public async Task EnsureTaskExecutorIsUsed()
		{
			// arrange
			var taskExecutorSpy = new AnonymousTaskExecutorSpy();
			var sut = new UpdateEndpoint(taskExecutorSpy, null);

			// act
			await sut.InvokeAsync(new ResetPassword(Guid.NewGuid(), "new password"));

			// assert
			taskExecutorSpy.WasExecuted.ShouldBeTrue();
		}

		[Test]
		public async Task Invoking_UseCaseReturnsSuccessfully_MapToDto()
		{
			// arrange
			var useCaseSpy = new UseCaseSpy();
			var (sessionRepository, _) = Test.SetupUserWithSession();
			var sut = new UpdateEndpoint(Test.AnonymousTaskExecutor(sessionRepository), useCaseSpy);

			// act
			var actionResult = await sut.InvokeAsync(new ResetPassword(Guid.NewGuid(), "new password"));

			// assert
			actionResult.ShouldBeOfType<OkObjectResult>();
			useCaseSpy.WasInvoked.ShouldBeTrue();
		}

		private class UseCaseSpy : IUpdatePasswordUseCase
		{
			public bool WasInvoked { get; private set; }

			public Task<Result> InvokeAsync(TicketId ticketId, Password password)
			{
				WasInvoked = true;
				return Task.FromResult(Result.Ok());
			}
		}
	}
}