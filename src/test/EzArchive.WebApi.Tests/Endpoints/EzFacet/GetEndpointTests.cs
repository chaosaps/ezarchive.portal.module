using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EzArchive.Application;
using EzArchive.Application.UseCases.Facets;
using EzArchive.Domain;
using EZArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.Endpoints.EzFacet;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;
using FacetResponse = EzArchive.Application.UseCases.Facets.FacetResponse;
using Session = EzArchive.Domain.Session;

namespace EzArchive.WebApi.Tests.Endpoints.EzFacet;

[TestFixture]
public class GetEndpointTests
{
	[Test]
	public async Task EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = new GetEndpoint(taskExecutorSpy, null);

		// act
		await sut.Invoke(new Guid());

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public async Task EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedResultTaskExecutor(sessionRepository);
		var useCaseSpy = new UseCaseSpy();
		var sut = new GetEndpoint(taskExecutor, useCaseSpy);

		// act
		var result = await sut.Invoke(sessionId.Value);

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		var wrappedResult = (result as OkObjectResult).Value as ResultWrapper<Dtos.Results.FacetResponse>;
		wrappedResult.Body.Results[0].Header.ShouldBe("header");
		wrappedResult.Body.Results[0].Position.ShouldBe("left");
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	public class UseCaseSpy : IGetFacetsForQueryUseCase
	{
		public bool WasCalled { get; private set; }

		public Task<Result<IReadOnlyList<FacetResponse>>> InvokeAsync(
			Session session,
			SearchQuery searchQuery,
			SearchQuery searchQueryFilter,
			IList<string> facetsList,
			string tag,
			Between between,
			FacetRange range)
		{
			WasCalled = true;
			var search = new Search(new AssetId("995F134B-36B3-4F92-A7A5-D5F8522868AA"), "1",
				new[] { new SearchField("key", "val") }, new string[0]);
			return Task.FromResult(new Result<IReadOnlyList<FacetResponse>>(new List<FacetResponse>
			{
				new FacetResponse("header", "left", new List<FacetResponse.FieldFacet>())
			}));
		}
	}
}