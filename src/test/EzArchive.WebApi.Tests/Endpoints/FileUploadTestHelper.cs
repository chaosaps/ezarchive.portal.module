using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;

namespace EzArchive.WebApi.Tests.Endpoints;

public static class FileUploadTestHelper
{
	public static (ControllerContext context, FormCollection form) HttpContextWithFile(string content, string name)
	{
		var context = new ControllerContext
		{
			HttpContext = new DefaultHttpContext
			{
				Request =
				{
					ContentType = "multipart/form-data",
					ContentLength = 0
				}
			}
		};

		var stream = new MemoryStream(Encoding.UTF8.GetBytes(content));
		var formFile = new FormFile(stream, 0, stream.Length, "name", name);

		var form = new FormCollection(
			new Dictionary<string, StringValues>(),
			new FormFileCollection { formFile });

		return (context, form);
	}

	public static (ControllerContext context, FormCollection form) HttpContextWithoutFile()
	{
		var context = new ControllerContext
		{
			HttpContext = new DefaultHttpContext
			{
				Request =
				{
					ContentType = "multipart/form-data",
					ContentLength = 0
				}
			}
		};

		var form = new FormCollection(
			new Dictionary<string, StringValues>(),
			new FormFileCollection());

		return (context, form);
	}
}