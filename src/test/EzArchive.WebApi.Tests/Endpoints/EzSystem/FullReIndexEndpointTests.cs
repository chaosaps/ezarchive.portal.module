using System;
using EzArchive.Application.UseCases.Assets;
using EzArchive.Domain;
using EzArchive.WebApi.Endpoints.EzSystem;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.WebApi.Tests.Endpoints.EzSystem;

[TestFixture]
public class FullReIndexEndpointTests
{
	[Test]
	public void EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = new FullReIndexEndpoint(taskExecutorSpy, null);

		// act
		sut.Invoke(new Guid(), true);

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public void EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedTaskExecutor(sessionRepository);
		var useCaseSpy = new UseCaseSpy();
		var sut = new FullReIndexEndpoint(taskExecutor, useCaseSpy);

		// act
		var result = sut.Invoke(sessionId.Value, true);

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	private class UseCaseSpy : IReIndexAssetsUseCase
	{
		public bool WasCalled { get; private set; }

		public Result Invoke(Session session, bool clean, uint? objectTypeId)
		{
			WasCalled = true;
			return Result.Ok();
		}
	}
}