using System;
using System.Collections.Generic;
using EzArchive.Application.UseCases.DataDefinitions;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.Endpoints.EzDataDefinition;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;
using DataDefinition = EzArchive.WebApi.Dtos.Results.DataDefinition;
using Session = EzArchive.Domain.Session;

namespace EzArchive.WebApi.Tests.Endpoints.EzDataDefinition;

[TestFixture]
public class GetEndpointTests
{
	[Test]
	public void EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = new GetEndpoint(taskExecutorSpy, null);

		// act
		sut.Invoke(new Guid());

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public void EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedResultTaskExecutor(sessionRepository);
		var useCaseSpy = new UseCaseSpy();
		var sut = new GetEndpoint(taskExecutor, useCaseSpy);

		// act
		var result = sut.Invoke(sessionId.Value);

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		var wrappedResult = (result as OkObjectResult).Value as ResultWrapper<DataDefinition>;
		wrappedResult.Body.Results.Count.ShouldBe(1);
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	public class UseCaseSpy : IGetDataDefinitionUseCase
	{
		public Result<IReadOnlyList<DataDefinitionUserResponse>> Invoke(Session session)
		{
			WasCalled = true;
			return new Result<IReadOnlyList<DataDefinitionUserResponse>>(new[]
			{
				new DataDefinitionUserResponse(
					"id", "name", DefinitionType.Metadata, 1, true, "desc", new List<FieldDefinition>())
			});
		}

		public bool WasCalled { get; private set; }
	}
}