using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Tags;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.Endpoints.EzTag;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;
using FacetResponse = EzArchive.Application.UseCases.Facets.FacetResponse;
using Session = EzArchive.Domain.Session;

namespace EzArchive.WebApi.Tests.Endpoints.EzTag;

[TestFixture]
public class GetEndpointTests
{
	[Test]
	public async Task EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = new GetEndpoint(taskExecutorSpy, null);

		// act
		await sut.Invoke(new Guid(), "");

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public async Task EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedResultTaskExecutor(sessionRepository);
		var useCaseSpy = new UseCaseSpy();
		var sut = new GetEndpoint(taskExecutor, useCaseSpy);

		// act
		var result = await sut.Invoke(sessionId.Value, "");

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		var wrappedResult = (result as OkObjectResult).Value as ResultWrapper<Dtos.Results.FacetResponse.Facet>;
		wrappedResult.Body.Results[0].Key.ShouldBe("key");
		wrappedResult.Body.Results[0].Count.ShouldBe(1u);
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	public class UseCaseSpy : IGetTagsUseCase
	{
		public bool WasCalled { get; private set; }

		public Task<Result<IReadOnlyList<FacetResponse.Facet>>> InvokeAsync(Session session, string tagPrefix)
		{
			WasCalled = true;
			return Task.FromResult(
				new Result<IReadOnlyList<FacetResponse.Facet>>(
					new List<FacetResponse.Facet>
					{
						new FacetResponse.Facet("key", 1)
					}));
		}
	}
}