using System;
using EzArchive.Application.UseCases.Files;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.Endpoints.EzFile;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;
using Session = EzArchive.Domain.Session;

namespace EzArchive.WebApi.Tests.Endpoints.EzFile;

[TestFixture]
public class SetFileReferenceEndpointTests
{
	[Test]
	public void EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = new SetFileReferenceEndpoint(taskExecutorSpy, null);

		// act
		sut.Invoke(new Guid(), 1, null);

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public void EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedTaskExecutor(sessionRepository);
		var useCaseSpy = new UseCaseSpy();
		var sut = new SetFileReferenceEndpoint(taskExecutor, useCaseSpy);

		// act
		var result = sut.Invoke(sessionId.Value, 1, null);

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		var wrappedResult = (result as OkObjectResult)?.Value as ResultWrapper<EndpointResult>;
		wrappedResult?.Body.Results[0].WasSuccess.ShouldBe(true);
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	public class UseCaseSpy : IUpdateFileReferenceUseCase
	{
		public bool WasCalled { get; private set; }

		public Result Invoke(Session session, FileId id, string fileName)
		{
			WasCalled = true;
			return Result.Ok();
		}
	}
}