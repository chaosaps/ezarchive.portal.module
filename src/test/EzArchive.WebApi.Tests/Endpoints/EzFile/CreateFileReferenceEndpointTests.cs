using System;
using EzArchive.Application.UseCases.Files;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.Endpoints.EzFile;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;
using Session = EzArchive.Domain.Session;

namespace EzArchive.WebApi.Tests.Endpoints.EzFile;

[TestFixture]
public class CreateFileReferenceEndpointTests
{
	[Test]
	public void EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = new CreateFileReferenceEndpoint(taskExecutorSpy, null);

		// act
		sut.Invoke(new Guid(), new Guid(), null, null, null, 1, "1");

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public void EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedResultTaskExecutor(sessionRepository);
		var useCaseSpy = new UseCaseSpy();
		var sut = new CreateFileReferenceEndpoint(taskExecutor, useCaseSpy);

		// act
		var result = sut.Invoke(sessionId.Value, new Guid(), null, null, null, 1, "1");

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		var wrappedResult = (result as OkObjectResult)?.Value as ResultWrapper<EndpointResult>;
		wrappedResult?.Body.Results[0].WasSuccess.ShouldBe(true);
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	public class UseCaseSpy : ICreateFileReferenceUseCase
	{
		public bool WasCalled { get; private set; }

		public Result<FileId> Invoke(Session session, AssetId assetId, string fileName, string originalFileName,
			string folderPath,
			uint formatId, uint? parentId)
		{
			WasCalled = true;
			return new Result<FileId>(new FileId(1));
		}
	}
}