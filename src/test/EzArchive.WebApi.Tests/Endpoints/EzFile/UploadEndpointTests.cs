using System;
using System.IO;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Files;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.Endpoints.EzFile;
using EzArchive.WebApi.TaskExecutors;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;
using Session = EzArchive.Domain.Session;

namespace EzArchive.WebApi.Tests.Endpoints.EzFile;

[TestFixture]
public class UploadEndpointTests
{
	[Test]
	public async Task EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = MakeEndpointWithFile(taskExecutorSpy);

		// act
		await sut.InvokeAsync(new Guid(), new Guid());

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public async Task EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedTaskExecutor(sessionRepository);
		var useCaseSpy = new UseCaseSpy();
		var sut = MakeEndpointWithFile(taskExecutor, useCaseSpy);

		// act
		var result = await sut.InvokeAsync(sessionId.Value, new Guid());

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		var wrappedResult = (result as OkObjectResult)?.Value as ResultWrapper<EndpointResult>;
		wrappedResult?.Body.Results[0].WasSuccess.ShouldBe(true);
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	private static UploadEndpoint MakeEndpointWithFile(
		IAuthenticatedAsyncTaskExecutor taskExecutor, UseCaseSpy useCase = null)
	{
		var sut = new UploadEndpoint(taskExecutor, useCase);
		sut.ControllerContext = FileUploadTestHelper.HttpContextWithFile("content", "file.ext").context;
		sut.Request.Form = FileUploadTestHelper.HttpContextWithFile("content", "file.ext").form;
		return sut;
	}

	class UseCaseSpy : IUploadFileToAssetUseCase
	{
		internal bool WasCalled = false;

		public Task<Result> InvokeAsync(Session session, AssetId assetId, string filename, Stream stream)
		{
			WasCalled = true;
			return Task.FromResult(Result.Ok());
		}
	}
}