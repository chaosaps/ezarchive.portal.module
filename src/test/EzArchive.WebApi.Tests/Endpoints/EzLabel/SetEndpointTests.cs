using System;
using EzArchive.Application.UseCases.Labels;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Inputs;
using EzArchive.WebApi.Endpoints.EzLabel;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.WebApi.Tests.Endpoints.EzLabel;

[TestFixture]
public class SetEndpointTests
{
	[Test]
	public void EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = new SetEndpoint(taskExecutorSpy, null);

		// act
		sut.Invoke(new Guid(), "1", new SetLabelRequest(null, "name"));

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public void EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedResultTaskExecutor(sessionRepository);
		var useCaseSpy = new UseCaseSpy();
		var sut = new SetEndpoint(taskExecutor, useCaseSpy);

		// act
		var result = sut.Invoke(sessionId.Value, "1", new SetLabelRequest(null, "name"));

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	public class UseCaseSpy : ISetLabelUseCase
	{
		public bool WasCalled { get; private set; }

		public Result<Label> Invoke(Session session, ProjectId projectId, LabelId id, string name)
		{
			WasCalled = true;
			return new Result<Label>(new Label(new LabelId(1), "name", 1));
		}
	}
}