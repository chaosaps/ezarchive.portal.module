using System;
using EzArchive.Application.UseCases.Labels;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.Endpoints.EzLabel;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;
using Session = EzArchive.Domain.Session;

namespace EzArchive.WebApi.Tests.Endpoints.EzLabel;

[TestFixture]
public class DeleteEndpointTests
{
	[Test]
	public void EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = new DeleteEndpoint(taskExecutorSpy, null);

		// act
		sut.Invoke(new Guid(), "1");

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public void EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedTaskExecutor(sessionRepository);
		var useCaseSpy = new UseCaseSpy();
		var sut = new DeleteEndpoint(taskExecutor, useCaseSpy);

		// act
		var result = sut.Invoke(sessionId.Value, "1");

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		var wrappedResult = (result as OkObjectResult)?.Value as ResultWrapper<EndpointResult>;
		wrappedResult?.Body.Results[0].WasSuccess.ShouldBe(true);
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	private class UseCaseSpy : IDeleteLabelUseCase
	{
		public bool WasCalled { get; private set; }

		public Result Invoke(Session session, LabelId id)
		{
			WasCalled = true;
			return Result.Ok();
		}
	}
}