using System;
using EzArchive.Application.UseCases.Labels;
using EzArchive.Domain;
using EzArchive.WebApi.Endpoints.EzLabel;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.WebApi.Tests.Endpoints.EzLabel;

[TestFixture]
public class AssociateWithEndpointTests
{
	[Test]
	public void EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = new AssociateWithEndpoint(taskExecutorSpy, null);

		// act
		sut.Invoke(new Guid(), 1, new Guid());

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public void EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedTaskExecutor(sessionRepository);
		var useCaseSpy = new UseCaseSpy();
		var sut = new AssociateWithEndpoint(taskExecutor, useCaseSpy);

		// act
		var result = sut.Invoke(sessionId.Value, 1, new Guid());

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	private class UseCaseSpy : IAssociateLabelWithAssetUseCase
	{
		public bool WasCalled { get; private set; }

		public Result Invoke(Session session, LabelId labelId, AssetId assetId)
		{
			WasCalled = true;
			return Result.Ok();
		}
	}
}