using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Annotations;
using EzArchive.Application.UseCases.DataDefinitions;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.Endpoints.EzAnnotationDefinition;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;
using DataDefinition = EzArchive.WebApi.Dtos.Results.DataDefinition;

namespace EzArchive.WebApi.Tests.Endpoints.EzAnnotationDefinition;

[TestFixture]
public class GetEndpointTests
{
	[Test]
	public async Task EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AnonymousTaskExecutorSpy();
		var sut = new GetEndpoint(taskExecutorSpy, null);

		// act
		await sut.InvokeAsync();

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public async Task EnsureUseCaseIsUsed()
	{
		// arrange
		var taskExecutor = Test.AnonymousTaskExecutor(Test.SetupUserWithSession().Item1);
		var useCaseSpy = new UseCaseSpy();
		var sut = new GetEndpoint(taskExecutor, useCaseSpy);

		// act
		var result = await sut.InvokeAsync();

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		var wrappedResult = (result as OkObjectResult).Value as ResultWrapper<DataDefinition>;
		wrappedResult.Body.Results[0].Fields.Count().ShouldBe(1);
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	public class UseCaseSpy : IGetAnnotationDefinitionsUseCase
	{
		public bool WasCalled { get; private set; }


		public Task<Result<IEnumerable<DataDefinitionUserResponse>>> InvokeAsync()
		{
			WasCalled = true;
			return Task.FromResult(
				new Result<IEnumerable<DataDefinitionUserResponse>>(new[]
				{
					new DataDefinitionUserResponse(
						"id",
						"name",
						DefinitionType.Annotation,
						1,
						true,
						"desc",
						new[] { new FieldDefinition { Id = "d1", DisplayName = "display name", Type = "string" } })
				}));
		}
	}
}