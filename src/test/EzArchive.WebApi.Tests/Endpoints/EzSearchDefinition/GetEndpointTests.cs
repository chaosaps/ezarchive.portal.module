using System.Collections.Generic;
using System.Linq;
using EzArchive.Application.UseCases.SearchDefinitions;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.Endpoints.EzSearchDefinition;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.WebApi.Tests.Endpoints.EzSearchDefinition;

[TestFixture]
public class GetEndpointTests
{
	[Test]
	public void EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AnonymousTaskExecutorSpy();
		var sut = new GetEndpoint(taskExecutorSpy, null);

		// act
		sut.Invoke();

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public void EnsureUseCaseIsUsed()
	{
		// arrange
		var taskExecutor = Test.AnonymousTaskExecutor(Test.SetupUserWithSession().Item1);
		var useCaseSpy = new UseCaseSpy();
		var sut = new GetEndpoint(taskExecutor, useCaseSpy);

		// act
		var result = sut.Invoke();

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		var wrappedResult = (result as OkObjectResult).Value as ResultWrapper<SearchDefinitionResult>;
		wrappedResult.Body.Results[0].Fields.Count().ShouldBe(1);
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	public class UseCaseSpy : IGetSearchDefinitionsUseCase
	{
		public Result<IReadOnlyList<SearchFieldDefinition>> Invoke()
		{
			WasCalled = true;
			return new Result<IReadOnlyList<SearchFieldDefinition>>(
				new[] { new SearchFieldDefinition("dn", new[] { "id1" }, "string", true, true), });
		}

		public bool WasCalled { get; private set; }
	}
}