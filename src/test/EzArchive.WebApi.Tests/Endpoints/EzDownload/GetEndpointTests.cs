using System;
using System.IO;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Files;
using EzArchive.Domain;
using EzArchive.WebApi.Endpoints.EzDownload;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;

namespace EzArchive.WebApi.Tests.Endpoints.EzDownload;

[TestFixture]
public class GetEndpointTests
{
	[Test]
	public async Task EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = new GetEndpoint(taskExecutorSpy, new AnonymousTaskExecutorSpy(), null);

		// act
		await sut.Invoke(new Guid(), 1);

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public async Task EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedResultTaskExecutor(sessionRepository);
		var useCaseSpy = new UseCaseSpy();
		var sut = new GetEndpoint(taskExecutor, new AnonymousTaskExecutorSpy(), useCaseSpy);

		// act
		var result = await sut.Invoke(sessionId.Value, 1);

		// assert
		result.ShouldBeOfType<FileStreamResult>();
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	private class UseCaseSpy : IGetFileStreamUseCase
	{
		public bool WasCalled { get; private set; }

		public Task<Result<(Stream stream, string contentType, string fileName)>> InvokeAsync(
			Session session,
			FileId fileId)
		{
			WasCalled = true;
			return Task.FromResult(new Result<(Stream stream, string contentType, string fileName)>(
				(new MemoryStream(), "text/csv", "name.csv")));
		}

		public Task<Result<(Stream stream, string contentType, string fileName)>> InvokeAsync(FileId fileId)
		{
			throw new NotImplementedException();
		}
	}
}