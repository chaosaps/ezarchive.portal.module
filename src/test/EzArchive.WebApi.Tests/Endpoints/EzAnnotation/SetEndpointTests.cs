using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Annotations;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.Endpoints.EzAnnotation;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;
using DataDefinition = EzArchive.Domain.DataDefinition;
using Session = EzArchive.Domain.Session;

namespace EzArchive.WebApi.Tests.Endpoints.EzAnnotation;

[TestFixture]
public class SetEndpointTests
{
	[Test]
	public async Task EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = new SetEndpoint(taskExecutorSpy, null);

		// act
		await sut.InvokeAsync(new Guid(), new Guid(), new Guid(), new Dictionary<string, string>());

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public async Task EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedResultTaskExecutor(sessionRepository);
		var useCaseSpy = new UseCaseSpy();
		var sut = new SetEndpoint(taskExecutor, useCaseSpy);

		// act
		var result = await sut.InvokeAsync(sessionId.Value, new Guid(), new Guid(), new Dictionary<string, string>());

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		var wrappedResult = (result as OkObjectResult)?.Value as ResultWrapper<IdentifierResult>;
		wrappedResult?.Body.Results[0].Identifier.ShouldBe("ad434e6f-f8cf-4bc9-821b-215ac55ce124");
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	private class UseCaseSpy : ISetAnnotationUseCase
	{
		public bool WasCalled { get; private set; }

		public Task<Result<AnnotationId>> InvokeAsync(
			Session session,
			AssetId assetId,
			DataDefinition.__Id definitionId,
			Dictionary<string, string> data,
			AnnotationVisibility visibility)
		{
			WasCalled = true;
			return Task.FromResult(new Result<AnnotationId>(new AnnotationId("ad434e6f-f8cf-4bc9-821b-215ac55ce124")));
		}
	}
}