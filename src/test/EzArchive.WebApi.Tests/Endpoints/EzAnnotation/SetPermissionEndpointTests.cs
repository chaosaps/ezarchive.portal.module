using System;
using System.Threading.Tasks;
using EzArchive.Application.UseCases.Annotations;
using EzArchive.Domain;
using EzArchive.WebApi.Dtos.Results;
using EzArchive.WebApi.Endpoints.EzAnnotation;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;
using Session = EzArchive.Domain.Session;

namespace EzArchive.WebApi.Tests.Endpoints.EzAnnotation;

[TestFixture]
public class SetPermissionEndpointTests
{
	[Test]
	public async Task EnsureTaskExecutorIsUsed()
	{
		// arrange
		var taskExecutorSpy = new AuthenticatedTaskExecutorSpy();
		var sut = new SetPermissionEndpoint(taskExecutorSpy, null);

		// act
		await sut.InvokeAsync(new Guid(), new Guid(), new Guid(), 1);

		// assert
		taskExecutorSpy.WasExecuted.ShouldBeTrue();
	}

	[Test]
	public async Task EnsureUseCaseIsUsed()
	{
		// arrange
		var (sessionRepository, sessionId) = Test.SetupUserWithSession();
		var taskExecutor = Test.AuthenticatedTaskExecutor(sessionRepository);
		var useCaseSpy = new UseCaseSpy();
		var sut = new SetPermissionEndpoint(taskExecutor, useCaseSpy);

		// act
		var result = await sut.InvokeAsync(sessionId.Value, new Guid(), new Guid(), 1);

		// assert
		result.ShouldBeOfType<OkObjectResult>();
		var wrappedResult = (result as OkObjectResult)?.Value as ResultWrapper<IdentifierResult>;
		wrappedResult?.Body.Results[0].Identifier.ShouldBe("ad434e6f-f8cf-4bc9-821b-215ac55ce124");
		useCaseSpy.WasCalled.ShouldBeTrue();
	}

	private class UseCaseSpy : ISetPermissionForAnnotationUseCase
	{
		public bool WasCalled { get; private set; }

		public Task<Result> InvokeAsync(
			Session session,
			AssetId assetId,
			AnnotationId id,
			AnnotationPermission permission)
		{
			WasCalled = true;
			return Task.FromResult(Result.Ok());
		}
	}
}